package pt.tiagocarvalho.twitter.services

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class SingleTest {

    private fun createSearchQuery(accounts: List<String>): String {
        return "(" + accounts.joinToString(separator = " OR ") { "from:@$it" } + ")"
    }

    @Test
    fun test1() {
        val input = listOf("Mintos")

        val expected = "(from:@Mintos)"
        val result = createSearchQuery(input)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun test2() {
        val input = listOf("Mintos", "Grupeer")

        val expected = "(from:@Mintos OR from:@Grupeer)"
        val result = createSearchQuery(input)
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun test3() {
        val input = emptyList<String>()

        val expected = "()"
        val result = createSearchQuery(input)
        Assertions.assertEquals(expected, result)
    }
}
