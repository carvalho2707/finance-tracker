package pt.tiagocarvalho.twitter.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import okhttp3.OkHttpClient
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import pt.tiagocarvalho.twitter.Constants
import pt.tiagocarvalho.twitter.model.AuthResponse
import pt.tiagocarvalho.twitter.utils.encodeBase64ToString
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TwitterServiceTest {

    private lateinit var twitterService: TwitterService

    @BeforeAll
    fun beforeAll() {
        val gson: Gson = GsonBuilder()
            .setDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy")
            .create()

        val httpClient = OkHttpClient.Builder()

        twitterService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(TwitterService::class.java)
    }

    @Test
    fun login() {
        val consumerKeyEncoded =
            URLEncoder.encode(Constants.API_KEY, StandardCharsets.UTF_8.toString())
        val consumerSecretKeyEncoded =
            URLEncoder.encode(Constants.API_SECRET_KEY, StandardCharsets.UTF_8.toString())
        val bearerTokenCredentials = "$consumerKeyEncoded:$consumerSecretKeyEncoded"
        val bearerTokenCredentialsEncoded = bearerTokenCredentials.encodeBase64ToString()

        val testObserver =
            twitterService.login(
                "Basic $bearerTokenCredentialsEncoded",
                "client_credentials"
            ).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { t: AuthResponse? -> t != null }
    }

    @Test
    fun searchTweets_badAuth() {
        val testObserver = twitterService.searchTweets(
            "",
            "",
            "recent",
            false,
            "extended",
            25
        ).test()
        testObserver.assertError { t -> t is HttpException }
        testObserver.assertError { t ->
            val httpErrorResponse = t as HttpException
            httpErrorResponse.code() == 400
        }
    }

    @Test
    fun searchTweets_badQueryParameters() {
        val auth = "Bearer AUTH_TOKEN"
        val testObserver = twitterService.searchTweets(
            auth,
            "",
            "recent",
            false,
            "extended",
            25
        ).test()
        testObserver.assertError { t -> t is HttpException }
        testObserver.assertError { t ->
            val httpErrorResponse = t as HttpException
            httpErrorResponse.code() == 400
        }
    }

    @Test
    fun searchSingleTweets() {
        val auth = "Bearer AUTH_TOKEN"
        val testObserver =
            twitterService.searchTweets(
                auth,
                "from:@Grupeer_com",
                "recent",
                true,
                "extended",
                1
            )
                .test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { it.statuses != null && it.statuses.isNotEmpty() }
    }

    @Test
    fun searchMultipleTweets() {
        val auth = "Bearer AUTH_TOKEN"
        val testObserver =
            twitterService.searchTweets(
                auth,
                SEARCH_QUERY,
                "recent",
                false,
                "extended",
                25
            ).test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { it.statuses != null && it.statuses.isNotEmpty() }
    }

    private companion object {
        private const val BASE_URL = "https://api.twitter.com"
        private const val SEARCH_QUERY =
            "(from:MintosPlatform OR from:Grupeer_com OR from:PeerBerry " +
                "OR from:Robocash1 OR from:p2pMillionaire OR " +
                "from:ExploreP2P) "
    }
}
