package pt.tiagocarvalho.currencies.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import pt.tiagocarvalho.currencies.model.LatestResponse
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CurrenciesServiceTest {

    private lateinit var currencyService: CurrencyService

    @BeforeAll
    fun beforeAll() {
        val gson: Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd")
            .create()

        val httpClient = OkHttpClient.Builder()

        currencyService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CurrencyService::class.java)
    }

    @Test
    fun getCurrencies() {
        val testObserver = currencyService.getLatestCurrencies(BASE_CURRENCY, SYMBOLS)
            .test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { t: LatestResponse? -> t != null }
    }

    private companion object {
        private const val BASE_URL = "https://api.exchangeratesapi.io"
        private const val BASE_CURRENCY = "EUR"
        private const val SYMBOLS = "GBP,CZK,DKK,PLN,RUB,SEK,USD,MXN,RON"
    }
}
