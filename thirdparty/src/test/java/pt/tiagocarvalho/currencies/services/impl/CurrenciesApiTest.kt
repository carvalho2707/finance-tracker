package pt.tiagocarvalho.currencies.services.impl

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import pt.tiagocarvalho.currencies.api.CurrenciesApi
import pt.tiagocarvalho.currencies.api.impl.CurrenciesApiImpl
import pt.tiagocarvalho.currencies.model.LatestResponse
import pt.tiagocarvalho.currencies.services.CurrencyService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CurrenciesApiTest {

    private lateinit var currencyService: CurrencyService
    private lateinit var currenciesApi: CurrenciesApi

    @BeforeAll
    fun beforeAll() {
        val gson: Gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd")
            .create()

        val httpClient = OkHttpClient.Builder()

        currencyService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CurrencyService::class.java)

        currenciesApi = CurrenciesApiImpl(currencyService)
    }

    @Test
    fun getCurrencies() {
        val testObserver = currenciesApi.getCurrencies(BASE_CURRENCY)
            .test()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValue { t: LatestResponse? -> t != null }
    }

    private companion object {
        private const val BASE_URL = "https://api.exchangeratesapi.io"
        private const val BASE_CURRENCY = "EUR"
    }
}
