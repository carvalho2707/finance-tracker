package pt.tiagocarvalho.currencies.model

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal
import java.util.Date

data class LatestResponse(
    val base: String,
    val date: Date,
    val rates: RatesResponse?
)

data class RatesResponse(
    @SerializedName("GBP")
    val gbp: BigDecimal,
    @SerializedName("CZK")
    val czk: BigDecimal,
    @SerializedName("DKK")
    val dkk: BigDecimal,
    @SerializedName("PLN")
    val pln: BigDecimal,
    @SerializedName("RUB")
    val rub: BigDecimal,
    @SerializedName("SEK")
    val sek: BigDecimal,
    @SerializedName("USD")
    val usd: BigDecimal,
    @SerializedName("MXN")
    val mxn: BigDecimal,
    @SerializedName("RON")
    val ron: BigDecimal
    // GEL,KZT
)
