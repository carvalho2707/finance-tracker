package pt.tiagocarvalho.currencies.api.impl

import io.reactivex.Single
import pt.tiagocarvalho.currencies.api.CurrenciesApi
import pt.tiagocarvalho.currencies.model.LatestResponse
import pt.tiagocarvalho.currencies.services.CurrencyService

class CurrenciesApiImpl(private val currencyService: CurrencyService) : CurrenciesApi {

    override fun getCurrencies(base: String): Single<LatestResponse> {
        return currencyService.getLatestCurrencies(base, SYMBOLS)
    }

    companion object {

        private const val SYMBOLS = "GBP,CZK,DKK,PLN,RUB,SEK,USD,MXN,RON"
        // GEL,KZT,BGN
    }
}
