package pt.tiagocarvalho.currencies.api

import io.reactivex.Single
import pt.tiagocarvalho.currencies.model.LatestResponse

interface CurrenciesApi {

    fun getCurrencies(base: String): Single<LatestResponse>
}
