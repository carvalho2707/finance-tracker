package pt.tiagocarvalho.currencies.services

import io.reactivex.Single
import pt.tiagocarvalho.currencies.model.LatestResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyService {

    @GET("/latest")
    fun getLatestCurrencies(
        @Query("base") base: String,
        @Query("symbols") symbols: String
    ): Single<LatestResponse>
}
