package pt.tiagocarvalho.twitter.api.impl

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.util.Locale
import pt.tiagocarvalho.twitter.Constants
import pt.tiagocarvalho.twitter.api.SearchApi
import pt.tiagocarvalho.twitter.model.AuthResponse
import pt.tiagocarvalho.twitter.model.TweetsResponse
import pt.tiagocarvalho.twitter.services.TwitterService
import pt.tiagocarvalho.twitter.utils.encodeBase64ToString

class SearchApiImpl(private val twitterService: TwitterService) : SearchApi {

    @ExperimentalStdlibApi
    override fun getTweets(accounts: List<String>): Observable<List<TweetsResponse>> {
        return Single.zip(
            authenticate(),
            createSearchQuery(accounts),
            BiFunction { t1: AuthResponse, t2: String ->
                Pair(
                    t1.tokenType.capitalize(Locale.ROOT) + " " + t1.accessToken,
                    t2
                )
            }
        )
            .flatMapObservable {
                twitterService.searchTweets(
                    it.first,
                    it.second, "recent", true, "extended", 50
                )
            }
            .map { it.statuses }
    }

    private fun createSearchQuery(accounts: List<String>): Single<String> {
        return Single.fromCallable {
            "(" + accounts.joinToString(separator = " OR ") { "from:@$it" } + ")"
        }
    }

    private fun authenticate(): Single<AuthResponse> {
        return Single.fromCallable { generateBearerToken() }
            .map { "Basic $it" }
            .flatMap { twitterService.login(it, "client_credentials") }
    }

    private fun generateBearerToken(): String {
        val consumerKeyEncoded =
            URLEncoder.encode(Constants.API_KEY, StandardCharsets.UTF_8.displayName())
        val consumerSecretKeyEncoded =
            URLEncoder.encode(Constants.API_SECRET_KEY, StandardCharsets.UTF_8.displayName())
        val bearerTokenCredentials = "$consumerKeyEncoded:$consumerSecretKeyEncoded"
        return bearerTokenCredentials.encodeBase64ToString()
    }
}
