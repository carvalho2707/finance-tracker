package pt.tiagocarvalho.twitter.api

import io.reactivex.Observable
import pt.tiagocarvalho.twitter.model.TweetsResponse

interface SearchApi {

    fun getTweets(accounts: List<String>): Observable<List<TweetsResponse>>
}
