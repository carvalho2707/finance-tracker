package pt.tiagocarvalho.twitter.model

import com.google.gson.annotations.SerializedName

data class AuthResponse(
    @SerializedName("token_type")
    val tokenType: String,

    @SerializedName("access_token")
    val accessToken: String
)
