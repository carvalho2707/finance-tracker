package pt.tiagocarvalho.twitter.model

data class ErrorResponse(
    val code: Int,
    val message: String
)
