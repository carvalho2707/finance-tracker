package pt.tiagocarvalho.twitter.model

data class StatusesResponse<T>(
    val statuses: List<T>,
    val errors: List<ErrorResponse>
)
