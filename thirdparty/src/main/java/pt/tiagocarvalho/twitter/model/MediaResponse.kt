package pt.tiagocarvalho.twitter.model

import com.google.gson.annotations.SerializedName

data class MediaResponse(
    @SerializedName("media_url_https")
    val mediaUrlHttps: String,
    val type: String
)
