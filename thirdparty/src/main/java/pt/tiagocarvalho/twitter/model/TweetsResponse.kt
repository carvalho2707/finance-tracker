package pt.tiagocarvalho.twitter.model

import com.google.gson.annotations.SerializedName
import java.util.Date

data class TweetsResponse(
    @SerializedName("created_at")
    val createdAt: Date,
    @SerializedName("id_str")
    val idStr: String,
    @SerializedName("full_text")
    val text: String,
    val user: UserResponse,
    val entities: EntitiesResponse?
)
