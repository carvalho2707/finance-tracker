package pt.tiagocarvalho.twitter.model

import com.google.gson.annotations.SerializedName

data class UserResponse(
    val name: String,
    @SerializedName("screen_name")
    val screenName: String,
    @SerializedName("profile_image_url_https")
    val profileImageUrlHttps: String
)
