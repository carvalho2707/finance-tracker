package pt.tiagocarvalho.twitter.services

import io.reactivex.Observable
import io.reactivex.Single
import pt.tiagocarvalho.twitter.model.AuthResponse
import pt.tiagocarvalho.twitter.model.StatusesResponse
import pt.tiagocarvalho.twitter.model.TweetsResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface TwitterService {

    @FormUrlEncoded
    @Headers("Content-Type: application/x-www-form-urlencoded;charset=UTF-8")
    @POST("/oauth2/token")
    fun login(
        @Header("Authorization") authorization: String,
        @Field("grant_type") grantType: String
    ): Single<AuthResponse>

    @GET("/1.1/search/tweets.json")
    fun searchTweets(
        @Header("authorization") authorization: String,
        @Query("q") query: String,
        @Query("result_type") resultType: String,
        @Query("include_entities") includeEntities: Boolean,
        @Query("tweet_mode") tweetMode: String,
        @Query("count") count: Int
    ): Observable<StatusesResponse<TweetsResponse>>
}
