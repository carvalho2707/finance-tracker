<a name="top"></a>

Release notes
=============

Version 5.5.0.0
--------------
* Mintos is now back to inWallet🔥
* Crowdestor is now back to inWallet🔥
* Enjoy!

Version 5.4.5.1
--------------
* Fixed grupeer bug

Version 5.4.5.0
--------------
* Added support for Two Factor Authentication for Crowdestor, Peerberry and Robocash 🔥

Version 5.4.3.0
--------------
* Fixed an error with local accounts.
* Replaced Last Session dialog with a new UI

Version 5.4.2.0
--------------
* Improved code quality
* Fixed bondora not having daily change

Version 5.4.1.0
--------------
* Renamed modules
* Purchase any item now removes ads.
* Request platforms is now available if you purchase Premium Supporter.
* Fixed import and export csv.
* Fixed a bug in savings/cash/gambling where some transactions were lost.
* Bondora now refreshes in background reducing speed to load the accounts scren.

Version 5.4.0.0
--------------
* Large UI update

Version 5.3.21.0
--------------
* Updated dependencies
* Fixed Grupeer bugs
* Fixed EstateGuru bugs

Version 5.3.20.1
--------------
* Fixed Mintos issues

Version 5.3.20.0
--------------
* Fixed Peerberry issues

Version 5.3.19.0
--------------
* Added Two-factor authentication to Grupeer

Version 5.3.18.0
--------------
* LenderMarket is now available(beta) 🔥🔥🔥🔥
* Fixed minor bugs

Version 5.3.17.8
--------------
* Updated Iuvo Platform
* Updated Bondora Platform

Version 5.3.17.7
--------------
* Fixed Grupeer bugs
* Fixed EstateGuru bugs

Version 5.3.17.6
--------------
* Fixed Twino bugs
* Fixed Grupeer bugs

Version 5.3.17.4
--------------
* You can now sign up in login screen with our referral link and earn money.
* Fixed daily change not correct on Mintos
* Fixed back navigation in accounts fragment

Version 5.3.17.3
--------------
* Fixed Robocash bugs
* Fixed Iuvo bugs
* Added support for appcoins billing
* Better usage of billing system

Version 5.3.17.2
--------------
* Fixed Iuvo
* Added support for appcoins billing
* Better usage of billing system

Version 5.3.16.1
--------------
* Updated Grupeer

Version 5.3.16.0
--------------
* Fixed Mintos bug
* Fixed Iuvo bug
* Fixed Bondora bugs

Version 5.3.15.3
--------------
* You can now clear statements in login or in the details page
* Simplified login process. After login details are already available. This will prevent Single errors.
* Added analytics to login
* Login Bug Fixes
* Error handling
* Added badge to alerts

Version 5.3.15.1
--------------
* Reimplemented sort and filter in accounts fragment
* Improved donation dialog
* UI Improvements
* Improved management screen
* Iuvo is now available 🔥🔥🔥🔥

Version 5.3.14.3
--------------
* Added error dialog to accounts and p2p
* Added telegram and WhatsApp support
* Added Request Platform Screen
* Fixed Crowdestor bugs

Version 5.3.14.2
--------------
* Fixed Twino bugs
* Fixed Crowdestor bugs
* Fixed EstateGuru bugs
* Fixed Grupeer bugs
* Fixed Bondora bugs
* Removed multiple dependencies

Version 5.3.14.1
--------------
* Fixed Twino bugs
* Fixed Crowdestor bugs
* Fixed EstateGuru bugs
* Fixed Grupeer bugs
* Removed multiple dependencies

Version 5.3.13.4
--------------
* Fixed missing ads
* UI Improvements
* General improvements

Version 5.3.12.0
--------------
* UI Improvements
* Bondora updates
* Peerberry Updates
* EstateGuru Updates

Version 5.3.11.0
--------------
* UI Improvements
* Fixed Bondora bugs
* Fixed Mintos bugs
* Fixed Peerberry Bugs
* Fixed statistics error
* Improved Analytics
* You can now edit and delete platforms by taping on a account in management screen

Version 5.3.9.0
--------------
* Fixed Twino Details displaying withdrawals with negative values
* Fixed EstateGuru displaying wrong values
* Added support for donations
* Added feature to exclude accounts from fetch

Version 5.3.8.1
--------------
* Updated EstateGuru statements
* Fixed Grupeer bug

Version 5.3.8.0
--------------
* Updated EstateGuru statements

Version 5.3.7.0
--------------
* Updated Bondora
* Updated EstateGuru

Version 5.3.6.0
--------------
* Bumped dependencies
* Replaced fabric with firebase.
* Updated Twino
* Updated Peerberry

Version 5.3.5.2
--------------
* Added new Bondora Statement Type
* Fixed Bondora missing Net Annual Return.
* Improved Bondora fetched info.
* Updates to raize bug
* Improvements to Dark theme
* Improved analytics

Version 5.3.4.3
--------------
* Fixed P2P Daily change not taking into account all statements
* Fixed EstateGuru Page changes

Version 5.3.4.2
--------------
* Added Cash Account type 🔥
* UI Improvements

Version 5.3.2
--------------
* Fixed Peerberry Login
* Added spanish to peerberry
* Fixed Mintos changed date format

Version 5.3.0
--------------
* Fixed mintos failing when no statements were available.
* Replaced object mapper with kotlin.
* Added new charts.
* Added EstateGuru

Version 5.2.18
--------------
* Replaced last session change snackbar with modal bottom sheet.
* Fixed bug with last session only triggering on positive values.
* Uniformed ThirdParty Details.
* Improved data fetched to be aligned with the model.
* Externalized loading screen to include more info

Version 5.2.17
--------------
* Added Network Usage to Settings
* Updated Splash Screen
* Renamed Settings classes
* Statements are now sorted by count desc

Version 5.2.16
--------------
* Removed scrollbars
* Added Material Dialog to inform the users of statements failures

Version 5.2.15
--------------
* Added missing type for Bondora
* UI Impropvements
* Renamed Idle Money to Cash Drag

Version 5.2.14
--------------
* Added missing type for grupeer
* Refactored Robocash Case

Version 5.2.13
--------------
* Fixed bug where Savings/Gambling import wouldn't reload the data 
* Refactored Get Overview. it only fetches if needed
* Login Screen UI Refactor
* Fixed Login allowing Savings/Bets acounts with the same name
* Removed hability to edit P2P accounts

Version 5.2.12
--------------
* Improvements to Accounts Screen
* Edit Account should not allow edit name
* Re added display mode to preferences
* Bondora now display values with 2 decimal places
* Savings and Gambling can now handle Bonus transactions
* Statements click now opens details
* Gambling wasn't showing in Management
* Added Deposit canceled and Withdrawal cancelled to mintos

Version 5.2.10
--------------
* Alerts Screen now have a animation
* Added log to implement multiple currencies
* Savings and Gambling can now handle Bonus transactions

Version 5.2.9
--------------
* Added Chart to display returns by month.
* Added Chart to display returns by year.
* Added Chart to display Returns Distributions for current month.
* Added Chart to display Returns Distributions for current year.
* Created statistics tab for returns
* Fixed Mintos crashing in some devices
* Fixed Bondora crashing in some devices

Version 5.2.1
--------------
* Fixed a bug in Mintos

Version 5.2.0
--------------
* Bondora, Twino and Crowdestor are now available 🔥🔥🔥🔥
* Fixed a bug in Grupeer fetching only 500 statements.
* UI Improvements
* Improved network usage

Version 5.1.1
--------------
* Fixed a bug causing Mintos to fail on Samsung devices
* Fixed a bug in Grupeer
* UI improvements

Version 5.1.0
--------------
* Fixed causing the app to reload from internet every time causing authentication issues and slowness.
* Big performance fixes 🔥
* Added official support for Android 10

Version 5.0.0
--------------
* Introduced Twitter Screen 🔥💻🎨
* You can now follow your popular platforms without leaving the app.

Version 4.8.4
--------------
* Fixed app crashing on delete account.
* Fixed an old issue causing mintos to fail sometimes.

Version 4.8.3
--------------
* Fixed missing statements from Grupeer
* Fixed edit account removing all statements
* Added empty message if there is no account in statements page

Version 4.8.0
--------------
* Introducing alerts! Now you can see different type of alerts in a single page.
* Currently only idle money alerts are available.
* Fixed spamming of notifications.
+ Added icon to open platform website in your favorite browser

Version 4.7.0
--------------
* Introduced interest frequency and recurring payments to Savings Account
* Fixed minor bugs

Version 4.6.0
--------------
* Added new feature to provide balance variation since last login
* Fixed a bug deleting accounts
* Fixed a bug fetching statements for the first time

Version 4.5.0
--------------
* Introducing Idle Money Notifications 🔔
* Get insights on how much money is idle in your accounts
* Fixed a bug when deleting accounts

Version 4.4.0
--------------
* Bug Fixes

Version 4.3.0
--------------
* Added support for mintos 2 auth factor
* Fixed mintos new login system

Version 4.2.7
--------------
*(02-09-2019)*
* Fixed app crashing for devices with languages different from English
* Fixed app crashing on some mintos pages
* Fixed XIRR calculation

Version 4.2.3
--------------
*(24-07-2019)*
* Bug fixes

Version 4.2.2
--------------
*(23-07-2019)*
* Bug fixes

Version 4.2.1
--------------
*(22-07-2019)*
* UI Improvements

Version 4.2.0
--------------
*(18-07-2019)*
* Added full support for Dark Theme
* Bug Fixes and UI Improvements

Version 4.1.1
--------------
*(18-07-2019)*
* Fixed widget bugs
* UI improvements on screen transitions

Version 4.1.0
--------------
*(17-07-2019)*
* Added balance widget
* Added more settings options

Version 3.11.2
--------------
*(02-07-2019)*

* Fixed Peerberry page on accounts without statements
* Fixed Gambling failing to add statements
* Day Night improvements
* Stability Fixes