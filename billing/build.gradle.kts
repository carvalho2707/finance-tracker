import dependencies.AnnotationProcessorsDependencies
import dependencies.Dependencies

plugins {
    id(BuildPlugins.ANDROID_LIBRARY)
    id(BuildPlugins.KOTLIN_ANDROID)
    id(BuildPlugins.KOTLIN_ANDROID_EXTENSIONS)
    id(BuildPlugins.KOTLIN_KAPT)
}

android {
    compileSdkVersion(BuildAndroidConfig.COMPILE_SDK_VERSION)

    defaultConfig {
        minSdkVersion(BuildAndroidConfig.MIN_SDK_VERSION)
        targetSdkVersion(BuildAndroidConfig.TARGET_SDK_VERSION)

        testInstrumentationRunner = BuildAndroidConfig.TEST_INSTRUMENTATION_RUNNER

        javaCompileOptions {
            annotationProcessorOptions {
                arguments["room.incremental"] = "true"
                arguments["room.schemaLocation"] = "$projectDir/schemas\".toString()"
            }
        }

        buildConfigField("String", "IAP_KEY_A", properties["INWALLET_GOOGLE_IAP_KEY_A"] as String)
        buildConfigField("String", "IAP_KEY_B", properties["INWALLET_GOOGLE_IAP_KEY_B"] as String)
        buildConfigField("String", "IAP_KEY_C", properties["INWALLET_GOOGLE_IAP_KEY_C"] as String)
        buildConfigField("String", "IAP_KEY_D", properties["INWALLET_GOOGLE_IAP_KEY_D"] as String)
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

    androidExtensions {
        isExperimental = true
    }
}

dependencies {
    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.COROUTINES)
    implementation(Dependencies.COROUTINES_ANDROID)
    implementation(Dependencies.BILLING)
    implementation(Dependencies.LIFECYCLE_LIVEDATA)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.ROOM)
    implementation(Dependencies.ROOM_KTX)
    kapt(AnnotationProcessorsDependencies.ROOM_COMPILER)
}