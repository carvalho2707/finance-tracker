/*
 * Copyright 2019 vmadalin.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Configuration version of all build dependencies
 */
object BuildDependenciesVersions {
    const val activity = "1.2.0"//https://developer.android.com/jetpack/androidx/releases/activity
    const val appcompact = "1.2.0" //https://developer.android.com/jetpack/androidx/releases/appcompat
    const val billing =
        "3.0.2" //https://developer.android.com/google/play/billing/billing_library_releases_notes
    const val card_view = "1.0.0"//https://developer.android.com/jetpack/androidx/releases/cardview
    const val constraint_layout =
        "2.0.4"  //https://developer.android.com/jetpack/androidx/releases/constraintlayout
    const val core = "1.3.2" //https://developer.android.com/jetpack/androidx/releases/core
    const val coroutines = "1.4.2"//https://github.com/Kotlin/kotlinx.coroutines/releases
    const val detekt = "1.15.0" //https://github.com/detekt/detekt/releases
    const val drawer_layout =
        "1.1.1"//https://developer.android.com/jetpack/androidx/releases/drawerlayout
    const val firebase_ads = "19.7.0"//https://firebase.google.com/support/release-notes/android
    const val firebase_analytics =
        "18.0.2"//https://firebase.google.com/support/release-notes/android
    const val firebase_config = "20.0.3"//https://firebase.google.com/support/release-notes/android
    const val firebase_crashlytics =
        "17.3.1"//https://firebase.google.com/support/release-notes/android
    const val fragment = "1.3.0"//https://developer.android.com/jetpack/androidx/releases/fragment
    const val glide = "4.12.0"//https://github.com/bumptech/glide/releases
    const val google_services = "4.3.5"
    const val gradle_crashlytics =
        "2.4.1"//https://firebase.google.com/support/release-notes/android
    const val gradle_version = "4.1.2"
    const val gradle_versions_plugin =
        "0.36.0"//https://github.com/ben-manes/gradle-versions-plugin/releases
    const val gson = "2.8.6"//https://github.com/google/gson/releases
    const val hilt_androidx =
        "1.0.0-alpha03"//https://developer.android.com/jetpack/androidx/releases/hilt
    const val hilt_version = "2.32-alpha"
    const val jsoup = "1.13.1"//https://jsoup.org/news/
    const val junit = "5.7.1" //https://junit.org/junit5/docs/current/release-notes/index.html
    const val kotlin = "1.4.30" //https://github.com/JetBrains/kotlin/releases
    const val ktlintVersion = "0.40.0"//https://github.com/pinterest/ktlint/releases
    const val lifecycle =
        "2.3.0" //https://developer.android.com/jetpack/androidx/releases/lifecycle
    const val lottie = "3.5.0"//https://github.com/airbnb/lottie-android/releases
    const val material =
        "1.3.0" //https://github.com/material-components/material-components-android/releases
    const val mp_android_chart = "3.1.0"//https://github.com/PhilJay/MPAndroidChart/releases
    const val multidex = "2.0.1"
    const val navigation =
        "2.3.3"//https://developer.android.com/jetpack/androidx/releases/navigation
    const val okhttp = "4.9.1"//https://github.com/square/okhttp/releases
    const val okhttp_logging_version =
        "4.9.1"//https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor
    const val preferences =
        "1.1.1"//https://developer.android.com/jetpack/androidx/releases/preference
    const val retrofit2 = "2.9.0"//https://github.com/square/retrofit
    const val room = "2.2.6"//https://developer.android.com/jetpack/androidx/releases/room
    const val rxkotlin2 = "2.4.0"//https://github.com/ReactiveX/RxKotlin/releases
    const val rxandroid = "2.1.1"//https://github.com/ReactiveX/RxAndroid/releases
    const val spotless = "5.10.1"//https://github.com/diffplug/spotless/releases
    const val swipedecorator = "1.2.3"
    const val test_ext = "1.1.2"//https://developer.android.com/jetpack/androidx/releases/test
    const val timber = "4.7.1"//https://github.com/JakeWharton/timber/releases
    const val vectordrawable = "1.1.0"
    const val viewpager2 =
        "1.0.0"//https://developer.android.com/jetpack/androidx/releases/viewpager2
    const val work = "2.5.0"//https://developer.android.com/jetpack/androidx/releases/work
    const val xirr = "1.1"//https://github.com/RayDeCampo/java-xirr
}
