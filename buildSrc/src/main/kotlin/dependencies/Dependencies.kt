/*
 * Copyright 2019 vmadalin.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dependencies

/**
 * Project dependencies, makes it easy to include external binaries or
 * other library modules to build.
 */
object Dependencies {
    const val ACTIVITY = "androidx.activity:activity-ktx:${BuildDependenciesVersions.activity}"
    const val APPCOMPAT = "androidx.appcompat:appcompat:${BuildDependenciesVersions.appcompact}"
    const val BILLING = "com.android.billingclient:billing-ktx:${BuildDependenciesVersions.billing}"
    const val CARD_VIEW = "androidx.cardview:cardview:${BuildDependenciesVersions.card_view}"
    const val CHARTS = "com.github.PhilJay:MPAndroidChart:${BuildDependenciesVersions.mp_android_chart}"
    const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:${BuildDependenciesVersions.constraint_layout}"
    const val CORE_KTX = "androidx.core:core-ktx:${BuildDependenciesVersions.core}"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${BuildDependenciesVersions.coroutines}"
    const val COROUTINES_ANDROID = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${BuildDependenciesVersions.coroutines}"
    const val DRAWER_LAYOUT = "androidx.drawerlayout:drawerlayout:${BuildDependenciesVersions.drawer_layout}"
    const val KOTLIN = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${BuildDependenciesVersions.kotlin}"
    const val KTLINT = "com.pinterest:ktlint:${BuildDependenciesVersions.ktlintVersion}"
    const val FIREBASE_ADS = "com.google.firebase:firebase-ads:${BuildDependenciesVersions.firebase_ads}"
    const val FIREBASE_ANALYTICS = "com.google.firebase:firebase-analytics-ktx:${BuildDependenciesVersions.firebase_analytics}"
    const val FIREBASE_CONFIG = "com.google.firebase:firebase-config-ktx:${BuildDependenciesVersions.firebase_config}"
    const val FIREBASE_CRASHLYTICS = "com.google.firebase:firebase-crashlytics-ktx:${BuildDependenciesVersions.firebase_crashlytics}"
    const val FRAGMENT_KTX = "androidx.fragment:fragment-ktx:${BuildDependenciesVersions.fragment}"
    const val GLIDE = "com.github.bumptech.glide:glide:${BuildDependenciesVersions.glide}"
    const val GSON = "com.google.code.gson:gson:${BuildDependenciesVersions.gson}"
    const val HILT_ANDROID = "com.google.dagger:hilt-android:${BuildDependenciesVersions.hilt_version}"
    const val HILT_VIEWMODEL = "androidx.hilt:hilt-lifecycle-viewmodel:${BuildDependenciesVersions.hilt_androidx}"
    const val HILT_WORK = "androidx.hilt:hilt-work:${BuildDependenciesVersions.hilt_androidx}"
    const val JSOUP = "org.jsoup:jsoup:${BuildDependenciesVersions.jsoup}"
    const val LIFECYCLE_COMMON = "androidx.lifecycle:lifecycle-common-java8:${BuildDependenciesVersions.lifecycle}"
    const val LIFECYCLE_LIVEDATA = "androidx.lifecycle:lifecycle-livedata-ktx:${BuildDependenciesVersions.lifecycle}"
    const val LIFECYCLE_VIEWMODEL = "androidx.lifecycle:lifecycle-viewmodel-ktx:${BuildDependenciesVersions.lifecycle}"
    const val LOTTIE = "com.airbnb.android:lottie:${BuildDependenciesVersions.lottie}"
    const val MATERIAL = "com.google.android.material:material:${BuildDependenciesVersions.material}"
    const val MULTIDEX = "androidx.multidex:multidex:${BuildDependenciesVersions.multidex}"
    const val NAVIGATION_FRAGMENT = "androidx.navigation:navigation-fragment-ktx:${BuildDependenciesVersions.navigation}"
    const val NAVIGATION_UI = "androidx.navigation:navigation-ui-ktx:${BuildDependenciesVersions.navigation}"
    const val OK_HTTP = "com.squareup.okhttp3:logging-interceptor:${BuildDependenciesVersions.okhttp_logging_version}"
    const val PREFERENCE_KTX = "androidx.preference:preference-ktx:${BuildDependenciesVersions.preferences}"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${BuildDependenciesVersions.retrofit2}"
    const val RETROFIT_GSON = "com.squareup.retrofit2:converter-gson:${BuildDependenciesVersions.retrofit2}"
    const val RETROFIT_SCALARS = "com.squareup.retrofit2:converter-scalars:${BuildDependenciesVersions.retrofit2}"
    const val RETROFIT_RX = "com.squareup.retrofit2:adapter-rxjava2:${BuildDependenciesVersions.retrofit2}"
    const val ROOM = "androidx.room:room-runtime:${BuildDependenciesVersions.room}"
    const val ROOM_KTX = "androidx.room:room-ktx:${BuildDependenciesVersions.room}"
    const val ROOM_RX = "androidx.room:room-rxjava2:${BuildDependenciesVersions.room}"
    const val RX_ANDROID = "io.reactivex.rxjava2:rxkotlin:${BuildDependenciesVersions.rxkotlin2}"
    const val RX_KOTLIN = "io.reactivex.rxjava2:rxandroid:${BuildDependenciesVersions.rxandroid}"
    const val SWIPE_DECORATOR = "it.xabaras.android:recyclerview-swipedecorator:${BuildDependenciesVersions.swipedecorator}"
    const val TIMBER = "com.jakewharton.timber:timber:${BuildDependenciesVersions.timber}"
    const val VECTOR_DRAWABLE = "androidx.vectordrawable:vectordrawable:${BuildDependenciesVersions.vectordrawable}"
    const val VIEW_PAGER = "androidx.viewpager2:viewpager2:${BuildDependenciesVersions.viewpager2}"
    const val XIRR = "org.decampo:xirr:${BuildDependenciesVersions.xirr}"
    const val WORK = "androidx.work:work-runtime-ktx:${BuildDependenciesVersions.work}"
    const val WORK_RX = "androidx.work:work-rxjava2:${BuildDependenciesVersions.work}"
}
