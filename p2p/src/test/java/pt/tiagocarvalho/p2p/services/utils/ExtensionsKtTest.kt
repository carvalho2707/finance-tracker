package pt.tiagocarvalho.p2p.services.utils

import java.math.BigDecimal
import java.util.Locale
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class ExtensionsKtTest {

    @Test
    fun convertFromMoneyToBigDecimal() {
        Locale.setDefault(Locale.US)
        val input = "6.49"

        val expected = BigDecimal("6.49")
        val result = input.convertFromMoneyToBigDecimal()
        Assertions.assertEquals(expected, result)
    }

    @Test
    fun convertFromMoneyToBigDecimalFr() {
        Locale.setDefault(Locale.FRANCE)
        val input = "6,49"

        val expected = BigDecimal("6.49")
        val result = input.convertFromMoneyToBigDecimal()
        Assertions.assertEquals(expected, result)
    }
}
