package pt.tiagocarvalho.p2p.services.utils

import java.io.BufferedReader
import java.math.BigDecimal
import org.junit.jupiter.api.Test
import pt.tiagocarvalho.p2p.services.model.mintos.Balance

class MintosParseBalance {

    @Test
    fun testBalance() {
        val inputStream =
            MintosParseBalance::class.java.getResourceAsStream("/input_mintos_script")
        val content = inputStream.bufferedReader().use(BufferedReader::readText)
        val result = convertToBalance(content)
        print(result)
    }

    private fun convertToBalance(script: String): Balance {
        val startIndex = script.indexOf("{") + 1
        val endIndex = script.indexOf("return")
        val content = script.substring(startIndex, endIndex)
        val contents: Map<String, String> = content.split(";")
            .filter { it.contains("=") }
            .associate {
                val property = it.split("=")
                val key = property[0].replace("\"", "")
                val value = property[1].replace("\"", "")
                key to value
            }
        return Balance(
            contents["x.accountBalance"]?.toBigDecimal() ?: BigDecimal.ZERO,
            contents["x.total"]?.toBigDecimal() ?: BigDecimal.ZERO,
            contents["x.totalReceivedInterest"]?.toBigDecimal() ?: BigDecimal.ZERO,
            contents["x.totalReceivedLatePaymentFee"]?.toBigDecimal() ?: BigDecimal.ZERO,
            contents["x.totalSecondaryMarketProfit"]?.toBigDecimal() ?: BigDecimal.ZERO,
            BigDecimal.ZERO,
            contents["x.totalSecondaryMarketFeePaid"]?.toBigDecimal() ?: BigDecimal.ZERO,
            BigDecimal.ZERO,
            BigDecimal.ZERO,
            BigDecimal.ZERO,
            BigDecimal.ZERO
        )
    }
}
