package pt.tiagocarvalho.p2p.services.utils

import java.io.BufferedReader
import java.math.BigDecimal
import org.junit.jupiter.api.Test

class CrowdestorDashboardConverterTest {

    @Test
    fun testParseFunction() {
        val inputStream =
            CrowdestorDashboardConverterTest::class.java.getResourceAsStream("/input_crowdestor_script")
        val content = inputStream.bufferedReader().use(BufferedReader::readText)
        val result = mapFunctionContents(content)
        print(result)
    }

    private fun mapFunctionContents(content: String): Map<String, BigDecimal> {
        val innerFunction = content.substringAfter("function callNumbers()")
        val endIndex = innerFunction.indexOf("$")
        val contents = innerFunction.substring(2, endIndex)
        return contents.split("\r\n").filter { it.isNotBlank() }.map { it.trim() }.associate {
            val properties = it.split("=")
            val key = properties[0].substring(4)
            val value = properties[1].trim().replace("'", "").convertFromMoneyToBigDecimal()
            key to value
        }
    }
}
