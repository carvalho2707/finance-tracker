package pt.tiagocarvalho.p2p.services.converter.estateguru

import java.util.Locale
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.utils.convertFromInterestToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text

internal class OverviewConverter : ModelConverter<Element, ThirdPartyDetails>() {

    override fun convert(input: Element): ThirdPartyDetails {
        val cards = input.getElementsByClass("card")

        val loans = cards.first().getElementsByClass("card-title").first().text().toInt()
        val list = cards.first().getElementsByClass("detail-item")

        var size = list[0].childNode(1).childNodeSize()
        val current = if (size == 3) {
            list[0].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[0].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        size = list[1].childNode(1).childNodeSize()
        val fourFifteen = if (size == 3) {
            list[1].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[1].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        size = list[2].childNode(1).childNodeSize()
        val sixteenThirty = if (size == 3) {
            list[2].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[2].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        size = list[3].childNode(1).childNodeSize()
        val thirtySixty = if (size == 3) {
            list[3].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[3].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        size = list[4].childNode(1).childNodeSize()
        val moreThanSixty = if (size == 3) {
            list[4].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[4].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        size = list[5].childNode(1).childNodeSize()
        val default = if (size == 3) {
            list[5].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[5].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        size = list[6].childNode(1).childNodeSize()
        val total = if (size == 3) {
            list[6].childNode(1).childNode(1).childNode(0).text().toInt()
        } else {
            list[6].childNode(1).childNode(3).childNode(0).text().toInt()
        }

        val late = total - fourFifteen - sixteenThirty - thirtySixty - moreThanSixty - default

        val netAnnualReturn = cards[1].getElementsByClass("card-title").first().text()
            .convertFromInterestToBigDecimal()
        val returnsDetails = cards[1].getElementsByClass("detail-item")
        val interest = returnsDetails[0].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val penalties = returnsDetails[1].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val referrals = returnsDetails[2].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val bonus = returnsDetails[3].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val cashback = returnsDetails[4].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val secondaryMarketProfitLoss =
            returnsDetails[5].childNode(1).childNode(3).childNode(0).text()
                .convertFromMoneyToBigDecimal(Locale.UK)
        val profit = returnsDetails[6].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val realProfit = profit.subtract(secondaryMarketProfitLoss)

        // val accountBalance = cards[2].getElementsByClass("card-title").first().text().convertFromMoneyToBigDecimal(Locale.UK)
        val accountDetails = cards[2].getElementsByClass("detail-item")
        val accountValue = accountDetails[0].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val deposit = accountDetails[1].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val withdrawals = accountDetails[9].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK).abs()
        val fees = accountDetails[11].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK).abs()
        val invested = accountDetails[12].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK).abs()
        // val totalInvestments = accountDetails[13].childNode(1).childNode(3).childNode(0).text().convertFromMoneyToBigDecimal(Locale.UK)
        val cumulativePrincipal = accountDetails[14].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val cumulativeInvestments = accountDetails[15].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        // val soldInvestments = accountDetails[16].childNode(1).childNode(3).childNode(0).text().convertFromMoneyToBigDecimal(Locale.UK)
        // val reserved = accountDetails[17].childNode(1).childNode(3).childNode(0).text().convertFromMoneyToBigDecimal(Locale.UK)
        val available = accountDetails[20].childNode(1).childNode(3).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)

        return ThirdPartyDetails(
            name = ThirdPartyName.ESTATEGURU,

            balance = accountValue,
            available = available,
            invested = invested,

            netAnnualReturn = netAnnualReturn,
            profit = realProfit,
            interests = interest,
            deposits = deposit,
            withdrawals = withdrawals,
            campaigns = bonus.add(referrals),
            cashback = cashback,
            serviceFees = penalties.add(fees),
            secondaryMarketTransactions = secondaryMarketProfitLoss,

            investments = loans,
            currentNumber = current,
            oneToFifteenLateNumber = fourFifteen,
            sixteenToThirtyLateNumber = sixteenThirty,
            thirtyToSixtyLateNumber = thirtySixty,
            moreThanSixtyLateNumber = moreThanSixty,
            defaultInvestmentNumber = default,
            lateNumber = late,

            cumulativePrincipal = cumulativePrincipal,
            cumulativeInvested = cumulativeInvestments
        )
    }
}
