package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.Jsoup.connect
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.DASHBOARD_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.GET_INVESTMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.INVESTMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.LATE_PAYMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.LOGIN_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.MY_INVESTMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.QUERY
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.STATEMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher.Constants.TRANSACTIONS_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.REFERER_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

class CrowdestorPageFetcher {

    fun getDashboard(cookies: Map<String, String>): Connection.Response {
        return connect(DASHBOARD_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(REFERER_KEY, LOGIN_URL)
            .header("cache-control", "no-cache")
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getTransactions(cookies: Map<String, String>): Connection.Response {
        return connect(TRANSACTIONS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(REFERER_KEY, "https://crowdestor.com/en/clients/dashboard")
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatements(query: String, cookies: Map<String, String>): Connection.Response {
        return connect(STATEMENTS_URL + query)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(REFERER_KEY, "https://crowdestor.com/en/clients/transactions")
            .header("x-requested-with", "XMLHttpRequest")
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getInvestments(cookies: Map<String, String>): Connection.Response {
        return connect(INVESTMENTS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getMyInvestmentsAjax(cookies: Map<String, String>): Connection.Response {
        return connect(MY_INVESTMENTS_URL + System.currentTimeMillis())
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getLatePayments(cookies: Map<String, String>): Connection.Response {
        return connect(LATE_PAYMENTS_URL + System.currentTimeMillis())
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getInvestmentsItems(cookies: Map<String, String>): Connection.Response {
        return connect(GET_INVESTMENTS_URL + QUERY + System.currentTimeMillis())
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .header("x-requested-with", "XMLHttpRequest")
            .method(Connection.Method.GET)
            .execute()
    }

    private object Constants {
        const val LOGIN_URL = "https://crowdestor.com/en/account"
        const val DASHBOARD_URL = "https://crowdestor.com/en/clients/dashboard"
        const val TRANSACTIONS_URL = "https://crowdestor.com/en/clients/transactions"
        const val STATEMENTS_URL = "https://crowdestor.com/en/clients/transactions/get_transactions"
        const val INVESTMENTS_URL = "https://crowdestor.com/en/clients/investments"
        const val MY_INVESTMENTS_URL = "https://crowdestor.com/en/clients/dashboard/getMyInvestmentsAjax?_="
        const val LATE_PAYMENTS_URL = "https://crowdestor.com/en/clients/dashboard/getLatePaymentsChartDataAjax?_="
        const val GET_INVESTMENTS_URL =
            "https://crowdestor.com/en/clients/investments/get_investments?"

        const val QUERY =
            "draw=1&columns%5B0%5D%5Bdata%5D=0&columns%5B0%5D%5Bname%5D=&columns%5B0%5D%5Bsearchable%5D=true&columns%5B0%5D%5Borderable%5D=true&columns%5B0%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B0%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B1%5D%5Bdata%5D=1&columns%5B1%5D%5Bname%5D=&columns%5B1%5D%5Bsearchable%5D=true&columns%5B1%5D%5Borderable%5D=true&columns%5B1%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B1%5D%5Bsearch%5D%5Bregex%5D=false&columns%5B2%5D%5Bdata%5D=2&columns%5B2%5D%5Bname%5D=&columns%5B2%5D%5Bsearchable%5D=true&columns%5B2%5D%5Borderable%5D=true&columns%5B2%5D%5Bsearch%5D%5Bvalue%5D=&columns%5B2%5D%5Bsearch%5D%5Bregex%5D=false&order%5B0%5D%5Bcolumn%5D=0&order%5B0%5D%5Bdir%5D=desc&start=0&length=20&search%5Bvalue%5D=&search%5Bregex%5D=false&filter_date_from=&filter_date_to=&filter_project=&_="
    }
}
