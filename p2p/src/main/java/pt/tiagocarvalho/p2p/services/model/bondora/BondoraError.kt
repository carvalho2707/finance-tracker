package pt.tiagocarvalho.p2p.services.model.bondora

data class BondoraError(
    val code: Int,
    val message: String,
    val details: String
)
