package pt.tiagocarvalho.p2p.services.cases

import com.google.gson.JsonObject
import java.math.BigDecimal
import java.util.Calendar
import java.util.Date
import java.util.HashMap
import java.util.Locale
import org.jsoup.Connection
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.robocash.RobocashHomePageConverter
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher
import pt.tiagocarvalho.p2p.services.model.robocash.DataStatement
import pt.tiagocarvalho.p2p.services.model.robocash.RobocashInvestmentGeneric
import pt.tiagocarvalho.p2p.services.model.robocash.RobocashSummary
import pt.tiagocarvalho.p2p.services.model.robocash.StatementResponse
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class RobocashCase : BaseCase() {

    private val robocashPageFetcher = RobocashPageFetcher()
    private val robocashHomePageConverter = RobocashHomePageConverter()
    private var key: String? = null

    private var keyCachedStatements: String? = null

    private fun cleanCache() {
        cookiesGlobal = HashMap()
        key = null
    }

    fun getDetails(cookies: String): ThirdPartyDetails {
        this.cookiesGlobal = parseCookies(cookies)
        val details = getMainPage()
        details.investments = getActiveInvestments()
        val dailyChange = getDailyChange()

        details.changePercentage = Utils.calculateChangePercentage(dailyChange, details.balance)
        details.changeValue = dailyChange
        return details
    }

    fun getStatements(
        cookies: String,
        date: Date? = null
    ): ThirdPartyStatementModel {
        this.cookiesGlobal = parseCookies(cookies)
        val summaryPageResponse = robocashPageFetcher.getSummaryPage(cookiesGlobal)
        replaceCookies(summaryPageResponse.cookies())
        val document = summaryPageResponse.parse()
        key = getKeyFromElement(document)

        val statementsPageResponse = robocashPageFetcher.getStatementsPage(cookiesGlobal)
        replaceCookies(statementsPageResponse.cookies())

        key = getKeyFromElement(statementsPageResponse.parse())
        val thirdPartyStatementModel = getStatementsInternal(cookiesGlobal, date)
        try {
            return thirdPartyStatementModel
        } catch (e: Exception) {
            val log = keyCachedStatements
            cleanCache()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getMainPage(): ThirdPartyDetails {
        val summaryPageResponse = robocashPageFetcher.getSummaryPage(cookiesGlobal)
        replaceCookies(summaryPageResponse.cookies())
        val document = summaryPageResponse.parse()
        key = getKeyFromElement(document)
        val body = document.body()

        try {
            return robocashHomePageConverter.convert(body)
        } catch (e: Exception) {
            val log = body?.toString()
            cleanCache()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getActiveInvestments(): Int {

        val summaryPageResponse = robocashPageFetcher.getSummaryPage(cookiesGlobal)
        replaceCookies(summaryPageResponse.cookies())
        val document = summaryPageResponse.parse()
        key = getKeyFromElement(document)

        try {

            val response = robocashPageFetcher.getMyInvestmentsPrePage(cookiesGlobal)
            replaceCookies(response.cookies())
            key = getKeyFromElement(response.parse())

            return getActiveInvestmentsInternal(cookiesGlobal)
        } catch (e: Exception) {
            cleanCache()
            val log = key
            cleanCache()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getDailyChange(): BigDecimal {
        val statementsPageResponse = robocashPageFetcher.getStatementsPage(cookiesGlobal)
        replaceCookies(statementsPageResponse.cookies())

        val date = getCurrentDateTimeString("dd-MM-yyyy")
        val summaryResponse = robocashPageFetcher.getUpdateSummary(cookiesGlobal, date, date)

        val body = summaryResponse.body()
        try {
            val robocashSummary =
                getGson().fromJson(body, RobocashSummary::class.java)
            return robocashSummary.addedFunds.add(robocashSummary.earnedInterests)
                .subtract(robocashSummary.withdrawnFunds)
        } catch (e: Exception) {
            val log = body?.toString()
            cleanCache()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getKeyFromElement(body: Element): String {
        return body.getElementsByTag("meta")[3].attr("content")
    }

    private fun getTypeFromStatement(statement: DataStatement): ThirdPartyStatementType {
        return when {
            statement.type.contains("Portfolio") && statement.type.contains("disabled") -> ThirdPartyStatementType.IGNORE
            statement.type.contains("portfolio") -> ThirdPartyStatementType.IGNORE
            statement.type == "Transaction cancelled" -> ThirdPartyStatementType.CANCELED
            statement.amount.type == "transaction_income" -> {
                when {
                    statement.type.contains("Paying") -> ThirdPartyStatementType.INTEREST
                    statement.type.contains("Returning") -> ThirdPartyStatementType.PRINCIPAL
                    statement.type.contains("Adding") -> ThirdPartyStatementType.DEPOSIT
                    statement.type.startsWith("Cashback payout for loan") -> ThirdPartyStatementType.BONUS
                    statement.type == "Cashback payout" -> ThirdPartyStatementType.BONUS
                    else -> ThirdPartyStatementType.INVALID
                }
            }
            else -> {
                when {
                    statement.type.startsWith("Cashback payout for loan") -> ThirdPartyStatementType.BONUS
                    statement.type.contains("Investing") -> ThirdPartyStatementType.INVESTMENT
                    statement.type.contains("withdrawal") -> ThirdPartyStatementType.WITHDRAWAL
                    statement.type.contains("Withdrawing funds") -> ThirdPartyStatementType.WITHDRAWAL
                    else -> ThirdPartyStatementType.INVALID
                }
            }
        }
    }

    private fun getActiveInvestmentsInternal(cookies: Map<String, String>): Int {
        val sorting = JsonObject()
        sorting.addProperty("created_part_id", "desc")

        val purchasedAt = JsonObject()
        purchasedAt.addProperty("0", "")
        purchasedAt.addProperty("1", "")

        val amount = JsonObject()
        amount.addProperty("0", "0")
        amount.addProperty("1", "0")

        val jsonObject = JsonObject()
        jsonObject.add("amount", amount)
        jsonObject.addProperty("creditor", "")
        jsonObject.addProperty("currency_id", "")
        jsonObject.addProperty("page", 0)
        jsonObject.addProperty("per_page", 10)
        jsonObject.addProperty("portfolio", "")
        jsonObject.add("purchased_at", purchasedAt)
        jsonObject.add("sorting", sorting)
        jsonObject.addProperty("status", "Not closed")
        jsonObject.addProperty("type", "")

        val response =
            robocashPageFetcher.getMyInvestmentsPage(cookies, jsonObject.toString(), key!!)
        replaceCookies(response.cookies())

        val bodyResponse = response.body()
        try {
            val robocashInvestmentGeneric =
                getGson().fromJson(bodyResponse, RobocashInvestmentGeneric::class.java)
            return robocashInvestmentGeneric.total
        } catch (e: Exception) {
            val log = bodyResponse?.toString()
            cleanCache()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getStatementsInternal(
        cookies: Map<String, String>,
        date: Date?
    ): ThirdPartyStatementModel {
        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()
        val sorting = JsonObject()
        sorting.addProperty("created_at", "desc")

        var jsonObject = JsonObject()
        jsonObject.addProperty("per_page", 10)
        jsonObject.add("sorting", sorting)
        jsonObject.addProperty("page", 0)

        var response: Connection.Response
        var bodyResponse: String
        var statementResponse: StatementResponse
        var pleaseContinue = true
        var i = 0

        val gson = getGson().newBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create()

        var auxDate: Date? = null
        if (date != null) {
            val cal = Calendar.getInstance()
            cal.time = date
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.SECOND, 0)
            cal.set(Calendar.MILLISECOND, 0)
            auxDate = cal.time
        }

        do {
            jsonObject = JsonObject()
            jsonObject.addProperty("per_page", 100)
            jsonObject.add("sorting", sorting)
            jsonObject.addProperty("page", i)

            response =
                robocashPageFetcher.getStatementsPageWithParams(
                    cookies,
                    jsonObject.toString(),
                    key!!
                )
            replaceCookies(response.cookies())

            bodyResponse = response.body()
            try {
                statementResponse =
                    gson.fromJson(bodyResponse, StatementResponse::class.java)
                for (dataStatement in statementResponse.data) {
                    val typeFromStatement = getTypeFromStatement(dataStatement)

                    if (typeFromStatement == ThirdPartyStatementType.IGNORE) {
                        continue
                    }

                    if (typeFromStatement == ThirdPartyStatementType.INVALID) {
                        issues.add(dataStatement.type)
                        continue
                    }

                    if (auxDate != null) {
                        if (dataStatement.date.after(auxDate)) {
                            statements.add(
                                ThirdPartyStatement(
                                    dataStatement.id,
                                    dataStatement.amount.value.convertFromMoneyToBigDecimal(Locale.UK).abs(),
                                    dataStatement.date,
                                    typeFromStatement,
                                    ThirdPartyName.ROBOCASH
                                )
                            )
                        } else {
                            pleaseContinue = false
                            break
                        }
                    } else {
                        statements.add(
                            ThirdPartyStatement(
                                dataStatement.id,
                                dataStatement.amount.value.convertFromMoneyToBigDecimal(Locale.UK).abs(),
                                dataStatement.date,
                                typeFromStatement,
                                ThirdPartyName.ROBOCASH
                            )
                        )
                    }
                }
                i++
            } catch (e: Exception) {
                val log = bodyResponse
                cleanCache()
                throw ThirdPartyException(e.message, log, e)
            }
        } while (statementResponse.nextPage && pleaseContinue)
        return ThirdPartyStatementModel(statements, issues)
    }
}
