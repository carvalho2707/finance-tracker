package pt.tiagocarvalho.p2p.services.converter.crowdestor

import java.math.BigDecimal
import java.util.Locale
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.crowdestor.Dashboard
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text

internal class DashboardConverter : ModelConverter<Element, Dashboard>() {

    override fun convert(input: Element): Dashboard {
        val content = input.getElementsByClass("balance")

        val netAnnualReturn = content[1].childNode(0).text().convertFromMoneyToBigDecimal(Locale.UK)
        val activeInvestmentsValue = content[2].childNode(0).text().trim().toInt()

        var functionContents: Map<String, BigDecimal> = emptyMap()
        input.getElementsByTag("script").firstOrNull { it.toString().contains("balance") }?.let {
            val function = it.childNode(0).toString()
            functionContents = mapFunctionContents(function)
        } ?: throw throw ThirdPartyException(
            "Invalid Function in Crowdestor",
            input.getElementsByTag("script").toString()
        )

        return Dashboard(
            balance = functionContents["balance"]!!,
            available = functionContents["availableFounds"]!!,
            invested = functionContents["investedFunds"]!!,
            investedByNow = functionContents["investedByNow"]!!,
            receivedByNow = functionContents["recivedByNow"]!!,
            activeInvestments = activeInvestmentsValue,
            netAnnualReturn = netAnnualReturn
        )
    }

    private fun mapFunctionContents(content: String): Map<String, BigDecimal> {
        val innerFunction = content.substringAfter("function callNumbers()")
        val endIndex = innerFunction.indexOf("$")
        val contents = innerFunction.substring(2, endIndex)
        return contents.split("\n").filter { it.isNotBlank() }.map { it.trim() }.associate {
            val properties = it.split("=")
            val key = properties[0].substring(4).trim()
            val value = properties[1].trim().replace("'", "").convertFromMoneyToBigDecimal()
            key to value
        }
    }
}
