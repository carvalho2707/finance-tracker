package pt.tiagocarvalho.p2p.services.model.crowdestor

enum class TransactionType constructor(val search: String) {
    DEPOSIT("admin_add_funds"),
    WITHDRAW("withdraw"),
    ALL(""),
    BONUS("bonus_invest_payout"),
    INTEREST("period_invest_payout"),
    REFERRAL("referral_payout"),
    CASHBACK("cashback_payout"),
    REGISTER_BALANCE("register_balance")
}
