package pt.tiagocarvalho.p2p.services.model.bondora

data class BondoraResponse<T>(
    val success: Boolean,
    val errors: List<BondoraError>?,
    val payload: T?,
    val count: Int?,
    val totalCount: Int?,
    val pageNr: Int?
)
