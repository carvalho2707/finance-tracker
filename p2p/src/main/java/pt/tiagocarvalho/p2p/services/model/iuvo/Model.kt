package pt.tiagocarvalho.p2p.services.model.iuvo

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class LoginResponse(
    val result: Result?,
    val status: Status
)

data class Result(
    @SerializedName("email") val email: String,
    @SerializedName("is_company") val is_company: Boolean,
    @SerializedName("pay_sys_settings") val pay_sys_settings: List<PaySysSettings>,
    @SerializedName("token_expiration_seconds") val token_expiration_seconds: Int,
    @SerializedName("status") val status: Status,
    @SerializedName("token_expiration_timestamp") val token_expiration_timestamp: String,
    @SerializedName("session_token") val session_token: String,
    @SerializedName("investor_id") val investor_id: Int
)

data class Status(val status: String)

data class PaySysSettings(
    @SerializedName("cashout_fee_amount") val cashout_fee_amount: BigDecimal,
    @SerializedName("pay_type") val pay_type: String,
    @SerializedName("cashout_min_amount") val cashout_min_amount: BigDecimal,
    @SerializedName("cashout_max_amount") val cashout_max_amount: BigDecimal,
    @SerializedName("deposit_fee_pct") val deposit_fee_pct: BigDecimal,
    @SerializedName("pay_sys_id") val pay_sys_id: Int,
    @SerializedName("is_cashout_allowed") val is_cashout_allowed: Int,
    @SerializedName("is_deposit_allowed") val is_deposit_allowed: Int,
    @SerializedName("deposit_max_amount") val deposit_max_amount: BigDecimal,
    @SerializedName("currency_code") val currency_code: String,
    @SerializedName("deposit_min_amount") val deposit_min_amount: BigDecimal,
    @SerializedName("cashout_fee_perc") val cashout_fee_perc: BigDecimal,
    @SerializedName("currency_id") val currency_id: Int,
    @SerializedName("deposit_fee_money") val deposit_fee_money: BigDecimal,
    @SerializedName("currency_code_digits") val currency_code_digits: Int
)

data class AccountBalance(
    val status: Status,
    val result: AccountResult
)

data class AccountResult(
    @SerializedName("session_info") val sessionInfo: SessionInfo,
    @SerializedName("balances") val balances: Balances
)

data class SessionInfo(
    @SerializedName("investor_id") val investor_id: Int,
    @SerializedName("expiration_timestamp") val expiration_timestamp: String
)

data class Balances(
    @SerializedName("EUR") val eur: CurrencyBalance
)

data class CurrencyBalance(
    @SerializedName("currency_code") val currency_code: String,
    @SerializedName("amount_visible") val amount_visible: BigDecimal,
    @SerializedName("amount") val amount: BigDecimal
)
