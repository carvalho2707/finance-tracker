package pt.tiagocarvalho.p2p.services.converter.mintos

import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.mintos.AccountStatement
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class MintosAccountStatementConverter :
    ModelConverter<List<AccountStatement>, ThirdPartyStatementModel>() {

    private val datePattern = "dd.MM.yyyy"

    override fun convert(input: List<AccountStatement>): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()
        input.filter { it.transactionId.isNotBlank() }
            .forEach { accountStatement ->
                val type = getTypeFromStatement(accountStatement.details)
                if (type == ThirdPartyStatementType.INVALID) {
                    issues.add(accountStatement.details)
                } else if (type == ThirdPartyStatementType.CURRENCY_EXCHANGE_OUT || type == ThirdPartyStatementType.CURRENCY_EXCHANGE_IN) {
                    return@forEach
                } else {
                    list.add(
                        ThirdPartyStatement(
                            accountStatement.transactionId,
                            accountStatement.turnover.convertFromMoneyToBigDecimal().abs(),
                            Utils.convertStringToDate(accountStatement.date, datePattern),
                            type,
                            ThirdPartyName.MINTOS
                        )
                    )
                }
            }

        return ThirdPartyStatementModel(list, issues)
    }

    private fun getTypeFromStatement(description: String): ThirdPartyStatementType {
        return when {
            description.contains("Deposits") -> ThirdPartyStatementType.DEPOSIT
            description.contains("Withdrawal") -> ThirdPartyStatementType.WITHDRAWAL
            description.contains("investment in loan") -> ThirdPartyStatementType.INVESTMENT
            description.contains("interest received") -> ThirdPartyStatementType.INTEREST
            description.contains("principal received", true) -> ThirdPartyStatementType.PRINCIPAL
            description.contains("secondary market transaction") -> ThirdPartyStatementType.PRINCIPAL
            description.contains("secondary market fee") -> ThirdPartyStatementType.FEE
            description.contains("late fees received") -> ThirdPartyStatementType.INTEREST
            description.contains("Cashback bonus") -> ThirdPartyStatementType.BONUS
            description.contains("Affiliate partner bonus") -> ThirdPartyStatementType.REFERRAL
            description.contains("loan agreement extended: interest received") -> ThirdPartyStatementType.INTEREST
            description.contains("Investment principal transit reconciliation Rebuy purpose") -> ThirdPartyStatementType.PRINCIPAL
            description.contains("Outgoing currency exchange transaction") -> ThirdPartyStatementType.CURRENCY_EXCHANGE_OUT
            description.contains("Incoming currency exchange transaction") -> ThirdPartyStatementType.CURRENCY_EXCHANGE_IN
            description.contains("FX commission with") -> ThirdPartyStatementType.FEE
            description.contains("Reversed deposit") -> ThirdPartyStatementType.DEPOSIT_CANCELED
            description.contains("Withdrawal cancelled") -> ThirdPartyStatementType.WITHDRAWAL_CANCELED
            description.contains("Refer a friend bonus") -> ThirdPartyStatementType.REFERRAL
            description.contains("Interest for pending payments") -> ThirdPartyStatementType.INTEREST
            description.contains("Pending payment interest income") -> ThirdPartyStatementType.INTEREST
            description.contains("Interest received from pending payments") -> ThirdPartyStatementType.INTEREST
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
