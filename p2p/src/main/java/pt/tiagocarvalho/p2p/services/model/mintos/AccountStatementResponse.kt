package pt.tiagocarvalho.p2p.services.model.mintos

internal data class AccountStatementResponse(val data: AccountData)
