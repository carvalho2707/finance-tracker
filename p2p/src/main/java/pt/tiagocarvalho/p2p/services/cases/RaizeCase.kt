package pt.tiagocarvalho.p2p.services.cases

import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.util.Calendar
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.converter.raize.RaizeOverviewConverter
import pt.tiagocarvalho.p2p.services.converter.raize.RaizeStatementConverter
import pt.tiagocarvalho.p2p.services.model.raize.SearchFilter
import pt.tiagocarvalho.p2p.services.retrofit.RaizeApi
import pt.tiagocarvalho.p2p.services.retrofit.RaizeRetrofitInterface
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class RaizeCase : BaseCase() {
    private val raizeOverviewConverter = RaizeOverviewConverter()
    private val raizeStatementConverter = RaizeStatementConverter()

    private var token: String? = null

    fun getDetails(token: String): ThirdPartyDetails {
        this.token = token
        val authHeader = "Bearer $token"

        val details: ThirdPartyDetails
        val raizeApi = RaizeRetrofitInterface.retrofitInstance.create(RaizeApi::class.java)
        val response = raizeApi.getCurrent(authHeader).execute().body()
        try {
            details = raizeOverviewConverter.convert(response!!)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, response?.toString(), e)
        }

        val dailyChange = getDailyChange(token)
        details.changePercentage = Utils.calculateChangePercentage(dailyChange, details.balance)
        details.changeValue = dailyChange
        return details
    }

    fun getStatements(
        token: String,
        from: Date? = getFirstPossibleDay()
    ): ThirdPartyStatementModel {
        this.token = token

        val authHeader = "Bearer $token"

        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()

        val raizeApi = RaizeRetrofitInterface.retrofitInstance.create(RaizeApi::class.java)
        val currentTime = getCurrentDateTimeString("yyyy-MM-dd")
        var isLastPage: Boolean
        var page = 0

        val searchFilter = SearchFilter(
            isPaged = true,
            isFilters = false,
            startDate = from?.toStringPattern("dd-MM-yyyy"),
            endDate = currentTime,
            page = page
        )

        do {
            val raizeGenericList =
                raizeApi.getTransactions(authHeader, searchFilter).execute().body()
                    ?: return ThirdPartyStatementModel(statements, issues)

            val thirdPartyStatementModel =
                raizeStatementConverter.convert(raizeGenericList.results!!)
            statements.addAll(thirdPartyStatementModel.statements)
            issues.addAll(thirdPartyStatementModel.issues)

            isLastPage = page++ >= raizeGenericList.numPages
        } while (!isLastPage)

        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getDailyChange(token: String): BigDecimal {
        this.token = token

        val authHeader = "Bearer $token"
        val currentTime = getCurrentDateTimeString("yyyy-MM-dd")

        val raizeApi = RaizeRetrofitInterface.retrofitInstance.create(RaizeApi::class.java)
        val searchFilter = SearchFilter(
            isPaged = false,
            isFilters = false,
            startDate = getFirstPossibleDay().toStringPattern("dd-MM-yyyy"),
            endDate = currentTime,
            page = 0
        )
        return raizeApi.getTransactions(authHeader, searchFilter)
            .execute()
            .body()?.results?.map {
                when (it.type) {
                    "referral", "walletCharged", "installmentReceived" ->
                        it.amount!!.convertFromMoneyToBigDecimal()
                            .negate()
                    "walletWithdrawal" -> it.amount!!.convertFromMoneyToBigDecimal().negate()
                    else -> ZERO
                }
            }?.sumByBigDecimal { it } ?: ZERO
    }

    private fun getFirstPossibleDay(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, -29)
        return cal.time
    }
}
