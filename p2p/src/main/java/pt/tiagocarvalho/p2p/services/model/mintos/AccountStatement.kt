package pt.tiagocarvalho.p2p.services.model.mintos

internal data class AccountStatement(
    val transactionId: String,
    val date: String,
    val details: String,
    val turnover: String,
    val balance: String,
    val total: Int
)
