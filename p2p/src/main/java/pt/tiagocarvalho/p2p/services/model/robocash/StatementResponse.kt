package pt.tiagocarvalho.p2p.services.model.robocash

import com.google.gson.annotations.SerializedName

internal data class StatementResponse(
    @SerializedName("current_values") val currentValues: String,
    val data: List<DataStatement>,
    @SerializedName("next_page") val nextPage: Boolean
)
