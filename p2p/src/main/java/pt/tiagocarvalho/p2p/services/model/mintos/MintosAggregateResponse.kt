package pt.tiagocarvalho.p2p.services.model.mintos

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class NetAnnualReturns(@SerializedName("978") val eur: BigDecimal?)

data class OutstandingPrincipals(@SerializedName("978") val eur: BigDecimal?)

data class PendingPayments(@SerializedName("978") val eur: BigDecimal?)

data class InvestmentData(@SerializedName("978") val eur: Data?)

data class InRecovery(@SerializedName("978") val eur: BigDecimal?)

data class Data(
    val activeCount: Int,
    val activeSum: BigDecimal,
    val late115Count: Int,
    val late115Sum: BigDecimal,
    val late1630Count: Int,
    val late1630Sum: BigDecimal,
    val late3160Count: Int,
    val late3160Sum: BigDecimal,
    val defaultCount: Int,
    val defaultSum: BigDecimal,
    val badDebtSum: BigDecimal,
    val badDebtCount: Int,
    val recoveryCount: Int,
    val recoverySum: BigDecimal,
    val totalCount: Int,
    val totalSum: BigDecimal,
    val delayedWithinGracePeriodSum: BigDecimal,
    val delayedWithinGracePeriodCount: Int
)

data class Balance(
    val accountBalance: BigDecimal,
    val total: BigDecimal,
    val totalReceivedInterest: BigDecimal,
    val totalReceivedLatePaymentFee: BigDecimal,
    val totalSecondaryMarketProfit: BigDecimal,
    val totalCurrencyExchangeFeePaid: BigDecimal,
    val totalSecondaryMarketFeePaid: BigDecimal,
    val totalMintosServiceFeePaid: BigDecimal,
    val totalReceivedReferAfriendBonus: BigDecimal,
    val totalReceivedAffiliateBonus: BigDecimal,
    val totalReceivedCashbackBonus: BigDecimal
)
