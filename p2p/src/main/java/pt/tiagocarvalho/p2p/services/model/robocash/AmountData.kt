package pt.tiagocarvalho.p2p.services.model.robocash

import com.google.gson.annotations.SerializedName

internal data class AmountData(
    @SerializedName("class") val type: String,
    var value: String,
    var symbol: String
)
