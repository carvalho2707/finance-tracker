package pt.tiagocarvalho.p2p.services.cases

import java.lang.Thread.sleep
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.text.ParseException
import java.util.Date
import java.util.concurrent.TimeUnit
import org.jsoup.Jsoup
import org.jsoup.nodes.DataNode
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.nodes.TextNode
import pt.tiagocarvalho.p2p.api.model.AuthenticationFailedException
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.bondora.StatementsConverter
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher
import pt.tiagocarvalho.p2p.services.model.bondora.AccessTokenRequest
import pt.tiagocarvalho.p2p.services.model.bondora.AccountStatementReportLine
import pt.tiagocarvalho.p2p.services.model.bondora.AuthError
import pt.tiagocarvalho.p2p.services.model.bondora.BondoraResponse
import pt.tiagocarvalho.p2p.services.model.bondora.Investment
import pt.tiagocarvalho.p2p.services.model.bondora.OverviewResponse
import pt.tiagocarvalho.p2p.services.model.bondora.Report
import pt.tiagocarvalho.p2p.services.model.bondora.ReportCreateRequest
import pt.tiagocarvalho.p2p.services.retrofit.BondoraApi
import pt.tiagocarvalho.p2p.services.retrofit.BondoraRetrofitInterface
import pt.tiagocarvalho.p2p.services.retrofit.BondoraRetrofitInterface.GRANT_TYPE
import pt.tiagocarvalho.p2p.services.retrofit.BondoraRetrofitInterface.REDIRECT_URI
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromInterestToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.isFiveHoursAgo
import pt.tiagocarvalho.p2p.services.utils.isToday
import pt.tiagocarvalho.p2p.services.utils.passedFiveMinutes
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text
import pt.tiagocarvalho.p2p.services.utils.toDate
import pt.tiagocarvalho.p2p.services.utils.toInitialDay
import retrofit2.Response

internal class BondoraCase(
    private val clientId: String,
    private val clientSecret: String
) : BaseCase() {

    private val bondoraPageFetcher = BondoraPageFetcher()
    private val statementsConverter = StatementsConverter()
    private val flushInterval: Long = TimeUnit.MINUTES.toMillis(2)
    private var lastFlushTime = System.nanoTime()
    private var investmentsCached: List<Investment>? = null
    private var formToken: String? = null
    private var headerToken: String? = null

    fun login(code: String): String {
        val bondoraApi =
            BondoraRetrofitInterface.retrofitInstance.create(BondoraApi::class.java)

        val accessTokenRequest = AccessTokenRequest(
            GRANT_TYPE,
            clientId,
            clientSecret,
            code,
            REDIRECT_URI,
            null,
            true
        )

        val response = bondoraApi.getAccessToken(accessTokenRequest).execute()
        if (response.isSuccessful) {
            val body = response.body()
            body?.accessToken?.let {
                return it
            }
        }

        if (response.errorBody() != null) {
            val errorBody =
                getGson().fromJson(response.errorBody()?.string(), AuthError::class.java)
            throw ThirdPartyException(
                "Error with code: $code and status: ${response.code()}",
                errorBody.toString()
            )
        }

        throw ThirdPartyException("Unknown error with code: $code")
    }

    fun getDetails(token: String, login: Login): ThirdPartyDetails {
        val bondoraApi =
            BondoraRetrofitInterface.retrofitInstance.create(BondoraApi::class.java)

        val response = bondoraApi.getBalance(token).execute()
        validateResponse(response)
        val payload = response.body()!!.payload!!
        val goGrowAccounts = payload.goGrowAccounts
        val goGrowAccountsInvested = payload.goGrowAccounts.sumByBigDecimal { it.totalSaved }

        val interestsGoGrow = goGrowAccounts.sumByBigDecimal { it.netProfit }

        val investmentList = getInvestments(token)
        val principalRemaining = investmentList.sumByBigDecimal { it.principalRemaining }

        val currentInvestments = investmentList.filter { it.loanStatusCode == 2 }
            .sumByBigDecimal { it.principalRemaining }
        val current = currentInvestments.add(goGrowAccountsInvested)
        val late = investmentList.filter { it.loanStatusCode == 100 }
            .sumByBigDecimal { it.principalRemaining }

        val site = getValuesFromSite(login)
        val investInterest = site.getOrElse("investProfit") { ZERO }
        val investedGoGrow = site.getOrElse("gogrow") { ZERO }.setScale(2, RoundingMode.HALF_UP)
        val interests = investInterest.add(interestsGoGrow)
        val balance = site.getOrElse("balance") { ZERO }.setScale(2, RoundingMode.HALF_UP)

        val investmentsDailyChange = getDailyChange(token)
        val goGrowDailyChange = getGoGrowDailyChange(login)
        val dailyChange = investmentsDailyChange.add(goGrowDailyChange)

        return ThirdPartyDetails(
            name = ThirdPartyName.BONDORA,

            balance = balance,
            available = site.getOrElse("available") { ZERO }.setScale(2, RoundingMode.HALF_UP),
            invested = investedGoGrow.add(principalRemaining).setScale(2, RoundingMode.HALF_UP),

            netAnnualReturn = site.getOrElse("net") { ZERO },
            interests = interests.setScale(2, RoundingMode.HALF_UP),
            profit = site.getOrElse("profit") { ZERO }.setScale(2, RoundingMode.HALF_UP),
            deposits = site.getOrElse("deposits") { ZERO }.setScale(2, RoundingMode.HALF_UP),
            withdrawals = site.getOrElse("withdrawals") { ZERO }.setScale(2, RoundingMode.HALF_UP),

            investments = investmentList.size + goGrowAccounts.size,

            currentTotal = current.setScale(2, RoundingMode.HALF_UP),
            lateTotal = late.setScale(2, RoundingMode.HALF_UP),

            changePercentage = Utils.calculateChangePercentage(
                dailyChange,
                balance
            ).setScale(2, RoundingMode.HALF_UP),
            changeValue = dailyChange.setScale(2, RoundingMode.HALF_UP)
        )
    }

    fun getStatements(
        date: Date,
        token: String,
        reUse: Boolean,
        daily: Boolean
    ): ThirdPartyStatementModel {
        val reportLines = getAccountReport(date.toInitialDay(), token, reUse, daily)
        return statementsConverter.convert(reportLines)
    }

    private fun getGoGrowDailyChange(login: Login): BigDecimal {
        loginInternal(login)

        val dashboardResponse = bondoraPageFetcher.getDashboard(cookiesGlobal)
        replaceCookies(dashboardResponse.cookies())

        val statementsResponse = bondoraPageFetcher.getStatementsIndex(cookiesGlobal)
        replaceCookies(statementsResponse.cookies())

        val body = statementsResponse.parse().body()
        var total = ZERO

        val accounts = body.getElementsByClass("well gg-account p0")

        if (accounts != null && accounts.isNotEmpty()) {
            accounts.forEach {
                val id = it.attr("data-url")
                val details = bondoraPageFetcher.getStatements(id, cookiesGlobal)
                replaceCookies(details.cookies())
                val temp = getDailyChangeFromGoGrowInternal(details.parse().body())
                total = total.add(temp)
            }
        } else {
            total = getDailyChangeFromGoGrowInternal(body)
        }

        return total
    }

    private fun getDailyChangeFromGoGrowInternal(body: Element): BigDecimal {
        try {
            var amountIn = ZERO
            var amountOut = ZERO
            body.getElementsByClass("tr-deposit").forEach {
                val date = handleMultipleDateFormats(it.childNode(1).childNode(0).text())
                if (date.isToday().not()) {
                    return@forEach
                }

                amountIn = amountIn.add(it.childNode(7).childNode(0).text().convertFromMoneyToBigDecimal())
            }

            body.getElementsByClass("tr-withdraw").forEach {
                val date = handleMultipleDateFormats(it.childNode(1).childNode(0).text())
                if (date.isToday().not()) {
                    return@forEach
                }

                amountOut = amountOut.add(it.childNode(7).childNode(0).text().convertFromMoneyToBigDecimal().abs())
            }

            return amountIn.subtract(amountOut)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body.toString(), e)
        }
    }

    private fun getValuesFromSite(login: Login): Map<String, BigDecimal> {
        loginInternal(login)

        val dashboardResponse = bondoraPageFetcher.getDashboard(cookiesGlobal)
        replaceCookies(dashboardResponse.cookies())

        val document = dashboardResponse.parse()
        headerToken = getLoginKey2FromDocument(document)

        val overviewResponse = bondoraPageFetcher.getOverview(headerToken!!, cookiesGlobal)
        replaceCookies(overviewResponse.cookies())

        var body = overviewResponse.body()
        val results = emptyMap<String, BigDecimal>().toMutableMap()

        try {
            val response = getGson().fromJson(body, OverviewResponse::class.java)
            response.stats.forEach {
                when (it.title) {
                    "Account value" ->
                        results["balance"] =
                            it.valueTooltip!!.convertFromMoneyToBigDecimal()
                    "Available funds" ->
                        results["available"] =
                            it.valueTooltip!!.convertFromMoneyToBigDecimal()
                    "Yield to maturity" ->
                        results["net"] =
                            it.value!!.convertFromInterestToBigDecimal()
                }
            }
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }

        val portfolioResponse =
            bondoraPageFetcher.getportfolioprofitability(headerToken!!, cookiesGlobal)
        replaceCookies(portfolioResponse.cookies())

        body = portfolioResponse.body()

        try {
            val response = Jsoup.parse(body)
            results["investProfit"] =
                response.body().childNode(0).childNode(3).childNode(17)
                    .childNode(9).childNode(0)
                    .text().trim().convertFromMoneyToBigDecimal()
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }

        val statsnumberResponse = bondoraPageFetcher.getstatsnumber(headerToken!!, cookiesGlobal)
        replaceCookies(statsnumberResponse.cookies())

        body = statsnumberResponse.body()

        try {
            val response = Jsoup.parse(body)
            val midContent =
                response.body().childNode(0).childNode(1)

            val firstSection = midContent.childNode(1).childNode(1).childNode(1)
                .childNode(1)

            val lastSection = midContent.childNode(3).childNode(1).childNode(1)
                .childNode(1)

            val goGrowNode =
                if (firstSection.childNodeSize() < 10) null else firstSection.childNode(10)
                    .childNode(
                        3
                    ).childNode(0)

            results["gogrow"] = if (goGrowNode != null && goGrowNode is TextNode)
                goGrowNode.text().convertFromMoneyToBigDecimal() else ZERO
            results["profit"] =
                lastSection.childNode(0).childNode(3).childNode(1).childNode(0).childNode(0).text()
                    .convertFromMoneyToBigDecimal()
            results["deposits"] =
                lastSection.childNode(4).childNode(3).childNode(0).childNode(0).text()
                    .convertFromMoneyToBigDecimal().abs()
            results["withdrawals"] =
                lastSection.childNode(6).childNode(3).childNode(0).childNode(0).text()
                    .convertFromMoneyToBigDecimal().abs()
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }

        return results
    }

    private fun loginInternal(login: Login) {
        val loginGetResponse = bondoraPageFetcher.getLogin()
        replaceCookies(loginGetResponse.cookies())

        val document = loginGetResponse.parse()

        formToken = getLoginKeyFromDocument(document)
        headerToken = getLoginKey2FromDocument(document)

        val loginResponse =
            bondoraPageFetcher.login(
                login.username,
                login.password,
                headerToken!!,
                formToken!!,
                cookies = cookiesGlobal
            )

        if (loginResponse.statusCode() == 406) {
            throw AuthenticationFailedException()
        }

        replaceCookies(loginResponse.cookies())
    }

    private fun getDailyChange(token: String): BigDecimal {
        return getStatements(Utils.getTodayDate(), token, reUse = true, daily = true)
            .statements
            .filter {
                it.type == ThirdPartyStatementType.DEPOSIT || it.type == ThirdPartyStatementType.WITHDRAWAL
                it.type == ThirdPartyStatementType.INTEREST || it.type == ThirdPartyStatementType.BONUS
            }
            .sumByBigDecimal { it.amount }
    }

    private fun getAccountReport(
        date: Date,
        token: String,
        reUse: Boolean,
        daily: Boolean
    ): List<AccountStatementReportLine> {
        var reportId: String? = null

        if (reUse) {
            reportId = reUseReportIfExists(token)
        }

        if (reportId == null) {
            println("Couldn't find and usable report")
            reportId = createReport(date, token)
            sleep(TimeUnit.SECONDS.toMillis(5))
        } else {
            println("Reusing report")
        }

        var response = getReport(reportId, token, daily)
        if (response == null) {
            reportId = createReport(date, token)
            sleep(TimeUnit.SECONDS.toMillis(5))
            response = getReport(reportId, token, daily)
        }

        if (response == null) {
            throw ThirdPartyException("Timeout fetching report")
        }

        validateResponse(response)
        return response.body()?.payload!!.result
    }

    private fun getReport(
        reportId: String,
        token: String,
        daily: Boolean
    ): Response<BondoraResponse<Report>>? {
        val bondoraApi =
            BondoraRetrofitInterface.retrofitInstance.create(BondoraApi::class.java)
        var response = bondoraApi.getReport(token, reportId).execute()

        var count = 0
        while (isReportGenerated(response).not()) {
            sleep(TimeUnit.SECONDS.toMillis(10))
            response = bondoraApi.getReport(token, reportId).execute()
            println("SLEEPING FOR A WHILE")

            if (daily && count > 1) {
                return null
            } else if (daily.not() && count > 2) {
                return null
            }
            count++
        }
        return response
    }

    private fun reUseReportIfExists(token: String): String? {
        val bondoraApi =
            BondoraRetrofitInterface.retrofitInstance.create(BondoraApi::class.java)

        val response = bondoraApi.getAllReports(token).execute()
        if (response.isSuccessful.not()) {
            return null
        }

        return response.body()?.payload
            ?.filter { it.reportType == TYPE }
            ?.filter { it.createdOn.isFiveHoursAgo() }
            ?.filter { it.generatedOn != null || it.createdOn.passedFiveMinutes().not() }
            ?.firstOrNull { it.periodStart.isToday() }
            ?.reportId
    }

    private fun createReport(date: Date, token: String): String {
        val bondoraApi =
            BondoraRetrofitInterface.retrofitInstance.create(BondoraApi::class.java)

        val today = Utils.getTodayDate()
        val reportRequest = ReportCreateRequest(TYPE, date, today)
        val response = bondoraApi.createReport(token, reportRequest).execute()
        validateResponse(response)
        return response.body()?.payload?.reportId!!
    }

    private fun getInvestments(token: String): List<Investment> {
        if (shouldFetch().not() && investmentsCached != null) {
            println("Using investments cache")
            return investmentsCached as List<Investment>
        } else {
            println("Investments cache is empty")
        }

        val bondoraApi =
            BondoraRetrofitInterface.retrofitInstance.create(BondoraApi::class.java)

        var total = 0
        var pageNr = 1
        val investments = emptyList<Investment>().toMutableList()

        do {
            val myInvestmentsResponse =
                bondoraApi.getInvestments(token, pageNr).execute()
            validateResponse(myInvestmentsResponse)
            val myInvestments = myInvestmentsResponse.body()!!
            total += myInvestments.count!!
            pageNr++
            myInvestments.payload?.let {
                investments.addAll(it.filter { loan -> loan.loanStatusCode != 4 })
            }
        } while (total < myInvestments.totalCount!!)

        investmentsCached = investments
        return investments
    }

    private fun validateResponse(response: Response<*>) {
        if (response.isSuccessful) {
            return
        }

        val errorBody = response.errorBody()?.string()

        throw ThirdPartyException(errorBody, response.code().toString(), null)
    }

    private fun recycle() {
        val shouldRecycle =
            System.nanoTime() - lastFlushTime >= TimeUnit.MILLISECONDS.toNanos(flushInterval)
        if (!shouldRecycle) return
        cleanCache()
    }

    private fun cleanCache() {
        investmentsCached = null
    }

    private fun shouldFetch(): Boolean {
        recycle()
        return investmentsCached == null
    }

    private fun isReportGenerated(response: Response<BondoraResponse<Report>>): Boolean {
        return response.isSuccessful &&
            response.body() != null &&
            response.body()!!.payload != null &&
            response.body()!!.payload?.generatedOn != null &&
            response.body()!!.payload?.result != null
    }

    private fun getLoginKeyFromDocument(document: Document): String {
        val first = document.select("input[name=__RequestVerificationToken]").first()
        return first.attr("value")
    }

    private fun getLoginKey2FromDocument(document: Document): String {
        return (document.getElementsByTag("script")[15].childNode(0) as DataNode).wholeData.trim()
            .split(
                "\'"
            )[1]
    }

    private fun handleMultipleDateFormats(textDate: String): Date {
        return try {
            textDate.toDate("dd/MM/yyyy")
        } catch (e: ParseException) {
            textDate.toDate("dd.MM.yyyy")
        }
    }

    companion object {
        // Debuggable field to check instance count
        private var mInstance: BondoraCase? = null
        private const val TYPE = 7

        @Synchronized
        fun getInstance(clientId: String, clientSecret: String): BondoraCase {
            if (mInstance == null) {
                mInstance = BondoraCase(clientId, clientSecret)
            }
            return mInstance as BondoraCase
        }
    }
}
