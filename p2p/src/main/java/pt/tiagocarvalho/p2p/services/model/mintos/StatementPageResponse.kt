package pt.tiagocarvalho.p2p.services.model.mintos

internal data class StatementPageResponse(val data: StatementPageData?)
