package pt.tiagocarvalho.p2p.services.converter.bondora

import java.math.RoundingMode
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.bondora.AccountStatementReportLine

internal class StatementsConverter :
    ModelConverter<List<AccountStatementReportLine>, ThirdPartyStatementModel>() {

    override fun convert(input: List<AccountStatementReportLine>): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        input.forEach { entry ->
            val type = convertToType(entry.description)
            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(entry.description)
            } else {
                list.add(
                    ThirdPartyStatement(
                        id = entry.number.toString(),
                        amount = entry.amount.setScale(2, RoundingMode.HALF_UP).abs(),
                        date = entry.transferDate,
                        type = type,
                        name = ThirdPartyName.BONDORA
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun convertToType(
        value: String
    ): ThirdPartyStatementType {
        return when {
            value.startsWith("TransferDeposit") -> ThirdPartyStatementType.DEPOSIT
            value.startsWith("TransferWithdraw|EE") -> ThirdPartyStatementType.WITHDRAWAL
            value.contains("TransferWithdraw|") -> ThirdPartyStatementType.WITHDRAWAL
            value == "TransferGoGrow" -> ThirdPartyStatementType.INVESTMENT
            value == "TransferAuctionBidWinner" -> ThirdPartyStatementType.INVESTMENT
            value == "TransferSecondMarketBidWinner" -> ThirdPartyStatementType.PRINCIPAL
            value == "TransferMainRepaiment" -> ThirdPartyStatementType.PRINCIPAL
            value == "TransferInterestRepaiment" -> ThirdPartyStatementType.INTEREST
            value == "TransferTrialFunds" -> ThirdPartyStatementType.BONUS
            value == "TransferExtraInterestRepaiment" -> ThirdPartyStatementType.INTEREST
            value == "TransferPartialMainRepaiment" -> ThirdPartyStatementType.PRINCIPAL
            value == "TransferGoGrowMainRepaiment" -> ThirdPartyStatementType.WITHDRAWAL
            value == "GoGrowWithdrawalFee" -> ThirdPartyStatementType.FEE
            value == "TransferGoGrowInterestRepaiment" -> ThirdPartyStatementType.INTEREST
            value.startsWith("TransferBonus") -> ThirdPartyStatementType.BONUS
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
