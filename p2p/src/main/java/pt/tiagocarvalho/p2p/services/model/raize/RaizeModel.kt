package pt.tiagocarvalho.p2p.services.model.raize

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class RaizeGenericList<T>(
    var results: List<T>? = ArrayList(),
    var page: Int = 0,
    var numPages: Int = 0
)

data class RaizeCurrent(
    var wallet: RaizeWallet? = null,
    var loans: List<RaizeLoans>? = null
)

class RaizeLoans

class RaizeTransaction(
    @SerializedName("type")
    var type: String? = null,
    @SerializedName("amount")
    var amount: String? = null,
    @SerializedName("operation")
    var operation: String? = null,
    @SerializedName("description")
    var description: String? = null,
    @SerializedName("projectId")
    var projectId: String? = null,
    @SerializedName("createdAt")
    var createdAt: String? = null,
    @SerializedName("status")
    var status: String? = null
)

data class RaizeWallet(
    @SerializedName("balance")
    var balance: BigDecimal? = null,
    @SerializedName("hold")
    var hold: BigDecimal? = null,
    @SerializedName("capital")
    var capital: BigDecimal? = null,
    @SerializedName("interest")
    var interest: BigDecimal? = null,
    @SerializedName("taxes")
    var taxes: BigDecimal? = null,
    @SerializedName("referrals")
    var referrals: BigDecimal? = null,
    @SerializedName("bonus")
    var bonus: BigDecimal? = null,
    @SerializedName("extras")
    var extras: BigDecimal? = null,
    @SerializedName("defaults")
    var defaults: BigDecimal? = null,
    @SerializedName("financed")
    var financed: BigDecimal? = null,
    @SerializedName("bought")
    var bought: BigDecimal? = null,
    @SerializedName("sold")
    var sold: BigDecimal? = null,
    @SerializedName("gains")
    var gains: BigDecimal? = null,
    @SerializedName("charged")
    var charged: BigDecimal? = null,
    @SerializedName("withdrawn")
    var withdrawn: BigDecimal? = null,
    @SerializedName("gain")
    var gain: BigDecimal? = null,
    @SerializedName("netGains")
    var netGains: BigDecimal? = null,
    @SerializedName("returnPercent")
    var returnPercent: BigDecimal? = null,
    @SerializedName("netReturnPercent")
    var netReturnPercent: BigDecimal? = null,
    @SerializedName("total")
    var total: BigDecimal? = null
)

data class SearchFilter(
    var isPaged: Boolean = false,
    var isFilters: Boolean = false,
    var startDate: String? = null,
    var endDate: String? = null,
    var page: Int = 0
)

data class PostTokenResponse(
    @SerializedName("mfaMethod") val mfaMethod: String?,
    @SerializedName("access_token") val accessToken: String?,
    @SerializedName("device_token") val deviceToken: String?
)
