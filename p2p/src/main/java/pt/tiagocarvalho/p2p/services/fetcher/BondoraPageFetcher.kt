package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.helper.HttpConnection.connect
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.DASHBOARD_URL
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.LOGIN_URL
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.OVERVIEW_URL
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.PORTFOLIO_URL
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.STATEMENTS_DETAILS
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.STATEMENTS_INDEX_URL
import pt.tiagocarvalho.p2p.services.fetcher.BondoraPageFetcher.Constants.STATS_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class BondoraPageFetcher {

    fun getLogin(): Connection.Response {
        return Jsoup.connect(LOGIN_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun login(
        username: String,
        password: String,
        formToken: String,
        headerToken: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(LOGIN_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(CONTENT_TYPE_KEY, "application/x-www-form-urlencoded")
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header("x-requested-with", "XMLHttpRequest")
            .header("__requestverificationtoken", headerToken)
            .data("Email", username)
            .data("Password", password)
            .data("__RequestVerificationToken", formToken)
            .data("returnUrl", "/en")
            .data("TimeZone", "")
            .cookies(cookies)
            .followRedirects(true)
            .ignoreContentType(true)
            .method(Connection.Method.POST)
            .ignoreHttpErrors(true)
            .execute()
    }

    fun getDashboard(
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(DASHBOARD_URL)
            .userAgent(USER_AGENT_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getOverview(
        token: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(OVERVIEW_URL)
            .userAgent(USER_AGENT_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .header("__requestverificationtoken", token)
            .header("x-requested-with", "XMLHttpRequest")
            .ignoreContentType(true)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatementsIndex(
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(STATEMENTS_INDEX_URL)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatements(
        id: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(STATEMENTS_DETAILS + id)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getportfolioprofitability(
        token: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(PORTFOLIO_URL)
            .userAgent(USER_AGENT_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .header("__requestverificationtoken", token)
            .header("x-requested-with", "XMLHttpRequest")
            .ignoreContentType(true)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getstatsnumber(
        token: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(STATS_URL)
            .userAgent(USER_AGENT_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .header("__requestverificationtoken", token)
            .header("x-requested-with", "XMLHttpRequest")
            .ignoreContentType(true)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    private object Constants {
        const val LOGIN_URL = "https://www.bondora.com/en/login"
        const val DASHBOARD_URL = "https://www.bondora.com/en/dashboard"
        const val OVERVIEW_URL = "https://www.bondora.com/en/dashboard/overviewnumbers/"
        const val PORTFOLIO_URL = "https://www.bondora.com/en/statistics/getportfolioprofitability"
        const val STATS_URL = "https://www.bondora.com/en/dashboard/statnumbers/"
        const val STATEMENTS_INDEX_URL = "https://www.bondora.com/en/statements/index/"
        const val STATEMENTS_DETAILS = "https://www.bondora.com"
    }
}
