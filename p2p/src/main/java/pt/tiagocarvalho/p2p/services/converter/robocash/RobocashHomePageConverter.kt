package pt.tiagocarvalho.p2p.services.converter.robocash

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Locale
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.utils.convertFromInterestToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class RobocashHomePageConverter : ModelConverter<Element, ThirdPartyDetails>() {

    override fun convert(input: Element): ThirdPartyDetails {
        val mainDivs = input.getElementsByClass("white-block-body")

        val availableAccount =
            input.getElementsByClass("client-top-balance")[0].getElementsByTag("value-with-roundings")
                .first()
                .attributes().get("value").convertFromMoneyToBigDecimal(Locale.UK)
        val balance =
            mainDivs[0].getElementsByTag("value-with-roundings").first().attributes().get("value")
                .convertFromMoneyToBigDecimal(Locale.UK)
        val earnedInterests =
            mainDivs[1].getElementsByTag("value-with-roundings").first().attributes().get("value")
                .convertFromMoneyToBigDecimal(Locale.UK)
        val interestByToday =
            mainDivs[2].getElementsByTag("value-with-roundings").first().attributes().get("value")
                .convertFromMoneyToBigDecimal(Locale.UK)
        val averageInterest =
            mainDivs[3].getElementsByClass("left").first().text().convertFromInterestToBigDecimal()
        val current =
            mainDivs[4].getElementsByTag("value-with-roundings").first().attributes().get("value")
                .convertFromMoneyToBigDecimal(Locale.UK)
        val overdue =
            mainDivs[4].getElementsByTag("value-with-roundings").last().attributes().get("value")
                .convertFromMoneyToBigDecimal(Locale.UK)
        val deposit =
            mainDivs[5].getElementsByClass("digit").first().text().convertFromMoneyToBigDecimal(Locale.UK)
        val withdrawn =
            mainDivs[5].getElementsByClass("digit").last().text().convertFromMoneyToBigDecimal(Locale.UK)

        var available = availableAccount
        var invested = BigDecimal.ZERO
        mainDivs.drop(6).forEach {
            val balancePortfolio =
                it.getElementsByTag("value-with-roundings").first().attr("value")
                    .convertFromMoneyToBigDecimal(Locale.UK)
            val loans = it.getElementsByClass("digit").last().text().convertFromMoneyToBigDecimal(Locale.UK)

            available = available.add(balancePortfolio)
            invested = invested.add(loans)
        }

        return ThirdPartyDetails(
            name = ThirdPartyName.ROBOCASH,

            balance = balance.setScale(2, RoundingMode.HALF_UP),
            available = available.setScale(2, RoundingMode.HALF_UP),
            invested = invested.setScale(2, RoundingMode.HALF_UP),

            interests = earnedInterests.setScale(2, RoundingMode.HALF_UP),
            profit = earnedInterests.setScale(2, RoundingMode.HALF_UP),
            netAnnualReturn = averageInterest.setScale(2, RoundingMode.HALF_UP),
            pendingInterests = interestByToday.setScale(2, RoundingMode.HALF_UP),
            currentTotal = current.setScale(2, RoundingMode.HALF_UP),
            lateTotal = overdue.setScale(2, RoundingMode.HALF_UP),
            deposits = deposit.setScale(2, RoundingMode.HALF_UP),
            withdrawals = withdrawn.setScale(2, RoundingMode.HALF_UP)
        )
    }
}
