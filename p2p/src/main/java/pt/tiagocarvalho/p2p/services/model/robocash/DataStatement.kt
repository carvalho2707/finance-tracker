package pt.tiagocarvalho.p2p.services.model.robocash

import com.google.gson.annotations.SerializedName
import java.util.Date

internal data class DataStatement(
    val id: String,
    val amount: AmountData,
    val type: String,
    @SerializedName("created_at") val date: Date
)
