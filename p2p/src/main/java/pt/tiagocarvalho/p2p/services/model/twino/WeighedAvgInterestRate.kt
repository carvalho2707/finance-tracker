package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class WeighedAvgInterestRate(

    @SerializedName("rateBuyback") val rateBuyback: BigDecimal,
    @SerializedName("rateBuybackInCurrency") val rateBuybackInCurrency: BigDecimal,
    @SerializedName("ratePaymentGuarantee") val ratePaymentGuarantee: BigDecimal,
    @SerializedName("total") val total: BigDecimal
)
