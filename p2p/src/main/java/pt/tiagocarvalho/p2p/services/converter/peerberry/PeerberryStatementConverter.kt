package pt.tiagocarvalho.p2p.services.converter.peerberry

import java.util.Locale
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.peerberry.PeerberryTransaction
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class PeerberryStatementConverter :
    ModelConverter<List<PeerberryTransaction>, ThirdPartyStatementModel>() {

    private val datePattern = "yyyy-MM-dd"

    override fun convert(input: List<PeerberryTransaction>): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        input.forEach {
            val type = convertType(it.type!!)

            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(it.type!!)
            } else {
                list.add(
                    ThirdPartyStatement(
                        it.id!!,
                        it.amount!!.convertFromMoneyToBigDecimal(Locale.UK).abs(),
                        Utils.convertStringToDate(it.bookingDate!!, datePattern),
                        type,
                        ThirdPartyName.PEERBERRY
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun convertType(desc: String): ThirdPartyStatementType {
        return when (desc) {
            "REPAYMENT_INTEREST" -> ThirdPartyStatementType.INTEREST
            "REPAYMENT_PRINCIPAL" -> ThirdPartyStatementType.PRINCIPAL
            "INVESTMENT" -> ThirdPartyStatementType.INVESTMENT
            "BUYBACK_PRINCIPAL" -> ThirdPartyStatementType.PRINCIPAL
            "BUYBACK_INTEREST" -> ThirdPartyStatementType.INTEREST
            "DEPOSIT" -> ThirdPartyStatementType.DEPOSIT
            "WITHDRAWAL" -> ThirdPartyStatementType.WITHDRAWAL
            "PRINCIPAL_REPAYMENT_CORRECTION" -> ThirdPartyStatementType.PRINCIPAL_FIX_NEGATIVE
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
