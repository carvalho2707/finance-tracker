package pt.tiagocarvalho.p2p.services.retrofit

import com.google.gson.JsonArray
import io.reactivex.Single
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE
import pt.tiagocarvalho.p2p.services.model.peerberry.LoginResponse
import pt.tiagocarvalho.p2p.services.model.peerberry.OverviewResponse
import pt.tiagocarvalho.p2p.services.model.peerberry.PeerberryGenericList
import pt.tiagocarvalho.p2p.services.model.peerberry.PeerberryTransaction
import pt.tiagocarvalho.p2p.services.model.peerberry.TfaRequest
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Query

interface PeerberryApi {

    @FormUrlEncoded
    @Headers(
        "Authorization: Basic",
        "Content-Type: application/x-www-form-urlencoded",
        "Accept: application/json, text/plain, */*",
        "Accept-Language: en",
        "Referer: https://peerberry.com/en/login",
        "Origin: https://peerberry.com",
        "User-Agent: $USER_AGENT_VALUE"
    )
    @POST("/v1/investor/login")
    fun login(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("gaClientId") gaClientId: String
    ): Single<LoginResponse>

    @Headers(
        "Authorization: Basic",
        "Content-Type: application/json",
        "Accept: application/json",
        "Accept-Language: en",
        "Referer: https://peerberry.com/en/login",
        "Origin: https://peerberry.com",
        "User-Agent: $USER_AGENT_VALUE"
    )
    @POST("/v1/investor/login/2fa")
    fun loginTfa(
        @Body tfaRquest: TfaRequest
    ): Single<LoginResponse>

    @Headers(
        "Accept-Language: en",
        "Referer: https://peerberry.com/en/overview",
        "User-Agent: $USER_AGENT_VALUE"
    )
    @GET("/v1/investor/overview")
    fun getOverview(@Header("Authorization") authorization: String): Call<OverviewResponse>

    @Headers(
        "Accept-Language: en",
        "User-Agent: $USER_AGENT_VALUE"
    )
    @GET("/v1/investor/token")
    fun getToken(@Header("Authorization") authorization: String): Call<JsonArray>

    @Headers(
        "Accept-Language: en",
        "User-Agent: $USER_AGENT_VALUE"
    )
    @GET("/v1/investor/transactions")
    fun getTransactions(
        @Header("Authorization") authorization: String,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String,
        @Query("transactionType") transactionType: String,
        @Query("page") page: Int
    ): Call<PeerberryGenericList<PeerberryTransaction>>
}
