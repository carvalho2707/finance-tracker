package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.helper.HttpConnection.connect
import pt.tiagocarvalho.p2p.services.fetcher.LenderMarketPageFetcher.Constants.LOGIN_URL
import pt.tiagocarvalho.p2p.services.fetcher.LenderMarketPageFetcher.Constants.STATEMENTS
import pt.tiagocarvalho.p2p.services.fetcher.LenderMarketPageFetcher.Constants.SUMMARY
import pt.tiagocarvalho.p2p.services.fetcher.LenderMarketPageFetcher.Constants.WELCOME_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class LenderMarketPageFetcher {

    fun getWelcomePage(): Connection.Response {
        return connect(WELCOME_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun login(
        username: String,
        password: String,
        token: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(LOGIN_URL)
            .header(CONTENT_TYPE_KEY, "application/x-www-form-urlencoded")
            .data("_csrf", token)
            .data("LoginForm[identificator]", username)
            .data("LoginForm[password]", password)
            .data("ajax", "login-form-top")
            .header("x-csrf-token", token)
            .cookies(cookies)
            .ignoreContentType(true)
            .followRedirects(true)
            .method(Connection.Method.POST)
            .ignoreHttpErrors(true)
            .execute()
    }

    fun getSummary(cookies: Map<String, String>): Connection.Response {
        return connect(SUMMARY)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .ignoreContentType(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatements(
        from: String,
        to: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(String.format(STATEMENTS, from, to))
            .ignoreContentType(true)
            .followRedirects(true)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    private object Constants {
        const val WELCOME_URL = "https://www.lendermarket.com/"
        const val LOGIN_URL = "https://www.lendermarket.com/account/login"
        const val SUMMARY = "https://www.lendermarket.com/summary"

        const val STATEMENTS =
            "https://www.lendermarket.com/statement?period=all&filter[createdFrom]=%s&filter[createdTo]=%s&filter[transactionTypes]=&action=search"
    }
}
