package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.Jsoup.connect
import pt.tiagocarvalho.p2p.services.fetcher.GrupeerPageFetcher.Constants.ACCOUNT_STATEMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.GrupeerPageFetcher.Constants.INVESTMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.GrupeerPageFetcher.Constants.OVERVIEW_URL
import pt.tiagocarvalho.p2p.services.fetcher.GrupeerPageFetcher.Constants.WHY_GRUPEER_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_JSON_UTF
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class GrupeerPageFetcher {

    fun getWhyGrupeer(): Connection.Response {
        return connect(WHY_GRUPEER_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .ignoreHttpErrors(true)
            .execute()
    }

    fun getOverviewPage(cookies: Map<String, String>): Connection.Response {
        return connect(OVERVIEW_URL)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_JSON_UTF)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .ignoreContentType(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getAccountStatementsPageWithParams(
        cookies: Map<String, String>,
        params: Map<String, String>
    ): Connection.Response {
        return connect(ACCOUNT_STATEMENTS_URL)
            .data(params)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getMyInvestmentsPage(cookies: Map<String, String>): Connection.Response {
        return connect(INVESTMENTS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getNumberOfInvestmentsInLastPage(
        url: String,
        cookies: Map<String, String>
    ): Connection.Response? {
        return connect(url)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    private object Constants {
        const val WHY_GRUPEER_URL = "https://www.grupeer.com/why-grupeer"
        const val OVERVIEW_URL = "https://www.grupeer.com/overview/main-data"
        const val ACCOUNT_STATEMENTS_URL = "https://www.grupeer.com/account-statement"
        const val INVESTMENTS_URL = "https://www.grupeer.com/my-investments"
    }
}
