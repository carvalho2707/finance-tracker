package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class AccountSummary(

    @SerializedName("deposits") val deposits: BigDecimal,
    @SerializedName("withdrawals") val withdrawals: BigDecimal,
    @SerializedName("xirr") val xirr: BigDecimal,
    @SerializedName("investmentBalance") val investmentBalance: BigDecimal,
    @SerializedName("lossOnWriteoff") val lossOnWriteoff: BigDecimal,
    @SerializedName("income") val income: BigDecimal,
    @SerializedName("interestWithoutCurrencyFluctuations") val interestWithoutCurrencyFluctuations: BigDecimal,
    @SerializedName("currencyFluctuations") val currencyFluctuations: BigDecimal,
    @SerializedName("accountValue") val accountValue: BigDecimal,
    @SerializedName("investments") val investments: BigDecimal,
    @SerializedName("penalties") val penalties: BigDecimal,
    @SerializedName("campaigns") val campaigns: BigDecimal,
    @SerializedName("corrections") val corrections: BigDecimal
)
