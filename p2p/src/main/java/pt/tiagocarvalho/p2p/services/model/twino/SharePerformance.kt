package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class SharePerformance(

    @SerializedName("buyback") val buyback: Buyback,
    @SerializedName("buybackInCurrency") val buybackInCurrency: BuybackInCurrency,
    @SerializedName("paymentGuarantee") val paymentGuarantee: PaymentGuarantee,
    @SerializedName("total") val total: BigDecimal,
    @SerializedName("buybackPercent") val buybackPercent: BigDecimal,
    @SerializedName("buybackInCurrencyRelativePercents") val buybackInCurrencyRelativePercents: BigDecimal,
    @SerializedName("paymentGuaranteePercent") val paymentGuaranteePercent: BigDecimal
)
