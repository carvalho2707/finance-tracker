package pt.tiagocarvalho.p2p.services.cases

import java.util.Date
import java.util.HashMap
import java.util.concurrent.TimeUnit
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.AuthenticationFailedException
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.converter.lendermarket.DetailsConverter
import pt.tiagocarvalho.p2p.services.converter.lendermarket.StatementsConverter
import pt.tiagocarvalho.p2p.services.fetcher.LenderMarketPageFetcher
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class LenderMarketCase : BaseCase() {

    private val fetcher = LenderMarketPageFetcher()
    private val detailsConverter = DetailsConverter()
    private val statementsConverter = StatementsConverter()

    private val flushInterval: Long = TimeUnit.MINUTES.toMillis(3)
    private var lastFlushTime = System.nanoTime()

    private fun recycle() {
        val shouldRecycle =
            System.nanoTime() - lastFlushTime >= TimeUnit.MILLISECONDS.toNanos(flushInterval)
        if (!shouldRecycle) return
        cleanCache()
    }

    private fun cleanCache() {
        cookiesGlobal = HashMap()
    }

    private fun shouldFetch(): Boolean {
        recycle()
        return true
    }

    fun login(login: Login) {
        val welcomeResponse = fetcher.getWelcomePage()
        replaceCookies(welcomeResponse.cookies())

        val token = getTokenFromElement(welcomeResponse.parse())

        val loginPage = fetcher.login(login.username, login.password, token, cookiesGlobal)
        replaceCookies(loginPage.cookies())
        val body = loginPage.parse().body()
        val hasError = body.getElementsByClass("has-error").size != 0
        if (hasError) {
            throw AuthenticationFailedException(body?.toString())
        }
    }

    fun getDetails(login: Login): ThirdPartyDetails {
        login(login)

        val summaryResponse = fetcher.getSummary(cookiesGlobal)
        replaceCookies(summaryResponse.cookies())
        val summary = summaryResponse.parse().body()

        val today = Utils.getTodayDate()
        val statements = getStatements(today, login, false)
            .statements.filter { isBalanceRelated(it) }

        return detailsConverter.convert(summary, statements)
    }

    fun getStatements(
        date: Date,
        login: Login,
        shouldLogin: Boolean = true
    ): ThirdPartyStatementModel {
        if (shouldLogin) {
            val shouldFetch = shouldFetch()
            if (shouldFetch) {
                login(login)
            }
        }

        val from = date.toStringPattern("yyyy-MM-dd")
        val to = Utils.getTodayDateAsString("yyyy-MM-dd")

        val statementsResponse = fetcher.getStatements(from, to, cookiesGlobal)
        replaceCookies(statementsResponse.cookies())
        val body = statementsResponse.parse().body()

        try {
            return statementsConverter.convert(body)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body?.toString(), e)
        }
    }

    private fun getTokenFromElement(body: Element): String {
        body.getElementsByTag("meta").forEach {
            if (it.attr("name") == "csrf-token") {
                return it.attr("content")
            }
        }
        throw ThirdPartyException("Couldn't find key")
    }
}
