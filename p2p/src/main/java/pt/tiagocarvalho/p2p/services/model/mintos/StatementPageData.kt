package pt.tiagocarvalho.p2p.services.model.mintos

internal data class StatementPageData(val accountStatements: List<AccountStatement>?)
