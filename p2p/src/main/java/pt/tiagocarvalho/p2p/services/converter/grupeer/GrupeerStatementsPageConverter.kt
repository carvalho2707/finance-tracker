package pt.tiagocarvalho.p2p.services.converter.grupeer

import org.jsoup.nodes.Element
import org.jsoup.nodes.TextNode
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class GrupeerStatementsPageConverter :
    ModelConverter<Element, ThirdPartyStatementModel>() {

    private val datePattern = "dd.MM.yyyy"

    override fun convert(input: Element): ThirdPartyStatementModel {
        val rows = input.getElementsByClass("flex-container-statement")
        if (rows.size > 0 && rows[0].hasClass("head")) {
            rows.removeAt(0)
        }

        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        rows.forEach {
            val dateString = it.childNode(1).childNode(3).childNode(0).toString()
            val date = Utils.convertStringToDate(dateString, datePattern)
            val descFirst =
                it.childNode(3).childNode(0).toString().replace("\n", "").trim { t -> t <= ' ' }
            var descSecond = ""
            if (it.childNode(3).childNodeSize() > 1) {
                descSecond = (it.childNode(3).childNode(1).childNode(0) as TextNode).text()
            }
            val turnover =
                it.childNode(5).childNode(3).childNode(0).toString().convertFromMoneyToBigDecimal()
            val balance =
                it.childNode(7).childNode(3).childNode(0).toString().convertFromMoneyToBigDecimal()
            val type = getTypeFromDescription(descFirst)

            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(descFirst)
            } else {
                list.add(
                    ThirdPartyStatement(
                        "$dateString$descFirst$descSecond$turnover$balance",
                        turnover.abs(),
                        date,
                        type,
                        ThirdPartyName.GRUPEER
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun getTypeFromDescription(label: String): ThirdPartyStatementType {
        return when {
            label.startsWith("Added") -> ThirdPartyStatementType.DEPOSIT
            label.startsWith("Withdrawal") -> ThirdPartyStatementType.WITHDRAWAL
            label.contains("Requested") && label.contains("withdraw") -> ThirdPartyStatementType.WITHDRAWAL
            label.startsWith("Bought") -> ThirdPartyStatementType.INVESTMENT
            label.contains("interest payment received") -> ThirdPartyStatementType.INTEREST
            label.contains("principal") -> ThirdPartyStatementType.PRINCIPAL
            label.contains("cashback") -> ThirdPartyStatementType.BONUS
            label.contains("Referral income") -> ThirdPartyStatementType.REFERRAL
            label.contains("Referral bonus") -> ThirdPartyStatementType.REFERRAL
            label == "Welcome bonus" -> ThirdPartyStatementType.BONUS
            label.startsWith("Bonus amount for referral registration") -> ThirdPartyStatementType.REFERRAL
            label == "Withdraw request declined" -> ThirdPartyStatementType.WITHDRAWAL_CANCELED
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
