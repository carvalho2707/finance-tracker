package pt.tiagocarvalho.p2p.services.converter.lendermarket

import java.math.RoundingMode
import java.util.Locale
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromInterestToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text

class DetailsConverter {

    fun convert(summary: Element, statements: List<ThirdPartyStatement>): ThirdPartyDetails {
        val header = summary.getElementsByClass("text-center")

        val balance = header[3].childNode(1).childNode(0).text().convertFromMoneyToBigDecimal(Locale.UK)
        val available = header[5].childNode(1).childNode(0).text().convertFromMoneyToBigDecimal(Locale.UK)
        val netReturns =
            header[7].childNode(1).childNode(0).text().convertFromInterestToBigDecimal()

        val tables = summary.getElementsByClass("summary-widget")

        val first = tables[0].childNode(1).childNode(1)
        val investedFunds = first.childNode(2).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val deposits = first.childNode(8).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val withdrawals = first.childNode(12).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)

        val second = tables[1].childNode(1).childNode(1)
        val principalReceived = second.childNode(2).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val interests = second.childNode(4).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val delayedInterests = second.childNode(6).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val campaigns = second.childNode(8).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val profit = second.childNode(10).childNode(3).childNode(0).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)

        val third = tables[2].childNode(3).childNode(1)
        val currentTotal = third.childNode(0).childNode(3).childNode(1).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val currentNumber = third.childNode(0).childNode(3).childNode(3).childNode(0).text().toInt()
        val oneToFifteenTotal = third.childNode(2).childNode(3).childNode(1).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val oneToFifteenNumber =
            third.childNode(2).childNode(3).childNode(3).childNode(0).text().toInt()
        val sixteenToThirtyTotal = third.childNode(4).childNode(3).childNode(1).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val sixteenToThirtyNumber =
            third.childNode(4).childNode(3).childNode(3).childNode(0).text().toInt()
        val thirtySixtyTotal = third.childNode(6).childNode(3).childNode(1).childNode(0).text()
            .convertFromMoneyToBigDecimal(Locale.UK)
        val thirtySixtyNumber =
            third.childNode(6).childNode(3).childNode(3).childNode(0).text().toInt()
        val lateTotal = oneToFifteenTotal.add(sixteenToThirtyTotal).add(thirtySixtyTotal)
        val lateNumber = oneToFifteenNumber + sixteenToThirtyNumber + thirtySixtyNumber

        val dailyChange = statements
            .map {
                if (it.type == ThirdPartyStatementType.WITHDRAWAL || it.type == ThirdPartyStatementType.FEE) {
                    it.amount.negate()
                } else {
                    it.amount
                }
            }.sumByBigDecimal { it }
        val dailyPercentage = Utils.calculateChangePercentage(dailyChange, balance)

        return ThirdPartyDetails(
            name = ThirdPartyName.LENDERMARKET,

            balance = balance.setScale(2, RoundingMode.HALF_UP),
            available = available.setScale(2, RoundingMode.HALF_UP),
            invested = investedFunds.setScale(2, RoundingMode.HALF_UP),

            netAnnualReturn = netReturns.setScale(2, RoundingMode.HALF_UP),
            deposits = deposits.setScale(2, RoundingMode.HALF_UP),
            withdrawals = withdrawals.setScale(2, RoundingMode.HALF_UP),
            profit = profit.setScale(2, RoundingMode.HALF_UP),

            interests = interests.setScale(2, RoundingMode.HALF_UP),
            latePaymentFees = delayedInterests.setScale(2, RoundingMode.HALF_UP),
            campaigns = campaigns.setScale(2, RoundingMode.HALF_UP),

            investments = currentNumber + oneToFifteenNumber + sixteenToThirtyNumber + thirtySixtyNumber,
            currentTotal = currentTotal.setScale(2, RoundingMode.HALF_UP),
            oneToFifteenLateTotal = oneToFifteenTotal.setScale(2, RoundingMode.HALF_UP),
            sixteenToThirtyLateTotal = sixteenToThirtyTotal.setScale(2, RoundingMode.HALF_UP),
            thirtyToSixtyLateTotal = thirtySixtyTotal.setScale(2, RoundingMode.HALF_UP),
            lateTotal = lateTotal.setScale(2, RoundingMode.HALF_UP),
            currentNumber = currentNumber,
            oneToFifteenLateNumber = oneToFifteenNumber,
            sixteenToThirtyLateNumber = sixteenToThirtyNumber,
            thirtyToSixtyLateNumber = thirtySixtyNumber,
            lateNumber = lateNumber,

            cumulativePrincipal = principalReceived.setScale(2, RoundingMode.HALF_UP),

            changeValue = dailyChange.setScale(2, RoundingMode.HALF_UP),
            changePercentage = dailyPercentage

        )
    }
}
