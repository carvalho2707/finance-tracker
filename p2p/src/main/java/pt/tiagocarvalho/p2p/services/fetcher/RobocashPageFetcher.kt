package pt.tiagocarvalho.p2p.services.fetcher

import java.util.concurrent.TimeUnit
import org.jsoup.Connection
import org.jsoup.Jsoup.connect
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.CABINET_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.CODE
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.EMAIL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.LOGIN_TOKEN
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.LOGIN_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.MFA_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.MY_INVESTMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.PASSWORD
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.STATEMENT_LIST_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.STATEMENT_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.SUMMARY_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.UPDATE_URL
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher.Constants.WELCOME_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_FORM_URL_UTF
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_JSON_UTF
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

class RobocashPageFetcher {

    fun getWelcomePage(): Connection.Response {
        return connect(WELCOME_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun postLoginPage(
        username: String,
        password: String,
        cookies: Map<String, String>,
        key: String
    ): Connection.Response {
        return connect(LOGIN_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .data(EMAIL, username)
            .data(PASSWORD, password)
            .data(LOGIN_TOKEN, key)
            .followRedirects(false)
            .timeout(TimeUnit.SECONDS.toMillis(60).toInt())
            .cookies(cookies)
            .method(Connection.Method.POST)
            .execute()
    }

    fun getMfa(
        cookies: Map<String, String>
    ): Connection.Response = connect(MFA_URL)
        .userAgent(USER_AGENT_VALUE)
        .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
        .timeout(TimeUnit.SECONDS.toMillis(60).toInt())
        .cookies(cookies)
        .method(Connection.Method.GET)
        .execute()

    fun postMfa(
        key: String,
        code: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(MFA_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .data(LOGIN_TOKEN, key)
            .data(CODE, code)
            .followRedirects(false)
            .timeout(TimeUnit.SECONDS.toMillis(60).toInt())
            .cookies(cookies)
            .method(Connection.Method.POST)
            .execute()
    }

    fun getCabinetGlobalPage(cookies: Map<String, String>): Connection.Response {
        return connect(CABINET_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getSummaryPage(welcomeCookies: Map<String, String>): Connection.Response {
        return connect(SUMMARY_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(welcomeCookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatementsPage(cookies: Map<String, String>): Connection.Response {
        return connect(STATEMENT_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatementsPageWithParams(
        cookies: Map<String, String>,
        params: String,
        key: String
    ): Connection.Response {
        return connect(STATEMENT_LIST_URL)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_JSON_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .userAgent(USER_AGENT_VALUE)
            .header("accept", "application/json, text/plain, */*")
            .header("accept-encoding", "gzip, deflate, br")
            .header("x-csrf-token", key)
            .header("x-requested-with", "XMLHttpRequest")
            .requestBody(params)
            .ignoreContentType(true)
            .cookies(cookies)
            .method(Connection.Method.POST)
            .execute()
    }

    fun getUpdateSummary(
        cookies: Map<String, String>,
        start: String,
        end: String
    ): Connection.Response {
        return connect(UPDATE_URL)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_JSON_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .userAgent(USER_AGENT_VALUE)
            .data("start", start)
            .data("end", end)
            .data("currency_id", "1")
            .ignoreContentType(true)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getMyInvestmentsPrePage(cookies: Map<String, String>): Connection.Response {
        return connect(MY_INVESTMENTS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getMyInvestmentsPage(
        cookies: Map<String, String>,
        body: String,
        key: String
    ): Connection.Response {
        return connect(MY_INVESTMENTS_URL)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_JSON_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .userAgent(USER_AGENT_VALUE)
            .header("accept", "application/json, text/plain, */*")
            .header("accept-encoding", "gzip, deflate, br")
            .header("x-csrf-token", key)
            .requestBody(body)
            .ignoreContentType(true)
            .cookies(cookies)
            .method(Connection.Method.POST)
            .execute()
    }

    private object Constants {
        const val WELCOME_URL = "https://robo.cash/"
        const val LOGIN_URL = "https://robo.cash/login"
        const val MFA_URL = "https://robo.cash/login/mfa"
        const val CABINET_URL = "https://robo.cash/cabinet"
        const val SUMMARY_URL = "https://robo.cash/cabinet/summary"
        const val STATEMENT_URL = "https://robo.cash/cabinet/statement"
        const val MY_INVESTMENTS_URL = "https://robo.cash/cabinet/myinvestments"
        const val STATEMENT_LIST_URL = "https://robo.cash/cabinet/statements?currency_id=1"
        const val UPDATE_URL = "https://robo.cash/cabinet/statement/update"

        const val EMAIL = "email"
        const val PASSWORD = "password"
        const val LOGIN_TOKEN = "_token"
        const val CODE = "security_code"
    }
}
