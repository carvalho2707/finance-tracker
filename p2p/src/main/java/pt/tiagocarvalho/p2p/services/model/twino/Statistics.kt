package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName

data class Statistics(

    @SerializedName("sharePerformance") val sharePerformance: SharePerformance,
    @SerializedName("byOriginalTerm") val byOriginalTerm: ByOriginalTerm,
    @SerializedName("byRemainingTerm") val byRemainingTerm: ByRemainingTerm,
    @SerializedName("weighedAvgInterestRate") val weighedAvgInterestRate: WeighedAvgInterestRate
)
