package pt.tiagocarvalho.p2p.services.model.twino

data class LoginResponse(val success: Boolean)
