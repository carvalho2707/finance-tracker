package pt.tiagocarvalho.p2p.services.model.crowdestor

data class StatementPage(
    val draw: String,
    val recordsTotal: Int,
    val recordsFiltered: Int,
    val data: List<List<String>>?
)
