package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class ByRemainingTerm(

    @SerializedName("amount3") val amount3: BigDecimal,
    @SerializedName("amount11") val amount11: BigDecimal,
    @SerializedName("amount12Plus") val amount12Plus: BigDecimal,
    @SerializedName("percent3") val percent3: BigDecimal,
    @SerializedName("percent11") val percent11: BigDecimal,
    @SerializedName("percent12Plus") val percent12Plus: BigDecimal,
    @SerializedName("weighedAvgTerm") val weighedAvgTerm: BigDecimal,
    @SerializedName("total") val total: BigDecimal
)
