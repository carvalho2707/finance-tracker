package pt.tiagocarvalho.p2p.services.converter.iuvo

import java.math.BigDecimal
import java.math.RoundingMode
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.services.model.iuvo.Balances
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.getSafe

internal class DetailsConverter {

    fun convert(
        accountBalance: Balances?,
        overview: Map<String, BigDecimal>,
        allTime: Map<String, BigDecimal>,
        dailyChange: BigDecimal
    ): ThirdPartyDetails {
        checkNotNull(accountBalance) { "accountBalance is null" }

        val balance = overview.getSafe("balance")
        val interests = overview.getSafe("interest")
        val bonus = allTime.getSafe("bonus")
        val latePaymentFees = overview.getSafe("latePaymentFees")
        val fees = allTime.getSafe("secondary_market_sell_brokerage_fee").abs()
        val profit = interests.add(bonus).add(latePaymentFees).subtract(fees)

        val invested = overview.getSafe("invested")
        val current = overview.getSafe("current")
        val lateTotal = invested.subtract(current)

        return ThirdPartyDetails(
            name = ThirdPartyName.IUVO,

            balance = balance.setScale(2, RoundingMode.HALF_UP),
            available = overview.getSafe("available").setScale(2, RoundingMode.HALF_UP),
            invested = invested.setScale(2, RoundingMode.HALF_UP),

            netAnnualReturn = overview.getSafe("netAnnualReturn").setScale(2, RoundingMode.HALF_UP),
            deposits = allTime.getSafe("deposit").setScale(2, RoundingMode.HALF_UP),
            withdrawals = allTime.getSafe("withdraw").setScale(2, RoundingMode.HALF_UP).abs(),
            profit = profit.setScale(2, RoundingMode.HALF_UP),

            interests = interests.setScale(2, RoundingMode.HALF_UP),
            latePaymentFees = latePaymentFees.setScale(2, RoundingMode.HALF_UP),
            serviceFees = fees.setScale(2, RoundingMode.HALF_UP),
            campaigns = bonus.setScale(2, RoundingMode.HALF_UP),

            investments = overview.getSafe("myInvestments").toInt(),
            currentTotal = current.setScale(2, RoundingMode.HALF_UP),
            oneToFifteenLateTotal = overview.getSafe("threeFifteen")
                .setScale(2, RoundingMode.HALF_UP),
            sixteenToThirtyLateTotal = overview.getSafe("fifteenSixteen")
                .setScale(2, RoundingMode.HALF_UP),
            thirtyToSixtyLateTotal = overview.getSafe("sixteenThirty")
                .setScale(2, RoundingMode.HALF_UP),
            moreThanSixtyLateTotal = overview.getSafe("moreThanSixty")
                .setScale(2, RoundingMode.HALF_UP),
            lateTotal = lateTotal.setScale(2, RoundingMode.HALF_UP),

            cumulativePrincipal = overview.getSafe("cumulativePayments")
                .setScale(2, RoundingMode.HALF_UP),

            changeValue = dailyChange.setScale(2, RoundingMode.HALF_UP),
            changePercentage = Utils.calculateChangePercentage(dailyChange, balance)
                .setScale(2, RoundingMode.HALF_UP)
        )
    }
}
