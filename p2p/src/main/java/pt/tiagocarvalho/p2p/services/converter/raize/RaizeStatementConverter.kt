package pt.tiagocarvalho.p2p.services.converter.raize

import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.raize.RaizeTransaction
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class RaizeStatementConverter :
    ModelConverter<List<RaizeTransaction>, ThirdPartyStatementModel>() {

    private val datePattern = "dd-MM-yyyy"

    override fun convert(input: List<RaizeTransaction>): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        input.map {
            val type = convertType(it.type!!)

            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(it.type!!)
            } else {
                list.add(
                    ThirdPartyStatement(
                        it.createdAt + it.amount + it.type + it.description + it.operation,
                        it.amount!!.convertFromMoneyToBigDecimal().abs(),
                        Utils.convertStringToDate(it.createdAt!!, datePattern),
                        type,
                        ThirdPartyName.RAIZE
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun convertType(type: String): ThirdPartyStatementType {
        return when (type) {
            "referral" -> ThirdPartyStatementType.REFERRAL
            "walletCharged" -> ThirdPartyStatementType.DEPOSIT
            "loanGranted" -> ThirdPartyStatementType.INVESTMENT
            "loanSold" -> ThirdPartyStatementType.PRINCIPAL
            "walletWithdrawal" -> ThirdPartyStatementType.WITHDRAWAL
            "bidAccepted" -> ThirdPartyStatementType.INVESTMENT
            "bidCreated" -> ThirdPartyStatementType.INVESTMENT
            "installmentReceived" -> ThirdPartyStatementType.INTEREST
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
