package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Buyback(

    @SerializedName("current") val current: BigDecimal,
    @SerializedName("currentPercent") val currentPercent: BigDecimal,
    @SerializedName("extended") val extended: BigDecimal,
    @SerializedName("extendedPercent") val extendedPercent: BigDecimal,
    @SerializedName("delayed") val delayed: BigDecimal,
    @SerializedName("delayedPercent") val delayedPercent: BigDecimal,
    @SerializedName("defaulted") val defaulted: BigDecimal,
    @SerializedName("defaultedPercent") val defaultedPercent: BigDecimal,
    @SerializedName("total") val total: BigDecimal
)
