package pt.tiagocarvalho.p2p.services.cases

import com.google.gson.JsonParser
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import org.jsoup.nodes.Document
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.converter.mintos.MintosAccountStatementConverter
import pt.tiagocarvalho.p2p.services.converter.mintos.MintosHomePageConverter
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher
import pt.tiagocarvalho.p2p.services.model.mintos.AccountStatementResponse
import pt.tiagocarvalho.p2p.services.model.mintos.Balance
import pt.tiagocarvalho.p2p.services.model.mintos.InRecovery
import pt.tiagocarvalho.p2p.services.model.mintos.InvestmentData
import pt.tiagocarvalho.p2p.services.model.mintos.NetAnnualReturns
import pt.tiagocarvalho.p2p.services.model.mintos.OutstandingPrincipals
import pt.tiagocarvalho.p2p.services.model.mintos.PendingPayments
import pt.tiagocarvalho.p2p.services.model.mintos.StatementPageResponse
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.toBigDecimalOrZero
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class MintosCase : BaseCase() {

    private val mintosPageFetcher = MintosPageFetcher()
    private val mintosHomePageConverter = MintosHomePageConverter()
    private val mintosAccountStatementConverter = MintosAccountStatementConverter()

    fun getDetails(cookies: String): ThirdPartyDetails {
        this.cookiesGlobal = parseCookies(cookies)
        val details = getMainPage()
        val withdrawals = getWithdrawals()
        val deposits = getDeposits()
        val dailyChange = getDailyChange()

        details.deposits = deposits.setScale(2, RoundingMode.HALF_UP)
        details.withdrawals = withdrawals.setScale(2, RoundingMode.HALF_UP)
        details.changePercentage = Utils.calculateChangePercentage(dailyChange, details.balance)
        details.changeValue = dailyChange

        return details
    }

    private fun getMainPage(): ThirdPartyDetails {
        val mainPage = mintosPageFetcher.getOverview(cookiesGlobal)
        replaceCookies(mainPage.cookies())

        val mintosInfo = HashMap<String, ThirdPartyDetails>()

        val aggregates =
            mintosPageFetcher.getOverviewAggregates(cookiesGlobal, 978)
        replaceCookies(aggregates.cookies())
        val aggregatesBody = aggregates.body() as String

        val overviewReturns =
            mintosPageFetcher.getOverviewNetReturns(cookiesGlobal, 978)
        replaceCookies(overviewReturns.cookies())
        val overviewReturnsBody = overviewReturns.body() as String

        val overview = mintosPageFetcher.getOverview2(cookiesGlobal)
        replaceCookies(overview.cookies())

        val overviewContent = overview.parse()
        val balance = getBalanceFromHtml(overviewContent)

        val info: ThirdPartyDetails
        try {
            val jsonObjectReturns =
                JsonParser().parse(overviewReturnsBody).asJsonObject["data"].asJsonObject
            val netReturnsElement = jsonObjectReturns["netAnnualReturns"]
            val netAnnualReturns =
                if (netReturnsElement != null && netReturnsElement.isJsonObject) getGson().fromJson(
                    netReturnsElement,
                    NetAnnualReturns::class.java
                )
                else null

            val netAnnualReturnsWithCampaignBonusesElement =
                jsonObjectReturns["netAnnualReturnsWithCampaignBonuses"]
            val netAnnualReturnsWithCampaignBonuses =
                if (netAnnualReturnsWithCampaignBonusesElement != null && netAnnualReturnsWithCampaignBonusesElement.isJsonObject) getGson().fromJson(
                    netAnnualReturnsWithCampaignBonusesElement,
                    NetAnnualReturns::class.java
                )
                else null

            val jsonObjectAggregates =
                JsonParser().parse(aggregatesBody).asJsonObject["data"].asJsonObject

            val outstandingPrincipalsElement = jsonObjectAggregates["outstandingPrincipals"]
            val outstandingPrincipals =
                if (outstandingPrincipalsElement != null && outstandingPrincipalsElement.isJsonObject) getGson().fromJson(
                    outstandingPrincipalsElement,
                    OutstandingPrincipals::class.java
                )
                else null

            val pendingPaymentsElement = jsonObjectAggregates["pendingPayments"]
            val pendingPayments =
                if (pendingPaymentsElement != null && pendingPaymentsElement.isJsonObject) getGson().fromJson(
                    pendingPaymentsElement,
                    PendingPayments::class.java
                )
                else null

            val inRecoveryElement = jsonObjectAggregates["inRecovery"]
            val inRecovery =
                if (inRecoveryElement != null && inRecoveryElement.isJsonObject) getGson().fromJson(
                    inRecoveryElement,
                    InRecovery::class.java
                )
                else null

            val investmentDataElement = jsonObjectAggregates["investmentData"]
            val investmentData =
                getGson().fromJson(investmentDataElement, InvestmentData::class.java)

            info = mintosHomePageConverter.convert(
                netAnnualReturnsWithCampaignBonuses ?: netAnnualReturns,
                outstandingPrincipals,
                pendingPayments,
                investmentData,
                inRecovery,
                balance
            )
        } catch (e: Exception) {
            throw ThirdPartyException(
                e.message,
                "$aggregatesBody-${overviewContent.getElementsByTag("script")}",
                e
            )
        }
        mintosInfo["978"] = info

        // TEMP
        val result = mintosInfo["978"]!!
        result.aggregates = mintosInfo.entries.joinToString(separator = ";")

        return result
    }

    private fun getWithdrawals(): BigDecimal {
        val from = getFirstPossibleDay()
        val to = getCurrentDateTimeString("dd.MM.yyyy")

        val filter = HashMap<String, String>()
        filter["account_statement_filter[fromDate]"] = from
        filter["account_statement_filter[toDate]"] = to
        filter["account_statement_filter[maxResults]"] = "300"
        filter["account_statement_filter[type][]"] = "37"

        val statementListResponse = mintosPageFetcher.getStatementsList(cookiesGlobal, filter)
        val body = statementListResponse.body() as String
        try {
            val accountStatementResponse =
                getGson().fromJson(body, AccountStatementResponse::class.java)

            val statementEntryGroups = accountStatementResponse.data.summary.statementEntryGroups
            return statementEntryGroups?.getOrDefault("37", ZERO)?.abs()
                ?.setScale(2, RoundingMode.HALF_UP) ?: ZERO
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    private fun getDeposits(): BigDecimal {
        val from = getFirstPossibleDay()
        val to = getCurrentDateTimeString("dd.MM.yyyy")

        val filter = HashMap<String, String>()
        filter["account_statement_filter[fromDate]"] = from
        filter["account_statement_filter[toDate]"] = to
        filter["account_statement_filter[maxResults]"] = "300"
        filter["account_statement_filter[type][]"] = "9"

        val statementListResponse = mintosPageFetcher.getStatementsList(cookiesGlobal, filter)
        val body = statementListResponse.body() as String
        try {
            val accountStatementResponse =
                getGson().fromJson(body, AccountStatementResponse::class.java)

            val statementEntryGroups = accountStatementResponse.data.summary.statementEntryGroups
            return statementEntryGroups?.getOrDefault("9", ZERO)?.abs()
                ?.setScale(2, RoundingMode.HALF_UP) ?: ZERO
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    fun getStatements(
        from: Date,
        cookies: String
    ): ThirdPartyStatementModel {
        this.cookiesGlobal = parseCookies(cookies)
        val to = getCurrentDateTimeString("dd.MM.yyyy")
        val fromString = from.toStringPattern("dd.MM.yyyy")

        val filter = HashMap<String, String>()
        filter["account_statement_filter[fromDate]"] = fromString
        filter["account_statement_filter[toDate]"] = to
        filter["account_statement_filter[maxResults]"] = "300"

        val statementListResponse = mintosPageFetcher.getStatementsList(cookiesGlobal, filter)
        var body = statementListResponse.body() as String

        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()

        val accountStatementResponse =
            getGson().fromJson(body, AccountStatementResponse::class.java)
        val pages: Int = accountStatementResponse.data.summary.total / 50 + 1

        for (i in 1..pages) {
            val statementPageResponse =
                mintosPageFetcher.getStatementsPage(cookiesGlobal, fromString, to, i)
            body = statementPageResponse.body() as String
            val accountStatementPagedResponse =
                getGson().fromJson(body, StatementPageResponse::class.java)
            val statementInfo =
                mintosAccountStatementConverter.convert(
                    accountStatementPagedResponse.data?.accountStatements ?: emptyList()
                )

            statements.addAll(statementInfo.statements)
            issues.addAll(statementInfo.issues)
        }
        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getDailyChange(): BigDecimal {
        val today = getCurrentDateTimeString("dd.MM.yyyy")

        val filter = HashMap<String, String>()
        filter["account_statement_filter[fromDate]"] = today
        filter["account_statement_filter[toDate]"] = today
        filter["account_statement_filter[maxResults]"] = "300"

        val statementListResponse = mintosPageFetcher.getStatementsList(cookiesGlobal, filter)
        val body = statementListResponse.body() as String
        try {
            val accountStatementResponse =
                getGson().fromJson(body, AccountStatementResponse::class.java)
            val entryGroup = accountStatementResponse.data.summary.statementEntryGroups
            return entryGroup?.map { mapEntryGroup(it.key, it.value) }?.sumByBigDecimal { it }
                ?: ZERO
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    private fun mapEntryGroup(key: String, value: BigDecimal): BigDecimal {
        return when (key) {
            "6", "7", "9", "13", "14", "17", "18", "19", "22", "23", "24", "25", "29", "30", "31", "35", "46", "47", "116", "117", "118", "119", "126", "127" -> value.abs()
            "28", "34", "37", "40", "48", "121", "122" -> value.abs().negate()
            else -> ZERO
        }
    }

    private fun getFirstPossibleDay(): String {
        val cal = Calendar.getInstance()
        cal.add(Calendar.YEAR, -29)
        val time = cal.time
        val formatter = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        return formatter.format(time)
    }

    private fun getBalanceFromHtml(content: Document): Balance {
        val balance: Balance? = null
        val scripts = content.getElementsByTag("script")
        scripts.forEach {
            if (it.toString().contains("accountBalance")) {
                return convertToBalance(it.toString())
            }
        }

        return balance ?: throw ThirdPartyException(scripts.toString())
    }

    private fun convertToBalance(script: String): Balance {
        val startIndex = script.indexOf("{") + 1
        val endIndex = script.indexOf("return")
        val content = script.substring(startIndex, endIndex)
        val contents: Map<String, String> = content.split(";")
            .filter { it.contains("=") }
            .associate {
                val property = it.split("=")
                val key = property[0].replace("\"", "").replaceBefore(".", "")
                val value = property[1].replace("\"", "")
                key to value
            }
        return Balance(
            contents[".accountBalance"].toBigDecimalOrZero(),
            contents[".total"].toBigDecimalOrZero(),
            contents[".totalReceivedInterest"].toBigDecimalOrZero(),
            contents[".totalReceivedLatePaymentFee"].toBigDecimalOrZero(),
            contents[".totalSecondaryMarketProfit"].toBigDecimalOrZero(),
            ZERO,
            contents[".totalSecondaryMarketFeePaid"].toBigDecimalOrZero(),
            ZERO,
            ZERO,
            ZERO,
            ZERO
        )
    }
}
