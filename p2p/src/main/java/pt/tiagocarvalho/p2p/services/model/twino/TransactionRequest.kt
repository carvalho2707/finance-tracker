package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName

data class TransactionRequest(

    @SerializedName("page") var page: Int,
    @SerializedName("pageSize") var pageSize: Int,
    @SerializedName("sortDirection") var sortDirection: String,
    @SerializedName("sortField") var sortField: String,
    @SerializedName("totalItems") var totalItems: Int,
    @SerializedName("processingDateFrom") var processingDateFrom: List<Int>,
    @SerializedName("processingDateTo") var processingDateTo: List<Int>,
    @SerializedName("transactionTypeList") var transactionTypeList: List<TransactionTypeList>,
    @SerializedName("accountTypeList") var accountTypeList: List<String>,
    @SerializedName("investmentLoanId") var investmentLoanId: String?
) {

    constructor() : this(
        1,
        20,
        "DESC",
        "created",
        0,
        emptyList(),
        emptyList(),
        emptyList(),
        emptyList(),
        null
    )
}
