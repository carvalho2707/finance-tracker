package pt.tiagocarvalho.p2p.services.model.twino

data class TwinoLoginTfa(
    val name: String,
    val password: String,
    val code: String?
)
