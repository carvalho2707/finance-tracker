package pt.tiagocarvalho.p2p.services.model.bondora

import java.math.BigDecimal

data class MyAccountBalance(
    val balance: BigDecimal,
    val reserved: BigDecimal,
    val bidRequestAmount: BigDecimal,
    val totalAvailable: BigDecimal,
    val goGrowAccounts: List<GoGrowAccount>
)
