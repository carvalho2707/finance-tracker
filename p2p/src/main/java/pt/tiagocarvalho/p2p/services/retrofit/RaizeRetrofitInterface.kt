package pt.tiagocarvalho.p2p.services.retrofit

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal object RaizeRetrofitInterface {

    private const val BASE_URL = " https://api.raize.pt/"
    private var retrofit: Retrofit? = null

    val retrofitInstance: Retrofit
        get() {
            if (retrofit == null) {
                val gson = GsonBuilder()
                    .setDateFormat("yyyy-MM-dd")
                    .create()

                val client = OkHttpClient.Builder().build()

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
            }
            return retrofit!!
        }
}
