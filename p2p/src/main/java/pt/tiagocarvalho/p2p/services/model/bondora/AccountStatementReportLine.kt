package pt.tiagocarvalho.p2p.services.model.bondora

import java.math.BigDecimal
import java.util.Date

data class AccountStatementReportLine(
    val transferDate: Date,
    val currency: String,
    val amount: BigDecimal,
    val number: Long,
    val description: String,
    val loanNumber: String?,
    val counterparty: String,
    val balanceAfterPayment: BigDecimal
)
