package pt.tiagocarvalho.p2p.services.model.crowdestor

data class InvestmentPage(
    val draw: String,
    val recordsTotal: Int,
    val recordsFiltered: Int
)
