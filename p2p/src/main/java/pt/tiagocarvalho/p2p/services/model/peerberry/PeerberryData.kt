package pt.tiagocarvalho.p2p.services.model.peerberry

import com.google.gson.annotations.SerializedName

data class OverviewResponse(
    val availableMoney: String,
    val invested: String,
    val totalProfit: String,
    val totalBalance: String,
    val investments: OverviewInvestments,
    val netAnnualReturn: String,
    val totalActiveInvestments: String,
)

data class OverviewInvestments(
    val current: String,
    val late15: String,
    val late30: String,
    val late60: String,
    val total: String,
)

data class PeerberryGenericList<T>(
    var data: List<T>? = null,
    var total: Int = 0,
    @SerializedName("_meta")
    var meta: Meta? = null
)

data class PeerberryTransaction(

    @SerializedName("booking_date")
    var bookingDate: String? = null,
    @SerializedName("loanId")
    var loanId: Int? = null,
    @SerializedName("investor_id")
    var investorId: Int? = null,
    @SerializedName("currency_id")
    var currencyId: Int? = null,
    @SerializedName("currency_sign")
    var currencySign: String? = null,
    var id: String? = null,
    var details: String? = null,
    var type: String? = null,
    var amount: String? = null

)

data class LoginResponse(
    @SerializedName("access_token") val accessToken: String? = null,
    @SerializedName("status") val status: String? = null,
    @SerializedName("mfaMethod") val mfaMethod: String? = null,
    @SerializedName("mfa") val mfa: Boolean? = null,
    @SerializedName("tfa_is_active") val tfaIsActive: Boolean? = null,
    @SerializedName("tfa_token") val tfaToken: String? = null,
    val unknownError: Boolean? = null,
    val passwordError: Boolean? = null,
    val invalidCode: Boolean? = false
)

data class Meta(
    var pages: Int = 0,
    var page: Int = 0
)

data class TfaRequest(
    @SerializedName("code") val code: String,
    @SerializedName("tfa_token") val tfaToken: String
)

data class Errors(
    val errors: Error
)

data class Error(
    val password: String?,
    val email: String?
)
