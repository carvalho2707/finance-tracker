package pt.tiagocarvalho.p2p.services.cases

import java.math.BigDecimal
import java.util.Date
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.AuthenticationFailedException
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.estateguru.OverviewConverter
import pt.tiagocarvalho.p2p.services.converter.estateguru.TransactionsConverter
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class EstateGuruCase : BaseCase() {

    private val estateGuruPageFetcher = EstateGuruPageFetcher()
    private val overviewConverter = OverviewConverter()
    private val transactionsConverter = TransactionsConverter()

    fun login(login: Login) {
        val welcomeResponse = estateGuruPageFetcher.getWelcomePage()
        replaceCookies(welcomeResponse.cookies())

        val loginResponse =
            estateGuruPageFetcher.login(
                login.username,
                login.password,
                cookiesGlobal
            )

        replaceCookies(loginResponse.cookies())

        if (loginResponse.statusCode() == 406) {
            throw AuthenticationFailedException()
        }
    }

    fun getDetails(login: Login): ThirdPartyDetails {
        val details = getMainPage(login)
        val dailyChange = getDailyChange(login)
        details.changePercentage = Utils.calculateChangePercentage(dailyChange, details.balance)
        details.changeValue = dailyChange
        return details
    }

    private fun getMainPage(login: Login): ThirdPartyDetails {
        login(login)

        val homeResponse = estateGuruPageFetcher.getHomePage(cookiesGlobal)
        replaceCookies(homeResponse.cookies())
        val body = homeResponse.parse().body()

        try {
            return overviewConverter.convert(body)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body?.toString(), e)
        }
    }

    private fun getDailyChange(login: Login): BigDecimal {
        val today = Utils.getTodayDate()

        val statements = getStatements(today, login, false).statements
        return statements
            .filter { isBalanceRelated(it) }
            .map {
                if (it.type == ThirdPartyStatementType.WITHDRAWAL || it.type == ThirdPartyStatementType.FEE) {
                    it.amount.negate()
                } else {
                    it.amount
                }
            }.sumByBigDecimal { it }
    }

    fun getStatements(
        date: Date,
        login: Login,
        shouldLogin: Boolean = true
    ): ThirdPartyStatementModel {
        if (shouldLogin) {
            // only login if we came from get statements or daily statements
            login(login)
        }

        val accountResponse = estateGuruPageFetcher.getAccountPage(cookiesGlobal)
        replaceCookies(accountResponse.cookies())

        val accountsBody = accountResponse.parse().body()
        val userId = getUserIdAccounts(accountsBody, login)

        val startDate = date.toStringPattern("dd.MM.yyyy")
        val today = Utils.getTodayDateAsString("dd.MM.yyyy")

        var shouldContinue = true
        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()
        val steps = 50
        var count = 1
        var offset = 0

        do {
            val response =
                estateGuruPageFetcher.getStatements(
                    userId,
                    startDate,
                    today,
                    offset,
                    cookiesGlobal
                )
            replaceCookies(response.cookies())
            val body = response.parse().body()
            try {
                val thirdPartyStatementModel = transactionsConverter.convert(body)
                statements.addAll(thirdPartyStatementModel.statements)
                issues.addAll(thirdPartyStatementModel.issues)

                val pagesDiv = body.getElementsByClass("page-info small")
                shouldContinue = if (pagesDiv.isNullOrEmpty()) {
                    false
                } else {
                    count < pagesDiv[0].childNode(0).text().substringAfterLast("").toInt()
                }

                count++
                offset = count * steps - steps
            } catch (e: Exception) {
                throw ThirdPartyException(e.message, login.username + ";" + login.password, e)
            }
        } while (shouldContinue)

        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getUserIdAccounts(body: Element, login: Login): String? {
        val userId = body.select("input[name=userId]").firstOrNull { it.attr("value").isNotBlank() }
        if (userId != null) {
            return userId.attr("value")
        } else {
            throw ThirdPartyException(
                login.username + ";" + login.password,
                body.getElementsByTag("script").toString(),
                null
            )
        }
    }
}
