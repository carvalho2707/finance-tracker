package pt.tiagocarvalho.p2p.services.converter.twino

import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.util.Calendar
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.twino.Transaction
import pt.tiagocarvalho.p2p.services.model.twino.TwinoFilterType

internal class TransactionConverter :
    ModelConverter<List<Transaction>, ThirdPartyStatementModel>() {

    override fun convert(input: List<Transaction>): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        input.forEach {
            val cal = Calendar.getInstance()
            cal.set(it.processingDate[0], it.processingDate[1] - 1, it.processingDate[2])

            val type = convertType(it.transactionType, it.accountType ?: "", it.amountRaw)

            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(it.transactionType)
            } else {
                list.add(
                    ThirdPartyStatement(
                        it.id.toString(),
                        it.amountRaw.setScale(2, RoundingMode.HALF_UP).abs(),
                        cal.time,
                        type,
                        ThirdPartyName.TWINO
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun convertType(
        transactionType: String,
        accountType: String,
        amount: BigDecimal
    ): ThirdPartyStatementType {
        val type = TwinoFilterType.valueOf(transactionType)

        return when {
            accountType == "INTEREST" -> ThirdPartyStatementType.INTEREST
            type == TwinoFilterType.BUY_SHARES -> {
                if (amount > ZERO) {
                    ThirdPartyStatementType.PRINCIPAL
                } else {
                    ThirdPartyStatementType.INVESTMENT
                }
            }
            type == TwinoFilterType.REPAYMENT -> ThirdPartyStatementType.PRINCIPAL
            type == TwinoFilterType.BUYBACK -> ThirdPartyStatementType.PRINCIPAL
            type == TwinoFilterType.REPURCHASE -> ThirdPartyStatementType.PRINCIPAL
            type == TwinoFilterType.EARLY_FULL_REPAYMENT -> ThirdPartyStatementType.PRINCIPAL
            type == TwinoFilterType.EARLY_PARTIAL_REPAYMENT -> ThirdPartyStatementType.PRINCIPAL
            type == TwinoFilterType.FUNDING ->
                ThirdPartyStatementType.DEPOSIT.takeIf { amount > ZERO }
                    ?: ThirdPartyStatementType.WITHDRAWAL
            type == TwinoFilterType.CASHBACK -> ThirdPartyStatementType.BONUS
            type == TwinoFilterType.WRITEOFF -> ThirdPartyStatementType.INVALID
            type == TwinoFilterType.ACCRUED_INTEREST -> ThirdPartyStatementType.INVALID
            type == TwinoFilterType.SCHEDULE -> ThirdPartyStatementType.INTEREST
            type == TwinoFilterType.RECOVERY -> ThirdPartyStatementType.INVALID
            type == TwinoFilterType.LOSS_ON_WRITEOFF -> ThirdPartyStatementType.INVALID
            type == TwinoFilterType.CURRENCY_FLUCTUATION -> ThirdPartyStatementType.INVALID
            type == TwinoFilterType.CORRECTION -> ThirdPartyStatementType.INVALID
            type == TwinoFilterType.EXTENSION -> ThirdPartyStatementType.IGNORE
            type == TwinoFilterType.BUY_OUT -> ThirdPartyStatementType.INVALID
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
