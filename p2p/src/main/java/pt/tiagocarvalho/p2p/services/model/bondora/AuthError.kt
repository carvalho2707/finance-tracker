package pt.tiagocarvalho.p2p.services.model.bondora

import com.google.gson.annotations.SerializedName

data class AuthError(
    val error: String,
    @SerializedName("error_description")
    val errorDescription: String
)
