package pt.tiagocarvalho.p2p.services.converter.crowdestor

import java.util.Locale
import org.jsoup.Jsoup
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text

internal class StatementsConverter :
    ModelConverter<List<List<String>>, ThirdPartyStatementModel>() {

    override fun convert(input: List<List<String>>): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        input.forEach { entry ->
            var first: Pair<String, String>? = null
            var second: Triple<String, String?, String?>? = null
            var third: String? = null
            entry.forEachIndexed { index, s ->
                when (index) {
                    0 -> first = parseFirst(s)
                    1 -> second = parseSecond(s)
                    2 -> third = parseThird(s)
                    else -> return@forEachIndexed
                }
            }

            if (second!!.first.contains("Registration balance")) {
                return@forEach
            }

            val type = convertToType(second!!.first)
            val date = Utils.convertStringToDate(first!!.first, "yyyy-MM-dd")
            var id = first!!.first + first!!.second + type + second!!.first

            second!!.second?.let { id += it }
            second!!.third?.let { id += it }

            id += third!!

            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(second!!.first)
            } else {
                list.add(
                    ThirdPartyStatement(
                        id = id,
                        amount = third!!.convertFromMoneyToBigDecimal(Locale.UK).abs(),
                        date = date,
                        type = type,
                        name = ThirdPartyName.CROWDESTOR
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun parseFirst(content: String): Pair<String, String> {
        val date =
            Jsoup.parse(content).getElementsByClass("transactions--date")[0].childNode(0).text()
        val time =
            Jsoup.parse(content).getElementsByClass("transactions--time")[0].childNode(0).text()

        return Pair(date, time)
    }

    private fun parseSecond(content: String): Triple<String, String?, String?> {
        val type =
            Jsoup.parse(content).getElementsByClass("transactions--name")[0].childNode(0).text()

        var projectName: String? = null
        var loadAgreement: String? = null

        val subInfo = Jsoup.parse(content).getElementsByClass("transactions--subinfo")
        if (subInfo.size == 1) {
            projectName =
                Jsoup.parse(content).getElementsByClass("transactions--subinfo")[0].childNode(1)
                    .childNode(0).text()
        }
        if (subInfo.size == 2) {
            loadAgreement =
                Jsoup.parse(content).getElementsByClass("transactions--subinfo")[1].childNode(1)
                    .childNode(0).text()
        }
        return Triple(type, projectName, loadAgreement)
    }

    private fun parseThird(content: String): String {
        return Jsoup.parse(content).getElementsByClass("transactions--amount")[0].childNode(0)
            .text()
    }

    private fun convertToType(value: String): ThirdPartyStatementType {
        return when (value) {
            "Bonus interest payment" -> ThirdPartyStatementType.INTEREST
            "Interest payment for project" -> ThirdPartyStatementType.INTEREST
            "Part payment for project" -> ThirdPartyStatementType.PRINCIPAL
            "Full payment for project" -> ThirdPartyStatementType.PRINCIPAL
            "Investment in project" -> ThirdPartyStatementType.INVESTMENT
            "Account deposit" -> ThirdPartyStatementType.DEPOSIT
            "Withdraw funds" -> ThirdPartyStatementType.WITHDRAWAL
            "Canceled investment in project" -> ThirdPartyStatementType.PRINCIPAL
            "Referral Bonus" -> ThirdPartyStatementType.REFERRAL
            "Cashback Bonus" -> ThirdPartyStatementType.BONUS
            "Primary market cashback" -> ThirdPartyStatementType.BONUS
            "Special Payment" -> ThirdPartyStatementType.BONUS
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
