package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.Jsoup.connect
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.ACCOUNT_STATEMENT_LIST
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.ACCOUNT_STATEMENT_PAGE
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.CURRENCY
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.FROM_DATE
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.MAX_RESULTS
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.OVERVIEW_NET_RETURNS
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.OVERVIEW_RESULTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.OVERVIEW_URL
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.OVERVIEW_URL2
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.PAGE
import pt.tiagocarvalho.p2p.services.fetcher.MintosPageFetcher.Constants.TO_DATE
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_FORM_URL_UTF
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class MintosPageFetcher {

    fun getOverview(
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(OVERVIEW_URL)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getOverview2(
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(OVERVIEW_URL2)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .referrer(OVERVIEW_URL)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .header("x-requested-with", "XMLHttpRequest")
            .method(Connection.Method.GET)
            .execute()
    }

    fun getOverviewAggregates(
        cookies: Map<String, String>,
        currency: Int
    ): Connection.Response {
        return connect(OVERVIEW_RESULTS_URL + currency)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .referrer(OVERVIEW_URL)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header("x-requested-with", "XMLHttpRequest")
            .ignoreContentType(true)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getOverviewNetReturns(
        cookies: Map<String, String>,
        currency: Int
    ): Connection.Response {
        return connect(OVERVIEW_NET_RETURNS + currency)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .referrer(OVERVIEW_URL)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header("x-requested-with", "XMLHttpRequest")
            .ignoreContentType(true)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatementsList(
        cookies: Map<String, String>,
        filter: Map<String, String>
    ): Connection.Response {
        return connect(ACCOUNT_STATEMENT_LIST)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header("origin", "https://www.mintos.com")
            .header("referer", "https://www.mintos.com/en/account-statement/")
            .userAgent(USER_AGENT_VALUE)
            .data(filter)
            .cookies(cookies)
            .ignoreContentType(true)
            .method(Connection.Method.POST)
            .execute()
    }

    fun getStatementsPage(
        cookies: Map<String, String>,
        from: String,
        to: String,
        page: Int
    ): Connection.Response {
        return connect(ACCOUNT_STATEMENT_PAGE)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_FORM_URL_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header("x-requested-with", "XMLHttpRequest")
            .userAgent(USER_AGENT_VALUE)
            .data(FROM_DATE, from)
            .data(TO_DATE, to)
            .data(MAX_RESULTS, "300")
            .data(CURRENCY, "978")
            .data(PAGE, page.toString())
            .ignoreContentType(true)
            .cookies(cookies)
            .method(Connection.Method.POST)
            .followRedirects(true)
            .execute()
    }

    private object Constants {
        const val OVERVIEW_URL = "https://www.mintos.com/en/overview/"
        const val OVERVIEW_URL2 =
            "https://www.mintos.com/webapp/en/overview/?&referrer=https://www.mintos.com&hash="
        const val OVERVIEW_RESULTS_URL =
            "https://www.mintos.com/en/overview-aggregates?currencyIsoCode="
        const val OVERVIEW_NET_RETURNS =
            "https://www.mintos.com/webapp/api/en/overview-net-annual-returns?currencyIsoCode="
        const val ACCOUNT_STATEMENT_LIST = "https://www.mintos.com/en/account-statement/list"
        const val ACCOUNT_STATEMENT_PAGE = "https://www.mintos.com/en/account-statement/page/"

        const val FROM_DATE = "account_statement_filter[fromDate]"
        const val TO_DATE = "account_statement_filter[toDate]"
        const val MAX_RESULTS = "account_statement_filter[maxResults]"
        const val CURRENCY = "account_statement_filter[currency]"
        const val PAGE = "account_statement_filter[page]"
    }
}
