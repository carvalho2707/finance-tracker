package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.Jsoup
import org.jsoup.helper.HttpConnection.connect
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.ACCOUNT_URL
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.EMAIL
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.HOME_URL
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.LOGIN_URL
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.PASSWORD
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.STATEMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.EstateGuruPageFetcher.Constants.WELCOME_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ORIGIN_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.REFERER_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class EstateGuruPageFetcher {

    fun getWelcomePage(): Connection.Response {
        return Jsoup.connect(WELCOME_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .method(Connection.Method.GET)
            .execute()
    }

    fun login(
        username: String,
        password: String,
        cookies: Map<String, String>
    ): Connection.Response {
        return connect(LOGIN_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(CONTENT_TYPE_KEY, "application/x-www-form-urlencoded")
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(REFERER_KEY, "https://estateguru.co/")
            .header(ORIGIN_KEY, "https://estateguru.co")
            .data(EMAIL, username)
            .data(PASSWORD, password)
            .cookies(cookies)
            .followRedirects(true)
            .method(Connection.Method.POST)
            .ignoreHttpErrors(true)
            .execute()
    }

    fun getHomePage(
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(HOME_URL)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .referrer("https://estateguru.co/")
            .followRedirects(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getAccountPage(cookies: Map<String, String>): Connection.Response {
        return Jsoup.connect(ACCOUNT_URL)
            .userAgent(USER_AGENT_VALUE)
            .cookies(cookies)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(REFERER_KEY, "https://estateguru.co/portal/portfolio/overview")
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatements(
        userId: String?,
        dateStart: String,
        dateEnd: String,
        offset: Int,
        cookies: Map<String, String>
    ): Connection.Response {
        return Jsoup.connect(STATEMENTS_URL)
            .userAgent(USER_AGENT_VALUE)
            .data("currentUserId", userId)
            .data("currentCurrency", "EUR")
            .data("userDetails", "")
            .data("showFutureTransactions", "false")
            .data("order", "")
            .data("sort", "")
            .data("filter_isFilter", "true")
            .data("filterTableId", "dataTableTransaction")
            .data("filter_dateApproveFilterFrom", dateStart)
            .data("filter_dateApproveFilterTo", dateEnd)
            .data("filter_loanName", "")
            .data("controller", "portfolio")
            .data("format", "null")
            .data("action", "ajaxFilterTransactions")
            .data("max", "50")
            .data("offset", offset.toString())
            .cookies(cookies)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(REFERER_KEY, "https://estateguru.co/portal/portfolio/account")
            .method(Connection.Method.POST)
            .execute()
    }

    private object Constants {
        const val WELCOME_URL = "https://estateguru.co/"
        const val LOGIN_URL = "https://estateguru.co/portal/login/authenticate"
        const val HOME_URL = "https://estateguru.co/portal/home"
        const val ACCOUNT_URL = "https://estateguru.co/portal/portfolio/account"
        const val STATEMENTS_URL = "https://estateguru.co/portal/portfolio/ajaxFilterTransactions"

        const val EMAIL = "username"
        const val PASSWORD = "password"
    }
}
