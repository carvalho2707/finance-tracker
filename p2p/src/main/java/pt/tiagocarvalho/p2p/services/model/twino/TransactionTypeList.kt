package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName

data class TransactionTypeList(

    @SerializedName("transactionType") val transactionType: String,
    @SerializedName("positive") val positive: Boolean?
)
