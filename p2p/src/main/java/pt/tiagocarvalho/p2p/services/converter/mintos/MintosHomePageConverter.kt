package pt.tiagocarvalho.p2p.services.converter.mintos

import java.math.RoundingMode
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.services.model.mintos.Balance
import pt.tiagocarvalho.p2p.services.model.mintos.InRecovery
import pt.tiagocarvalho.p2p.services.model.mintos.InvestmentData
import pt.tiagocarvalho.p2p.services.model.mintos.NetAnnualReturns
import pt.tiagocarvalho.p2p.services.model.mintos.OutstandingPrincipals
import pt.tiagocarvalho.p2p.services.model.mintos.PendingPayments
import pt.tiagocarvalho.p2p.services.utils.orZero

internal class MintosHomePageConverter {

    fun convert(
        netAnnualReturns: NetAnnualReturns?,
        outstandingPrincipals: OutstandingPrincipals?,
        pendingPayments: PendingPayments?,
        investmentData: InvestmentData,
        inRecovery: InRecovery?,
        balance: Balance
    ): ThirdPartyDetails {
        val available = balance.accountBalance
        val invested = outstandingPrincipals?.eur.orZero()
        val pendingPayment = pendingPayments?.eur.orZero()
        val recovery = inRecovery?.eur.orZero()
        val total = available.add(invested).add(pendingPayment).add(recovery)
        val fees =
            balance.totalMintosServiceFeePaid
                .add(balance.totalCurrencyExchangeFeePaid)
                .add(balance.totalSecondaryMarketFeePaid)
                .add(balance.totalMintosServiceFeePaid)

        val campaigns =
            balance.totalReceivedReferAfriendBonus.add(
                balance.totalReceivedAffiliateBonus
            )
        val secondaryMarket =
            balance.totalSecondaryMarketProfit.subtract(
                balance.totalSecondaryMarketFeePaid
            )

        val lateTotal =
            (investmentData.eur?.delayedWithinGracePeriodSum.orZero())
                .add(investmentData.eur?.late115Sum.orZero())
                .add(investmentData.eur?.late1630Sum.orZero())
                .add(investmentData.eur?.late3160Sum.orZero())
                .add(investmentData.eur?.recoverySum.orZero())
                .add(investmentData.eur?.defaultSum.orZero())
                .add(investmentData.eur?.badDebtSum.orZero())

        val lateNumber =
            (
                investmentData.eur?.delayedWithinGracePeriodCount
                    ?: 0
                ) + (investmentData.eur?.late115Count ?: 0) + (
                investmentData.eur?.late1630Count
                    ?: 0
                ) + (
                investmentData.eur?.late3160Count
                    ?: 0
                ) + (investmentData.eur?.recoveryCount ?: 0) + (
                investmentData.eur?.defaultCount
                    ?: 0
                ) + (investmentData.eur?.badDebtCount ?: 0)

        return ThirdPartyDetails(
            name = ThirdPartyName.MINTOS,

            balance = total.setScale(2, RoundingMode.HALF_UP),
            available = available.setScale(2, RoundingMode.HALF_UP),
            invested = invested.setScale(2, RoundingMode.HALF_UP),

            netAnnualReturn = (
                netAnnualReturns?.eur.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            interests = balance.totalReceivedInterest.setScale(
                2,
                RoundingMode.HALF_UP
            ),
            latePaymentFees = balance.totalReceivedLatePaymentFee.setScale(
                2,
                RoundingMode.HALF_UP
            ),
            secondaryMarketTransactions = secondaryMarket.setScale(2, RoundingMode.HALF_UP),
            cashback = balance.totalReceivedCashbackBonus.setScale(
                2,
                RoundingMode.HALF_UP
            ),
            serviceFees = fees.setScale(2, RoundingMode.HALF_UP),
            campaigns = campaigns.setScale(2, RoundingMode.HALF_UP),
            profit = balance.total.setScale(
                2,
                RoundingMode.HALF_UP
            ),

            investments = (investmentData.eur?.totalCount ?: 0),
            currentTotal = (
                investmentData.eur?.activeSum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            gracePeriodTotal = (
                investmentData.eur?.delayedWithinGracePeriodSum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            oneToFifteenLateTotal = (
                investmentData.eur?.late115Sum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            sixteenToThirtyLateTotal = (
                investmentData.eur?.late1630Sum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            thirtyToSixtyLateTotal = (
                investmentData.eur?.late3160Sum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            moreThanSixtyLateTotal = (
                investmentData.eur?.recoverySum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            defaultInvestmentTotal = (
                investmentData.eur?.defaultSum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            badDebtTotal = (
                investmentData.eur?.badDebtSum.orZero()
                ).setScale(2, RoundingMode.HALF_UP),
            lateTotal = lateTotal.setScale(2, RoundingMode.HALF_UP),

            currentNumber = investmentData.eur?.activeCount ?: 0,
            gracePeriodNumber = investmentData.eur?.delayedWithinGracePeriodCount
                ?: 0,
            oneToFifteenLateNumber = investmentData.eur?.late115Count ?: 0,
            sixteenToThirtyLateNumber = investmentData.eur?.late1630Count ?: 0,
            thirtyToSixtyLateNumber = investmentData.eur?.late3160Count ?: 0,
            moreThanSixtyLateNumber = investmentData.eur?.recoveryCount ?: 0,
            defaultInvestmentNumber = investmentData.eur?.defaultCount ?: 0,
            badDebtNumber = investmentData.eur?.badDebtCount ?: 0,
            lateNumber = lateNumber,

            pendingPayments = pendingPayments?.eur.orZero()
        )
    }
}
