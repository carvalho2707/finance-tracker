package pt.tiagocarvalho.p2p.services.cases

import com.google.gson.reflect.TypeToken
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.crowdestor.DashboardConverter
import pt.tiagocarvalho.p2p.services.converter.crowdestor.InvestmentsConverter
import pt.tiagocarvalho.p2p.services.converter.crowdestor.StatementsConverter
import pt.tiagocarvalho.p2p.services.fetcher.CrowdestorPageFetcher
import pt.tiagocarvalho.p2p.services.model.crowdestor.Dashboard
import pt.tiagocarvalho.p2p.services.model.crowdestor.InvestmentPage
import pt.tiagocarvalho.p2p.services.model.crowdestor.LatePayment
import pt.tiagocarvalho.p2p.services.model.crowdestor.LatePaymentResponse
import pt.tiagocarvalho.p2p.services.model.crowdestor.MyInvestments
import pt.tiagocarvalho.p2p.services.model.crowdestor.StatementPage
import pt.tiagocarvalho.p2p.services.model.crowdestor.TransactionType
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class CrowdestorCase : BaseCase() {

    private val crowdestorPageFetcher = CrowdestorPageFetcher()
    private val dashboardConverter = DashboardConverter()
    private val statementsConverter = StatementsConverter()
    private val investmentsConverter = InvestmentsConverter()

    fun getDetails(cookies: String): ThirdPartyDetails {
        this.cookiesGlobal = parseCookies(cookies)
        val dashboard = getDashboard()
        val myInvestments = getMyInvestments()
        val latePayments = getLatePayments()
        getTransactions()

        val lateTotal = myInvestments.filter { it.lateDays > 0 }.sumByBigDecimal { it.amount.toBigDecimal() }
        val currentTotal = myInvestments.filter { it.lateDays == 0 }.sumByBigDecimal { it.amount.toBigDecimal() }

        val currentNumber = latePayments.filter { it.name == "Current" }.count()
        val lateNumber = dashboard.activeInvestments - currentNumber

        val deposits =
            getStatements(
                null,
                null,
                TransactionType.DEPOSIT
            ).statements.sumByBigDecimal { it.amount }
        val withdrawals =
            getStatements(
                null,
                null,
                TransactionType.WITHDRAW
            ).statements.sumByBigDecimal { it.amount }
        val referral =
            getStatements(
                null,
                null,
                TransactionType.REFERRAL
            ).statements.sumByBigDecimal { it.amount }
        val bonus = getStatements(
            null,
            null,
            TransactionType.BONUS
        ).statements.sumByBigDecimal { it.amount }
        val cashback =
            getStatements(
                null,
                null,
                TransactionType.CASHBACK
            ).statements.sumByBigDecimal { it.amount }
        val interest =
            getStatements(
                null,
                null,
                TransactionType.INTEREST
            ).statements.sumByBigDecimal { it.amount }

        val totalInvestments = getInvestments()
        val dailyChange = getDailyChange()

        val profit = interest.add(cashback).add(referral)

        return ThirdPartyDetails(
            name = ThirdPartyName.CROWDESTOR,

            balance = dashboard.balance.setScale(2, RoundingMode.HALF_UP),
            available = dashboard.available.setScale(2, RoundingMode.HALF_UP),
            invested = dashboard.invested.setScale(2, RoundingMode.HALF_UP),

            netAnnualReturn = dashboard.netAnnualReturn.setScale(2, RoundingMode.HALF_UP),
            profit = profit.setScale(2, RoundingMode.HALF_UP),
            interests = interest.setScale(2, RoundingMode.HALF_UP),
            deposits = deposits.setScale(2, RoundingMode.HALF_UP),
            withdrawals = withdrawals.setScale(2, RoundingMode.HALF_UP),
            cashback = cashback.setScale(2, RoundingMode.HALF_UP),
            campaigns = referral.add(bonus).setScale(2, RoundingMode.HALF_UP),

            investments = totalInvestments,
            currentTotal = currentTotal,
            lateTotal = lateTotal,
            currentNumber = currentNumber,
            lateNumber = lateNumber,

            changePercentage = Utils.calculateChangePercentage(dailyChange, dashboard.balance),
            changeValue = dailyChange
        )
    }

    fun getStatements(date: Date, cookies: String): ThirdPartyStatementModel {
        this.cookiesGlobal = parseCookies(cookies)
        val dashboardResponse = crowdestorPageFetcher.getDashboard(cookiesGlobal)
        replaceCookies(dashboardResponse.cookies())
        getTransactions()

        val to = Utils.getTodayDateAsString("yyyy-MM-dd")
        return getStatements(date.toStringPattern("yyyy-MM-dd"), to, TransactionType.ALL)
    }

    private fun getDashboard(): Dashboard {
        val dashboardResponse = crowdestorPageFetcher.getDashboard(cookiesGlobal)
        replaceCookies(dashboardResponse.cookies())
        val element = dashboardResponse.parse()

        try {
            return dashboardConverter.convert(element)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, element?.toString(), e)
        }
    }

    private fun getMyInvestments(): List<MyInvestments> {
        val myInvestmentsResponse = crowdestorPageFetcher.getMyInvestmentsAjax(cookiesGlobal)
        replaceCookies(myInvestmentsResponse.cookies())
        val body = myInvestmentsResponse.body()
        val statementObject = getGson().fromJson<List<List<MyInvestments>>>(body, object : TypeToken<List<List<MyInvestments>>>() {}.type)
        try {
            return statementObject.flatten()
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body?.toString(), e)
        }
    }

    private fun getLatePayments(): List<LatePayment> {
        val myInvestmentsResponse = crowdestorPageFetcher.getLatePayments(cookiesGlobal)
        replaceCookies(myInvestmentsResponse.cookies())
        val body = myInvestmentsResponse.body()
        val statementObject = getGson().fromJson(body, LatePaymentResponse::class.java)
        try {
            return statementObject.data
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body?.toString(), e)
        }
    }

    private fun getDailyChange(): BigDecimal {
        val today = Utils.getTodayDateAsString("yyyy-MM-dd")

        getTransactions()

        return getStatements(today, today, TransactionType.ALL)
            .statements
            .filter { it.type != ThirdPartyStatementType.INVESTMENT }
            .sumByBigDecimal { it.amount }
    }

    private fun getTransactions() {
        val response = crowdestorPageFetcher.getTransactions(cookiesGlobal)
        replaceCookies(response.cookies())
    }

    private fun getStatements(
        from: String?,
        to: String?,
        type: TransactionType
    ): ThirdPartyStatementModel {
        var counter = 1
        var start = 0
        var isLastPage: Boolean
        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()

        do {

            val parameters = LinkedHashMap(emptyMap<String, String?>())
            from?.let {
                parameters.put("filter_date_from", it)
            }
            to?.let {
                parameters.put("filter_date_to", it)
            }
            parameters["filter_type"] = type.search
            parameters["_"] = System.currentTimeMillis().toString()
            parameters["draw"] = counter.toString()
            parameters["length"] = "50"
            parameters["start"] = start.toString()

            val query = createQueryUrl(parameters)
            val response = crowdestorPageFetcher.getStatements(query, cookiesGlobal)
            replaceCookies(response.cookies())
            val body = response.body()

            val statementObject = getGson().fromJson(body, StatementPage::class.java)

            try {
                val content = statementsConverter.convert(statementObject?.data ?: emptyList())
                statements.addAll(content.statements)
                issues.addAll(content.issues)

                isLastPage = (counter * 50 >= statementObject.recordsTotal)
                counter++
                start++
            } catch (e: Exception) {
                throw ThirdPartyException(e.message, body?.toString(), e)
            }
        } while (isLastPage.not())

        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getInvestments(): Int {
        val investmentsResponse = crowdestorPageFetcher.getInvestments(cookiesGlobal)
        replaceCookies(investmentsResponse.cookies())

        val currentInvestmentsResponse = crowdestorPageFetcher.getInvestmentsItems(cookiesGlobal)
        replaceCookies(currentInvestmentsResponse.cookies())
        val body = currentInvestmentsResponse.body()

        val investmentPage = getGson().fromJson(body, InvestmentPage::class.java)

        try {
            return investmentsConverter.convert(investmentPage)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body?.toString(), e)
        }
    }

    private fun createQueryUrl(parameters: Map<String, String?>): String {
        return QUERY_STRING + parameters.map { (k, v) -> v?.let { "$k=$v" } ?: "$k=" }.joinToString(
            "&"
        )
    }

    companion object {
        private const val QUERY_STRING =
            "?columns[0][data]=0&columns[0][name]=&columns[0][searchable]=true&columns[0][orderable]=true&columns[0][search][value]=&columns[0][search][regex]=false&columns[1][data]=1&columns[1][name]=&columns[1][searchable]=true&columns[1][orderable]=true&columns[1][search][value]=&columns[1][search][regex]=false&columns[2][data]=2&columns[2][name]=&columns[2][searchable]=true&columns[2][orderable]=true&columns[2][search][value]=&columns[2][search][regex]=false&order[0][column]=0&order[0][dir]=desc&search[value]=&search[regex]=false&"
    }
}
