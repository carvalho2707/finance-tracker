package pt.tiagocarvalho.p2p.services.model.bondora

data class ReportResponse(
    val reportId: String
)
