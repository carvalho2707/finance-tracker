package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName

data class MyInvestmentsCount(
    @SerializedName("totalRecords") val totalRecords: Int
)
