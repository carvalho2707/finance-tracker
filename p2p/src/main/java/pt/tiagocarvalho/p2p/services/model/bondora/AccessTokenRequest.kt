package pt.tiagocarvalho.p2p.services.model.bondora

import com.google.gson.annotations.SerializedName

data class AccessTokenRequest(
    @SerializedName("grant_type")
    val grantType: String,
    @SerializedName("client_id")
    val clientId: String,
    @SerializedName("client_secret")
    val clientSecret: String,
    @SerializedName("code")
    val code: String,
    @SerializedName("redirect_uri")
    val redirectUri: String,
    @SerializedName("refresh_token")
    val refreshToken: String?,
    @SerializedName("non_expiring")
    val nonExpiring: Boolean
)
