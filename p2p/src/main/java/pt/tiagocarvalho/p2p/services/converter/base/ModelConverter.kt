package pt.tiagocarvalho.p2p.services.converter.base

internal abstract class ModelConverter<I, O> {

    abstract fun convert(input: I): O
}
