package pt.tiagocarvalho.p2p.services.cases

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Date
import java.util.HashMap
import org.jsoup.nodes.Element
import org.jsoup.nodes.TextNode
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.grupeer.GrupeerStatementsPageConverter
import pt.tiagocarvalho.p2p.services.fetcher.GrupeerPageFetcher
import pt.tiagocarvalho.p2p.services.model.grupeer.Overview
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromInterestToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class GrupeerCase : BaseCase() {

    companion object {
        // Debuggable field to check instance count
        private val mInstance = GrupeerCase()

        @Synchronized
        fun getInstance() = mInstance
    }

    private val grupeerPageFetcher = GrupeerPageFetcher()
    private val grupeerStatementsPageConverter = GrupeerStatementsPageConverter()

    fun getDetails(cookies: String): ThirdPartyDetails {
        this.cookiesGlobal = parseCookies(cookies)
        val details = getMainPageInternal()
        val netReturn = getNetReturn()
        val dailyChange = getDailyChange()
        val depositsWithdrawals = getDepositsWithdrawals()
        val activeInvestments = getActiveInvestments()

        details.netAnnualReturn = netReturn.setScale(2, RoundingMode.HALF_UP)
        details.investments = activeInvestments
        details.deposits = depositsWithdrawals.first.setScale(2, RoundingMode.HALF_UP)
        details.withdrawals = depositsWithdrawals.second.setScale(2, RoundingMode.HALF_UP)
        details.changePercentage = Utils.calculateChangePercentage(dailyChange, details.balance)
        details.changeValue = dailyChange

        return details
    }

    fun getStatements(cookies: String, from: Date): ThirdPartyStatementModel {
        this.cookiesGlobal = parseCookies(cookies)
        return getStatementsInternal(from)
    }

    private fun getStatementsInternal(from: Date): ThirdPartyStatementModel {
        val requestParameters = HashMap<String, String>()
        requestParameters["from"] = from.toStringPattern("dd-MM-yyyy")
        requestParameters["to"] = getCurrentDateTimeString("dd-MM-yyyy")
        requestParameters["submit"] = "Filter"

        val accountStatementResponse =
            grupeerPageFetcher.getAccountStatementsPageWithParams(
                cookiesGlobal,
                requestParameters
            )

        replaceCookies(accountStatementResponse.cookies())

        val body = accountStatementResponse.parse().body()
        val counter: Int
        try {
            counter = getPagesFromElement(body)
        } catch (e: Exception) {
            val log = body?.toString()
            throw ThirdPartyException(e.message, log, e)
        }
        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()

        for (i in 1..counter) {
            requestParameters["page"] = i.toString()
            val pageResponse = grupeerPageFetcher.getAccountStatementsPageWithParams(
                cookiesGlobal,
                requestParameters
            )
            replaceCookies(pageResponse.cookies())
            try {
                val thirdPartyStatementModel =
                    grupeerStatementsPageConverter.convert(pageResponse.parse().body())
                statements.addAll(thirdPartyStatementModel.statements)
                issues.addAll(thirdPartyStatementModel.issues)
            } catch (e: Exception) {
                val log = body?.toString()
                throw ThirdPartyException(e.message, log, e)
            }
        }

        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getDepositsWithdrawals(): Pair<BigDecimal, BigDecimal> {
        var deposits = BigDecimal.ZERO
        var withdrawals = BigDecimal.ZERO
        val firstDate = Utils.convertStringToDate("01-01-1950", "dd-MM-yyyy")
        getStatementsInternal(firstDate).statements.forEach {
            if (it.type == ThirdPartyStatementType.DEPOSIT) {
                deposits = deposits.add(it.amount)
            } else if (it.type == ThirdPartyStatementType.WITHDRAWAL) {
                withdrawals = withdrawals.add(it.amount)
            }
        }
        return Pair(deposits, withdrawals)
    }

    private fun getMainPage(): ThirdPartyDetails {
        val overviewResponse = grupeerPageFetcher.getOverviewPage(cookiesGlobal).body()
        try {
            return getGson().fromJson(overviewResponse, Overview::class.java)!!.run {
                ThirdPartyDetails(
                    name = ThirdPartyName.GRUPEER,
                    balance = balanceTotal.setScale(2, RoundingMode.HALF_UP),
                    available = currentBalance.setScale(2, RoundingMode.HALF_UP),
                    invested = dataInvestedFunds.setScale(2, RoundingMode.HALF_UP),
                    interests = incomeInterest.setScale(2, RoundingMode.HALF_UP),
                    cashback = incomeCashBack.setScale(2, RoundingMode.HALF_UP),
                    profit = incomeInterest.add(incomeCashBack).add(incomeBonus).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ),
                    campaigns = incomeBonus.add(incomeReferral).setScale(2, RoundingMode.HALF_UP)
                )
            }
        } catch (e: Exception) {
            val log = overviewResponse?.toString()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getDailyChange(): BigDecimal {
        val currentDate = getCurrentDateTimeString("dd-MM-yyyy")
        val requestParameters = HashMap<String, String>()
        requestParameters["from"] = currentDate
        requestParameters["to"] = currentDate
        requestParameters["submit"] = "Filter"

        val accountStatementResponse =
            grupeerPageFetcher.getAccountStatementsPageWithParams(
                cookiesGlobal,
                requestParameters
            )
        val body = accountStatementResponse.parse().body()
        try {
            val statements =
                grupeerStatementsPageConverter.convert(body)
            return statements.statements.filter { isBalanceRelated(it) }
                .map {
                    if (it.type == ThirdPartyStatementType.WITHDRAWAL || it.type == ThirdPartyStatementType.FEE) {
                        it.amount.negate()
                    } else {
                        it.amount
                    }
                }.sumByBigDecimal { it }
        } catch (e: Exception) {
            val log = body?.toString()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getActiveInvestments(): Int {
        val myInvestmentsResponse = grupeerPageFetcher.getMyInvestmentsPage(cookiesGlobal)
        replaceCookies(myInvestmentsResponse.cookies())
        val body = myInvestmentsResponse.parse().body()
        try {
            return getActiveInvestmentsAux(
                body,
                (cookiesGlobal as MutableMap<String, String>?)!!
            )
        } catch (e: Exception) {
            val log = body?.toString()
            throw ThirdPartyException(e.message, log, e)
        }
    }

    private fun getActiveInvestmentsAux(body: Element, cookies: MutableMap<String, String>): Int {
        val div = body.getElementsByClass("d-flex justify-content-center").first()

        if (div == null || div.childNodeSize() == 1) {
            return 0
        }

        val pages = (div.childNode(1) as Element).getElementsByTag("li")
        val count = pages.size - 2
        val lastPage = pages[3].childNode(0).attr("href")

        return if (count == 1) {
            val lastPageResponse =
                grupeerPageFetcher.getNumberOfInvestmentsInLastPage(lastPage, cookies)
            if (lastPageResponse != null) {
                getMyInvestmentsCount(lastPageResponse.parse().body())
            } else {
                0
            }
        } else {
            val lastPageResponse =
                grupeerPageFetcher.getNumberOfInvestmentsInLastPage(lastPage, cookies)
            if (lastPageResponse != null) {
                val lastPageCount =
                    getMyInvestmentsCount(lastPageResponse.parse().body())
                (count - 1) * 10 + lastPageCount
            } else {
                0
            }
        }
    }

    private fun getNetReturn(): BigDecimal {
        val welcomePage = grupeerPageFetcher.getWhyGrupeer()
        replaceCookies(welcomePage.cookies())
        val body = welcomePage.parse().body()
        return getNetReturnFromElement(body)
    }

    private fun getNetReturnFromElement(element: Element): BigDecimal {
        return (
            element.getElementsByClass("highest-yields-info-percentage").first()
                .childNode(0) as TextNode
            ).text()
            .convertFromInterestToBigDecimal()
    }

    private fun getPagesFromElement(element: Element): Int {
        val div = element.getElementsByClass("d-flex justify-content-center").first()

        if (div == null || div.childNodeSize() == 1) {
            return 2
        }

        val elements = (div.childNode(1) as Element).getElementsByTag("li")
        val pages = elements[elements.size - 2].childNode(0).childNode(0).toString()
        return pages.toInt()
    }

    private fun getMyInvestmentsCount(element: Element): Int {
        return element.getElementsByClass("flex-container").size - 1
    }

    private fun getMainPageInternal(): ThirdPartyDetails {
        return try {
            getMainPage()
        } catch (e: Exception) {
            try {
                getMainPage()
            } catch (ex: Exception) {
                throw ex
            }
        }
    }
}
