package pt.tiagocarvalho.p2p.services.retrofit

import pt.tiagocarvalho.p2p.services.model.bondora.AccessToken
import pt.tiagocarvalho.p2p.services.model.bondora.AccessTokenRequest
import pt.tiagocarvalho.p2p.services.model.bondora.BondoraResponse
import pt.tiagocarvalho.p2p.services.model.bondora.Investment
import pt.tiagocarvalho.p2p.services.model.bondora.MyAccountBalance
import pt.tiagocarvalho.p2p.services.model.bondora.Report
import pt.tiagocarvalho.p2p.services.model.bondora.ReportCreateRequest
import pt.tiagocarvalho.p2p.services.model.bondora.ReportItem
import pt.tiagocarvalho.p2p.services.model.bondora.ReportResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface BondoraApi {

    @GET("api/v1/account/balance")
    fun getBalance(@Header("Authorization") authorization: String): Call<BondoraResponse<MyAccountBalance>>

    @GET("api/v1/account/investments")
    fun getInvestments(
        @Header("Authorization") authorization: String,
        @Query("PageNr") pageNr: Int
    ): Call<BondoraResponse<List<Investment>>>

    @POST("api/v1/report")
    fun createReport(
        @Header("Authorization") authorization: String,
        @Body reportCreateRequest: ReportCreateRequest
    ): Call<BondoraResponse<ReportResponse>>

    @GET("api/v1/report/{reportId}")
    fun getReport(
        @Header("Authorization") authorization: String,
        @Path("reportId") reportId: String
    ): Call<BondoraResponse<Report>>

    @GET("api/v1/reports")
    fun getAllReports(@Header("Authorization") authorization: String): Call<BondoraResponse<List<ReportItem>>>

    @POST("oauth/access_token")
    fun getAccessToken(@Body accessTokenRequest: AccessTokenRequest): Call<AccessToken>
}
