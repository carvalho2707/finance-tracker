package pt.tiagocarvalho.p2p.services.model.crowdestor

import java.math.BigDecimal

data class Dashboard(
    val balance: BigDecimal,
    val available: BigDecimal,
    val invested: BigDecimal,
    val investedByNow: BigDecimal,
    val receivedByNow: BigDecimal,
    val activeInvestments: Int,
    val netAnnualReturn: BigDecimal
)

data class MyInvestments(
    val amount: String,
    val lateDays: Int
)

data class LatePaymentResponse(val data: List<LatePayment>)

data class LatePayment(
    val name: String,
    val quantity: Int
)
