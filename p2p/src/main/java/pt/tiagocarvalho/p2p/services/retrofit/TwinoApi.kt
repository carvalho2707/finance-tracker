package pt.tiagocarvalho.p2p.services.retrofit

import io.reactivex.Single
import pt.tiagocarvalho.p2p.services.model.twino.LoginResponse
import pt.tiagocarvalho.p2p.services.model.twino.TwinoLoginTfa
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface TwinoApi {

    @GET("/ws/public/check2fa")
    fun checkTfa(@Query("email") email: String): Single<Boolean>

    @POST("/ws/public/login2fa")
    fun loginTfa(@Body twinoLoginTfa: TwinoLoginTfa): Single<Response<LoginResponse>>
}
