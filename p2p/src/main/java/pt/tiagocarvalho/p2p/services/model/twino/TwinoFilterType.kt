package pt.tiagocarvalho.p2p.services.model.twino

enum class TwinoFilterType(val hasPositive: Boolean) {
    REPAYMENT(false),
    EARLY_FULL_REPAYMENT(false),
    EARLY_PARTIAL_REPAYMENT(false),
    BUY_SHARES(false),
    FUNDING(true),
    EXTENSION(false),
    ACCRUED_INTEREST(false),
    BUYBACK(false),
    SCHEDULE(false),
    RECOVERY(false),
    REPURCHASE(false),
    LOSS_ON_WRITEOFF(false),
    WRITEOFF(false),
    CURRENCY_FLUCTUATION(false),
    CASHBACK(false),
    CORRECTION(false),
    BUY_OUT(false);

    companion object {

        fun valuesForChange(): Array<TwinoFilterType> {
            return arrayOf(
                FUNDING,
                ACCRUED_INTEREST,
                LOSS_ON_WRITEOFF,
                WRITEOFF,
                CASHBACK,
                CORRECTION
            )
        }
    }
}
