package pt.tiagocarvalho.p2p.services.converter.lendermarket

import java.util.Locale
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text
import pt.tiagocarvalho.p2p.services.utils.toDate

class StatementsConverter {

    fun convert(body: Element): ThirdPartyStatementModel {
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        val tableBody = body.getElementsByTag("tbody")
        if (tableBody.size > 0) {
            tableBody[1].childNodes()
                .forEachIndexed { index, element ->
                    if (index % 2 == 0) {
                        return@forEachIndexed
                    }
                    if (element.childNodeSize() == 1) {
                        return@forEachIndexed
                    }
                    val date = element.childNode(0).childNode(0).text().toDate("dd.MM.yyyy")
                    val loanId =
                        if (element.childNode(1).childNodeSize() > 0) {
                            element.childNode(1).childNode(0).childNode(0).text()
                        } else {
                            ""
                        }
                    val label = element.childNode(2).childNode(0).text()
                    val total = element.childNode(3).childNode(0).childNode(0).childNode(0).text()
                        .convertFromMoneyToBigDecimal(Locale.UK)
                    val balance = element.childNode(4).childNode(0).childNode(0).text()
                        .convertFromMoneyToBigDecimal(Locale.UK)
                    val type = getTypeFromLabel(label)

                    if (type == ThirdPartyStatementType.INVALID) {
                        issues.add(element.toString())
                    } else {
                        list.add(
                            ThirdPartyStatement(
                                "$date$loanId$label$total$balance",
                                total.abs(),
                                date,
                                type,
                                ThirdPartyName.LENDERMARKET
                            )
                        )
                    }
                }
        }

        return ThirdPartyStatementModel(list, issues)
    }

    private fun getTypeFromLabel(label: String): ThirdPartyStatementType {
        return when (label) {
            "Deposit" -> ThirdPartyStatementType.DEPOSIT
            "Investment" -> ThirdPartyStatementType.INVESTMENT
            "Principal received" -> ThirdPartyStatementType.PRINCIPAL
            "Interest received" -> ThirdPartyStatementType.INTEREST
            "Delayed interest received" -> ThirdPartyStatementType.INTEREST
            "Withdrawal" -> ThirdPartyStatementType.WITHDRAWAL
            "Campaign rewards and Bonuses" -> ThirdPartyStatementType.BONUS
            "Campaign rewards and Bonuses (Extension)" -> ThirdPartyStatementType.BONUS
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
