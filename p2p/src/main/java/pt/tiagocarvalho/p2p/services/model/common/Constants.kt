package pt.tiagocarvalho.p2p.services.model.common

internal object Constants {

    const val USER_AGENT_VALUE =
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36"
    const val CONTENT_TYPE_KEY = "Content-Type"
    const val CONTENT_TYPE_FORM_URL_UTF = "application/x-www-form-urlencoded;charset=UTF-8"
    const val CONTENT_TYPE_FORM_URL = "application/x-www-form-urlencoded"
    const val CONTENT_TYPE_JSON_UTF = "application/json;charset=UTF-8"
    const val CONTENT_TYPE_JSON = "application/json"
    const val ACCEPTED_LANGUAGE_KEY = "Accept-Language"
    const val ACCEPTED_LANGUAGE_VALUE = "en-GB,en,en-US"
    const val ORIGIN_KEY = "Origin"
    const val REFERER_KEY = "Referer"
}
