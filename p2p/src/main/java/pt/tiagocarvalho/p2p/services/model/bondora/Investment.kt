package pt.tiagocarvalho.p2p.services.model.bondora

import java.math.BigDecimal

class Investment(
    val amount: BigDecimal,
    val principalRemaining: BigDecimal,
    val loanStatusCode: Int
)
