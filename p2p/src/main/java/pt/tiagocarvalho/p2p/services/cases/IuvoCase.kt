package pt.tiagocarvalho.p2p.services.cases

import java.math.BigDecimal
import java.util.Calendar
import java.util.Date
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.AuthenticationFailedException
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.iuvo.DetailsConverter
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher
import pt.tiagocarvalho.p2p.services.model.iuvo.AccountBalance
import pt.tiagocarvalho.p2p.services.model.iuvo.LoginResponse
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.getFirstDayOfMonth
import pt.tiagocarvalho.p2p.services.utils.getLastDayOfMonth
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

internal class IuvoCase : BaseCase() {

    private val iuvoPageFetcher = IuvoPageFetcher()
    private val converter = DetailsConverter()

    private lateinit var sessionToken: String
    private var investorId: Int = 0

    fun login(login: Login) {

        val loginResponse = iuvoPageFetcher.login(login.username, login.password)
        val body = loginResponse.body()
        try {
            val result = getGson().fromJson(loginResponse.body(), LoginResponse::class.java)
            if (result?.status?.status == "ui_error" || result?.result == null) {
                throw AuthenticationFailedException(body)
            }

            sessionToken = result.result.session_token
            investorId = result.result.investor_id
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    fun getDetails(login: Login): ThirdPartyDetails {
        login(login)

        val accountBalanceResponse =
            iuvoPageFetcher.getAccountBalance(sessionToken)
        val accountBalance =
            getGson().fromJson(accountBalanceResponse.body(), AccountBalance::class.java)

        val overviewResponse = iuvoPageFetcher.getOverview(sessionToken).body()
        val overview: Map<String, BigDecimal>
        try {
            overview = parseOverview(overviewResponse)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, overviewResponse, e)
        }

        val mainStatements = iuvoPageFetcher.getPreAccountStatements(sessionToken).body()
        val accountId: Int
        try {
            accountId = parseAccountId(mainStatements)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, mainStatements, e)
        }

        val allTimeStatements = iuvoPageFetcher.getAccountStatements(
            sessionToken,
            accountId,
            "2006-01-01",
            Utils.getTodayDateAsString("yyyy-MM-dd")
        ).body()
        val allTime: Map<String, BigDecimal>
        try {
            allTime = parseAccountStatement(allTimeStatements)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, allTimeStatements, e)
        }

        val todayStatements = iuvoPageFetcher.getAccountStatements(
            sessionToken,
            accountId,
            Utils.getTodayDateAsString("yyyy-MM-dd"),
            Utils.getTodayDateAsString("yyyy-MM-dd")
        ).body()
        val dailyChange: BigDecimal
        try {
            val today = parseAccountStatement(todayStatements)
            dailyChange = getDailyChange(today)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, todayStatements, e)
        }

        try {
            return converter.convert(
                accountBalance?.result?.balances,
                overview,
                allTime,
                dailyChange
            )
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, e)
        }
    }

    fun getStatements(startDate: Date, login: Login): ThirdPartyStatementModel {
        login(login)

        val mainStatements = iuvoPageFetcher.getPreAccountStatements(sessionToken).body()
        val accountId: Int
        try {
            accountId = parseAccountId(mainStatements)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, mainStatements, e)
        }

        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()
        val currentMonth = Utils.getTodayMonth() + 1
        val currentYear = Utils.getTodayYear()
        val cal = Calendar.getInstance()
        cal.time = startDate

        do {
            val from = cal.getFirstDayOfMonth()
            val to = cal.getLastDayOfMonth()

            val todayStatements = iuvoPageFetcher.getAccountStatements(
                sessionToken,
                accountId,
                from.toStringPattern("yyyy-MM-dd"),
                to.toStringPattern("yyyy-MM-dd")
            ).body()

            println("from: $from - to: $to")
            parseAccountStatement(todayStatements)
                .entries.forEach {

                    when (val statementType = convertTypeToStatementType(it.key)) {
                        ThirdPartyStatementType.INVALID -> issues.add(it.key)
                        ThirdPartyStatementType.IGNORE -> {
                            // ignore
                        }
                        else -> {
                            val id = cal.time.toStringPattern("MM-yyyy") + "-" + it.key
                            statements.add(
                                ThirdPartyStatement(
                                    id,
                                    it.value.abs(),
                                    cal.time,
                                    statementType,
                                    ThirdPartyName.IUVO
                                )
                            )
                        }
                    }
                }

            val sameMonthOfYear =
                cal.get(Calendar.MONTH) + 1 == currentMonth && cal.get(Calendar.YEAR) == currentYear
            cal.add(Calendar.MONTH, 1)
        } while (sameMonthOfYear.not())

        return ThirdPartyStatementModel(statements, issues)
    }

    private fun parseAccountStatement(statement: String?): Map<String, BigDecimal> {
        checkNotNull(statement) { "statement is null" }
        val result = emptyMap<String, BigDecimal>().toMutableMap()
        val response = Jsoup.parse(statement)
        val table = response.getElementsByClass("table table-bordered p2p-table")
        if (table.isNullOrEmpty()) {
            return result
        }
        (table[0].childNode(1) as Element).children()
            .forEach {
                if (it.getElementsByClass("p2p-lang-link").size > 0) {
                    val type =
                        it.childNode(1).childNode(0).attr("href").substringAfter("trans_type_page=")
                            .substringBefore("&")
                    val amount = it.childNode(3).childNode(0).childNode(0).text().toBigDecimal()
                    result[type] = amount
                }
            }

        return result
    }

    private fun parseOverview(overview: String?): Map<String, BigDecimal> {
        checkNotNull(overview) { "overview is null" }
        val result = emptyMap<String, BigDecimal>().toMutableMap()
        val response = Jsoup.parse(overview)
        val entries = response.getElementsByClass("col-md-4")

        if (entries.size > 0) {
            val balance = entries[0].childNode(3).childNode(0).text().removeSuffix(" EUR")

            val entryFirst = entries[0].childNode(5).childNode(1)
            val currency = entryFirst.childNode(1).childNode(3).childNode(0).text()
            val available =
                entryFirst.childNode(3).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val invested =
                entryFirst.childNode(5).childNode(3).childNode(0).text().removeSuffix(" EUR")

            val netAnnualReturn = entries[1].childNode(3).childNode(0).text().removeSuffix(" %")

            val entrySecond = entries[1].childNode(7).childNode(1)
            val principalAmount =
                entrySecond.childNode(3).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val interest =
                entrySecond.childNode(5).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val latePaymentFees =
                entrySecond.childNode(7).childNode(3).childNode(0).text().removeSuffix(" EUR")

            val myInvestments = entries[2].childNode(3).childNode(0).text()

            val entryThird = entries[2].childNode(5).childNode(1)
            val current =
                entryThird.childNode(1).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val threeFifteen =
                entryThird.childNode(3).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val fifteenSixteen =
                entryThird.childNode(5).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val sixteenThirty =
                entryThird.childNode(7).childNode(3).childNode(0).text().removeSuffix(" EUR")
            val moreThanSixty =
                entryThird.childNode(9).childNode(3).childNode(0).text().removeSuffix(" EUR")

            result["balance"] = balance.toBigDecimal()
            result["available"] = available.toBigDecimal()
            result["invested"] = invested.toBigDecimal()
            result["netAnnualReturn"] = netAnnualReturn.toBigDecimal()
            result["cumulativePayments"] = principalAmount.toBigDecimal()
            result["interest"] = interest.toBigDecimal()
            result["latePaymentFees"] = latePaymentFees.toBigDecimal()
            result["myInvestments"] = myInvestments.toBigDecimal()
            result["current"] = current.toBigDecimal()
            result["threeFifteen"] = threeFifteen.toBigDecimal()
            result["fifteenSixteen"] = fifteenSixteen.toBigDecimal()
            result["sixteenThirty"] = sixteenThirty.toBigDecimal()
            result["moreThanSixty"] = moreThanSixty.toBigDecimal()
        }

        return result
    }

    private fun parseAccountId(body: String?): Int {
        checkNotNull(body) { "body is null" }
        val response = Jsoup.parse(body)
        val element = response.getElementById("investor_account_id")
        return if (element.tagName() == "input") {
            // single currency
            response.select("input[id=investor_account_id]").first().attr("value").toInt()
        } else {
            // multi currency
            response.select("select[id=investor_account_id]").first()
                .getElementsByTag("option")
                .firstOrNull {
                    it.childNode(0).text() == "EUR (€)"
                }
                ?.attr("value")?.toInt() ?: 0
        }
    }

    private fun getDailyChange(today: Map<String, BigDecimal>): BigDecimal {
        return today
            .filterKeys { balanceRelated.contains(it) }
            .map { it.value }
            .sumByBigDecimal { it }
    }

    private fun convertTypeToStatementType(type: String): ThirdPartyStatementType {
        return when (type) {
            "bonus" -> ThirdPartyStatementType.REFERRAL
            "payment_interest_buyback" -> ThirdPartyStatementType.INTEREST
            "payment_principal_buyback" -> ThirdPartyStatementType.PRINCIPAL
            "deposit" -> ThirdPartyStatementType.DEPOSIT
            "payment_interest_early" -> ThirdPartyStatementType.INTEREST
            "payment_principal_early" -> ThirdPartyStatementType.PRINCIPAL
            "payment_interest" -> ThirdPartyStatementType.INTEREST
            "primary_market_auto_invest" -> ThirdPartyStatementType.INVESTMENT
            "primary_market_invest" -> ThirdPartyStatementType.INVESTMENT
            "payment_principal" -> ThirdPartyStatementType.PRINCIPAL
            "late_fee" -> ThirdPartyStatementType.INTEREST
            "outgoing_currency_exchange" -> ThirdPartyStatementType.CURRENCY_EXCHANGE_OUT
            "incoming_currency_exchange" -> ThirdPartyStatementType.CURRENCY_EXCHANGE_IN
            "secondary_market_sell_brokerage_fee" -> ThirdPartyStatementType.FEE
            "secondary_market_invest" -> ThirdPartyStatementType.INVESTMENT
            "secondary_market_buy_discount" -> ThirdPartyStatementType.BONUS
            "secondary_market_sale" -> ThirdPartyStatementType.PRINCIPAL
            "secondary_market_sale_discount" -> ThirdPartyStatementType.BONUS
            "withdraw" -> ThirdPartyStatementType.WITHDRAWAL
            "withdraw_cancelled", "withdraw_rejected", "withdraw_request" -> ThirdPartyStatementType.IGNORE
            else -> ThirdPartyStatementType.INVALID
        }
    }

    companion object {

        private val balanceRelated = listOf(
            "bonus",
            "payment_interest_buyback",
            "deposit",
            "payment_interest_early",
            "payment_interest",
            "late_fee",
            "outgoing_currency_exchange",
            "secondary_market_sell_brokerage_fee",
            "secondary_market_invest",
            "secondary_market_buy_discount",
            "secondary_market_sale_discount",
            "withdraw"
        )
    }
}
