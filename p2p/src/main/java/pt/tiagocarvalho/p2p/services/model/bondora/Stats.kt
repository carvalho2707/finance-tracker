package pt.tiagocarvalho.p2p.services.model.bondora

import com.google.gson.annotations.SerializedName

data class Stats(
    @SerializedName("Title") val title: String?,
    @SerializedName("Tooltip") val tooltip: String?,
    @SerializedName("Value") val value: String?,
    @SerializedName("ValueTooltip") val valueTooltip: String?,
    @SerializedName("SmallValue") val smallValue: String?,
    @SerializedName("CustomButton") val customButton: String?,
    @SerializedName("SmallTooltip") val smallTooltip: String?
)
