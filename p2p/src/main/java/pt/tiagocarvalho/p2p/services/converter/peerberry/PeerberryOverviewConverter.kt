package pt.tiagocarvalho.p2p.services.converter.peerberry

import java.math.RoundingMode
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.peerberry.OverviewResponse
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class PeerberryOverviewConverter : ModelConverter<OverviewResponse, ThirdPartyDetails>() {

    override fun convert(input: OverviewResponse): ThirdPartyDetails {
        val investmentsTotal = input.investments.total.convertFromMoneyToBigDecimal()
        val investmentsCurrent = input.investments.current.convertFromMoneyToBigDecimal()
        val late = investmentsTotal.subtract(investmentsCurrent)

        return ThirdPartyDetails(
            name = ThirdPartyName.PEERBERRY,

            available = input.availableMoney.convertFromMoneyToBigDecimal(scale = true),
            balance = input.totalBalance.convertFromMoneyToBigDecimal(scale = true),
            invested = input.invested.convertFromMoneyToBigDecimal(scale = true),

            netAnnualReturn = input.netAnnualReturn.convertFromMoneyToBigDecimal(scale = true),
            interests = input.totalProfit.convertFromMoneyToBigDecimal(scale = true),
            profit = input.totalProfit.convertFromMoneyToBigDecimal(scale = true),

            investments = input.totalActiveInvestments.toInt(),

            currentTotal = input.investments.current.convertFromMoneyToBigDecimal(scale = true),
            oneToFifteenLateTotal = input.investments.late15.convertFromMoneyToBigDecimal(scale = true),
            sixteenToThirtyLateTotal = input.investments.late30.convertFromMoneyToBigDecimal(scale = true),
            thirtyToSixtyLateTotal = input.investments.late60.convertFromMoneyToBigDecimal(scale = true),
            lateTotal = late.setScale(2, RoundingMode.HALF_UP)
        )
    }
}
