package pt.tiagocarvalho.p2p.services.retrofit

import io.reactivex.Single
import pt.tiagocarvalho.p2p.services.model.common.Constants
import pt.tiagocarvalho.p2p.services.model.raize.PostTokenResponse
import pt.tiagocarvalho.p2p.services.model.raize.RaizeCurrent
import pt.tiagocarvalho.p2p.services.model.raize.RaizeGenericList
import pt.tiagocarvalho.p2p.services.model.raize.RaizeTransaction
import pt.tiagocarvalho.p2p.services.model.raize.SearchFilter
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST

interface RaizeApi {

    @Headers("User-Agent: ${Constants.USER_AGENT_VALUE}")
    @FormUrlEncoded
    @POST("/oauth2/token")
    fun postToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("trust") trust: Boolean?,
        @Field("totp") totp: String?,
        @Field("device") device: String?
    ): Single<PostTokenResponse>

    @Headers("User-Agent: ${Constants.USER_AGENT_VALUE}")
    @GET("/overview/current")
    fun getCurrent(@Header("Authorization") authorization: String): Call<RaizeCurrent>

    @Headers("User-Agent: ${Constants.USER_AGENT_VALUE}")
    @POST("/transactions/search")
    fun getTransactions(
        @Header("Authorization") authorization: String,
        @Body searchFilter: SearchFilter
    ): Call<RaizeGenericList<RaizeTransaction>>
}
