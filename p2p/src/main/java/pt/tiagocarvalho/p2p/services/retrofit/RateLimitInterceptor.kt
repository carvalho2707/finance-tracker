package pt.tiagocarvalho.p2p.services.retrofit

import java.util.concurrent.TimeUnit
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Response

class RateLimitInterceptor : Interceptor {

    override fun intercept(chain: Chain): Response {
        val request = chain.request()
        println("delaying " + request.method + " " + request.url)
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(2))
        } catch (e: InterruptedException) {
            return chain.proceed(request)
        }
        println("resumed " + request.method + " " + request.url)

        return chain.proceed(request)
    }
}
