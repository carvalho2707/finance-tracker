package pt.tiagocarvalho.p2p.services.model.bondora

import java.util.Date

data class Report(
    val result: List<AccountStatementReportLine>,
    val reportId: String,

    val createdOn: Date,
    val generatedOn: Date,
    val periodStart: Date,
    val periodEnd: Date,
    val reportType: Int
)
