package pt.tiagocarvalho.p2p.services.cases

import com.google.gson.Gson
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.HashMap
import java.util.Locale
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType

internal abstract class BaseCase {

    private val gson = Gson()
    protected var cookiesGlobal: MutableMap<String, String> = HashMap()

    protected fun getCurrentDateTimeString(pattern: String): String {
        val time = Calendar.getInstance().time
        val formatter = SimpleDateFormat(pattern, Locale.getDefault())
        return formatter.format(time)
    }

    protected fun getGson() = gson

    protected fun isBalanceRelated(statement: ThirdPartyStatement): Boolean {
        return statement.type == ThirdPartyStatementType.DEPOSIT ||
            statement.type == ThirdPartyStatementType.INTEREST ||
            statement.type == ThirdPartyStatementType.FEE ||
            statement.type == ThirdPartyStatementType.WITHDRAWAL ||
            statement.type == ThirdPartyStatementType.BONUS ||
            statement.type == ThirdPartyStatementType.REFERRAL
    }

    protected fun replaceCookies(cookies: Map<String, String>) {
        cookies.forEach { entry -> cookiesGlobal[entry.key] = entry.value }
    }

    protected fun parseCookies(cookies: String): MutableMap<String, String> {
        try {
            return cookies.split(";").associate {
                val (left, right) = it.split("=")
                left to right
            }.toMutableMap()
        } catch (e: IndexOutOfBoundsException) {
            print(cookies)
            throw e
        }
    }
}
