package pt.tiagocarvalho.p2p.services.converter.raize

import java.math.BigDecimal
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.raize.RaizeCurrent

internal class RaizeOverviewConverter : ModelConverter<RaizeCurrent, ThirdPartyDetails>() {

    override fun convert(input: RaizeCurrent): ThirdPartyDetails {
        val wallet = input.wallet!!
        return ThirdPartyDetails(
            name = ThirdPartyName.RAIZE,

            balance = wallet.balance ?: BigDecimal.ZERO,

            netAnnualReturn = wallet.returnPercent ?: BigDecimal.ZERO,
            interests = wallet.gain ?: BigDecimal.ZERO,
            deposits = wallet.charged ?: BigDecimal.ZERO,
            withdrawals = wallet.withdrawn ?: BigDecimal.ZERO,
            taxes = wallet.taxes ?: BigDecimal.ZERO,
            campaigns = wallet.referrals ?: BigDecimal.ZERO,

            investments = input.loans?.size ?: 0
        )
    }
}
