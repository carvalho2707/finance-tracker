package pt.tiagocarvalho.p2p.services.cases

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Locale
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.converter.peerberry.PeerberryOverviewConverter
import pt.tiagocarvalho.p2p.services.converter.peerberry.PeerberryStatementConverter
import pt.tiagocarvalho.p2p.services.retrofit.PeerberryApi
import pt.tiagocarvalho.p2p.services.retrofit.PeerberryRetrofitInterface
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal

internal class PeerberryCase : BaseCase() {
    private val peerberryOverviewConverter = PeerberryOverviewConverter()
    private val peerberryStatementConverter = PeerberryStatementConverter()

    fun getDetails(token: String): ThirdPartyDetails {
        val details = getMainPage(token)
        val deposits = getDeposits(token)
        val withdrawals = getWithdrawals(token)
        val dailyChange = getDailyChange(token)

        details.deposits = deposits.setScale(2, RoundingMode.HALF_UP)
        details.withdrawals = withdrawals.setScale(2, RoundingMode.HALF_UP)
        details.changePercentage = Utils.calculateChangePercentage(dailyChange, details.balance)
        details.changeValue = dailyChange

        return details
    }

    fun getStatements(
        token: String,
        from: String = ""
    ): ThirdPartyStatementModel {
        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()

        val peerberryApi =
            PeerberryRetrofitInterface.retrofitInstance.create(PeerberryApi::class.java)
        var isLastPage: Boolean
        var page = 1
        do {
            val transactions =
                peerberryApi.getTransactions(token, from, "", "0", page).execute().body()
            try {

                val thirdPartyStatementModel =
                    peerberryStatementConverter.convert(transactions?.data ?: emptyList())
                statements.addAll(thirdPartyStatementModel.statements)
                issues.addAll(thirdPartyStatementModel.issues)
            } catch (e: Exception) {
                val log = transactions?.toString()
                throw ThirdPartyException(e.message, log, e)
            }
            isLastPage = page++ >= transactions?.meta?.pages ?: 0
        } while (!isLastPage)
        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getMainPage(token: String): ThirdPartyDetails {
        val peerberryApi =
            PeerberryRetrofitInterface.retrofitInstance.create(PeerberryApi::class.java)

        peerberryApi.getToken(token).execute()
        val thirdPartyDetails: ThirdPartyDetails?
        var response = peerberryApi.getOverview(token).execute().body()
        if (response == null) {
            peerberryApi.getToken(token).execute()
            response = peerberryApi.getOverview(token).execute().body()
        }
        try {
            thirdPartyDetails = peerberryOverviewConverter.convert(response!!)
        } catch (e: Exception) {
            val log = response?.toString()
            throw ThirdPartyException(e.message, log, e)
        }

        return thirdPartyDetails
    }

    private fun getDeposits(token: String): BigDecimal {
        var total = BigDecimal.ZERO

        val peerberryApi =
            PeerberryRetrofitInterface.retrofitInstance.create(PeerberryApi::class.java)
        var isLastPage: Boolean
        var page = 1
        do {
            val transactions =
                peerberryApi.getTransactions(token, "", "", "1", page).execute().body()
            try {
                transactions?.data?.forEach {
                    total = total.add(
                        it.amount?.convertFromMoneyToBigDecimal(Locale.UK) ?: BigDecimal.ZERO
                    )
                }
            } catch (e: Exception) {
                val log = transactions?.toString()
                throw ThirdPartyException(e.message, log, e)
            }
            isLastPage = page++ >= transactions?.meta?.pages ?: 0
        } while (!isLastPage)
        return total
    }

    private fun getWithdrawals(token: String): BigDecimal {
        var total = BigDecimal.ZERO

        val peerberryApi =
            PeerberryRetrofitInterface.retrofitInstance.create(PeerberryApi::class.java)
        var isLastPage: Boolean
        var page = 1
        do {
            val transactions =
                peerberryApi.getTransactions(token, "", "", "2", page).execute().body()
            try {
                transactions?.data?.forEach {
                    total = total.add(
                        it.amount?.convertFromMoneyToBigDecimal(Locale.UK)?.abs() ?: BigDecimal.ZERO
                    )
                }
            } catch (e: Exception) {
                val log = transactions?.toString()
                throw ThirdPartyException(e.message, log, e)
            }
            isLastPage = page++ >= transactions?.meta?.pages ?: 0
        } while (!isLastPage)
        return total
    }

    private fun getDailyChange(token: String): BigDecimal {
        val startDate = getCurrentDateTimeString("yyyy-MM-dd")
        val peerberryApi =
            PeerberryRetrofitInterface.retrofitInstance.create(PeerberryApi::class.java)

        var dailyChange = BigDecimal.ZERO
        var isLastPage: Boolean
        var page = 1
        do {
            val transactions =
                peerberryApi.getTransactions(token, startDate, "", "", page).execute().body()
            try {
                transactions?.data?.forEach {
                    when (it.type!!) {
                        "REPAYMENT_INTEREST", "DEPOSIT", "BUYBACK_INTEREST" ->
                            dailyChange =
                                dailyChange.add(
                                    it.amount?.convertFromMoneyToBigDecimal(Locale.UK)
                                        ?: BigDecimal.ZERO
                                )
                        "WITHDRAWAL" ->
                            dailyChange =
                                dailyChange.subtract(
                                    it.amount?.convertFromMoneyToBigDecimal(Locale.UK)?.abs()
                                        ?: BigDecimal.ZERO
                                )
                    }
                }
            } catch (e: Exception) {
                val log = transactions?.toString()
                throw ThirdPartyException(e.message, log, e)
            }
            isLastPage = page++ >= transactions?.meta?.pages ?: 0
        } while (!isLastPage)

        return dailyChange
    }
}
