package pt.tiagocarvalho.p2p.services.model.mintos

internal data class AccountData(val summary: Summary)
