package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName

data class TransactionPage(

    @SerializedName("results") val results: List<Transaction>,
    @SerializedName("totalRecords") val totalRecords: Int,
    @SerializedName("page") val page: Int
)
