package pt.tiagocarvalho.p2p.services.model.mintos

import com.google.gson.internal.LinkedTreeMap
import java.math.BigDecimal

internal data class Summary(
    val statementEntryGroups: LinkedTreeMap<String, BigDecimal>?,
    val total: Int,
    val accountStatements: List<AccountStatement>
)
