package pt.tiagocarvalho.p2p.services.model.robocash

import java.math.BigDecimal

internal data class RobocashSummary(
    val addedFunds: BigDecimal,
    val withdrawnFunds: BigDecimal,
    val boughtLoans: BigDecimal,
    val returnedLoans: BigDecimal,
    val earnedInterests: BigDecimal,
    val openingAccountValue: BigDecimal,
    val closingAccountValue: BigDecimal
)
