package pt.tiagocarvalho.p2p.services.utils

import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

object Utils {

    fun calculateChangePercentage(change: BigDecimal, balance: BigDecimal): BigDecimal {
        if (balance.compareTo(ZERO) == 0) {
            return ZERO
        }
        val startValue = balance.subtract(change)
        if (startValue.compareTo(ZERO) == 0) {
            return change
        }
        return change.divide(startValue, 8, RoundingMode.HALF_UP).multiply(BigDecimal(100))
    }

    fun convertStringToDate(date: String, pattern: String): Date {
        val formatter = SimpleDateFormat(pattern, Locale.getDefault())
        return formatter.parse(date)
    }

    fun convertDateToString(date: Date, pattern: String = "dd-MM-yyyy"): String {
        val formatter = SimpleDateFormat(pattern, Locale.getDefault())
        return formatter.format(date)
    }

    fun getTodayDateAsString(pattern: String): String {
        val cal = Calendar.getInstance()
        return convertDateToString(cal.time, pattern)
    }

    fun getTodayDate(): Date {
        val cal = Calendar.getInstance()
        return cal.time
    }

    fun getTodayMonth(): Int {
        val cal = Calendar.getInstance()
        return cal.get(Calendar.MONTH)
    }

    fun getTodayYear(): Int {
        val cal = Calendar.getInstance()
        return cal.get(Calendar.YEAR)
    }
}
