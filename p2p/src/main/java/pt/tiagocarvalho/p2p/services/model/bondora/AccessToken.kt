package pt.tiagocarvalho.p2p.services.model.bondora

import com.google.gson.annotations.SerializedName

data class AccessToken(
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("scope")
    val scope: String,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("expires_in")
    val expiresIn: Int,
    @SerializedName("valid_until")
    val validUntil: Int
)
