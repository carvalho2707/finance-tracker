package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.helper.HttpConnection.connect
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.ACCOUNT_BALANCE
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.ACCOUNT_STATEMENTS
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.LOGIN
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.LOGIN_URL
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.OVERVIEW_URL
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.PASSWORD
import pt.tiagocarvalho.p2p.services.fetcher.IuvoPageFetcher.Constants.STATEMENTS
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class IuvoPageFetcher {

    fun login(
        username: String,
        password: String
    ): Connection.Response {
        return connect(LOGIN_URL)
            .header(CONTENT_TYPE_KEY, "application/x-www-form-urlencoded")
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .data(LOGIN, username)
            .data(PASSWORD, password)
            .ignoreContentType(true)
            .followRedirects(true)
            .method(Connection.Method.POST)
            .ignoreHttpErrors(true)
            .execute()
    }

    fun getAccountBalance(
        sessionToken: String
    ): Connection.Response {
        return connect(ACCOUNT_BALANCE + sessionToken)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .ignoreContentType(true)
            .followRedirects(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getOverview(
        sessionToken: String
    ): Connection.Response {
        return connect(String.format(OVERVIEW_URL, sessionToken))
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .ignoreContentType(true)
            .followRedirects(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getPreAccountStatements(
        sessionToken: String
    ): Connection.Response {
        return connect(String.format(STATEMENTS, sessionToken))
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .ignoreContentType(true)
            .followRedirects(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getAccountStatements(
        sessionToken: String,
        accountId: Int,
        from: String,
        to: String
    ): Connection.Response {
        return connect(String.format(ACCOUNT_STATEMENTS, sessionToken, accountId, from, to))
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .ignoreContentType(true)
            .followRedirects(true)
            .method(Connection.Method.GET)
            .execute()
    }

    private object Constants {
        const val LOGIN_URL = "https://tbp2p.iuvo-group.com/p2p-ui/?p0=login;=en_US"
        const val ACCOUNT_BALANCE =
            "https://tbp2p.iuvo-group.com/p2p-ui/?p0=get_account_balance&p2="
        const val OVERVIEW_URL =
            "https://tbp2p.iuvo-group.com/p2p-ui/app?p0=overview_page;p2=%s;lang=en_US&screen_width=1028&screen_height=871"

        const val STATEMENTS =
            "https://tbp2p.iuvo-group.com/p2p-ui/app?p0=account_statement_grouped_page;p2=%s;lang=en_US&screen_width=904&screen_height=871"
        const val ACCOUNT_STATEMENTS =
            "https://tbp2p.iuvo-group.com/p2p-ui/app?p0=account_statement_grouped_page;p2=%s;;tmms=90000;investor_account_id=%d&date_from=%s&date_to=%s;lang=en_US&screen_width=1028&screen_height=871"

        const val LOGIN = "login"
        const val PASSWORD = "password"
    }
}
