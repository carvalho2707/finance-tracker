package pt.tiagocarvalho.p2p.services.converter.crowdestor

import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.model.crowdestor.InvestmentPage

internal class InvestmentsConverter : ModelConverter<InvestmentPage, Int>() {

    override fun convert(input: InvestmentPage): Int {
        return input.recordsTotal
    }
}
