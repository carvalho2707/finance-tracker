package pt.tiagocarvalho.p2p.services.model.grupeer

import java.math.BigDecimal
import java.util.Date

data class Overview(
    val currencySign: String,
    val periodFrom: Date,
    val periodTo: Date,
    val balanceTotal: BigDecimal,
    val dataInvestedFunds: BigDecimal,
    val currentBalance: BigDecimal,
    val incomeTotal: BigDecimal,
    val incomeInterest: BigDecimal,
    val incomeCashBack: BigDecimal,
    val incomeBonus: BigDecimal,
    val incomeReferral: BigDecimal
)
