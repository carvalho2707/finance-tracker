package pt.tiagocarvalho.p2p.services.converter.estateguru

import java.util.Locale
import org.jsoup.nodes.Element
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.base.ModelConverter
import pt.tiagocarvalho.p2p.services.utils.convertFromMoneyToBigDecimal
import pt.tiagocarvalho.p2p.services.utils.text
import pt.tiagocarvalho.p2p.services.utils.toDate

internal class TransactionsConverter : ModelConverter<Element, ThirdPartyStatementModel>() {

    override fun convert(input: Element): ThirdPartyStatementModel {
        val table = input.getElementsByTag("tbody").first()
        val issues = mutableListOf<String>()
        val list = mutableListOf<ThirdPartyStatement>()

        if (table.childNodes().size == 3 && table.getElementsByClass("no-data-inner")
            .isNotEmpty()
        ) {
            return ThirdPartyStatementModel(list, issues)
        }

        table.childNodes().forEachIndexed { index, element ->

            if (index % 2 == 0) {
                return@forEachIndexed
            }

            val date = element.childNode(1).childNode(0).text().split(",").first()
            val time = element.childNode(1).childNode(0).text().split(",").last()
            val description = element.childNode(5).childNode(0).text().trim()
            val type = convertToType(description)
            val project = if (element.childNode(9).childNodes().size > 1) {
                element.childNode(9).childNode(1).childNode(1).childNode(0).text().trim()
            } else {
                "-"
            }
            val amount =
                element.childNode(13).childNode(0).text().trim().convertFromMoneyToBigDecimal(Locale.UK)
            val id = "$date$time$project$description$amount"

            if (type == ThirdPartyStatementType.INVALID) {
                issues.add(description)
            } else {
                list.add(
                    ThirdPartyStatement(
                        id = id,
                        amount = amount.abs(),
                        date = date.toDate("dd.MM.yyyy"),
                        type = type,
                        name = ThirdPartyName.ESTATEGURU
                    )
                )
            }
        }
        return ThirdPartyStatementModel(list, issues)
    }

    private fun convertToType(value: String): ThirdPartyStatementType {
        return when (value) {
            "Interest" -> ThirdPartyStatementType.INTEREST
            "Investment" -> ThirdPartyStatementType.INVESTMENT
            "Investment (Auto Invest)" -> ThirdPartyStatementType.INVESTMENT
            "Principal" -> ThirdPartyStatementType.PRINCIPAL
            "Withdraw" -> ThirdPartyStatementType.WITHDRAWAL
            "Withdrawal" -> ThirdPartyStatementType.WITHDRAWAL
            "Deposit" -> ThirdPartyStatementType.DEPOSIT
            "Fee" -> ThirdPartyStatementType.FEE
            "Fee (Sale fee)" -> ThirdPartyStatementType.FEE
            "Penalty" -> ThirdPartyStatementType.INTEREST
            "Indemnity" -> ThirdPartyStatementType.BONUS
            "Cashback" -> ThirdPartyStatementType.BONUS
            "Referrals" -> ThirdPartyStatementType.BONUS
            "Referral" -> ThirdPartyStatementType.BONUS
            "Bonus" -> ThirdPartyStatementType.BONUS
            "Secondary Market Deal" -> ThirdPartyStatementType.PRINCIPAL
            "Secondary Market" -> ThirdPartyStatementType.PRINCIPAL
            "Secondary Market Loss" -> ThirdPartyStatementType.BONUS
            "Secondary Market Profit" -> ThirdPartyStatementType.BONUS
            else -> ThirdPartyStatementType.INVALID
        }
    }
}
