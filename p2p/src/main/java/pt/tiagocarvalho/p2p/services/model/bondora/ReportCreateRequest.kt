package pt.tiagocarvalho.p2p.services.model.bondora

import java.util.Date

data class ReportCreateRequest(
    val reportType: Int,
    val periodStart: Date,
    val periodEnd: Date
)
