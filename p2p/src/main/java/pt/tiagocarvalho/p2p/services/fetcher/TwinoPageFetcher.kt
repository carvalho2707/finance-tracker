package pt.tiagocarvalho.p2p.services.fetcher

import org.jsoup.Connection
import org.jsoup.Jsoup.connect
import pt.tiagocarvalho.p2p.services.fetcher.TwinoPageFetcher.Constants.ACCOUNT_SUMMARY
import pt.tiagocarvalho.p2p.services.fetcher.TwinoPageFetcher.Constants.MY_INVESTMENTS_URL
import pt.tiagocarvalho.p2p.services.fetcher.TwinoPageFetcher.Constants.REQUEST_BODY_MY_INVESTMENTS
import pt.tiagocarvalho.p2p.services.fetcher.TwinoPageFetcher.Constants.STATISTICS_URL
import pt.tiagocarvalho.p2p.services.fetcher.TwinoPageFetcher.Constants.TRANSACTIONS_URL
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.ACCEPTED_LANGUAGE_VALUE
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_JSON_UTF
import pt.tiagocarvalho.p2p.services.model.common.Constants.CONTENT_TYPE_KEY
import pt.tiagocarvalho.p2p.services.model.common.Constants.USER_AGENT_VALUE

internal class TwinoPageFetcher {

    fun getAccountSummary(cookies: Map<String, String>): Connection.Response {
        return connect(ACCOUNT_SUMMARY)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .ignoreContentType(true)
            .followRedirects(true)
            .cookies(cookies)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getStatistics(cookies: Map<String, String>): Connection.Response {
        return connect(STATISTICS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .ignoreContentType(true)
            .method(Connection.Method.GET)
            .execute()
    }

    fun getMyInvestments(cookies: Map<String, String>): Connection.Response {
        return connect(MY_INVESTMENTS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_JSON_UTF)
            .followRedirects(true)
            .cookies(cookies)
            .requestBody(REQUEST_BODY_MY_INVESTMENTS)
            .ignoreContentType(true)
            .method(Connection.Method.POST)
            .execute()
    }

    fun getTransactions(cookies: Map<String, String>, body: String): Connection.Response {
        return connect(TRANSACTIONS_URL)
            .userAgent(USER_AGENT_VALUE)
            .header(CONTENT_TYPE_KEY, CONTENT_TYPE_JSON_UTF)
            .header(ACCEPTED_LANGUAGE_KEY, ACCEPTED_LANGUAGE_VALUE)
            .followRedirects(true)
            .cookies(cookies)
            .requestBody(body)
            .ignoreContentType(true)
            .method(Connection.Method.POST)
            .execute()
    }

    private object Constants {
        const val ACCOUNT_SUMMARY = "https://www.twino.eu/ws/web/investor/my-account-summary"
        const val STATISTICS_URL = "https://www.twino.eu/ws/web/investor/my-shares/statistics"
        const val TRANSACTIONS_URL = "https://www.twino.eu/ws/web/investor/account-entries/list"
        const val MY_INVESTMENTS_URL = "https://www.twino.eu/ws/web/investor/my-shares"

        const val REQUEST_BODY_MY_INVESTMENTS =
            "{\"page\":1,\"pageSize\":20,\"query\":{\"sortOption\":{\"direction\":\"DESC\"},\"loanStatuses\":[\"CURRENT\",\"EXTENDED\",\"DELAYED\",\"DEFAULTED\"]}}"
    }
}
