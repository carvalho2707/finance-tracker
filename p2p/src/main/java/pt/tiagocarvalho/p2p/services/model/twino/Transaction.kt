package pt.tiagocarvalho.p2p.services.model.twino

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Transaction(

    @SerializedName("id") val id: Int,
    @SerializedName("transactionType") val transactionType: String,
    @SerializedName("accountType") val accountType: String?,
    @SerializedName("bookingDateTime") val bookingDateTime: List<Int>?,
    @SerializedName("amount") val amount: BigDecimal,
    @SerializedName("amountRaw") val amountRaw: BigDecimal,
    @SerializedName("amountLoanCurrency") val amountLoanCurrency: Double?,
    @SerializedName("investmentLoanNumber") val investmentLoanNumber: String?,
    @SerializedName("investmentLoanId") val investmentLoanId: Int?,
    @SerializedName("processingDate") val processingDate: List<Int>,
    @SerializedName("actualLoanCurrency") val actualLoanCurrency: String?
)
