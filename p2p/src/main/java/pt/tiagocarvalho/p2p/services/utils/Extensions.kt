package pt.tiagocarvalho.p2p.services.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit
import kotlin.math.abs
import org.jsoup.nodes.Node
import org.jsoup.nodes.TextNode

fun String.convertFromMoneyToBigDecimal(): BigDecimal {
    return NumberFormat.getInstance(Locale.getDefault())
        .parse(this.replace("[-+$€() ]".toRegex(), "")).toBigDecimal()
}

fun String.convertFromMoneyToBigDecimal(scale: Boolean): BigDecimal {
    val result = this.convertFromMoneyToBigDecimal()
    return if (scale) result.setScale(2, RoundingMode.HALF_UP)
    else result
}

fun String.convertFromMoneyToBigDecimal(locale: Locale): BigDecimal {
    val numberFormat = NumberFormat.getInstance(locale)
    val textValue = this.replace("[-+$€() ]".toRegex(), "")
    return numberFormat.parse(textValue).toBigDecimal()
}

fun String.convertFromInterestToBigDecimal(): BigDecimal {
    return this.replace("[%]".toRegex(), "").toBigDecimal().setScale(2, RoundingMode.HALF_UP)
}

fun Number.toBigDecimal(): BigDecimal =
    this.toString().toBigDecimal()

fun String?.toBigDecimalOrZero(): BigDecimal =
    this?.toBigDecimalOrNull()?.setScale(2, RoundingMode.HALF_UP) ?: BigDecimal.ZERO

fun BigDecimal?.orZero(): BigDecimal = this ?: BigDecimal.ZERO

inline fun <T> Iterable<T>.sumByBigDecimal(selector: (T) -> BigDecimal): BigDecimal {
    var sum: BigDecimal = BigDecimal.ZERO
    for (element in this) {
        sum += selector(element)
    }
    return sum
}

fun Date.toStringPattern(pattern: String): String {
    val formatter = SimpleDateFormat(pattern, Locale.getDefault())
    return formatter.format(this)
}

fun String.toDate(pattern: String): Date {
    val formatter = SimpleDateFormat(pattern, Locale.getDefault())
    return formatter.parse(this)
}

fun Date.toInitialDay(): Date {
    val cal = Calendar.getInstance()
    cal.time = this
    cal.set(Calendar.HOUR_OF_DAY, 0)
    cal.set(Calendar.MINUTE, 0)
    cal.set(Calendar.SECOND, 0)
    cal.set(Calendar.MILLISECOND, 0)
    return cal.time
}

fun Calendar.getFirstDayOfMonth(): Date {
    with(this) {
        set(Calendar.DAY_OF_MONTH, getActualMinimum(Calendar.DAY_OF_MONTH))
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 1)
        return time
    }
}

fun Calendar.getLastDayOfMonth(): Date {
    with(this) {
        set(Calendar.DAY_OF_MONTH, getActualMaximum(Calendar.DAY_OF_MONTH))
        set(Calendar.HOUR_OF_DAY, 23)
        set(Calendar.MINUTE, 59)
        set(Calendar.SECOND, 59)
        set(Calendar.MILLISECOND, 59)
        return time
    }
}

fun Date.isFiveHoursAgo(): Boolean =
    abs(Utils.getTodayDate().time - this.time) < TimeUnit.HOURS.toMillis(5)

fun Date.passedFiveMinutes(): Boolean =
    this.time + TimeUnit.MINUTES.toMillis(5) > Utils.getTodayDate().time

fun Date.isToday(): Boolean {
    val cal1 = Calendar.getInstance()
    val cal2 = Calendar.getInstance()
    cal1.time = this
    cal2.time = Utils.getTodayDate()
    return cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
        cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
}

fun Node.text(): String {
    if (this !is TextNode) {
        throw ClassCastException()
    }
    return this.text()
}

fun Map<String, BigDecimal>.getSafe(key: String): BigDecimal =
    this.getOrElse(key) { BigDecimal.ZERO }

inline fun <reified T> Gson.fromJson(json: String) =
    fromJson<T>(json, object : TypeToken<T>() {}.type)
