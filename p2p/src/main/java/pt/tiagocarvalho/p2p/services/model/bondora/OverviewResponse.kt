package pt.tiagocarvalho.p2p.services.model.bondora

import com.google.gson.annotations.SerializedName

data class OverviewResponse(
    @SerializedName("Stats") val stats: List<Stats>,
    @SerializedName("ShowOverview") val show: Boolean
)
