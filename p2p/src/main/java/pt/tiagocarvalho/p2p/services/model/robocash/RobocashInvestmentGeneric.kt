package pt.tiagocarvalho.p2p.services.model.robocash

internal data class RobocashInvestmentGeneric(val total: Int)
