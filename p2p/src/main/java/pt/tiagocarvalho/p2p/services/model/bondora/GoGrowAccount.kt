package pt.tiagocarvalho.p2p.services.model.bondora

import java.math.BigDecimal

data class GoGrowAccount(
    val name: String,
    val netDeposits: BigDecimal,
    val netProfit: BigDecimal,
    val totalSaved: BigDecimal
)
