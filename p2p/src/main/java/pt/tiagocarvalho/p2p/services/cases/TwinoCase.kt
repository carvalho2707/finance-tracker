package pt.tiagocarvalho.p2p.services.cases

import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Calendar
import java.util.Date
import java.util.Locale
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyName
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType
import pt.tiagocarvalho.p2p.services.converter.twino.TransactionConverter
import pt.tiagocarvalho.p2p.services.fetcher.TwinoPageFetcher
import pt.tiagocarvalho.p2p.services.model.twino.AccountSummary
import pt.tiagocarvalho.p2p.services.model.twino.MyInvestmentsCount
import pt.tiagocarvalho.p2p.services.model.twino.Statistics
import pt.tiagocarvalho.p2p.services.model.twino.TransactionPage
import pt.tiagocarvalho.p2p.services.model.twino.TransactionRequest
import pt.tiagocarvalho.p2p.services.model.twino.TransactionTypeList
import pt.tiagocarvalho.p2p.services.model.twino.TwinoFilterType
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal

internal class TwinoCase : BaseCase() {

    private val twinoPageFetcher = TwinoPageFetcher()
    private val transactionConverter = TransactionConverter()

    fun getDetails(sessionId: String): ThirdPartyDetails {
        cookiesGlobal["hazelcast.sessionId"] = sessionId
        val accountSummary = getAccountSummary()
        val statistics = getStatistics()
        val bbStats = statistics.sharePerformance.buyback
        val pgStats = statistics.sharePerformance.paymentGuarantee
        val investmentCount = getMyInvestments()
        val dailyChange = getDailyChange()

        return ThirdPartyDetails(
            name = ThirdPartyName.TWINO,

            balance = accountSummary.accountValue.setScale(2, RoundingMode.HALF_UP),
            available = accountSummary.investmentBalance.setScale(2, RoundingMode.HALF_UP),
            invested = accountSummary.investments.setScale(2, RoundingMode.HALF_UP),

            netAnnualReturn = accountSummary.xirr.setScale(2, RoundingMode.HALF_UP),
            profit = accountSummary.income.setScale(2, RoundingMode.HALF_UP),
            interests = accountSummary.interestWithoutCurrencyFluctuations.setScale(
                2,
                RoundingMode.HALF_UP
            ),
            deposits = accountSummary.deposits.setScale(2, RoundingMode.HALF_UP),
            withdrawals = accountSummary.withdrawals.abs().setScale(2, RoundingMode.HALF_UP),
            campaigns = accountSummary.campaigns.setScale(2, RoundingMode.HALF_UP),
            currencyFluctuations = accountSummary.currencyFluctuations.abs().setScale(
                2,
                RoundingMode.HALF_UP
            ),
            writeOff = accountSummary.lossOnWriteoff.abs().setScale(2, RoundingMode.HALF_UP),
            serviceFees = accountSummary.penalties.abs().setScale(2, RoundingMode.HALF_UP),

            investments = investmentCount,
            currentTotal = bbStats.current.add(pgStats.current).setScale(2, RoundingMode.HALF_UP),
            defaultInvestmentTotal = bbStats.defaulted.add(pgStats.defaulted).setScale(
                2,
                RoundingMode.HALF_UP
            ),
            lateTotal = bbStats.delayed.add(pgStats.delayed).setScale(2, RoundingMode.HALF_UP),

            changePercentage = Utils.calculateChangePercentage(
                dailyChange,
                accountSummary.accountValue.setScale(2, RoundingMode.HALF_UP)
            ),
            changeValue = dailyChange.setScale(2, RoundingMode.HALF_UP)
        )
    }

    fun getStatements(date: Date, sessionId: String): ThirdPartyStatementModel {
        cookiesGlobal["hazelcast.sessionId"] = sessionId
        val accountSummaryResponse = twinoPageFetcher.getAccountSummary(cookiesGlobal)
        replaceCookies(accountSummaryResponse.cookies())

        val to = Utils.getTodayDate()
        val filter = getNoFilter()

        return getStatements(date, to, filter)
    }

    private fun getAccountSummary(): AccountSummary {
        val accountSummaryResponse = twinoPageFetcher.getAccountSummary(cookiesGlobal)
        replaceCookies(accountSummaryResponse.cookies())
        val body = accountSummaryResponse.body()

        try {
            return getGson().fromJson(body, AccountSummary::class.java)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    private fun getStatistics(): Statistics {
        val statisticsResponse = twinoPageFetcher.getStatistics(cookiesGlobal)
        replaceCookies(statisticsResponse.cookies())
        val body = statisticsResponse.body()

        try {
            return getGson().fromJson(body, Statistics::class.java)
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    private fun getMyInvestments(): Int {
        val myInvestmentsResponse = twinoPageFetcher.getMyInvestments(cookiesGlobal)
        replaceCookies(myInvestmentsResponse.cookies())
        val body = myInvestmentsResponse.body()

        try {
            return getGson().fromJson(body, MyInvestmentsCount::class.java).totalRecords
        } catch (e: Exception) {
            throw ThirdPartyException(e.message, body, e)
        }
    }

    private fun getDailyChange(): BigDecimal {
        val filter = getChangeFilter()
        val today = Utils.getTodayDate()

        return getStatements(today, today, filter).statements
            .map {
                if (it.type == ThirdPartyStatementType.WITHDRAWAL || it.type == ThirdPartyStatementType.FEE) {
                    it.amount.negate()
                } else {
                    it.amount
                }
            }.sumByBigDecimal { it }
    }

    private fun getStatements(
        from: Date,
        to: Date,
        filter: List<TransactionTypeList>
    ): ThirdPartyStatementModel {
        var counter = 1
        var isLastPage: Boolean
        val statements = mutableListOf<ThirdPartyStatement>()
        val issues = mutableListOf<String>()

        val transactionRequest = TransactionRequest()
        transactionRequest.processingDateFrom = getDateList(from)
        transactionRequest.processingDateTo = getDateList(to)
        transactionRequest.transactionTypeList = filter

        do {
            transactionRequest.page = counter
            val response = twinoPageFetcher.getTransactions(
                cookiesGlobal,
                getGson().toJson(transactionRequest, TransactionRequest::class.java)
            )
            replaceCookies(response.cookies())
            val body = response.body()

            val transactionPage = getGson().fromJson(body, TransactionPage::class.java)
            val statementList = transactionConverter.convert(transactionPage.results)

            try {
                statements.addAll(statementList.statements)
                issues.addAll(statementList.issues)

                isLastPage = (transactionPage.page * 20 >= transactionPage.totalRecords)
                counter++
            } catch (e: Exception) {
                throw ThirdPartyException(e.message, body?.toString(), e)
            }
        } while (isLastPage.not())

        return ThirdPartyStatementModel(statements, issues)
    }

    private fun getDateList(date: Date): List<Int> {
        val cal = Calendar.getInstance()
        cal.time = date
        return listOf(
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH) + 1,
            cal.get(Calendar.DAY_OF_MONTH)
        )
    }

    private fun getNoFilter(): List<TransactionTypeList> {
        val addFalse = emptyList<TwinoFilterType>().toMutableList()
        val list: MutableList<TransactionTypeList> = TwinoFilterType.values().map {
            if (it.hasPositive) {
                addFalse.add(it)
                TransactionTypeList(it.name.toUpperCase(Locale.ROOT), true)
            } else {
                TransactionTypeList(it.name.toUpperCase(Locale.ROOT), null)
            }
        }.toMutableList()
        addFalse.forEach {
            list.add(TransactionTypeList(it.name.toUpperCase(Locale.ROOT), false))
        }
        return list
    }

    private fun getChangeFilter(): List<TransactionTypeList> {
        val addFalse = emptyList<TwinoFilterType>().toMutableList()
        val list: MutableList<TransactionTypeList> = TwinoFilterType.valuesForChange()
            .map {
                if (it.hasPositive) {
                    addFalse.add(it)
                    TransactionTypeList(it.name.toUpperCase(Locale.ROOT), true)
                } else {
                    TransactionTypeList(it.name.toUpperCase(Locale.ROOT), null)
                }
            }.toMutableList()
        addFalse.forEach {
            list.add(TransactionTypeList(it.name.toUpperCase(Locale.ROOT), false))
        }
        return list
    }
}
