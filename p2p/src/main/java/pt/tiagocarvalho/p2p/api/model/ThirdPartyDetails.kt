package pt.tiagocarvalho.p2p.api.model

import java.math.BigDecimal

data class ThirdPartyDetails(
    var name: ThirdPartyName,

    // account
    var balance: BigDecimal = BigDecimal.ZERO,
    var available: BigDecimal = BigDecimal.ZERO,
    var invested: BigDecimal = BigDecimal.ZERO,

    // returns
    var netAnnualReturn: BigDecimal = BigDecimal.ZERO,
    var deposits: BigDecimal = BigDecimal.ZERO,
    var withdrawals: BigDecimal = BigDecimal.ZERO,
    var profit: BigDecimal = BigDecimal.ZERO,

    var interests: BigDecimal = BigDecimal.ZERO,
    var latePaymentFees: BigDecimal = BigDecimal.ZERO,
    var badDebt: BigDecimal = BigDecimal.ZERO,
    var secondaryMarketTransactions: BigDecimal = BigDecimal.ZERO,
    var serviceFees: BigDecimal = BigDecimal.ZERO,
    var campaigns: BigDecimal = BigDecimal.ZERO,
    var cashback: BigDecimal = BigDecimal.ZERO,
    var taxes: BigDecimal = BigDecimal.ZERO,
    var currencyFluctuations: BigDecimal = BigDecimal.ZERO,
    var writeOff: BigDecimal = BigDecimal.ZERO,

    // investments
    var investments: Int = 0,
    val currentTotal: BigDecimal = BigDecimal.ZERO,
    var gracePeriodTotal: BigDecimal = BigDecimal.ZERO,
    var oneToFifteenLateTotal: BigDecimal = BigDecimal.ZERO,
    var sixteenToThirtyLateTotal: BigDecimal = BigDecimal.ZERO,
    var thirtyToSixtyLateTotal: BigDecimal = BigDecimal.ZERO,
    var moreThanSixtyLateTotal: BigDecimal = BigDecimal.ZERO,
    var defaultInvestmentTotal: BigDecimal = BigDecimal.ZERO,
    var badDebtTotal: BigDecimal = BigDecimal.ZERO,
    val lateTotal: BigDecimal = BigDecimal.ZERO,

    var currentNumber: Int = 0,
    var gracePeriodNumber: Int = 0,
    var oneToFifteenLateNumber: Int = 0,
    var sixteenToThirtyLateNumber: Int = 0,
    var thirtyToSixtyLateNumber: Int = 0,
    var moreThanSixtyLateNumber: Int = 0,
    var defaultInvestmentNumber: Int = 0,
    var badDebtNumber: Int = 0,
    val lateNumber: Int = 0,

    var pendingPayments: BigDecimal = BigDecimal.ZERO,
    var pendingInterests: BigDecimal = BigDecimal.ZERO,

    val cumulativeInvested: BigDecimal = BigDecimal.ZERO,
    val cumulativePrincipal: BigDecimal = BigDecimal.ZERO,

    // savings
    var interestFrequency: String = "",
    var recurringAmount: BigDecimal = BigDecimal.ZERO,
    var recurringFrequency: String = "",

    var changePercentage: BigDecimal = BigDecimal.ZERO,
    var changeValue: BigDecimal = BigDecimal.ZERO,

    // temp
    var balances: String? = null,
    var aggregates: String? = null

)
