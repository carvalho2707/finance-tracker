package pt.tiagocarvalho.p2p.api

import io.reactivex.Observable
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ApiResponse
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.IuvoCase
import pt.tiagocarvalho.p2p.services.utils.Utils
import retrofit2.Response

object IuvoAPI {

    private val iuvoCase: IuvoCase = IuvoCase()

    fun authenticate(login: Login): Observable<ApiResponse<Boolean>> {
        return Observable.create { emitter ->
            try {
                iuvoCase.login(login)
                val apiResponse = ApiResponse.create(Response.success(true))
                emitter.onNext(apiResponse)
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

    fun getDetails(login: Login): ThirdPartyDetails {
        return iuvoCase.getDetails(login)
    }

    fun getStatements(
        date: Date?,
        login: Login
    ): ThirdPartyStatementModel {
        return if (date == null) {
            iuvoCase.getStatements(
                Utils.convertStringToDate("2012-01-01", "yyyy-MM-dd"),
                login
            )
        } else {
            iuvoCase.getStatements(date, login)
        }
    }

    fun getTodayStatements(login: Login): ThirdPartyStatementModel {
        return iuvoCase.getStatements(Utils.getTodayDate(), login)
    }
}
