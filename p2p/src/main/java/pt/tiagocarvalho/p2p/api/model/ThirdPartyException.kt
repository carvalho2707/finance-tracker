package pt.tiagocarvalho.p2p.api.model

class ThirdPartyException(
    message: String?,
    val content: String?,
    throwable: Throwable?
) : Exception(message ?: "Unknown Error", throwable) {

    constructor(message: String?) : this(message, null, null)

    constructor(message: String?, content: String?) : this(message, content, null)

    constructor(message: String?, throwable: Throwable?) : this(message, null, throwable)
}
