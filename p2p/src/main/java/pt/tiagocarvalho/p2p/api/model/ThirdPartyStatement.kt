package pt.tiagocarvalho.p2p.api.model

import java.math.BigDecimal
import java.util.Date

data class ThirdPartyStatement(
    var id: String,
    var amount: BigDecimal,
    var date: Date,
    var type: ThirdPartyStatementType,
    var name: ThirdPartyName
)
