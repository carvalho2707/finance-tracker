package pt.tiagocarvalho.p2p.api

import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.RobocashCase
import pt.tiagocarvalho.p2p.services.utils.Utils

object RobocashAPI {

    private val robocashCase = RobocashCase()

    fun getDetails(cookies: String): ThirdPartyDetails = robocashCase.getDetails(cookies)

    fun getStatements(cookies: String, date: Date?): ThirdPartyStatementModel {
        return if (date == null) {
            robocashCase.getStatements(cookies)
        } else {
            robocashCase.getStatements(cookies, date)
        }
    }

    fun getTodayStatements(cookies: String): ThirdPartyStatementModel {
        return robocashCase.getStatements(cookies, Utils.getTodayDate())
    }
}
