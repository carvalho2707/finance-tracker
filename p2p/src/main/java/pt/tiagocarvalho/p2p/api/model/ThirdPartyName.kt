package pt.tiagocarvalho.p2p.api.model

enum class ThirdPartyName {
    GRUPEER,
    PEERBERRY,
    MINTOS,
    RAIZE,
    CROWDESTOR,
    ROBOCASH,
    TWINO,
    BONDORA,
    ESTATEGURU,
    IUVO,
    LENDERMARKET
}
