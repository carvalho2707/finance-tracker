package pt.tiagocarvalho.p2p.api

import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.PeerberryCase
import pt.tiagocarvalho.p2p.services.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.toStringPattern

object PeerberryAPI {

    private val peerberryCase: PeerberryCase = PeerberryCase()

    fun getDetails(token: String): ThirdPartyDetails {
        return peerberryCase.getDetails("Bearer $token")
    }

    fun getStatements(token: String, date: Date?): ThirdPartyStatementModel {
        return if (date == null) {
            peerberryCase.getStatements("Bearer $token")
        } else {
            peerberryCase.getStatements("Bearer $token", date.toStringPattern("yyyy-MM-dd"))
        }
    }

    fun getTodayStatements(token: String): ThirdPartyStatementModel {
        val todayDate = Utils.getTodayDateAsString("yyyy-MM-dd")
        return peerberryCase.getStatements("Bearer $token", todayDate)
    }
}
