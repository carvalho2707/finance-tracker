package pt.tiagocarvalho.p2p.api

import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.RaizeCase
import pt.tiagocarvalho.p2p.services.utils.Utils

object RaizeAPI {

    private val raizeCase: RaizeCase = RaizeCase()

    fun getDetails(token: String): ThirdPartyDetails {
        return raizeCase.getDetails(token)
    }

    fun getStatements(token: String, date: Date?): ThirdPartyStatementModel {
        return if (date == null) {
            raizeCase.getStatements(token)
        } else {
            raizeCase.getStatements(token, date)
        }
    }

    fun getTodayStatements(token: String): ThirdPartyStatementModel {
        return raizeCase.getStatements(token, Utils.getTodayDate())
    }
}
