package pt.tiagocarvalho.p2p.api

import java.util.Calendar
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.MintosCase
import pt.tiagocarvalho.p2p.services.utils.Utils

object MintosAPI {

    private val mintosCase: MintosCase = MintosCase()

    fun getDetails(cookies: String): ThirdPartyDetails {
        return mintosCase.getDetails(cookies)
    }

    fun getStatements(
        date: Date?,
        cookies: String
    ): ThirdPartyStatementModel {
        return if (date == null) {
            mintosCase.getStatements(getFirstPossibleDay(), cookies)
        } else {
            mintosCase.getStatements(date, cookies)
        }
    }

    fun getTodayStatements(
        cookies: String
    ): ThirdPartyStatementModel {
        return mintosCase.getStatements(Utils.getTodayDate(), cookies)
    }

    private fun getFirstPossibleDay(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.YEAR, -29)
        return cal.time
    }
}
