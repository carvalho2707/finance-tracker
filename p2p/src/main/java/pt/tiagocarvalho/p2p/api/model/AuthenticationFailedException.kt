package pt.tiagocarvalho.p2p.api.model

class AuthenticationFailedException(
    message: String?
) : Exception(message) {

    constructor() : this("")
}
