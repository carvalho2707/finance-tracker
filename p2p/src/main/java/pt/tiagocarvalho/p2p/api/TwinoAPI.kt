package pt.tiagocarvalho.p2p.api

import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.TwinoCase
import pt.tiagocarvalho.p2p.services.utils.Utils

object TwinoAPI {

    private val twinoCase: TwinoCase = TwinoCase()

    fun getDetails(sessionId: String) = twinoCase.getDetails(sessionId)

    fun getStatements(
        date: Date?,
        sessionId: String
    ): ThirdPartyStatementModel {
        return if (date == null) {
            twinoCase.getStatements(
                Utils.convertStringToDate("01-01-1950", "dd-MM-yyyy"),
                sessionId
            )
        } else {
            twinoCase.getStatements(date, sessionId)
        }
    }

    fun getTodayStatements(sessionId: String) =
        twinoCase.getStatements(Utils.getTodayDate(), sessionId)
}
