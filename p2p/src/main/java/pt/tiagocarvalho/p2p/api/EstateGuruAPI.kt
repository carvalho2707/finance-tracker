package pt.tiagocarvalho.p2p.api

import io.reactivex.Observable
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ApiResponse
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.EstateGuruCase
import pt.tiagocarvalho.p2p.services.utils.Utils
import retrofit2.Response

object EstateGuruAPI {

    private val estateGuruCase: EstateGuruCase = EstateGuruCase()

    fun authenticate(login: Login): Observable<ApiResponse<Boolean>> {
        return Observable.create { emitter ->
            try {
                estateGuruCase.login(login)
                val apiResponse = ApiResponse.create(Response.success(true))
                emitter.onNext(apiResponse)
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

    fun getDetails(login: Login): ThirdPartyDetails {
        return estateGuruCase.getDetails(login)
    }

    fun getStatements(
        date: Date?,
        login: Login
    ): ThirdPartyStatementModel {
        return if (date == null) {
            estateGuruCase.getStatements(
                Utils.convertStringToDate("1950-01-01", "yyyy-MM-dd"),
                login
            )
        } else {
            estateGuruCase.getStatements(date, login)
        }
    }

    fun getTodayStatements(
        login: Login
    ): ThirdPartyStatementModel {
        return estateGuruCase.getStatements(Utils.getTodayDate(), login)
    }
}
