package pt.tiagocarvalho.p2p.api

import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.GrupeerCase
import pt.tiagocarvalho.p2p.services.utils.Utils

object GrupeerAPI {

    private val grupeerCase = GrupeerCase.getInstance()

    fun getDetails(cookies: String): ThirdPartyDetails {
        return grupeerCase.getDetails(cookies)
    }

    fun getStatements(cookies: String, date: Date?): ThirdPartyStatementModel {
        return if (date == null) {
            grupeerCase.getStatements(
                cookies,
                Utils.convertStringToDate("01-01-1950", "dd-MM-yyyy")
            )
        } else {
            grupeerCase.getStatements(cookies, date)
        }
    }

    fun getTodayStatements(cookies: String): ThirdPartyStatementModel {
        return grupeerCase.getStatements(cookies, Utils.getTodayDate())
    }
}
