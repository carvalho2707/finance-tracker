package pt.tiagocarvalho.p2p.api

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.BondoraCase
import pt.tiagocarvalho.p2p.services.utils.Utils

class BondoraAPI private constructor(clientId: String, clientSecret: String) {

    private val bondoraCase: BondoraCase = BondoraCase.getInstance(clientId, clientSecret)

    fun authenticate(code: String): Single<String> {
        return Single.create { emitter ->
            try {
                val token = bondoraCase.login(code)
                emitter.onSuccess(token)
            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
    }

    fun getDetails(token: String, login: Login): ThirdPartyDetails {
        println("Token: $token")
        return bondoraCase.getDetails("Bearer $token", login)
    }

    fun getStatements(
        date: Date?,
        token: String
    ): ThirdPartyStatementModel {
        println("Token: $token")
        return if (date == null) {
            bondoraCase.getStatements(
                Utils.convertStringToDate("01-01-2009", "dd-MM-yyyy"),
                "Bearer $token",
                reUse = false,
                daily = false
            )
        } else {
            bondoraCase.getStatements(date, "Bearer $token", reUse = false, daily = false)
        }
    }

    fun getTodayStatements(
        token: String
    ): ThirdPartyStatementModel {
        println("Token: $token")
        return bondoraCase.getStatements(
            Utils.getTodayDate(), "Bearer $token",
            reUse = true,
            daily = true
        )
    }

    companion object {
        // Debuggable field to check instance count
        private var mInstance: BondoraAPI? = null

        @Synchronized
        fun getInstance(clientId: String, clientSecret: String): BondoraAPI {
            if (mInstance == null) {
                mInstance = BondoraAPI(clientId, clientSecret)
            }
            return mInstance as BondoraAPI
        }
    }
}
