package pt.tiagocarvalho.p2p.api.model

enum class ThirdPartyStatementType {
    DEPOSIT,
    WITHDRAWAL,
    INTEREST,
    FEE,
    INVESTMENT,
    PRINCIPAL,
    PRINCIPAL_FIX_NEGATIVE,
    BONUS,
    CURRENCY_EXCHANGE_OUT,
    CURRENCY_EXCHANGE_IN,
    DEPOSIT_CANCELED,
    WITHDRAWAL_CANCELED,
    REFERRAL,
    CANCELED,
    INVALID,
    IGNORE,
}
