package pt.tiagocarvalho.p2p.api.model

data class ThirdPartyStatementModel(
    val statements: List<ThirdPartyStatement>,
    val issues: List<String>
)
