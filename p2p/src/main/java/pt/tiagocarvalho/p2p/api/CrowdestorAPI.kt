package pt.tiagocarvalho.p2p.api

import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel
import pt.tiagocarvalho.p2p.services.cases.CrowdestorCase
import pt.tiagocarvalho.p2p.services.utils.Utils

object CrowdestorAPI {

    private val crowdestorCase = CrowdestorCase()

    fun getDetails(cookies: String) = crowdestorCase.getDetails(cookies)

    fun getStatements(
        date: Date?,
        cookies: String
    ): ThirdPartyStatementModel {
        return if (date == null) {
            crowdestorCase.getStatements(
                Utils.convertStringToDate("1950-01-01", "yyyy-MM-dd"),
                cookies
            )
        } else {
            crowdestorCase.getStatements(date, cookies)
        }
    }

    fun getTodayStatements(cookies: String) =
        crowdestorCase.getStatements(Utils.getTodayDate(), cookies)
}
