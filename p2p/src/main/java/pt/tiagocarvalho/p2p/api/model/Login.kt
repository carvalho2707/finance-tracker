package pt.tiagocarvalho.p2p.api.model

data class Login(val username: String, val password: String)
