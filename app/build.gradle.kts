import dependencies.AnnotationProcessorsDependencies
import dependencies.Dependencies
import dependencies.TestAndroidDependencies
import dependencies.TestDependencies
import extensions.implementation
import extensions.kapt

plugins {
    id(BuildPlugins.ANDROID_APPLICATION)
    id(BuildPlugins.GMS)
    id(BuildPlugins.CRASHLYTICS)
    id(BuildPlugins.HILT)
    id(BuildPlugins.KOTLIN_ANDROID)
    id(BuildPlugins.KOTLIN_ANDROID_EXTENSIONS)
    id(BuildPlugins.KOTLIN_KAPT)
    id(BuildPlugins.NAVIGATION_SAFE_ARGS)
}

android {
    compileSdkVersion(BuildAndroidConfig.COMPILE_SDK_VERSION)
    defaultConfig {
        applicationId = BuildAndroidConfig.APPLICATION_ID
        minSdkVersion(BuildAndroidConfig.MIN_SDK_VERSION)
        targetSdkVersion(BuildAndroidConfig.TARGET_SDK_VERSION)
        buildToolsVersion(BuildAndroidConfig.BUILD_TOOLS_VERSION)

        versionCode = BuildAndroidConfig.VERSION_CODE
        versionName = BuildAndroidConfig.VERSION_NAME

        vectorDrawables.useSupportLibrary = BuildAndroidConfig.SUPPORT_LIBRARY_VECTOR_DRAWABLES
        testInstrumentationRunner = BuildAndroidConfig.TEST_INSTRUMENTATION_RUNNER
        multiDexEnabled = BuildAndroidConfig.MULTIDEX_ENABLED

        javaCompileOptions {
            annotationProcessorOptions {
                arguments["room.schemaLocation"] = "$projectDir/schemas.toString()"
                arguments["room.incremental"] = "true"
            }
        }

        buildConfigField("String", "P2P_EMAIL", properties["P2P_EMAIL"] as String)
        buildConfigField(
            "String",
            "P2P_PRIMARY_PASSWORD",
            properties["P2P_PRIMARY_PASSWORD"] as String
        )
    }

    signingConfigs {
        create(BuildType.RELEASE) {
            storeFile = file(properties["INWALLET_STORE_FILE"] as String)
            storePassword = properties["INWALLET_STORE_PASSWORD"] as String
            keyAlias = properties["INWALLET_KEY_ALIAS"] as String
            keyPassword = properties["INWALLET_KEY_PASSWORD"] as String
        }
    }

    buildTypes {
        getByName(BuildType.RELEASE) {
            proguardFiles("proguard-android-optimize.txt", "proguard-rules.pro")
            isMinifyEnabled = BuildTypeRelease.isMinifyEnabled
            isTestCoverageEnabled = BuildTypeRelease.isTestCoverageEnabled
            manifestPlaceholders(mutableMapOf("admobAppId" to (properties["ADMOB_APP_ID"] as String)))
            firebaseCrashlytics {
                mappingFileUploadEnabled = false
            }
            signingConfig = signingConfigs.getByName("release")

            buildConfigField(
                "String",
                "BONDORA_CLIENT_ID",
                properties["BONDORA_CLIENT_ID"] as String
            )
            buildConfigField(
                "String",
                "BONDORA_CLIENT_SECRET",
                properties["BONDORA_CLIENT_SECRET"] as String
            )

        }
        getByName(BuildType.DEBUG) {
            applicationIdSuffix = BuildTypeDebug.applicationIdSuffix
            versionNameSuffix = BuildTypeDebug.versionNameSuffix
            isMinifyEnabled = BuildTypeDebug.isMinifyEnabled
            isTestCoverageEnabled = BuildTypeDebug.isTestCoverageEnabled

            manifestPlaceholders(mutableMapOf("admobAppId" to (properties["ADMOB_APP_ID_DEV"] as String)))

            buildConfigField(
                "String",
                "BONDORA_CLIENT_ID",
                properties["BONDORA_DEV_CLIENT_ID"] as String
            )
            buildConfigField(
                "String",
                "BONDORA_CLIENT_SECRET",
                properties["BONDORA_DEV_CLIENT_SECRET"] as String
            )
        }
    }

    buildFeatures {
        dataBinding = true
    }

    androidExtensions {
        isExperimental = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }

}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(project(BuildModules.P2P))
    implementation(project(BuildModules.THIRDPARTY))
    implementation(project(BuildModules.BILLING))

    implementation(Dependencies.KOTLIN)
    implementation(Dependencies.ACTIVITY)
    implementation(Dependencies.APPCOMPAT)
    implementation(Dependencies.MULTIDEX)
    implementation(Dependencies.CORE_KTX)
    implementation(Dependencies.VECTOR_DRAWABLE)
    implementation(Dependencies.DRAWER_LAYOUT)
    implementation(Dependencies.CARD_VIEW)
    implementation(Dependencies.CONSTRAINT_LAYOUT)
    implementation(Dependencies.PREFERENCE_KTX)
    implementation(Dependencies.FRAGMENT_KTX)
    implementation(Dependencies.NAVIGATION_FRAGMENT)
    implementation(Dependencies.NAVIGATION_UI)
    implementation(Dependencies.MATERIAL)
    implementation(Dependencies.LIFECYCLE_LIVEDATA)
    kapt(AnnotationProcessorsDependencies.LIFECYCLE)
    implementation(Dependencies.LIFECYCLE_VIEWMODEL)
    implementation(Dependencies.LIFECYCLE_COMMON)

    implementation(Dependencies.HILT_ANDROID)
    kapt(AnnotationProcessorsDependencies.HILT_COMPILER)
    implementation(Dependencies.HILT_VIEWMODEL)
    implementation(Dependencies.HILT_WORK)
    kapt(AnnotationProcessorsDependencies.HILT_ANDROID)
    implementation(Dependencies.GSON)
    implementation(Dependencies.RX_ANDROID)
    implementation(Dependencies.RX_KOTLIN)
    implementation(Dependencies.ROOM)
    implementation(Dependencies.ROOM_KTX)
    kapt(AnnotationProcessorsDependencies.ROOM_COMPILER)
    implementation(Dependencies.ROOM_RX)
    implementation(Dependencies.TIMBER)
    implementation(Dependencies.CHARTS)
    implementation(Dependencies.SWIPE_DECORATOR)
    implementation(Dependencies.VIEW_PAGER)
    implementation(Dependencies.GLIDE)
    kapt(AnnotationProcessorsDependencies.GLIDE)
    implementation(Dependencies.RETROFIT)
    implementation(Dependencies.RETROFIT_RX)
    implementation(Dependencies.RETROFIT_GSON)
    implementation(Dependencies.RETROFIT_SCALARS)
    implementation(Dependencies.JSOUP)
    implementation(Dependencies.WORK)
    implementation(Dependencies.WORK_RX)
    implementation(Dependencies.XIRR)
    implementation(Dependencies.LOTTIE)
    implementation(Dependencies.FIREBASE_CONFIG)
    implementation(Dependencies.FIREBASE_ANALYTICS)
    implementation(Dependencies.FIREBASE_ADS)
    implementation(Dependencies.FIREBASE_CRASHLYTICS)

    testImplementation(TestDependencies.JUNIT)
    androidTestImplementation(TestAndroidDependencies.JUNIT)

}
