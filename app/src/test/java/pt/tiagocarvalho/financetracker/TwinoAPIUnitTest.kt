package pt.tiagocarvalho.financetracker

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import pt.tiagocarvalho.p2p.api.TwinoAPI

class TwinoAPIUnitTest {

    private val sessionId = "sdfsdf"

    @Test
    fun testGetDetails() {
        val result = TwinoAPI.getDetails(sessionId)

        assertNotNull(result)
    }

    @Test
    fun testGetStatements() {
        val result = TwinoAPI.getStatements(null, sessionId)
        assertNotNull(result)
        Assertions.assertTrue(result.statements.isNotEmpty())
    }

    @Test
    fun testGetTodayStatements() {
        val result = TwinoAPI.getTodayStatements(sessionId)
        assertNotNull(result)
        Assertions.assertTrue(result.statements.isEmpty())
    }
}
