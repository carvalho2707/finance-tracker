package pt.tiagocarvalho.financetracker

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import pt.tiagocarvalho.p2p.api.IuvoAPI
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.services.utils.toDate

class IuvoAPIUnitTest {

    @Test
    fun testLogin() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val testObserver = IuvoAPI.authenticate(login).test()
        testObserver.assertNoErrors()
    }

    @Test
    fun testLoginFail() {
        val login = Login(BuildConfig.P2P_EMAIL, "aaa")

        val testObserver = IuvoAPI.authenticate(login).test()
        testObserver.assertNoValues()
    }

    @Test
    fun testGetDetails() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result = IuvoAPI.getDetails(login)

        assertNotNull(result)
    }

    @Test
    fun testGetStatements() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result = IuvoAPI.getStatements("2019-12-12".toDate("yyyy-MM-dd"), login)
        assertNotNull(result)
        Assertions.assertTrue(result.statements.isNotEmpty())
    }

    @Test
    fun testGetTodayStatements() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result = IuvoAPI.getTodayStatements(login)
        assertNotNull(result)
        Assertions.assertTrue(result.statements.isEmpty())
    }
}
