package pt.tiagocarvalho.financetracker

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import pt.tiagocarvalho.p2p.api.EstateGuruAPI
import pt.tiagocarvalho.p2p.api.model.AuthenticationFailedException
import pt.tiagocarvalho.p2p.api.model.Login

class EstateGuruAPIUnitTest {

    @Test
    fun testLogin() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val testObserver = EstateGuruAPI.authenticate(login).test()
        testObserver.assertNoErrors()
    }

    @Test
    fun testLoginFail() {
        val login = Login(BuildConfig.P2P_EMAIL, "aaa")

        val testObserver = EstateGuruAPI.authenticate(login).test()
        testObserver.assertNoValues()
        testObserver.assertError { it is AuthenticationFailedException }
    }

    @Test
    fun testGetDetails() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result = EstateGuruAPI.getDetails(login)

        assertNotNull(result)
    }

    @Test
    fun testGetStatements() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result = EstateGuruAPI.getStatements(null, login)
        assertNotNull(result)
        Assertions.assertTrue(result.statements.isNotEmpty())
    }

    @Test
    fun testGetTodayStatements() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result = EstateGuruAPI.getTodayStatements(login)
        assertNotNull(result)
        Assertions.assertTrue(result.statements.isEmpty())
    }
}
