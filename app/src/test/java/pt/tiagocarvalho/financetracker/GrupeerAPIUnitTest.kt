package pt.tiagocarvalho.financetracker

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import pt.tiagocarvalho.p2p.api.GrupeerAPI

class GrupeerAPIUnitTest {

    private val cookies = ""

    @Test
    fun testGetDetails() {
        val result = GrupeerAPI.getDetails(cookies)
        assertNotNull(result)
    }

    @Test
    fun testGetStatements() {
        val result = GrupeerAPI.getStatements(cookies, null)
        assertNotNull(result)
        assertTrue(result.statements.isNotEmpty())
    }

    @Test
    fun testGetTodayStatements() {
        val result = GrupeerAPI.getTodayStatements(cookies)
        assertNotNull(result)
        assertTrue(result.statements.isEmpty())
    }
}
