package pt.tiagocarvalho.financetracker

import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import pt.tiagocarvalho.p2p.api.BondoraAPI
import pt.tiagocarvalho.p2p.api.model.Login

class BondoraAPIUnitTest {

    private val token = "mSYWiHljAYA0KglO6x7xqaSSnkBMrxEcRUBfWUlCsuh36WTn"

    @Test
    fun testLoginFail() {
        val code = "a_random_code"
        val testObserver =
            BondoraAPI.getInstance(BuildConfig.BONDORA_CLIENT_ID, BuildConfig.BONDORA_CLIENT_SECRET)
                .authenticate(code).test()

        testObserver.assertNoValues()
    }

    @Test
    fun testGetDetails() {
        val login = Login(BuildConfig.P2P_EMAIL, BuildConfig.P2P_PRIMARY_PASSWORD)
        val result =
            BondoraAPI.getInstance(BuildConfig.BONDORA_CLIENT_ID, BuildConfig.BONDORA_CLIENT_SECRET)
                .getDetails(token, login)

        assertNotNull(result)
    }

    @Test
    fun testGetStatements() {
        val result =
            BondoraAPI.getInstance(BuildConfig.BONDORA_CLIENT_ID, BuildConfig.BONDORA_CLIENT_SECRET)
                .getStatements(null, token)
        assertNotNull(result)
        assertTrue(result.statements.isNotEmpty())
    }

    @Test
    fun testGetTodayStatements() {
        val result =
            BondoraAPI.getInstance(BuildConfig.BONDORA_CLIENT_ID, BuildConfig.BONDORA_CLIENT_SECRET)
                .getTodayStatements(token)
        assertNotNull(result)
        assertTrue(result.statements.isEmpty())
    }
}
