package pt.tiagocarvalho.financetracker.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.TwitterAccountDao
import pt.tiagocarvalho.financetracker.data.remote.BondoraService
import pt.tiagocarvalho.financetracker.data.remote.CashService
import pt.tiagocarvalho.financetracker.data.remote.CrowdestorService
import pt.tiagocarvalho.financetracker.data.remote.EstateGuruService
import pt.tiagocarvalho.financetracker.data.remote.GamblingService
import pt.tiagocarvalho.financetracker.data.remote.GrupeerService
import pt.tiagocarvalho.financetracker.data.remote.IuvoService
import pt.tiagocarvalho.financetracker.data.remote.LenderMarketService
import pt.tiagocarvalho.financetracker.data.remote.MintosService
import pt.tiagocarvalho.financetracker.data.remote.PeerberryService
import pt.tiagocarvalho.financetracker.data.remote.RaizeService
import pt.tiagocarvalho.financetracker.data.remote.RobocashService
import pt.tiagocarvalho.financetracker.data.remote.SavingsService
import pt.tiagocarvalho.financetracker.data.remote.TwinoService
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.BondoraRepository
import pt.tiagocarvalho.financetracker.repository.CashRepository
import pt.tiagocarvalho.financetracker.repository.CrowdestorRepository
import pt.tiagocarvalho.financetracker.repository.EstateGuruRepository
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.repository.GrupeerRepository
import pt.tiagocarvalho.financetracker.repository.IuvoRepository
import pt.tiagocarvalho.financetracker.repository.LenderMarketRepository
import pt.tiagocarvalho.financetracker.repository.MintosRepository
import pt.tiagocarvalho.financetracker.repository.PeerberryRepository
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.repository.PreferencesRepository
import pt.tiagocarvalho.financetracker.repository.RaizeRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.repository.RobocashRepository
import pt.tiagocarvalho.financetracker.repository.SavingsRepository
import pt.tiagocarvalho.financetracker.repository.SettingsRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.repository.TwinoRepository
import pt.tiagocarvalho.financetracker.repository.TwitterAccountRepository
import pt.tiagocarvalho.financetracker.repository.UpdatesRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.twitter.api.SearchApi
import pt.tiagocarvalho.twitter.api.impl.SearchApiImpl
import pt.tiagocarvalho.twitter.services.TwitterService

@InstallIn(SingletonComponent::class)
@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideGrupeerRepository(
        grupeerService: GrupeerService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): GrupeerRepository = GrupeerRepository(
        grupeerService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideMintosRepository(
        mintosService: MintosService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): MintosRepository = MintosRepository(
        mintosService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideRobocashRepository(
        robocashService: RobocashService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): RobocashRepository = RobocashRepository(
        robocashService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideRaizeRepository(
        raizeService: RaizeService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): RaizeRepository = RaizeRepository(
        raizeService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun providePeerberryRepository(
        peerberryService: PeerberryService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): PeerberryRepository = PeerberryRepository(
        peerberryService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideSavingsRepository(
        platformDetailsDao: PlatformDetailsDao,
        savingsStatementDao: SavingsStatementDao,
        platformStatementDao: PlatformStatementDao,
        savingsService: SavingsService,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        platformDao: PlatformDao,
        analyticsLogger: AnalyticsLogger
    ): SavingsRepository = SavingsRepository(
        platformDetailsDao,
        savingsStatementDao,
        platformStatementDao,
        savingsService,
        preferencesHelper,
        logger,
        platformDao,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideGamblingRepository(
        platformDetailsDao: PlatformDetailsDao,
        gamblingStatementDao: GamblingStatementDao,
        platformStatementDao: PlatformStatementDao,
        gamblingService: GamblingService,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        platformDao: PlatformDao,
        analyticsLogger: AnalyticsLogger
    ): GamblingRepository = GamblingRepository(
        platformDetailsDao,
        gamblingStatementDao,
        platformStatementDao,
        gamblingService,
        preferencesHelper,
        logger,
        platformDao,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun providePlatformRepository(
        platformDao: PlatformDao,
        savingsAccountDao: SavingsAccountDao,
        gamblingAccountDao: GamblingAccountDao,
        cashAccountDao: CashAccountDao,
        preferencesHelper: PreferencesHelper,
        platformDetailsDao: PlatformDetailsDao,
        context: Context
    ): PlatformRepository = PlatformRepository(
        platformDao,
        savingsAccountDao,
        gamblingAccountDao,
        cashAccountDao,
        preferencesHelper,
        platformDetailsDao,
        context
    )

    @Provides
    @Singleton
    fun provideAccountsRepository(
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        preferencesHelper: PreferencesHelper,
        grupeerRepository: GrupeerRepository,
        mintosRepository: MintosRepository,
        peerberryRepository: PeerberryRepository,
        raizeRepository: RaizeRepository,
        robocashRepository: RobocashRepository,
        savingsRepository: SavingsRepository,
        gamblingRepository: GamblingRepository,
        bondoraRepository: BondoraRepository,
        twinoRepository: TwinoRepository,
        crowdestorRepository: CrowdestorRepository,
        estateGuruRepository: EstateGuruRepository,
        cashRepository: CashRepository,
        iuvoRepository: IuvoRepository,
        lenderMarketRepository: LenderMarketRepository,
        logger: Logger,
        analyticsLogger: AnalyticsLogger,
        platformDetailsRepository: PlatformDetailsRepository
    ): AccountsRepository = AccountsRepository(
        platformDao,
        platformDetailsDao,
        preferencesHelper,
        grupeerRepository,
        mintosRepository,
        peerberryRepository,
        raizeRepository,
        robocashRepository,
        savingsRepository,
        gamblingRepository,
        bondoraRepository,
        twinoRepository,
        crowdestorRepository,
        estateGuruRepository,
        cashRepository,
        iuvoRepository,
        lenderMarketRepository,
        logger,
        analyticsLogger,
        platformDetailsRepository
    )

    @Provides
    @Singleton
    fun providePlatformDetailsRepository(
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper
    ): PlatformDetailsRepository = PlatformDetailsRepository(
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper
    )

    @Provides
    @Singleton
    fun providePreferencesRepository(preferencesHelper: PreferencesHelper): PreferencesRepository =
        PreferencesRepository(preferencesHelper)

    @Provides
    @Singleton
    fun provideStatementsRepository(
        platformDao: PlatformDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper
    ): StatementsRepository =
        StatementsRepository(platformDao, platformStatementDao, preferencesHelper)

    @Provides
    @Singleton
    fun provideSearchApi(
        twitterService: TwitterService
    ): SearchApi = SearchApiImpl(twitterService)

    @Provides
    @Singleton
    fun provideTwitterAccountRepository(
        twitterAccountDao: TwitterAccountDao
    ): TwitterAccountRepository = TwitterAccountRepository(twitterAccountDao)

    @Provides
    @Singleton
    fun provideBondoraRepository(
        bondoraService: BondoraService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): BondoraRepository = BondoraRepository(
        bondoraService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideTwinoRepository(
        twinoService: TwinoService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): TwinoRepository = TwinoRepository(
        twinoService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideCrowdestorRepository(
        crowdestorService: CrowdestorService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): CrowdestorRepository = CrowdestorRepository(
        crowdestorService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideIuvoRepository(
        iuvoService: IuvoService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): IuvoRepository = IuvoRepository(
        iuvoService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideLenderMarketRepository(
        lenderMarketService: LenderMarketService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): LenderMarketRepository = LenderMarketRepository(
        lenderMarketService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideCashRepository(
        platformDetailsDao: PlatformDetailsDao,
        cashStatementDao: CashStatementDao,
        platformStatementDao: PlatformStatementDao,
        cashService: CashService,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        platformDao: PlatformDao,
        analyticsLogger: AnalyticsLogger
    ): CashRepository = CashRepository(
        platformDetailsDao,
        cashStatementDao,
        platformStatementDao,
        cashService,
        preferencesHelper,
        logger,
        platformDao,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideEstateGuruRepository(
        estateGuruService: EstateGuruService,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        preferencesHelper: PreferencesHelper,
        logger: Logger,
        analyticsLogger: AnalyticsLogger
    ): EstateGuruRepository = EstateGuruRepository(
        estateGuruService,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        preferencesHelper,
        logger,
        analyticsLogger
    )

    @Provides
    @Singleton
    fun provideRefreshRepository(
        grupeerRepository: GrupeerRepository,
        mintosRepository: MintosRepository,
        peerberryRepository: PeerberryRepository,
        raizeRepository: RaizeRepository,
        robocashRepository: RobocashRepository,
        savingsRepository: SavingsRepository,
        gamblingRepository: GamblingRepository,
        bondoraRepository: BondoraRepository,
        twinoRepository: TwinoRepository,
        crowdestorRepository: CrowdestorRepository,
        estateGuruRepository: EstateGuruRepository,
        iuvoRepository: IuvoRepository,
        lenderMarketRepository: LenderMarketRepository,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao
    ): RefreshRepository = RefreshRepository(
        grupeerRepository,
        mintosRepository,
        peerberryRepository,
        raizeRepository,
        robocashRepository,
        savingsRepository,
        gamblingRepository,
        bondoraRepository,
        twinoRepository,
        crowdestorRepository,
        estateGuruRepository,
        iuvoRepository,
        lenderMarketRepository,
        platformDetailsDao,
        platformStatementDao
    )

    @Provides
    @Singleton
    fun provideSettingsRepository(
        context: Context,
        logger: Logger
    ): SettingsRepository = SettingsRepository(
        context,
        logger
    )

    @Provides
    @Singleton
    fun provideUpdatesRepository(
        preferencesHelper: PreferencesHelper,
        schedulerProvider: SchedulerProvider,
        platformDao: PlatformDao,
        platformDetailsDao: PlatformDetailsDao,
        platformStatementDao: PlatformStatementDao,
        savingsStatementDao: SavingsStatementDao,
        cashStatementDao: CashStatementDao,
        gamblingStatementDao: GamblingStatementDao,
        logger: Logger
    ): UpdatesRepository = UpdatesRepository(
        preferencesHelper,
        schedulerProvider,
        platformDao,
        platformDetailsDao,
        platformStatementDao,
        savingsStatementDao,
        cashStatementDao,
        gamblingStatementDao,
        logger
    )
}
