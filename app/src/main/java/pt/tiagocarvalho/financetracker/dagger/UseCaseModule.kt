package pt.tiagocarvalho.financetracker.dagger

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.CashRepository
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.repository.PreferencesRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.repository.SavingsRepository
import pt.tiagocarvalho.financetracker.repository.TwitterAccountRepository
import pt.tiagocarvalho.financetracker.ui.accounts.AccountsUseCase
import pt.tiagocarvalho.financetracker.ui.alerts.AlertsUseCase
import pt.tiagocarvalho.financetracker.ui.auth.peerberry.PeerberryAuthUseCase
import pt.tiagocarvalho.financetracker.ui.auth.raize.RaizeAuthUseCase
import pt.tiagocarvalho.financetracker.ui.auth.robocash.RobocashAuthUseCase
import pt.tiagocarvalho.financetracker.ui.auth.twino.TwinoAuthUseCase
import pt.tiagocarvalho.financetracker.ui.details.cash.CashUseCase
import pt.tiagocarvalho.financetracker.ui.details.gambling.GamblingUseCase
import pt.tiagocarvalho.financetracker.ui.details.p2p.P2pDetailsUseCase
import pt.tiagocarvalho.financetracker.ui.details.savings.SavingsUseCase
import pt.tiagocarvalho.financetracker.ui.main.MainUseCase
import pt.tiagocarvalho.financetracker.ui.onboarding.OnboardingUseCase
import pt.tiagocarvalho.financetracker.ui.splash.SplashUseCase
import pt.tiagocarvalho.financetracker.ui.tweets.TwitterUseCase
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.retrofit.PeerberryApi
import pt.tiagocarvalho.p2p.services.retrofit.RaizeApi
import pt.tiagocarvalho.p2p.services.retrofit.TwinoApi
import pt.tiagocarvalho.twitter.api.SearchApi

@InstallIn(SingletonComponent::class)
@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun provideP2pDetailsUseCase(
        refreshRepository: RefreshRepository
    ) = P2pDetailsUseCase(
        refreshRepository
    )

    @Provides
    @Singleton
    fun provideAccountsUseCase(
        accountsRepository: AccountsRepository,
        preferencesRepository: PreferencesRepository,
        refreshRepository: RefreshRepository,
        platformDetailsRepository: PlatformDetailsRepository
    ) = AccountsUseCase(
        accountsRepository,
        preferencesRepository,
        refreshRepository,
        platformDetailsRepository
    )

    @Provides
    @Singleton
    fun provideAlertsUseCase(
        platformDetailsRepository: PlatformDetailsRepository,
        refreshRepository: RefreshRepository,
        accountsRepository: AccountsRepository
    ) = AlertsUseCase(
        platformDetailsRepository,
        refreshRepository,
        accountsRepository
    )

    @Provides
    @Singleton
    fun provideTwitterUseCase(
        searchApi: SearchApi,
        twitterAccountRepository: TwitterAccountRepository
    ) = TwitterUseCase(
        searchApi,
        twitterAccountRepository
    )

    @Provides
    @Singleton
    fun provideSplashUseCase(
        preferencesHelper: PreferencesHelper
    ) = SplashUseCase(
        preferencesHelper
    )

    @Provides
    @Singleton
    fun provideOnboardingUseCase(
        preferencesHelper: PreferencesHelper
    ) = OnboardingUseCase(preferencesHelper)

    @Provides
    @Singleton
    fun provideMainUseCase(
        context: Context
    ) = MainUseCase(
        context
    )

    @Provides
    @Singleton
    fun provideSavingsUseCase(
        savingsRepository: SavingsRepository,
        context: Context
    ) = SavingsUseCase(
        savingsRepository,
        context
    )

    @Provides
    @Singleton
    fun provideCashUseCase(
        cashRepository: CashRepository,
        context: Context
    ) = CashUseCase(
        cashRepository,
        context
    )

    @Provides
    @Singleton
    fun provideGamblingUseCase(
        gamblingRepository: GamblingRepository,
        context: Context
    ) = GamblingUseCase(
        gamblingRepository,
        context
    )

    @Provides
    @Singleton
    fun providePeerberryAuthUseCase(
        peerberryApi: PeerberryApi,
        logger: Logger
    ) = PeerberryAuthUseCase(
        peerberryApi,
        logger
    )

    @Provides
    @Singleton
    fun provideTwinoAuthUseCase(
        twinoApi: TwinoApi,
        logger: Logger
    ) = TwinoAuthUseCase(
        twinoApi,
        logger
    )

    @Provides
    @Singleton
    fun provideRobocashAuthUseCase(
        logger: Logger
    ) = RobocashAuthUseCase(
        logger
    )

    @Provides
    @Singleton
    fun provideRaizeAuthUseCase(
        raizeApi: RaizeApi,
        preferencesHelper: PreferencesHelper,
        logger: Logger
    ) = RaizeAuthUseCase(
        raizeApi,
        preferencesHelper,
        logger
    )
}
