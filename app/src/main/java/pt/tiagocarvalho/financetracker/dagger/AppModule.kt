package pt.tiagocarvalho.financetracker.dagger

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import androidx.room.RoomDatabase.Callback
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.WorkManager
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelperImpl
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_10_11
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_11_12
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_12_13
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_13_14
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_1_2
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_2_3
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_3_4
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_4_5
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_5_6
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_6_7
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_7_8
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_8_9
import pt.tiagocarvalho.financetracker.data.local.storage.AppDatabase.Companion.MIGRATION_9_10
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.TwitterAccountDao
import pt.tiagocarvalho.financetracker.data.remote.BondoraService
import pt.tiagocarvalho.financetracker.data.remote.CashService
import pt.tiagocarvalho.financetracker.data.remote.CrowdestorService
import pt.tiagocarvalho.financetracker.data.remote.EstateGuruService
import pt.tiagocarvalho.financetracker.data.remote.GamblingService
import pt.tiagocarvalho.financetracker.data.remote.GrupeerService
import pt.tiagocarvalho.financetracker.data.remote.IuvoService
import pt.tiagocarvalho.financetracker.data.remote.LenderMarketService
import pt.tiagocarvalho.financetracker.data.remote.MintosService
import pt.tiagocarvalho.financetracker.data.remote.PeerberryService
import pt.tiagocarvalho.financetracker.data.remote.RaizeService
import pt.tiagocarvalho.financetracker.data.remote.RobocashService
import pt.tiagocarvalho.financetracker.data.remote.SavingsService
import pt.tiagocarvalho.financetracker.data.remote.TwinoService
import pt.tiagocarvalho.financetracker.data.remote.impl.BondoraServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.CashServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.CrowdestorServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.EstateGuruServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.GamblingServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.GrupeerServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.IuvoServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.LenderMarketServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.MintosServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.PeerberryServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.RaizeServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.RobocashServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.SavingsServiceImpl
import pt.tiagocarvalho.financetracker.data.remote.impl.TwinoServiceImpl
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.AppSchedulerProvider
import pt.tiagocarvalho.financetracker.utils.Constants.TWITTER_BASE_URL
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLoggerImpl
import pt.tiagocarvalho.financetracker.utils.log.CrashlyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.retrofit.PeerberryApi
import pt.tiagocarvalho.p2p.services.retrofit.RaizeApi
import pt.tiagocarvalho.p2p.services.retrofit.TwinoApi
import pt.tiagocarvalho.twitter.services.TwitterService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

@InstallIn(SingletonComponent::class)
@Module
class AppModule {

    @Provides
    fun provideContext(application: Application): Context = application

    @Provides
    @Singleton
    fun providePreferencesHelper(context: Context): PreferencesHelper =
        PreferencesHelperImpl(context.getSharedPreferences("data", Context.MODE_PRIVATE))

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences =
        context.getSharedPreferences("data", Context.MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideSchedulerProvider(): SchedulerProvider = AppSchedulerProvider()

    @Provides
    @Singleton
    fun provideAppExecutors(): AppExecutors = AppExecutors()

    @Provides
    @Singleton
    fun provideWorkManager(context: Context): WorkManager = WorkManager.getInstance(context)

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context): AppDatabase {
        val rdc: Callback = object : Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                db.execSQL("INSERT INTO twitter_account VALUES('MintosPlatform', 1)")
                db.execSQL("INSERT INTO twitter_account VALUES('Grupeer_com', 1)")
                db.execSQL("INSERT INTO twitter_account VALUES('PeerBerry', 1)")
                db.execSQL("INSERT INTO twitter_account VALUES('Robocash1', 1)")
                db.execSQL("INSERT INTO twitter_account VALUES('p2pMillionaire', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('ExploreP2P', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('Bondora_com', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('TWINO_eu', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('Viventor_P2P', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('Zopa', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('Seedrs', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('FASTINVEST_COM', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('Estate_Guru', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('Crowdestate_eu', 0)")
                db.execSQL("INSERT INTO twitter_account VALUES('VIAINVEST_', 0)")
            }
        }

        return Room.databaseBuilder(context, AppDatabase::class.java, "financetracker.db")
            .addCallback(rdc)
            .addMigrations(
                MIGRATION_1_2,
                MIGRATION_2_3,
                MIGRATION_3_4,
                MIGRATION_4_5,
                MIGRATION_5_6,
                MIGRATION_6_7,
                MIGRATION_7_8,
                MIGRATION_8_9,
                MIGRATION_9_10,
                MIGRATION_10_11,
                MIGRATION_11_12,
                MIGRATION_12_13,
                MIGRATION_13_14
            )
            .build()
    }

    @Singleton
    @Provides
    fun providePlatformDetailsDao(db: AppDatabase): PlatformDetailsDao = db.platformDetailsDao()

    @Singleton
    @Provides
    fun providePlatformStatementDao(db: AppDatabase): PlatformStatementDao =
        db.platformStatementDao()

    @Singleton
    @Provides
    fun provideSavingsAccountDao(db: AppDatabase): SavingsAccountDao = db.savingsAccountDao()

    @Singleton
    @Provides
    fun provideGamblingAccountDao(db: AppDatabase): GamblingAccountDao = db.gamblingAccountDao()

    @Singleton
    @Provides
    fun provideCashAccountDao(db: AppDatabase): CashAccountDao = db.cashAccountDao()

    @Singleton
    @Provides
    fun provideGamblingStatementDao(db: AppDatabase): GamblingStatementDao =
        db.gamblingStatementDao()

    @Singleton
    @Provides
    fun provideSavingsStatementDao(db: AppDatabase): SavingsStatementDao = db.savingsStatementDao()

    @Singleton
    @Provides
    fun provideCashStatementDao(db: AppDatabase): CashStatementDao = db.cashStatementDao()

    @Singleton
    @Provides
    fun providePlatformDao(db: AppDatabase): PlatformDao = db.platformDao()

    @Singleton
    @Provides
    fun provideTwitterAccountDao(db: AppDatabase): TwitterAccountDao = db.twitterAccountDao()

    @Singleton
    @Provides
    fun provideMintosService(): MintosService = MintosServiceImpl()

    @Singleton
    @Provides
    fun provideGrupeerService(): GrupeerService = GrupeerServiceImpl()

    @Singleton
    @Provides
    fun providePeerberryService(): PeerberryService = PeerberryServiceImpl()

    @Singleton
    @Provides
    fun provideRaizeService(): RaizeService = RaizeServiceImpl()

    @Singleton
    @Provides
    fun provideRobocashService(): RobocashService = RobocashServiceImpl()

    @Singleton
    @Provides
    fun provideSavingsService(
        savingsAccountDao: SavingsAccountDao,
        savingsStatementDao: SavingsStatementDao
    ): SavingsService = SavingsServiceImpl(savingsStatementDao, savingsAccountDao)

    @Singleton
    @Provides
    fun provideGamblingService(
        gamblingStatementDao: GamblingStatementDao
    ): GamblingService = GamblingServiceImpl(gamblingStatementDao)

    @Singleton
    @Provides
    fun provideBondoraService(): BondoraService = BondoraServiceImpl()

    @Singleton
    @Provides
    fun provideTwinoService(): TwinoService = TwinoServiceImpl()

    @Singleton
    @Provides
    fun provideCrowdestorService(): CrowdestorService = CrowdestorServiceImpl()

    @Singleton
    @Provides
    fun provideEstateGuruService(): EstateGuruService = EstateGuruServiceImpl()

    @Singleton
    @Provides
    fun provideIuvoService(): IuvoService = IuvoServiceImpl()

    @Singleton
    @Provides
    fun provideLenderMarketService(): LenderMarketService = LenderMarketServiceImpl()

    @Singleton
    @Provides
    fun provideCashService(
        cashStatementDao: CashStatementDao
    ): CashService = CashServiceImpl(cashStatementDao)

    @Singleton
    @Provides
    fun provideLogger(): Logger = CrashlyticsLogger()

    @Singleton
    @Provides
    fun provideAnalyticsLogger(context: Context): AnalyticsLogger = AnalyticsLoggerImpl(context)

    @Singleton
    @Provides
    fun provideTwitterService(): TwitterService {
        val gson: Gson = GsonBuilder()
            .setDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy")
            .create()

        return Retrofit.Builder()
            .baseUrl(TWITTER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(TwitterService::class.java)
    }

    @Singleton
    @Provides
    fun providePeerberryApi(): PeerberryApi = Retrofit.Builder()
        .baseUrl("https://api.peerberry.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(PeerberryApi::class.java)

    @Singleton
    @Provides
    fun provideTwinoApi(): TwinoApi = Retrofit.Builder()
        .baseUrl("https://www.twino.eu/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(TwinoApi::class.java)

    @Singleton
    @Provides
    fun provideRaizeApi(): RaizeApi {
        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd")
            .create()

        return Retrofit.Builder()
            .baseUrl("https://api.raize.pt/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(RaizeApi::class.java)
    }
}
