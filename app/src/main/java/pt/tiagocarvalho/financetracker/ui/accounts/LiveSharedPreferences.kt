package pt.tiagocarvalho.financetracker.ui.accounts

import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import io.reactivex.Observable
import io.reactivex.ObservableOnSubscribe
import javax.inject.Inject

class LiveSharedPreferences @Inject constructor(private val preferences: SharedPreferences) {

    private lateinit var listener: OnSharedPreferenceChangeListener
    private val updates: Observable<String>

    init {
        updates = Observable.create(
            ObservableOnSubscribe<String> { emitter ->
                listener = OnSharedPreferenceChangeListener { _, key -> emitter.onNext(key) }

                emitter.setCancellable {
                    preferences.unregisterOnSharedPreferenceChangeListener(
                        listener
                    )
                }

                preferences.registerOnSharedPreferenceChangeListener(listener)
            }
        ).share()
    }

    fun listenUpdatesOnly(keys: List<String>): MultiPreferenceAny =
        MultiPreferenceAny(updates, keys)
}
