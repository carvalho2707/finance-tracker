package pt.tiagocarvalho.financetracker.ui.billing

import android.app.Activity
import android.app.Application
import androidx.lifecycle.LiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.billing.localdb.AugmentedSkuDetails
import pt.tiagocarvalho.financetracker.billing.localdb.Premium
import pt.tiagocarvalho.financetracker.billing.repository.BillingRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import timber.log.Timber

@HiltViewModel
class BillingViewModel @Inject constructor(application: Application) : BaseViewModel() {

    val premiumLiveData: LiveData<Premium>
    val inappSkuDetailsListLiveData: LiveData<List<AugmentedSkuDetails>>
    val errorEvent: LiveData<String>

    private val repository: BillingRepository = BillingRepository.getInstance(application)

    init {
        repository.startDataSourceConnections()
        premiumLiveData = repository.premiumLiveData
        inappSkuDetailsListLiveData = repository.inappSkuDetailsListLiveData
        errorEvent = repository.errorEvent
    }

    override fun onCleared() {
        super.onCleared()
        Timber.d("onCleared")
        repository.endDataSourceConnections()
    }

    fun makePurchase(activity: Activity, augmentedSkuDetails: AugmentedSkuDetails) {
        repository.launchBillingFlow(activity, augmentedSkuDetails)
    }
}
