package pt.tiagocarvalho.financetracker.ui.auth.peerberry

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class PeerberryAuthDialogFragmentModule {

    @Provides
    fun provideViewModel(
        peerberryAuthUseCase: PeerberryAuthUseCase,
        schedulerProvider: SchedulerProvider
    ) = PeerberryAuthViewModel(
        peerberryAuthUseCase,
        schedulerProvider
    )
}
