package pt.tiagocarvalho.financetracker.ui.onboarding

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import pt.tiagocarvalho.financetracker.databinding.ItemOnboardingBinding

class OnboardingViewPagerAdapter :
    ListAdapter<OnboardingItem, RecyclerView.ViewHolder>(DataItemDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        OnboardingViewHolder(
            ItemOnboardingBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val plant = getItem(position)
        (holder as OnboardingViewHolder).bind(plant)
    }

    class OnboardingViewHolder(
        private val binding: ItemOnboardingBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: OnboardingItem) {
            binding.apply {
                binding.title.setText(item.title)
                binding.body.setText(item.body)
                binding.animation.setAnimation(item.icon)
                executePendingBindings()
            }
        }
    }
}

private class DataItemDiffCallback : DiffUtil.ItemCallback<OnboardingItem>() {

    override fun areItemsTheSame(oldItem: OnboardingItem, newItem: OnboardingItem): Boolean =
        oldItem.title == newItem.title

    override fun areContentsTheSame(oldItem: OnboardingItem, newItem: OnboardingItem): Boolean =
        oldItem == newItem
}
