package pt.tiagocarvalho.financetracker.ui.base

import android.app.Dialog
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder

abstract class BaseDialogFragment : DialogFragment() {

    fun getClipboardManager() =
        activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // We want to create a dialog, but we also want to use DataBinding for the content view.
        // We can do that by making an empty dialog and adding the content later.
        return MaterialAlertDialogBuilder(requireContext()).create()
    }
}
