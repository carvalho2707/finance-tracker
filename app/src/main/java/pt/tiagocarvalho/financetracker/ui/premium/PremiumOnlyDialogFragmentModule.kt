package pt.tiagocarvalho.financetracker.ui.premium

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
class PremiumOnlyDialogFragmentModule {

    @Provides
    fun provideViewModel(): PremiumOnlyViewModel = PremiumOnlyViewModel()
}
