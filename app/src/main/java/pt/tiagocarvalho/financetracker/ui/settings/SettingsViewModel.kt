package pt.tiagocarvalho.financetracker.ui.settings

import androidx.lifecycle.MutableLiveData
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Completable
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.repository.SettingsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.isNoNetworkException

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val settingsRepository: SettingsRepository
) : BaseViewModel() {

    val nextPlatforms: MutableLiveData<String> = MutableLiveData()
    val networkUsage: MutableLiveData<String> = MutableLiveData()

    fun loadNextPlatforms() {
        compositeDisposable.add(
            Completable.fromAction {
                val remoteConfig = FirebaseRemoteConfig.getInstance()
                remoteConfig.setConfigSettingsAsync(
                    FirebaseRemoteConfigSettings.Builder()
                        .build()
                )
                remoteConfig.fetch().addOnCompleteListener {
                    remoteConfig.activate().addOnCompleteListener {
                        val value = remoteConfig.getString(FIREBASE_KEY_NEXT_PLATFORMS)
                        nextPlatforms.postValue(value)
                    }
                }
            }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({}, { handleError(it) })
        )
    }

    fun loadNetworkUsage() {
        compositeDisposable.add(
            settingsRepository.getNetworkUsage(BuildConfig.APPLICATION_ID)
                .subscribeOn(schedulerProvider.io())
                .doOnSuccess { networkUsage.postValue(it) }
                .subscribe()
        )
    }

    private fun handleError(throwable: Throwable?) {
        if (throwable.isNoNetworkException()) {
            nextPlatforms.postValue("No internet connection. Try again later!")
        } else {
            nextPlatforms.postValue("An error occurred. Try again later!")
        }
    }

    companion object {
        private const val FIREBASE_KEY_NEXT_PLATFORMS = "next_platforms"
    }
}
