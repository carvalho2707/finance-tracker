package pt.tiagocarvalho.financetracker.ui.management.request_platform

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import androidx.core.view.marginBottom
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePaddingRelative
import androidx.databinding.ObservableFloat
import androidx.fragment.app.viewModels
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentRequestPlatformBinding
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.ui.premium.PremiumOnlyDialogFragment
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.Utils.slideOffsetToAlpha
import pt.tiagocarvalho.financetracker.utils.doOnApplyWindowInsets
import pt.tiagocarvalho.financetracker.utils.lerp
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_COLLAPSED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_EXPANDED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_HIDDEN

@AndroidEntryPoint
class RequestPlatformFragment : BaseFragment() {

    companion object {
        private const val ALPHA_CHANGEOVER = 0.33f
        private const val ALPHA_DESC_MAX = 0f
        private const val ALPHA_HEADER_MAX = 0.67f
        private const val ALPHA_CONTENT_END_ALPHA = 1f
        private const val ALPHA_CONTENT_START_ALPHA = 0.1f
    }

    private lateinit var binding: FragmentRequestPlatformBinding
    private lateinit var behavior: BottomSheetBehavior<*>

    private val requestPlatformViewModel: RequestPlatformViewModel by viewModels()
    private val contentFadeInterpolator = LinearOutSlowInInterpolator()
    private var headerAlpha = ObservableFloat(1f)
    private var descriptionAlpha = ObservableFloat(1f)
    private var recyclerviewAlpha = ObservableFloat(1f)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentRequestPlatformBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            viewModel = requestPlatformViewModel
            headerAlpha = this@RequestPlatformFragment.headerAlpha
            descriptionAlpha = this@RequestPlatformFragment.descriptionAlpha
            recyclerviewAlpha = this@RequestPlatformFragment.recyclerviewAlpha
        }

        // Pad the bottom of the RecyclerView so that the content scrolls up above the nav bar
        binding.recyclerviewFilter.doOnApplyWindowInsets { v, insets, padding ->
            v.updatePaddingRelative(bottom = padding.bottom + insets.systemWindowInsetBottom)
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        behavior = BottomSheetBehavior.from(binding.filterSheet)

        // Update the peek and margins so that it scrolls and rests within sys ui
        val peekHeight = behavior.peekHeight
        val marginBottom = binding.root.marginBottom
        binding.root.doOnApplyWindowInsets { v, insets, _ ->
            val gestureInsets = insets.systemGestureInsets
            // Update the peek height so that it is above the navigation bar
            behavior.peekHeight = gestureInsets.bottom + peekHeight

            v.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = marginBottom + insets.systemWindowInsetTop
            }
        }

        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                updateFilterContentsAlpha(slideOffset)
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                super.onStateChanged(bottomSheet, newState)

                if (newState == STATE_EXPANDED) {
                    requestPlatformViewModel.premium.value?.let {
                        if (it) {
                            binding.request.isEnabled = true
                        } else {
                            showPopUpPremium()
                        }
                    }
                    binding.animation.playAnimation()
                } else if (newState == STATE_COLLAPSED) {
                    binding.inputPlatform.text?.clear()
                    Utils.hideKeyboard(requireContext(), binding.root)
                }
            }
        })

        binding.collapseArrow.setOnClickListener {
            behavior.state = if (behavior.skipCollapsed) STATE_HIDDEN else STATE_COLLAPSED
        }

        binding.expand.setOnClickListener {
            behavior.state = STATE_EXPANDED
        }

        requestPlatformViewModel.back.observe(viewLifecycleOwner) {
            behavior.state = STATE_COLLAPSED
        }

        requestPlatformViewModel.navigateToBilling.observe(viewLifecycleOwner) {
            findNavController().navigate(MainDirections.actionGlobalBilling())
        }

        // This fragment is in the layout of a parent fragment, so its view hierarchy is restored
        // when the parent's hierarchy is restored. However, the dispatch order seems to traverse
        // child fragments first, meaning the views we care about have not actually been restored
        // when onViewStateRestored is called (otherwise we would do this there).
        binding.filterSheet.doOnLayout {
            val slideOffset = when (behavior.state) {
                STATE_EXPANDED -> 1f
                STATE_COLLAPSED -> 0f
                else /*BottomSheetBehavior.STATE_HIDDEN*/ -> -1f
            }
            updateFilterContentsAlpha(slideOffset)
        }

        requestPlatformViewModel.load()
    }

    private fun showPopUpPremium() {
        PremiumOnlyDialogFragment.newInstance(
            R.string.premium_title_request_platforms,
            R.string.premium_subtitle_request_platforms
        ).show(
            requireActivity().supportFragmentManager,
            "dialog_only_premium"
        )
    }

    private fun updateFilterContentsAlpha(slideOffset: Float) {
        // If we have filter set, we will crossfade the header and description views.
        // Alpha of normal header views increases as the sheet expands, while alpha of
        // description views increases as the sheet collapses. To prevent overlap, we use
        // a threshold at which the views "trade places".
        headerAlpha.set(slideOffsetToAlpha(slideOffset, ALPHA_CHANGEOVER, ALPHA_HEADER_MAX))
        descriptionAlpha.set(slideOffsetToAlpha(slideOffset, ALPHA_CHANGEOVER, ALPHA_DESC_MAX))
        // Due to the content view being visible below the navigation bar, we apply a short alpha
        // transition
        recyclerviewAlpha.set(
            lerp(
                ALPHA_CONTENT_START_ALPHA, ALPHA_CONTENT_END_ALPHA,
                contentFadeInterpolator.getInterpolation(slideOffset)
            )
        )
    }

    override fun onToolbarItemClicked(menuItem: MenuItem) = false
}
