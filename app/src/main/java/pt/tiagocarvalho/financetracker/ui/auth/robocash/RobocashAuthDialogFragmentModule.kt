package pt.tiagocarvalho.financetracker.ui.auth.robocash

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class RobocashAuthDialogFragmentModule {

    @Provides
    fun provideViewModel(
        robocashAuthUseCase: RobocashAuthUseCase,
        schedulerProvider: SchedulerProvider
    ) = RobocashAuthViewModel(
        robocashAuthUseCase,
        schedulerProvider
    )
}
