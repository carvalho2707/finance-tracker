package pt.tiagocarvalho.financetracker.ui.details.p2p

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.annotation.StringRes
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentDataBindingComponent
import pt.tiagocarvalho.financetracker.databinding.FragmentP2pDetailsBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.CANCEL
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.ui.details.common.DetailsAdapter
import pt.tiagocarvalho.financetracker.ui.details.common.PastDayAdapter
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.financetracker.webview.BaseWebViewNavigator
import pt.tiagocarvalho.financetracker.widget.DotsIndicatorDecoration
import pt.tiagocarvalho.financetracker.widget.SpacingItemDecoration
import pt.tiagocarvalho.financetracker.widget.StartSnapHelper
import pt.tiagocarvalho.p2p.api.model.Login

@AndroidEntryPoint
class P2PDetailsFragment : BaseFragment(), BaseWebViewNavigator {

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentP2pDetailsBinding

    private val p2PDetailsViewModel: P2PDetailsViewModel by viewModels()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    private val params by navArgs<P2PDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentP2pDetailsBinding.inflate(inflater, container, false)

        binding.apply {
            viewModel = p2PDetailsViewModel
            lifecycleOwner = viewLifecycleOwner
            platformEnum = PlatformEnum.valueOf(params.name)
            var isToolbarShown = true
            var scrollRange = -1
            appbar.addOnOffsetChangedListener(
                AppBarLayout.OnOffsetChangedListener { barLayout, verticalOffset ->
                    if (scrollRange == -1) {
                        scrollRange = barLayout?.totalScrollRange!!
                    }
                    if (scrollRange + verticalOffset == 0) {
                        toolbarTitle.visibility = View.VISIBLE
                        isToolbarShown = true
                    } else if (isToolbarShown) {
                        toolbarTitle.visibility = View.GONE
                        isToolbarShown = false
                    }
                }
            )

            toolbar.setNavigationOnClickListener { view ->
                view.findNavController().navigateUp()
            }
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerToolbarWithNavigation(binding.toolbar)

        val detailsAdapter = DetailsAdapter()
        val adapter = PastDayAdapter(
            dataBindingComponent,
            appExecutors,
            requireContext()
        )

        setupUi(detailsAdapter, adapter)

        p2PDetailsViewModel.status.observe(viewLifecycleOwner) { status ->
            if (status == Status.LOADING) {
                binding.animationView.visibility = View.VISIBLE
                binding.webView.visibility = View.GONE
                binding.animationView.setAnimation(R.raw.loading)
                binding.animationView.repeatCount = -1
                binding.animationView.playAnimation()
            } else binding.animationView.repeatCount = 0
        }

        p2PDetailsViewModel.tfaRequired.observe(viewLifecycleOwner) {
            if (AuthDialogUtils.belongsToAuthDialog(PlatformEnum.valueOf(params.name))) {
                loadAuthDialog(it)
            } else {
                loadWebView(it)
            }
        }

        p2PDetailsViewModel.statements.observe(viewLifecycleOwner) { statements ->
            scheduleAnimation(adapter)
            adapter.submitList(statements)
        }

        p2PDetailsViewModel.dataList.observe(viewLifecycleOwner) { data ->
            scheduleAnimation(adapter)
            detailsAdapter.submitList(data)
        }

        p2PDetailsViewModel.issues.observe(viewLifecycleOwner) {
            if (it) showIssuesDialog()
        }

        p2PDetailsViewModel.showErrorDialog.observe(viewLifecycleOwner) {
            showErrorDialog(it)
        }

        p2PDetailsViewModel.loadInfo(params.name, false)
    }

    private fun setupUi(
        detailsAdapter: DetailsAdapter,
        pastDayAdapter: PastDayAdapter
    ) {

        binding.rvData.apply {
            addItemDecoration(
                SpacingItemDecoration(
                    requireContext(),
                    R.dimen.spacing_data_adapter
                )
            )
            addItemDecoration(DotsIndicatorDecoration(requireContext()))
            StartSnapHelper().attachToRecyclerView(this)
            adapter = detailsAdapter
        }

        binding.rvDetails.apply {
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
            adapter = pastDayAdapter
        }
    }

    private fun showIssuesDialog() {
        showDialog(R.string.dialog_statements_issue_title, R.string.dialog_statements_issue_message)
    }

    private fun showErrorDialog(isNetwork: Boolean) {
        if (isNetwork) {
            showDialog(R.string.dialog_statements_issue_title, R.string.no_network_message)
        } else {
            showDialog(R.string.dialog_generic_issue_title, R.string.dialog_generic_issue_message)
        }
    }

    private fun showDialog(@StringRes title: Int, @StringRes message: Int) {
        val dialog = MaterialAlertDialogBuilder(requireContext())
            .setTitle(getString(title))
            .setMessage(getString(message))
            .setPositiveButton(getString(R.string.dismiss)) { dialog, _ ->
                dialog.dismiss()
            }
            .setNegativeButton(getString(R.string.ask_in_telegram)) { dialog, _ ->
                startTelegram()
                dialog.dismiss()
            }
            .create()
        dialog.show()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(login: Login) {
        val platformEnum = PlatformEnum.valueOf(params.name)
        val webViewClient =
            BaseWebViewClient.getClient(
                requireContext(),
                layoutInflater,
                platformEnum,
                login,
                this,
                isLogin = false,
                frequentUpdates = true
            )

        binding.animationView.visibility = View.INVISIBLE
        binding.webView.visibility = View.VISIBLE
        binding.webView.webViewClient = webViewClient

        val webSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.userAgentString = Constants.USER_AGENT_VALUE

        binding.webView.loadUrl(webViewClient.getInitialUrl())
    }

    private fun loadAuthDialog(login: Login) {
        AuthDialogUtils.showAuthDialogFragment(
            PlatformEnum.valueOf(params.name),
            login.username,
            login.password,
            this@P2PDetailsFragment
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val platform = PlatformEnum.valueOf(data?.getStringExtra(PLATFORM)!!)
        if (resultCode == SUCCESS) {
            p2PDetailsViewModel.loadInfo(true, WebViewData(platform, data.getStringExtra(TOKEN)!!))
        } else if (resultCode == INVALID_USERNAME_PASSWORD_CODE ||
            resultCode == UNKNOWN_ERROR_CODE ||
            resultCode == CANCEL
        ) {
            p2PDetailsViewModel.loadInfoOffline(PlatformEnum.valueOf(params.name))
        }
    }

    override fun loadInfo(webViewData: WebViewData?) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            binding.animationView.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
        }
        p2PDetailsViewModel.loadInfo(true, webViewData!!)
    }

    override fun onError(platformEnum: PlatformEnum, errorCode: Int) {
        // NO NETWORK
        p2PDetailsViewModel.loadInfoOffline(platformEnum)
    }

    override fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String?
    ) = Unit

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_refresh -> p2PDetailsViewModel.loadInfo(params.name, true)
            R.id.menu_website -> {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(PlatformEnum.valueOf(params.name).url)
                startActivity(i)
            }
            R.id.menu_reset_statements -> p2PDetailsViewModel.resetStatements(params.name)
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun startTelegram() {
        try {
            val uri = Uri.parse("https://t.me/inWalletOfficial")
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        } catch (exception: ActivityNotFoundException) {
            exception.printStackTrace()
            view?.let {
                Snackbar.make(it, R.string.generic_error_message, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun scheduleAnimation(adapter: PastDayAdapter) {
        val controller =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        binding.rvDetails.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        binding.rvDetails.scheduleLayoutAnimation()
    }
}
