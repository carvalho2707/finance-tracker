package pt.tiagocarvalho.financetracker.ui.alerts

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class AlertsFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        logger: Logger,
        alertsUseCase: AlertsUseCase
    ): AlertsViewModel = AlertsViewModel(
        schedulerProvider,
        logger,
        alertsUseCase
    )
}
