package pt.tiagocarvalho.financetracker.ui.statistics

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import java.util.EnumMap
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@HiltViewModel
class StatisticsViewModel @Inject constructor(
    private val refreshRepository: RefreshRepository,
    private val accountsRepository: AccountsRepository,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    var status: MutableLiveData<Status> = MutableLiveData()

    var tfaRequired: MutableLiveData<Platform> = MutableLiveData()
    private var requiredPlatforms: MutableList<Platform> = mutableListOf()
    private var loadedData: MutableMap<PlatformEnum, WebViewData> =
        EnumMap(PlatformEnum::class.java)

    fun loadRefreshNeeded(force: Boolean) {
        compositeDisposable.add(
            Single.zip(
                refreshRepository.isRefreshDetailsNeeded(force),
                refreshRepository.isRefreshStatementsNeeded(),
                { t1: Boolean, t2: Boolean -> t1 || t2 }
            )
                .doOnSuccess {
                    if (it) {
                        loadTwoFactorPlatforms()
                    } else {
                        status.postValue(Status.SUCCESS)
                    }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .subscribe()

        )
    }

    private fun loadTwoFactorPlatforms() {
        compositeDisposable.add(
            accountsRepository.getTwoFactorPlatforms()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            requiredPlatforms = it.toMutableList()
                            tfaRequired.value = requiredPlatforms.first()
                            requiredPlatforms.removeAt(0)
                        } else {
                            refreshDetails()
                        }
                    },
                    {},
                    {
                        refreshDetails()
                    }
                )
        )
    }

    fun loadNextAuth(webViewData: WebViewData?) {
        webViewData?.let {
            loadedData[it.platform] = it
        }
        if (requiredPlatforms.size > 0) {
            tfaRequired.postValue(requiredPlatforms.first())
            requiredPlatforms.removeAt(0)
        } else {
            requiredPlatforms = mutableListOf()
            tfaRequired = MutableLiveData()
            refreshDetails()
        }
    }

    private fun refreshDetails() {
        compositeDisposable.add(
            refreshRepository.refreshDetails(loadedData)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnComplete { status.postValue(Status.SUCCESS) }
                .subscribe({}, { it.printStackTrace() })
        )
    }
}
