package pt.tiagocarvalho.financetracker.ui.new_transaction

import android.animation.Animator
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import java.util.Calendar.DAY_OF_MONTH
import java.util.Calendar.MONTH
import java.util.Calendar.YEAR
import java.util.Calendar.getInstance
import java.util.Locale.getDefault
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentAddTransactionBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Utils.hideKeyboard
import pt.tiagocarvalho.p2p.services.utils.Utils

@AndroidEntryPoint
class AddTransactionFragment : BaseFragment() {

    private lateinit var binding: FragmentAddTransactionBinding
    private lateinit var platformEnum: PlatformEnum

    private val params by navArgs<AddTransactionFragmentArgs>()
    private val addTransactionViewModel: AddTransactionViewModel by viewModels()
    private var mYear: Int? = null
    private var mMonth: Int? = null
    private var mDay: Int? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentAddTransactionBinding.inflate(inflater, container, false)

        binding.apply {
            viewModel = addTransactionViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        registerToolbarWithNavigation(binding.toolbar)

        platformEnum = PlatformEnum.valueOf(params.platform)

        addTransactionViewModel.status.observe(viewLifecycleOwner) {
            when (it) {
                Status.LOADING -> {
                    hideKeyboard(requireContext(), view)
                    showLoading()
                }
                Status.SUCCESS -> showSuccess()
                Status.ERROR -> showError()
                else -> return@observe
            }
        }

        addTransactionViewModel.type.observe(viewLifecycleOwner) {
            binding.inputType.setText(it, false)
        }

        addTransactionViewModel.selectDate.observe(viewLifecycleOwner) {
            selectDate()
        }

        addTransactionViewModel.save.observe(viewLifecycleOwner) {
            save()
        }

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        val adapter = ArrayAdapter(
            requireContext(),
            R.layout.material_spinner_item,
            StatementType.getValues()
        )
        binding.inputType.setAdapter(adapter)
        binding.platformTitleLogin.setImageResource(platformEnum.logoId)

        if (params.id != null) {
            addTransactionViewModel.loadInfoForEdit(params.id as String, platformEnum)
        }
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun showLoading() {
        binding.body.visibility = View.GONE
        binding.animationView.visibility = View.VISIBLE
        binding.animationView.setAnimation(R.raw.loading)
        binding.animationView.speed = 0.75f
        binding.animationView.repeatCount = -1
        binding.animationView.playAnimation()
    }

    private fun showSuccess() {
        binding.animationView.visibility = View.VISIBLE
        binding.body.visibility = View.GONE
        binding.animationView.setAnimation(R.raw.success)
        binding.animationView.speed = 0.75f
        binding.animationView.repeatCount = 0
        binding.animationView.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) = Unit

            override fun onAnimationEnd(animation: Animator?) {
                findNavController().navigateUp()
            }

            override fun onAnimationCancel(animation: Animator?) = Unit

            override fun onAnimationStart(animation: Animator?) = Unit
        })
        binding.animationView.playAnimation()
    }

    private fun showError() {
        binding.animationView.setAnimation(R.raw.error)
        binding.animationView.speed = 1.0f
        binding.animationView.visibility = View.VISIBLE
        binding.animationView.repeatCount = 0
        binding.animationView.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) = Unit

            override fun onAnimationEnd(animation: Animator?) {
                binding.animationView.visibility = View.GONE
                binding.body.visibility = View.VISIBLE
            }

            override fun onAnimationCancel(animation: Animator?) = Unit

            override fun onAnimationStart(animation: Animator?) = Unit
        })
        binding.animationView.playAnimation()
    }

    private fun selectDate() {
        val c = getInstance()
        mYear = c.get(YEAR)
        mMonth = c.get(MONTH)
        mDay = c.get(DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { _, year, monthOfYear, dayOfMonth ->
                mYear = year
                mMonth = monthOfYear
                mDay = dayOfMonth
                val displayMonth = mMonth!! + 1
                binding.etDate.setText(
                    String.format(
                        getDefault(),
                        "%s-%d-%d",
                        dayOfMonth.toString(),
                        displayMonth,
                        year
                    )
                )
            },
            mYear!!, mMonth!!, mDay!!
        )
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private fun save() {
        if (!validateInput()) {
            return
        }

        if (params.id != null) {
            val c = getInstance()
            c.time = Utils.convertStringToDate(binding.etDate.text.toString(), "dd-MM-yyyy")
            mYear = c.get(YEAR)
            mMonth = c.get(MONTH)
            mDay = c.get(DAY_OF_MONTH)
        }

        addTransactionViewModel.addOrUpdate(
            params.name,
            platformEnum,
            params.id,
            binding.etAmount.text.toString(),
            mYear,
            mMonth,
            mDay,
            binding.inputType.editableText.toString()
        )
    }

    private fun validateInput(): Boolean {
        var valid = true
        val email = binding.etAmount.text.toString()
        val type = binding.inputType.text.toString()
        if (email.isBlank()) {
            binding.etAmount.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.etAmount.error = null
        }

        if (isMissingDateInput()) {
            binding.etDate.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.etDate.error = null
        }

        if (type.isBlank()) {
            binding.tilType.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilType.error = null
        }

        return valid
    }

    private fun isMissingDateInput() =
        binding.etDate.text?.isEmpty() == true && (mYear == null || mMonth == null || mDay == null)
}
