package pt.tiagocarvalho.financetracker.ui.onboarding

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(ActivityComponent::class)
@Module
class OnboardingActivityModule {

    @Provides
    fun provideViewModel(
        provider: SchedulerProvider,
        onboardingUseCase: OnboardingUseCase
    ): OnboardingViewModel = OnboardingViewModel(
        provider,
        onboardingUseCase
    )
}
