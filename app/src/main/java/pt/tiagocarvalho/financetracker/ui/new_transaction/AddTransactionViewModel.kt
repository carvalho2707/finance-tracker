package pt.tiagocarvalho.financetracker.ui.new_transaction

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.Locale
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.repository.CashRepository
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.repository.SavingsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.utils.Utils

@HiltViewModel
class AddTransactionViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val savingsRepository: SavingsRepository,
    private val gamblingRepository: GamblingRepository,
    private val cashRepository: CashRepository,
    private val logger: Logger,
    private val preferencesHelper: PreferencesHelper
) : BaseViewModel() {

    val status: MutableLiveData<Status> = MutableLiveData()
    val selectDate: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val save: SingleLiveEvent<Boolean> = SingleLiveEvent()

    val date: ObservableField<String> = ObservableField("")
    val amount: ObservableField<String> = ObservableField("")
    val type: MutableLiveData<String> = MutableLiveData()

    fun addOrUpdate(
        name: String,
        platformEnum: PlatformEnum,
        id: String?,
        amount: String,
        mYear: Int?,
        mMonth: Int?,
        mDay: Int?,
        type: String
    ) {
        when (platformEnum) {
            PlatformEnum.SAVINGS -> addOrUpdateSavings(name, id, amount, mYear, mMonth, mDay, type)
            PlatformEnum.BETS -> addOrUpdateBets(name, id, amount, mYear, mMonth, mDay, type)
            PlatformEnum.CASH -> addOrUpdateCash(name, id, amount, mYear, mMonth, mDay, type)
            else -> return
        }
    }

    fun addDate() {
        selectDate.value = true
    }

    fun save() {
        save.value = true
    }

    private fun addOrUpdateCash(
        name: String,
        id: String?,
        amount: String,
        mYear: Int?,
        mMonth: Int?,
        mDay: Int?,
        type: String
    ) {
        if (id != null) {
            val localId = id.removePrefix(name + "_").toLong()
            compositeDisposable.add(
                cashRepository.editTransaction(
                    amount.toDouble(),
                    mYear!!,
                    mMonth!!,
                    mDay!!,
                    StatementType.valueOf(type.toUpperCase(Locale.ROOT)),
                    localId
                )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { onSubscribe() }
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe({ onSuccess(name, PlatformEnum.CASH) }, { onError(it) })
            )
        } else {
            compositeDisposable.add(
                cashRepository.addTransaction(
                    name,
                    amount.toDouble(),
                    mYear!!,
                    mMonth!!,
                    mDay!!,
                    StatementType.valueOf(type.toUpperCase(Locale.ROOT))
                )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { onSubscribe() }
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe({ onSuccess(name, PlatformEnum.CASH) }, { onError(it) })
            )
        }
    }

    private fun addOrUpdateBets(
        name: String,
        id: String?,
        amount: String,
        mYear: Int?,
        mMonth: Int?,
        mDay: Int?,
        type: String
    ) {
        if (id != null) {
            val localId = id.removePrefix(name + "_").toLong()
            compositeDisposable.add(
                gamblingRepository.editTransaction(
                    amount.toDouble(),
                    mYear!!,
                    mMonth!!,
                    mDay!!,
                    StatementType.valueOf(type.toUpperCase(Locale.ROOT)),
                    localId
                )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { onSubscribe() }
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe({ onSuccess(name, PlatformEnum.BETS) }, { onError(it) })
            )
        } else {
            compositeDisposable.add(
                gamblingRepository.addTransaction(
                    name,
                    amount.toDouble(),
                    mYear!!,
                    mMonth!!,
                    mDay!!,
                    StatementType.valueOf(type.toUpperCase(Locale.ROOT))
                )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { onSubscribe() }
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe({ onSuccess(name, PlatformEnum.BETS) }, { onError(it) })
            )
        }
    }

    private fun addOrUpdateSavings(
        name: String,
        id: String?,
        amount: String,
        mYear: Int?,
        mMonth: Int?,
        mDay: Int?,
        type: String
    ) {
        if (id != null) {
            val localId = id.removePrefix(name + "_").toLong()
            compositeDisposable.add(
                savingsRepository.editTransaction(
                    amount.toDouble(),
                    mYear!!,
                    mMonth!!,
                    mDay!!,
                    StatementType.valueOf(type.toUpperCase(Locale.ROOT)),
                    localId
                )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { onSubscribe() }
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe({ onSuccess(name, PlatformEnum.SAVINGS) }, { onError(it) })
            )
        } else {
            compositeDisposable.add(
                savingsRepository.addTransaction(
                    name,
                    amount.toDouble(),
                    mYear!!,
                    mMonth!!,
                    mDay!!,
                    StatementType.valueOf(type.toUpperCase(Locale.ROOT))
                )
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .doOnSubscribe { onSubscribe() }
                    .delay(1, TimeUnit.SECONDS)
                    .subscribe({ onSuccess(name, PlatformEnum.SAVINGS) }, { onError(it) })
            )
        }
    }

    private fun onSuccess(name: String, platformEnum: PlatformEnum) {
        preferencesHelper.put(platformEnum.prefLastUpdateStatement + name, 0L)
        preferencesHelper.put(platformEnum.prefLastUpdateDetails + name, 0L)
        preferencesHelper.put(platformEnum.prefLastFullStatementScan + name, 0L)
        status.postValue(Status.SUCCESS)
    }

    private fun onError(it: Throwable) {
        logger.log(it)
        status.postValue(Status.ERROR)
    }

    private fun onSubscribe() {
        status.postValue(Status.LOADING)
    }

    fun loadInfoForEdit(id: String, platform: PlatformEnum) {
        when (platform) {
            PlatformEnum.SAVINGS -> compositeDisposable.add(
                savingsRepository.loadStatement(id)
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .subscribe(
                        {
                            type.postValue(it.type.name)
                            amount.set(it.amount.toString())
                            date.set(Utils.convertDateToString(it.date))
                        },
                        { logger.log(it) }
                    )
            )
            PlatformEnum.BETS -> compositeDisposable.add(
                gamblingRepository.loadStatement(id)
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .subscribe(
                        {
                            type.postValue(it.type.name)
                            amount.set(it.amount.toString())
                            date.set(Utils.convertDateToString(it.date))
                        },
                        { logger.log(it) }
                    )
            )
            PlatformEnum.CASH -> compositeDisposable.add(
                cashRepository.loadStatement(id)
                    .subscribeOn(schedulerProvider.io())
                    .observeOn(schedulerProvider.ui())
                    .subscribe(
                        {
                            type.postValue(it.type.name)
                            amount.set(it.amount.toString())
                            date.set(Utils.convertDateToString(it.date))
                        },
                        { logger.log(it) }
                    )
            )
            else -> return
        }
    }
}
