package pt.tiagocarvalho.financetracker.ui.tweets

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.storage.model.TwitterAccount
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.TweetItem
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.isNoNetworkException
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class TweetsViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger,
    private val analyticsLogger: AnalyticsLogger,
    private val twitterUseCase: TwitterUseCase
) : BaseViewModel() {

    var status: MutableLiveData<Status> = MutableLiveData()
    var tweets: MutableLiveData<List<TweetItem>> = MutableLiveData()
    var errorType: MutableLiveData<Int> = MutableLiveData()
    var twitterChips: MutableLiveData<List<TwitterAccount>> = MutableLiveData()
    val filter: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun loadTweets() {
        compositeDisposable.add(
            twitterUseCase.getTweets()
                .delay(1, TimeUnit.SECONDS)
                .doOnSubscribe {
                    analyticsLogger.logTwitter()
                    status.postValue(Status.LOADING)
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnNext {
                    status.postValue(Status.SUCCESS)
                    tweets.postValue(it)
                }
                .subscribe({}, { onError(it) })
        )
    }

    private fun onError(throwable: Throwable) {
        status.postValue(Status.ERROR)
        if (throwable.isNoNetworkException()) {
            errorType.postValue(0)
        } else {
            logger.log(throwable)
            errorType.postValue(1)
        }
    }

    fun loadTwitterAccounts() {
        compositeDisposable.add(
            twitterUseCase.getTwitterAccounts()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ twitterChips.postValue(it) }, { onError(it) })
        )
    }

    fun updateTwitterAccount(name: String, checked: Boolean) {
        twitterUseCase.updateTwitterAccount(name, checked)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe()
    }

    fun hideFilter() {
        filter.value = true
    }
}
