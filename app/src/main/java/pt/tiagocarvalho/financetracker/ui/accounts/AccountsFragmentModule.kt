package pt.tiagocarvalho.financetracker.ui.accounts

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class AccountsFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        accountsUseCase: AccountsUseCase
    ): AccountsViewModel = AccountsViewModel(
        schedulerProvider,
        accountsUseCase
    )
}
