package pt.tiagocarvalho.financetracker.ui.statistics

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class StatisticsFragmentModule {

    @Provides
    fun provideViewModel(
        refreshRepository: RefreshRepository,
        accountRepository: AccountsRepository,
        schedulerProvider: SchedulerProvider
    ): StatisticsViewModel =
        StatisticsViewModel(refreshRepository, accountRepository, schedulerProvider)
}
