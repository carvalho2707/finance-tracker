package pt.tiagocarvalho.financetracker.ui.statements

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class StatementsFragmentModule {

    @Provides
    fun provideViewModel(
        statementsRepository: StatementsRepository,
        accountsRepository: AccountsRepository,
        refreshRepository: RefreshRepository,
        schedulerProvider: SchedulerProvider,
        logger: Logger
    ): StatementsViewModel = StatementsViewModel(
        statementsRepository,
        accountsRepository,
        refreshRepository,
        schedulerProvider,
        logger
    )
}
