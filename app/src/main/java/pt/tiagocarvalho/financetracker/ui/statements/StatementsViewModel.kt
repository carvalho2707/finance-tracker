package pt.tiagocarvalho.financetracker.ui.statements

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.EnumMap
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementsStatus
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.isNoNetworkException
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class StatementsViewModel @Inject constructor(
    private val statementsRepository: StatementsRepository,
    private val accountsRepository: AccountsRepository,
    private val refreshRepository: RefreshRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger
) : BaseViewModel() {

    val statements: MutableLiveData<List<StatementsStatus>> = MutableLiveData()
    var status: MutableLiveData<Status> = MutableLiveData()
    var errorType: MutableLiveData<Int> = MutableLiveData()

    var tfaRequired: MutableLiveData<Platform> = MutableLiveData()
    private var requiredPlatforms: MutableList<Platform> = mutableListOf()
    private var loadedData: MutableMap<PlatformEnum, WebViewData> =
        EnumMap(PlatformEnum::class.java)

    fun loadRefreshNeeded() {
        compositeDisposable.add(
            refreshRepository.isRefreshStatementsNeeded()
                .doOnSuccess {
                    if (it) {
                        loadTwoFactorPlatforms()
                    } else {
                        loadStatements()
                    }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .subscribe({}, { onError(it) })
        )
    }

    fun loadNextAuth(webViewData: WebViewData?) {
        webViewData?.let {
            loadedData[webViewData.platform] = it
        }
        if (requiredPlatforms.size > 0) {
            tfaRequired.postValue(requiredPlatforms.first())
            requiredPlatforms.removeAt(0)
        } else {
            refreshStatements()
        }
    }

    fun loadStatements() {
        compositeDisposable.add(
            statementsRepository.loadStatements()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .subscribe(
                    {
                        status.postValue(Status.SUCCESS)
                        statements.postValue(it)
                    },
                    { onError(it) }
                )
        )
    }

    private fun onError(throwable: Throwable) {
        status.postValue(Status.ERROR)
        if (throwable.isNoNetworkException()) {
            errorType.postValue(0)
        } else {
            logger.log(throwable)
            errorType.postValue(1)
        }
    }

    private fun loadTwoFactorPlatforms() {
        compositeDisposable.add(
            accountsRepository.getTwoFactorPlatforms()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            requiredPlatforms = it.toMutableList()
                            tfaRequired.value = requiredPlatforms.first()
                            requiredPlatforms.removeAt(0)
                        } else {
                            refreshStatements()
                        }
                    },
                    { onError(it) },
                    { refreshStatements() }
                )
        )
    }

    private fun refreshStatements() {
        compositeDisposable.add(
            refreshRepository.refreshFullStatements(loadedData)
                .subscribeOn(schedulerProvider.io())
                .doOnComplete { loadStatements() }
                .subscribe({}, { onError(it) })
        )
    }
}
