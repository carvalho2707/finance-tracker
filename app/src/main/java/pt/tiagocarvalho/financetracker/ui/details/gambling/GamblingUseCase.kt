package pt.tiagocarvalho.financetracker.ui.details.gambling

import android.content.Context
import android.net.Uri
import io.reactivex.Completable
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingStatement
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.utils.CsvUtils
import pt.tiagocarvalho.financetracker.utils.toDate

class GamblingUseCase(
    val gamblingRepository: GamblingRepository,
    val context: Context
) {

    internal fun importCsv(uri: Uri, name: String): Completable =
        CsvUtils.readCsvFromUri(uri, context)
            .map { parseCsvToSavingsStatement(name, it) }
            .flatMapCompletable { statements ->
                gamblingRepository.deleteAllByName(name)
                    .andThen(gamblingRepository.triggerFetchCompletable(name))
                    .andThen(gamblingRepository.insert(statements))
            }

    private fun parseCsvToSavingsStatement(
        name: String,
        entries: List<String>
    ): List<GamblingStatement> = entries.map { line ->
        val list = line.split(",")
        GamblingStatement(
            null,
            name,
            list[0].toDouble(),
            list[1].toDate(),
            StatementType.valueOf(list[2])
        )
    }
}
