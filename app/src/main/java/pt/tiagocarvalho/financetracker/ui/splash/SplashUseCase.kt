package pt.tiagocarvalho.financetracker.ui.splash

import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_RATER_SEEN_ONBOARDING

class SplashUseCase(private val preferencesHelper: PreferencesHelper) {

    internal fun hasSeenOnboarding(): Single<Boolean> = Single.create {
        val seenOnboarding = preferencesHelper.get(PREF_RATER_SEEN_ONBOARDING, false)
        it.onSuccess(seenOnboarding)
    }
}
