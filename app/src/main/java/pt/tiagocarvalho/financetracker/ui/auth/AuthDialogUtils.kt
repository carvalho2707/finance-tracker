package pt.tiagocarvalho.financetracker.ui.auth

import android.content.ClipDescription
import android.content.ClipboardManager
import android.text.TextUtils
import androidx.fragment.app.Fragment
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.ui.auth.peerberry.PeerberryAuthDialogFragment
import pt.tiagocarvalho.financetracker.ui.auth.raize.RaizeAuthDialogFragment
import pt.tiagocarvalho.financetracker.ui.auth.robocash.RobocashAuthDialogFragment
import pt.tiagocarvalho.financetracker.ui.auth.twino.TwinoAuthDialogFragment

object AuthDialogUtils {

    const val USERNAME = "username"
    const val PASSWORD = "password"
    const val FREQUENT_UPDATES = "frequent_updates"
    const val REQUEST_CODE = 7780
    const val TOKEN = "token"
    const val PLATFORM = "platform"
    const val INVALID_USERNAME_PASSWORD_CODE = 7788
    const val UNKNOWN_ERROR_CODE = 7789
    const val SUCCESS = 7790
    const val CANCEL = 7791

    fun belongsToAuthDialog(platformEnum: PlatformEnum) =
        platformEnum == PlatformEnum.PEERBERRY ||
            platformEnum == PlatformEnum.TWINO ||
            platformEnum == PlatformEnum.ROBOCASH ||
            platformEnum == PlatformEnum.RAIZE

    fun showAuthDialogFragment(
        platformEnum: PlatformEnum,
        username: String,
        password: String,
        parent: Fragment,
        frequentUpdates: Boolean = false
    ) {
        when (platformEnum) {
            PlatformEnum.PEERBERRY -> {
                val fragment =
                    PeerberryAuthDialogFragment.newInstance(username, password, frequentUpdates)
                fragment.setTargetFragment(parent, 6660)
                fragment.show(parent.parentFragmentManager, "peerberry_auth_dialog_fragment")
            }
            PlatformEnum.TWINO -> {
                val fragment =
                    TwinoAuthDialogFragment.newInstance(username, password, frequentUpdates)
                fragment.setTargetFragment(parent, 6660)
                fragment.show(parent.parentFragmentManager, "twino_auth_dialog_fragment")
            }
            PlatformEnum.ROBOCASH -> {
                val fragment =
                    RobocashAuthDialogFragment.newInstance(username, password, frequentUpdates)
                fragment.setTargetFragment(parent, 6660)
                fragment.show(parent.parentFragmentManager, "robocash_auth_dialog_fragment")
            }
            PlatformEnum.RAIZE -> {
                val fragment =
                    RaizeAuthDialogFragment.newInstance(username, password, frequentUpdates)
                fragment.setTargetFragment(parent, 6660)
                fragment.show(parent.parentFragmentManager, "raize_auth_dialog_fragment")
            }
            else -> return
        }
    }

    fun isValidPaste(clipboardManager: ClipboardManager?): Boolean =
        if (clipboardManager?.primaryClipDescription?.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN) == true) {
            val text = getTextFromClipboard(clipboardManager)
            text != null && TextUtils.isDigitsOnly(text) && text.length == 6
        } else {
            false
        }

    fun getTextFromClipboard(clipboardManager: ClipboardManager?) =
        clipboardManager?.primaryClip?.getItemAt(0)
            ?.text?.toString()
            ?.replace(Regex("[^\\d.]"), "")
}
