package pt.tiagocarvalho.financetracker.ui.auth.robocash

import io.reactivex.Single
import java.util.HashMap
import org.jsoup.nodes.Document
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.fetcher.RobocashPageFetcher

class RobocashAuthUseCase(
    private val logger: Logger
) {

    private val fetcher = RobocashPageFetcher()
    private var cookiesGlobal: MutableMap<String, String> = HashMap()
    private var key: String? = null

    internal fun login(email: String, password: String): Single<LoginResponse> = Single.create {
        try {
            val welcomePage = fetcher.getWelcomePage()
            replaceCookies(welcomePage.cookies())
            key = getLoginKeyFromDocument(welcomePage.parse())

            val loginPage = fetcher.postLoginPage(email, password, cookiesGlobal, key!!)
            replaceCookies(loginPage.cookies())
            val loginResponse = when (loginPage.header("location")) {
                "https://robo.cash/login/mfa" -> LoginResponse(tfaActive = true)
                "https://robo.cash" -> LoginResponse(isPasswordError = true)
                else -> LoginResponse(cookies = cookiesGlobal)
            }
            it.onSuccess(loginResponse)
        } catch (e: Exception) {
            logger.log(e)
            it.onError(e)
        }
    }

    fun loginTfa(code: String): Single<LoginResponse> = Single.create {
        try {
            val mfaGetPage = fetcher.getMfa(cookiesGlobal)
            replaceCookies(mfaGetPage.cookies())
            key = getLoginKeyFromDocument(mfaGetPage.parse())

            val mfaPage = fetcher.postMfa(key!!, code, cookiesGlobal)
            replaceCookies(mfaPage.cookies())
            val location = mfaPage.header("location")
            val loginResponse =
                if (location == "https://robo.cash/login/mfa") LoginResponse(isCodeError = true)
                else LoginResponse(cookies = cookiesGlobal)
            it.onSuccess(loginResponse)
        } catch (e: Exception) {
            logger.log(e)
            it.onError(e)
        }
    }

    private fun replaceCookies(cookies: Map<String, String>) {
        cookies.forEach { entry -> cookiesGlobal[entry.key] = entry.value }
    }

    private fun getLoginKeyFromDocument(document: Document): String {
        return document.selectFirst("meta[name=csrf-token]").attr("content")
    }
}
