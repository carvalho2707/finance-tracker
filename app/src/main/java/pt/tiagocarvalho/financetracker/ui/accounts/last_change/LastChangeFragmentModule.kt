package pt.tiagocarvalho.financetracker.ui.accounts.last_change

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.ui.accounts.AccountsUseCase
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class LastChangeFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        accountsUseCase: AccountsUseCase,
        preferencesHelper: PreferencesHelper
    ) = LastChangeViewModel(
        schedulerProvider,
        accountsUseCase,
        preferencesHelper
    )
}
