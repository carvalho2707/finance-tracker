package pt.tiagocarvalho.financetracker.ui.main

import android.content.Context
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.billing.localdb.LocalBillingDb
import pt.tiagocarvalho.financetracker.billing.localdb.Premium

class MainUseCase(
    private val context: Context
) {

    internal fun canShowAds(): Single<Boolean> = loadPremium()
        .map { it.level < 1 }

    private fun loadPremium(): Single<Premium> = Single.create {
        val premium = LocalBillingDb.getInstance(
            context
        ).entitlementsDao().getPremium()
        if (premium == null) {
            it.onSuccess(Premium(false, 0, null))
        } else {
            it.onSuccess(premium)
        }
    }
}
