package pt.tiagocarvalho.financetracker.ui.billing

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isGone
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.billing.localdb.Premium
import pt.tiagocarvalho.financetracker.databinding.FragmentBillingBinding
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.AppExecutors

@AndroidEntryPoint
class BillingFragment : BaseFragment() {

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var adapter: SkuDetailsAdapter
    private lateinit var binding: FragmentBillingBinding

    private val billingViewModel: BillingViewModel by viewModels()
    private val premium: ObservableBoolean = ObservableBoolean(false)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentBillingBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = billingViewModel
            lifecycleOwner = viewLifecycleOwner
            premium = this@BillingFragment.premium
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = SkuDetailsAdapter(
            appExecutors,
            requireContext()
        ) { skuDetails ->
            requireActivity().let {
                billingViewModel.makePurchase(it, skuDetails)
            }
        }

        registerToolbarWithNavigation(binding.toolbar)

        binding.recyclerViewBilling.adapter = adapter

        billingViewModel.inappSkuDetailsListLiveData.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

        billingViewModel.premiumLiveData.observe(viewLifecycleOwner) {
            updateUi(it)
        }

        billingViewModel.errorEvent.observe(
            viewLifecycleOwner,
            { message ->
                message?.let {
                    binding.textViewBillingError.apply {
                        text = "${getString(R.string.iap_unavailable)} ($message)"
                        isGone = false
                    }
                }
            }
        )
    }

    private fun updateUi(premium: Premium?) {
        val active = premium != null
        this.premium.set(active)
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean =
        if (menuItem.itemId == R.id.menu_settings) {
            findNavController().navigate(MainDirections.actionGlobalSettings())
            true
        } else super.onOptionsItemSelected(menuItem)
}
