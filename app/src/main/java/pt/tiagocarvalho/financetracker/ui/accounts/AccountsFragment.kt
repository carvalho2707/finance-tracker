package pt.tiagocarvalho.financetracker.ui.accounts

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.util.EnumMap
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.databinding.FragmentAccountsBinding
import pt.tiagocarvalho.financetracker.model.DisplayMode
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.repository.PreferencesRepository
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.CANCEL
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.ui.main.MainActivity
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.utils.Preferences
import pt.tiagocarvalho.financetracker.utils.navigateSafe
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.financetracker.webview.BaseWebViewNavigator
import pt.tiagocarvalho.financetracker.widget.BadgeDrawerToggle
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_COLLAPSED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_EXPANDED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_HIDDEN
import pt.tiagocarvalho.p2p.api.model.Login

@AndroidEntryPoint
class AccountsFragment : BaseFragment(), BaseWebViewNavigator {

    @Inject
    lateinit var liveSharedPreferences: LiveSharedPreferences

    @Inject
    lateinit var preferencesRepository: PreferencesRepository

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentAccountsBinding
    private lateinit var filterBottomSheetBehavior: BottomSheetBehavior<*>
    private lateinit var dailyChangeBottomSheetBehavior: BottomSheetBehavior<*>
    private lateinit var toggle: BadgeDrawerToggle

    private val accountsViewModel: AccountsViewModel by viewModels()
    private var filterState = STATE_HIDDEN
    private var dailyChangeState = STATE_HIDDEN
    private var globalFabVisibility = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            onBackPressed()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentAccountsBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = accountsViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val displayMode = DisplayMode.valueOf(preferencesRepository.getDisplayMode())
        val adapter =
            AccountsAdapter(appExecutors, requireContext(), displayMode) { onClickAccount(it) }

        binding.accountList.adapter = adapter
        binding.accountList.addOnScrollListener(object :
                RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    if (dy > 0 && binding.fab.visibility == View.VISIBLE) {
                        binding.fab.hide()
                    } else if (dy < 0 && binding.fab.visibility != View.VISIBLE) {
                        if (globalFabVisibility) {
                            binding.fab.show()
                        }
                    }
                }
            })

        toggle = BadgeDrawerToggle(
            requireActivity(),
            (requireActivity() as MainActivity).drawer,
            binding.toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        registerToolbarWithNavigation(binding.toolbar)

        binding.fab.setOnClickListener {
            findNavController().navigateSafe(AccountsFragmentDirections.showManagement())
        }

        liveSharedPreferences.listenUpdatesOnly(Preferences.getPreferences())
            .observe(viewLifecycleOwner) { accountsViewModel.loadRefreshNeeded(false) }

        accountsViewModel.accounts.observe(viewLifecycleOwner) { onAccounts(it, adapter) }

        accountsViewModel.tfaRequired.observe(viewLifecycleOwner) { login ->
            login?.let {
                if (AuthDialogUtils.belongsToAuthDialog(it.type)) {
                    loadAuthDialog(it)
                } else {
                    loadWebView(it)
                }
            }
        }

        accountsViewModel.cashDragAlerts.observe(viewLifecycleOwner) {
            handleCashDragAlerts(it)
        }

        accountsViewModel.status.observe(viewLifecycleOwner) { status ->
            handleStatus(status)
        }

        accountsViewModel.showFloatActionButton.observe(viewLifecycleOwner) {
            globalFabVisibility = it
            binding.fab.isVisible = it
        }

        accountsViewModel.showErrorDialog.observe(viewLifecycleOwner) {
            showErrorDialog()
        }

        filterBottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.filter_sheet))
        dailyChangeBottomSheetBehavior =
            BottomSheetBehavior.from(view.findViewById(R.id.dailyChangeSheet))

        filterBottomSheetBehavior.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    filterState = newState
                }
            })

        dailyChangeBottomSheetBehavior.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    dailyChangeState = newState
                }
            })

        accountsViewModel.loadRefreshNeeded(false)
    }

    private fun onAccounts(accounts: List<AccountItem>, adapter: AccountsAdapter) {
        if (accounts.isEmpty()) {
            binding.noAccountsMessage.visibility = View.VISIBLE
        } else {
            binding.noAccountsMessage.visibility = View.GONE
        }

        scheduleAnimation(adapter)
        adapter.submitList(accounts)
    }

    private fun onClickAccount(it: AccountItem) {
        val directions = when (it.platform.platformTypeEnum) {
            PlatformTypeEnum.P2P -> AccountsFragmentDirections.showP2P(it.name)
            PlatformTypeEnum.SAVINGS -> AccountsFragmentDirections.showSavings(it.name)
            PlatformTypeEnum.GAMBLING -> AccountsFragmentDirections.showGambling(it.name)
            PlatformTypeEnum.CASH -> AccountsFragmentDirections.showCash(it.name)
        }

        findNavController().navigateSafe(directions)
    }

    override fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String?
    ) = Unit

    override fun onError(platformEnum: PlatformEnum, errorCode: Int) {
        if (errorCode == 0) {
            accountsViewModel.loadAccounts(false, EnumMap(PlatformEnum::class.java))
        }
    }

    override fun loadInfo(webViewData: WebViewData?) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            binding.layoutLoading.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
        }
        accountsViewModel.loadNextAuth(webViewData)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(login: Platform) {
        binding.layoutLoading.visibility = View.INVISIBLE
        binding.webView.visibility = View.VISIBLE
        val client = BaseWebViewClient.getClient(
            requireContext(),
            layoutInflater,
            login.type,
            Login(login.username!!, login.password!!),
            this,
            isLogin = false,
            frequentUpdates = true
        )
        binding.webView.webViewClient = client

        val webSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.userAgentString = Constants.USER_AGENT_VALUE

        binding.webView.loadUrl(client.getInitialUrl())
    }

    private fun loadAuthDialog(platform: Platform) {
        AuthDialogUtils.showAuthDialogFragment(
            platform.type,
            platform.username!!,
            platform.password!!,
            this@AccountsFragment
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val platform = PlatformEnum.valueOf(data?.getStringExtra(PLATFORM)!!)
        if (resultCode == SUCCESS) {
            accountsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)!!))
        } else if (resultCode == INVALID_USERNAME_PASSWORD_CODE ||
            resultCode == UNKNOWN_ERROR_CODE
        ) {
            accountsViewModel.loadAccounts(false, EnumMap(PlatformEnum::class.java))
        } else if (resultCode == CANCEL) {
            accountsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)))
        }
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_refresh -> accountsViewModel.loadRefreshNeeded(true)
            R.id.menu_filter -> {
                filterState = if (filterState == STATE_EXPANDED) {
                    STATE_HIDDEN
                } else {
                    STATE_EXPANDED
                }
                filterBottomSheetBehavior.state = filterState
            }
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun scheduleAnimation(adapter: AccountsAdapter) {
        val controller =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        binding.accountList.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        binding.accountList.scheduleLayoutAnimation()
    }

    private fun handleStatus(status: Status?) {
        if (status == Status.LOADING) {
            if (binding.successGroup.visibility == View.VISIBLE) {
                binding.fab.visibility = View.GONE
                binding.successGroup.visibility = View.GONE
                binding.imageView.visibility = View.GONE
                binding.webView.visibility = View.GONE
                binding.layoutLoading.visibility = View.VISIBLE
            }
            binding.noAccountsMessage.visibility = View.GONE
        } else {
            accountsViewModel.handleFloatActionButton()
            binding.successGroup.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
            binding.layoutLoading.visibility = View.GONE

            if (status == Status.ERROR) {
                binding.imageView.visibility = View.VISIBLE
            }
        }
    }

    private fun showErrorDialog() {
        with(MaterialAlertDialogBuilder(requireContext())) {
            setTitle(getString(R.string.dialog_generic_issue_title))
            setMessage(getString(R.string.dialog_generic_issue_message))
            setPositiveButton(getString(R.string.dismiss)) { dialog, _ ->
                dialog.dismiss()
            }
            setNegativeButton(getString(R.string.ask_in_telegram)) { dialog, _ ->
                startTelegram()
                dialog.dismiss()
            }
            create()
            show()
        }
    }

    private fun startTelegram() {
        try {
            val uri = Uri.parse("https://t.me/inWalletOfficial")
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        } catch (exception: ActivityNotFoundException) {
            exception.printStackTrace()
            view?.let {
                Snackbar.make(it, R.string.generic_error_message, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun handleCashDragAlerts(notificationCount: Int) {
        requireActivity().let {
            (it as MainActivity).updateDrawer(notificationCount)
        }
        if (notificationCount > 0) {
            toggle.isBadgeEnabled = true
            toggle.badgeText = notificationCount.toString()
        } else {
            toggle.isBadgeEnabled = false
        }
        toggle.syncState()
    }

    private fun onBackPressed(): Boolean {
        if (::filterBottomSheetBehavior.isInitialized && filterBottomSheetBehavior.state == STATE_EXPANDED) {
            // collapse or hide the sheet
            if (filterBottomSheetBehavior.isHideable && filterBottomSheetBehavior.skipCollapsed) {
                filterBottomSheetBehavior.state = STATE_HIDDEN
            } else {
                filterBottomSheetBehavior.state = STATE_COLLAPSED
            }
            return true
        } else if (::dailyChangeBottomSheetBehavior.isInitialized && dailyChangeBottomSheetBehavior.state == STATE_EXPANDED) {
            // collapse or hide the sheet
            if (dailyChangeBottomSheetBehavior.isHideable && dailyChangeBottomSheetBehavior.skipCollapsed) {
                dailyChangeBottomSheetBehavior.state = STATE_HIDDEN
            } else {
                dailyChangeBottomSheetBehavior.state = STATE_COLLAPSED
            }
            return true
        }
        findNavController().navigateUp()
        return false
    }
}
