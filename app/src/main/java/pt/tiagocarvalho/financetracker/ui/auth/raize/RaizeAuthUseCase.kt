package pt.tiagocarvalho.financetracker.ui.auth.raize

import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.ui.auth.LoginResponse
import pt.tiagocarvalho.financetracker.utils.Preferences.RAIZE_DEVICE_ID
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.model.raize.PostTokenResponse
import pt.tiagocarvalho.p2p.services.retrofit.RaizeApi
import retrofit2.HttpException

class RaizeAuthUseCase(
    private val raizeApi: RaizeApi,
    private val preferencesHelper: PreferencesHelper,
    private val logger: Logger
) {

    internal fun login(email: String, password: String): Single<LoginResponse> {
        val deviceId = preferencesHelper.get(RAIZE_DEVICE_ID)
        return raizeApi.postToken(
            "RAIZE",
            "RAIZE",
            "password",
            email,
            password,
            null,
            null,
            deviceId
        )
            .map { mapPostToken(it) }
            .onErrorReturn { mapErrorLogin(it) }
    }

    internal fun loginTfa(email: String, password: String, code: String): Single<LoginResponse> {
        return raizeApi.postToken(
            "RAIZE",
            "RAIZE",
            "password",
            email,
            password,
            true,
            code,
            null
        )
            .map { mapPostToken(it) }
            .doOnSuccess {
                if (it.secondaryToken != null) preferencesHelper.put(
                    RAIZE_DEVICE_ID,
                    it.secondaryToken
                )
            }
            .onErrorReturn { mapErrorLogin(it) }
    }

    private fun mapPostToken(response: PostTokenResponse): LoginResponse {
        return when {
            response.mfaMethod == "authenticator" -> LoginResponse(tfaActive = true)
            response.accessToken != null -> LoginResponse(
                token = response.accessToken,
                secondaryToken = response.deviceToken
            )
            else -> {
                logger.log(response.toString())
                LoginResponse(isUnknownError = true)
            }
        }
    }

    private fun mapErrorLogin(throwable: Throwable) = when (throwable) {
        is HttpException -> {
            when {
                throwable.response()?.code() == 401 -> LoginResponse(isPasswordError = true)
                throwable.response()?.code() == 403 -> LoginResponse(isCodeError = true)
                else -> {
                    val body = throwable.response()?.errorBody()?.string()
                    if (body != null) {
                        logger.log(body)
                    } else {
                        logger.log(throwable)
                    }
                    LoginResponse(isUnknownError = true)
                }
            }
        }
        else -> {
            logger.log(throwable)
            LoginResponse(isUnknownError = true)
        }
    }
}
