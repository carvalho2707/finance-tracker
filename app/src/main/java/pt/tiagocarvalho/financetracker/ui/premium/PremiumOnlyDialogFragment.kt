package pt.tiagocarvalho.financetracker.ui.premium

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.databinding.DialogPremiumOnlyBinding
import pt.tiagocarvalho.financetracker.ui.base.BaseDialogFragment
import pt.tiagocarvalho.financetracker.utils.executeAfter

@AndroidEntryPoint
class PremiumOnlyDialogFragment : BaseDialogFragment() {

    companion object {

        private const val TITLE = "title"
        private const val SUBTITLE = "subtitle"

        @JvmStatic
        fun newInstance(
            @StringRes title: Int,
            @StringRes subtitle: Int
        ): PremiumOnlyDialogFragment {
            val fragment = PremiumOnlyDialogFragment()
            val bundle = Bundle().apply {
                putInt(TITLE, title)
                putInt(SUBTITLE, subtitle)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    private val title: Int by lazy {
        if (requireArguments().containsKey(TITLE)) {
            requireArguments().getInt(TITLE)
        } else {
            throw IllegalArgumentException("title not found")
        }
    }

    private val subtitle: Int by lazy {
        if (requireArguments().containsKey(SUBTITLE)) {
            requireArguments().getInt(SUBTITLE)
        } else {
            throw IllegalArgumentException("subtitle not found")
        }
    }

    private lateinit var binding: DialogPremiumOnlyBinding

    private val premiumOnlyViewModel: PremiumOnlyViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = DialogPremiumOnlyBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = premiumOnlyViewModel
            lifecycleOwner = viewLifecycleOwner
            title.text = getText(this@PremiumOnlyDialogFragment.title)
            subtitle.text = getText(this@PremiumOnlyDialogFragment.subtitle)
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        premiumOnlyViewModel.navigateToBilling.observe(viewLifecycleOwner) {
            findNavController().navigate(MainDirections.actionGlobalBilling())
            dismiss()
        }

        premiumOnlyViewModel.closeDialog.observe(viewLifecycleOwner) {
            dismiss()
        }

        binding.executeAfter {
            lifecycleOwner = viewLifecycleOwner
        }

        if (showsDialog) {
            (requireDialog() as AlertDialog).setView(binding.root)
        }
    }
}
