package pt.tiagocarvalho.financetracker.ui.statistics.general

import android.graphics.Color
import android.graphics.Typeface.DEFAULT_BOLD
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.MPPointF
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.ArrayList
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentGeneralBinding
import pt.tiagocarvalho.financetracker.model.GeneralStatistics
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Constants.MATERIAL_COLORS_500
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.charts.CurrencyValueFormatter

@AndroidEntryPoint
class GeneralFragment : BaseFragment() {

    companion object {
        fun newInstance(): GeneralFragment = GeneralFragment()
    }

    private lateinit var binding: FragmentGeneralBinding

    private val generalViewModel: GeneralViewModel by viewModels()
    private var color = 0
    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentGeneralBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = generalViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        generalViewModel.status.observe(viewLifecycleOwner) { status ->
            if (status == Status.LOADING) {
                binding.contentScroll.visibility = View.GONE
                binding.noAccountsMessage.visibility = View.GONE
                binding.animationView.setAnimation(R.raw.loading)
                binding.animationView.repeatCount = -1
                binding.animationView.playAnimation()
            } else binding.animationView.repeatCount = 0
        }

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        color = Utils.themeAttributeToColorInt(android.R.attr.textColorPrimary, requireContext())
        createChartBalanceByType()
        createChartStatementsType()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        generalViewModel.statistics.observe(viewLifecycleOwner) {
            it?.let {
                if (it.show) {
                    binding.noAccountsMessage.visibility = View.GONE
                    binding.contentScroll.visibility = View.VISIBLE
                    updateUI(it)
                } else {
                    binding.contentScroll.visibility = View.GONE
                    binding.noAccountsMessage.visibility = View.VISIBLE
                }
            }
        }

        generalViewModel.loadGeneralStatistics()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> generalViewModel.loadGeneralStatistics()
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun updateUI(statistics: GeneralStatistics?) {
        updateChartBalanceByType(statistics?.balanceByType)
        updateChartStatementsType(
            statistics?.deposits ?: BigDecimal.ZERO,
            statistics?.withdrawals ?: BigDecimal.ZERO,
            statistics?.fees ?: BigDecimal.ZERO,
            statistics?.interests ?: BigDecimal.ZERO,
            statistics?.referrals ?: BigDecimal.ZERO,
            statistics?.cashback ?: BigDecimal.ZERO
        )
    }

    override fun onToolbarItemClicked(menuItem: MenuItem) = false

    private fun createChartBalanceByType() {
        binding.pcByType.setUsePercentValues(true)
        binding.pcByType.description.isEnabled = false
        binding.pcByType.dragDecelerationFrictionCoef = 0.95f
        binding.pcByType.setTransparentCircleAlpha(0)
        binding.pcByType.setHoleColor(Color.TRANSPARENT)
        binding.pcByType.holeRadius = 50f
        binding.pcByType.transparentCircleRadius = 61f
        binding.pcByType.legend.textColor = color
        binding.pcByType.legend.isWordWrapEnabled = true
        binding.pcByType.legend.textSize = 7f
        binding.pcByType.rotationAngle = 0f
        binding.pcByType.setExtraOffsets(0f, 0f, 0f, -5f)
        binding.pcByType.centerText = getString(R.string.portfolio_allocation)
        binding.pcByType.setCenterTextSize(10f)
        binding.pcByType.setCenterTextColor(
            Utils.themeAttributeToColorRes(
                R.attr.colorPrimary,
                requireContext()
            )
        )
        binding.pcByType.setCenterTextTypeface(DEFAULT_BOLD)
        binding.pcByType.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry, h: Highlight) {
                val pieEntry = e as PieEntry
                val text = pieEntry.label + ":    " + numberFormat.format(pieEntry.value.toDouble())
                val mySnackbar = Snackbar.make(binding.pcByType, text, Snackbar.LENGTH_LONG)
                mySnackbar.show()
            }

            override fun onNothingSelected() = Unit
        })
    }

    private fun updateChartBalanceByType(balanceByType: MutableMap<String, BigDecimal>?) {
        val entries = ArrayList<PieEntry>()

        for (entry in balanceByType!!.entries) {
            entries.add(
                PieEntry(
                    entry.value.setScale(2, RoundingMode.HALF_UP).toFloat(),
                    entry.key
                )
            )
        }

        val dataSet = PieDataSet(entries, null)

        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        dataSet.colors = MATERIAL_COLORS_500
        dataSet.valueTextColor = color

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(binding.pcByType))
        data.setValueTextSize(7f)
        data.setValueTextColor(color)

        binding.pcByType.data = data
        binding.pcByType.setDrawEntryLabels(false)
        binding.pcByType.highlightValues(null)
        binding.pcByType.animateY(1400, Easing.EaseInOutQuad)
        binding.pcByType.setEntryLabelColor(color)
        binding.pcByType.setEntryLabelTextSize(7f)
        binding.pcByType.invalidate()
    }

    private fun createChartStatementsType() {
        binding.pcDeposits.description.isEnabled = false
        binding.pcDeposits.dragDecelerationFrictionCoef = 0.95f
        binding.pcDeposits.setTransparentCircleAlpha(0)
        binding.pcDeposits.setHoleColor(Color.TRANSPARENT)
        binding.pcDeposits.holeRadius = 50f
        binding.pcDeposits.transparentCircleRadius = 61f
        binding.pcDeposits.legend.textColor = color
        binding.pcDeposits.legend.isWordWrapEnabled = true
        binding.pcDeposits.legend.textSize = 7f
        binding.pcDeposits.rotationAngle = 0f
        binding.pcDeposits.setExtraOffsets(0f, 0f, 0f, -5f)
        binding.pcDeposits.centerText = getString(R.string.turnover)
        binding.pcDeposits.setCenterTextSize(10f)
        binding.pcDeposits.setCenterTextColor(
            Utils.themeAttributeToColorRes(
                R.attr.colorPrimary,
                requireContext()
            )
        )
        binding.pcDeposits.setCenterTextTypeface(DEFAULT_BOLD)
    }

    private fun updateChartStatementsType(
        deposits: BigDecimal = BigDecimal.ZERO,
        withdrawals: BigDecimal = BigDecimal.ZERO,
        fees: BigDecimal = BigDecimal.ZERO,
        interests: BigDecimal = BigDecimal.ZERO,
        referrals: BigDecimal = BigDecimal.ZERO,
        cashback: BigDecimal = BigDecimal.ZERO
    ) {
        val entries = ArrayList<PieEntry>()

        entries.add(PieEntry(deposits.toFloat(), getString(R.string.deposits_label)))
        entries.add(PieEntry(withdrawals.toFloat(), getString(R.string.withdrawals_label)))
        entries.add(PieEntry(interests.toFloat(), getString(R.string.gains)))
        entries.add(PieEntry(fees.toFloat(), getString(R.string.fees)))
        entries.add(PieEntry(referrals.toFloat(), getString(R.string.referrals)))
        entries.add(PieEntry(cashback.toFloat(), getString(R.string.cashback)))

        val dataSet = PieDataSet(entries, null)

        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        dataSet.valueTextSize = 7f
        dataSet.colors = MATERIAL_COLORS_500
        dataSet.valueTextColor = color

        val data = PieData(dataSet)
        data.setValueTextSize(7f)
        data.setValueTextColor(color)
        data.setValueFormatter(CurrencyValueFormatter())

        binding.pcDeposits.data = data
        binding.pcDeposits.setDrawEntryLabels(false)
        binding.pcDeposits.highlightValues(null)
        binding.pcDeposits.animateY(1400, Easing.EaseInOutQuad)
        binding.pcDeposits.setEntryLabelColor(color)
        binding.pcDeposits.setEntryLabelTextSize(7f)
        binding.pcDeposits.invalidate()
    }
}
