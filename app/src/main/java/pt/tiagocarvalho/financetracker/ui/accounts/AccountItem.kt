package pt.tiagocarvalho.financetracker.ui.accounts

import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.PlatformEnum

data class AccountItem(
    val platform: PlatformEnum,
    val name: String,
    val balance: BigDecimal,
    val changeValue: BigDecimal,
    val changePercentage: BigDecimal
)
