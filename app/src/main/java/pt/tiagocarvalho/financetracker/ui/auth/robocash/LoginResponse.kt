package pt.tiagocarvalho.financetracker.ui.auth.robocash

data class LoginResponse(
    val tfaActive: Boolean = false,
    val cookies: Map<String, String>? = null,
    val isPasswordError: Boolean = false,
    val isCodeError: Boolean = false
)
