package pt.tiagocarvalho.financetracker.ui.details.p2p

import io.reactivex.Observable
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.DataItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.repository.RefreshRepository

class P2pDetailsUseCase(
    val refreshRepository: RefreshRepository
) {

    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    internal fun loadDetails(
        shouldFetch: Boolean,
        name: String,
        data: String? = null
    ): Observable<P2pDetails> = refreshRepository.loadDetails(shouldFetch, name, data)
        .flatMapObservable { detailsResource ->
            refreshRepository.loadStatements(shouldFetch, name, data)
                .map { statementsPair -> mapToP2pDetails(detailsResource, statementsPair) }
        }

    internal fun loadDetailsOffline(
        platformEnum: PlatformEnum
    ): Observable<P2pDetails> = refreshRepository.loadDetailsOffline(platformEnum)
        .flatMapObservable { detailsResource ->
            refreshRepository.loadStatementsOffline(platformEnum)
                .map { statementsPair -> mapToP2pDetails(detailsResource, statementsPair) }
        }

    internal fun shouldRefreshDailyStatements(name: String): Boolean =
        refreshRepository.shouldRefreshDailyStatements(name)

    private fun mapToP2pDetails(
        detailsResource: Resource<PlatformDetails>,
        statementsPair: Pair<Boolean, List<PlatformStatement>>
    ): P2pDetails {
        val dataList = mapToDataItem(detailsResource.data!!)
        return P2pDetails(
            detailsResource.status,
            detailsResource.data.balance,
            statementsPair.first,
            statementsPair.second,
            dataList
        )
    }

    private fun mapToDataItem(details: PlatformDetails): List<DataItem> = listOf(
        DataItem(R.string.deposits_label, numberFormat.format(details.deposits!!.toDouble())),
        DataItem(R.string.profit_label, numberFormat.format(details.profit!!.toDouble())),
        DataItem(R.string.available_label, numberFormat.format(details.available!!.toDouble())),
        DataItem(
            R.string.net_annual_return_label,
            details.netAnnualReturn!!.toPlainString() + " %"
        ),
        DataItem(R.string.active_investments_label, details.myInvestments!!.toString()),
        DataItem(R.string.withdrawals_label, numberFormat.format(details.withdrawals!!.toDouble())),
        DataItem(R.string.daily_change, numberFormat.format(details.changeValue.toDouble())),
        DataItem(R.string.current_label, numberFormat.format(details.currentTotal!!.toDouble())),
        DataItem(R.string.late_label, numberFormat.format(details.lateTotal!!.toDouble()))
    )
}
