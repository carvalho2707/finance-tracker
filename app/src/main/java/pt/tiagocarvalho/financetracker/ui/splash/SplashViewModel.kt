package pt.tiagocarvalho.financetracker.ui.splash

import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent

@HiltViewModel
class SplashViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val splashUseCase: SplashUseCase
) : BaseViewModel() {

    val singleLiveEvent: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun start() {
        compositeDisposable.add(
            splashUseCase.hasSeenOnboarding()
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    { singleLiveEvent.value = it },
                    { singleLiveEvent.value = false }
                )
        )
    }
}
