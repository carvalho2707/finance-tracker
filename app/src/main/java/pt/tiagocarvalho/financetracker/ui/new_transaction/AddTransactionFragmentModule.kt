package pt.tiagocarvalho.financetracker.ui.new_transaction

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.CashRepository
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.repository.SavingsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class AddTransactionFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        savingsRepository: SavingsRepository,
        gamblingRepository: GamblingRepository,
        cashRepository: CashRepository,
        logger: Logger,
        preferencesHelper: PreferencesHelper
    ): AddTransactionViewModel = AddTransactionViewModel(
        schedulerProvider,
        savingsRepository,
        gamblingRepository,
        cashRepository,
        logger,
        preferencesHelper
    )
}
