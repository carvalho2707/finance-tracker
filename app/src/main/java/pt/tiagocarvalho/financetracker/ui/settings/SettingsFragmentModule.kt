package pt.tiagocarvalho.financetracker.ui.settings

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.SettingsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class SettingsFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        settingsRepository: SettingsRepository
    ): SettingsViewModel = SettingsViewModel(schedulerProvider, settingsRepository)
}
