package pt.tiagocarvalho.financetracker.ui.accounts.filter

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.PreferencesRepository

@InstallIn(FragmentComponent::class)
@Module
class AccountsFilterFragmentModule {

    @Provides
    fun provideViewModel(
        preferencesRepository: PreferencesRepository
    ) = AccountsFilterViewModel(preferencesRepository)
}
