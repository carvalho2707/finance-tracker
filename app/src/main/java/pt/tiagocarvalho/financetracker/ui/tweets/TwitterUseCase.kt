package pt.tiagocarvalho.financetracker.ui.tweets

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.storage.model.TwitterAccount
import pt.tiagocarvalho.financetracker.model.TweetItem
import pt.tiagocarvalho.financetracker.repository.TwitterAccountRepository
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.utils.toStringPattern
import pt.tiagocarvalho.twitter.api.SearchApi
import pt.tiagocarvalho.twitter.model.TweetsResponse

class TwitterUseCase(
    private val searchApi: SearchApi,
    private val twitterAccountRepository: TwitterAccountRepository
) {

    internal fun getTweets(): Observable<List<TweetItem>> =
        twitterAccountRepository.getEnabledTwitterAccounts()
            .map { it.map { twitterAccount -> twitterAccount.name } }
            .flatMapObservable { searchApi.getTweets(it) }
            .map { convertToTweetItem(it) }

    internal fun getTwitterAccounts(): Single<List<TwitterAccount>> =
        twitterAccountRepository.getTwitterAccounts()

    internal fun updateTwitterAccount(name: String, checked: Boolean): Completable =
        twitterAccountRepository.updateTwitterAccount(TwitterAccount(name, checked))

    private fun convertToTweetItem(list: List<TweetsResponse>): List<TweetItem> = list.map {
        TweetItem(
            it.idStr,
            it.user.profileImageUrlHttps,
            it.user.name,
            it.createdAt.toStringPattern("MMM dd"),
            it.text,
            getImage(it),
            Constants.TWITTER_URL + it.user.screenName,
            Constants.TWITTER_URL + it.user.screenName + "/status/" + it.idStr
        )
    }

    private fun getImage(tweetsResponse: TweetsResponse): String? =
        tweetsResponse.entities?.media?.firstOrNull { it.type == "photo" }
            ?.mediaUrlHttps
}
