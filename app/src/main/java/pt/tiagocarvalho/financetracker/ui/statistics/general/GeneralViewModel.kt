package pt.tiagocarvalho.financetracker.ui.statistics.general

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.model.GeneralStatistics
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class GeneralViewModel @Inject constructor(
    private val platformDetailsRepository: PlatformDetailsRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger
) : BaseViewModel() {

    var statusObservable: ObservableField<Status> = ObservableField()
    val statistics: MutableLiveData<GeneralStatistics> = MutableLiveData()
    var status: MutableLiveData<Status> = MutableLiveData()

    fun loadGeneralStatistics() {
        compositeDisposable.add(
            platformDetailsRepository.loadGeneralStatistics()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .subscribe(
                    {
                        status.postValue(Status.SUCCESS) // TODO
                        statusObservable.set(Status.SUCCESS)
                        statistics.postValue(it)
                    },
                    {
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        logger.log(it)
                    }
                )
        )
    }
}
