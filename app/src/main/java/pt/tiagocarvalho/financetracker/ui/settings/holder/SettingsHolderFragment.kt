package pt.tiagocarvalho.financetracker.ui.settings.holder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentSettingsHolderBinding
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.ui.settings.SettingsFragment

@AndroidEntryPoint
class SettingsHolderFragment : BaseFragment() {

    private lateinit var binding: FragmentSettingsHolderBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentSettingsHolderBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerToolbarWithNavigation(binding.toolbar)

        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.body, SettingsFragment())
            .commit()
    }

    override fun onToolbarItemClicked(menuItem: MenuItem) = false
}
