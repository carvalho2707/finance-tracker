package pt.tiagocarvalho.financetracker.ui.login

import android.content.Context
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Completable
import io.reactivex.Single
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.DuplicatedAccountException
import pt.tiagocarvalho.financetracker.model.Frequency
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.isNoNetworkException
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.p2p.api.model.AuthenticationFailedException
import pt.tiagocarvalho.p2p.api.model.Login

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val platformRepository: PlatformRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger,
    private val context: Context,
    private val platformDetailsRepository: PlatformDetailsRepository,
    private val statementsRepository: StatementsRepository,
    private val preferencesHelper: PreferencesHelper,
    private val refreshRepository: RefreshRepository
) : BaseViewModel() {

    val status: MutableLiveData<Resource<Any>> = MutableLiveData()
    var tfaRequired: MutableLiveData<Triple<PlatformEnum, Login, Boolean>> = MutableLiveData()
    var editPlatform: MutableLiveData<Platform> = MutableLiveData()
    val networkError: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val signUp: SingleLiveEvent<String> = SingleLiveEvent()
    val login: SingleLiveEvent<Boolean> = SingleLiveEvent()

    val platform: ObservableField<PlatformEnum> = ObservableField()
    val email: ObservableField<String> = ObservableField()
    val password: ObservableField<String> = ObservableField()
    val savings: ObservableField<String> = ObservableField()
    val interests: ObservableField<String> = ObservableField()
    val recurringAmount: ObservableField<String> = ObservableField("0")
    val frequentUpdates: ObservableBoolean = ObservableBoolean(true)
    val button: ObservableInt = ObservableInt(R.string.login_button)
    var edit: ObservableBoolean = ObservableBoolean(false)

    fun setup(name: String, platform: String) {
        compositeDisposable.add(
            platformRepository.getPlatformByName(name)
                .flatMapCompletable { setupEditUi(it) }
                .onErrorResumeNext { setupNewUi(platform) }
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe()
        )
    }

    fun login(
        platform: String,
        email: String,
        password: String,
        name: String,
        interests: String,
        frequency: String,
        recurringAmount: String,
        recurringFrequency: String,
        frequentUpdates: Boolean
    ) {
        status.value = Resource.loading()
        val edit = edit.get()
        when (val platformEnum = PlatformEnum.valueOf(platform)) {
            PlatformEnum.MINTOS,
            PlatformEnum.RAIZE,
            PlatformEnum.GRUPEER,
            PlatformEnum.PEERBERRY,
            PlatformEnum.CROWDESTOR,
            PlatformEnum.TWINO,
            PlatformEnum.ROBOCASH,
            PlatformEnum.BONDORA -> loginTwoStep(platformEnum, email, password, frequentUpdates)
            PlatformEnum.ESTATEGURU,
            PlatformEnum.LENDERMARKET,
            PlatformEnum.IUVO -> generalLogin(platformEnum, email, password, edit, frequentUpdates)
            PlatformEnum.BETS -> loginLocal(platformEnum, name, edit)
            PlatformEnum.CASH -> loginLocal(platformEnum, name, edit)
            PlatformEnum.SAVINGS -> loginSavings(
                name,
                interests,
                frequency,
                recurringAmount,
                recurringFrequency,
                edit
            )
        }
    }

    fun openSignUp(tag: Any) {
        signUp.value = tag as String
    }

    fun login() {
        login.value = true
    }

    fun delete() {
        val name = getName()
        compositeDisposable.add(
            statementsRepository.deleteStatements(name)
                .andThen(platformDetailsRepository.deleteDetails(name))
                .andThen(platformRepository.deletePlatform(name))
                .andThen(deletePreferences(platform.get()!!, name))
                .delay(1, TimeUnit.SECONDS)
                .doOnSubscribe { status.postValue(Resource.loading()) }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ onSuccess() }, { onError(it) })
        )
    }

    fun resetStatements() {
        val name = getName()
        compositeDisposable.add(
            statementsRepository.deleteStatements(name)
                .andThen(deleteStatementsPreferences(platform.get()!!, name))
                .doOnSubscribe { status.postValue(Resource.loading()) }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ onSuccess() }, { onError(it) })
        )
    }

    private fun getName(): String {
        val platformEnum = platform.get()!!
        return when {
            platformEnum.platformTypeEnum == PlatformTypeEnum.P2P -> platformEnum.name
            savings.get() != null -> savings.get()!!
            else -> error("Invalid Name")
        }
    }

    // P2P without 2FA
    private fun generalLogin(
        platformEnum: PlatformEnum,
        email: String,
        password: String,
        edit: Boolean,
        frequentUpdates: Boolean
    ) {
        compositeDisposable.add(
            Single.just(true)
                .observeOn(schedulerProvider.io())
                .flatMap { refreshRepository.login(platformEnum, email, password) }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        onNextP2P(
                            email,
                            password,
                            platformEnum,
                            edit,
                            frequentUpdates,
                            platformDetails = it
                        )
                    },
                    { onError(it) }
                )
        )
    }

    private fun generalLogin2Step(
        platformEnum: PlatformEnum,
        email: String,
        password: String,
        cookies: String,
        edit: Boolean,
        frequentUpdates: Boolean
    ) {
        compositeDisposable.add(
            Single.just(true)
                .observeOn(schedulerProvider.io())
                .flatMap { refreshRepository.login(platformEnum, email, password, cookies) }
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        onNextP2P(
                            email,
                            password,
                            platformEnum,
                            edit,
                            frequentUpdates,
                            it
                        )
                    },
                    { onError(it) }
                )
        )
    }

    // Local accounts
    private fun loginLocal(platformEnum: PlatformEnum, name: String, edit: Boolean) {
        compositeDisposable.add(
            platformRepository.getPlatformByName(name)
                .flatMapCompletable { Completable.complete() }
                .onErrorResumeNext {
                    Completable.fromAction {
                        if (platformEnum == PlatformEnum.CASH) {
                            platformRepository.addOrUpdateCash(platformEnum, name, edit)
                        } else {
                            platformRepository.addOrUpdateGambling(platformEnum, name, edit)
                        }
                    }
                        .andThen { onSuccess() }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .delay(1, TimeUnit.SECONDS)
                .subscribe(
                    { onError(DuplicatedAccountException()) },
                    { onError(it) }
                )
        )
    }

    private fun loginSavings(
        name: String,
        interests: String,
        frequency: String,
        recurringAmount: String,
        recurringFrequency: String,
        edit: Boolean
    ) {
        compositeDisposable.add(
            platformRepository.getPlatformByName(name)
                .flatMapCompletable {
                    if (edit) {
                        Completable.fromAction {
                            platformRepository.addOrUpdateSavings(
                                PlatformEnum.SAVINGS,
                                name,
                                interests.toBigDecimal(),
                                Frequency.valueOf(frequency),
                                recurringAmount.toBigDecimal(),
                                Frequency.valueOf(recurringFrequency),
                                edit
                            )
                        }
                            .andThen { onSuccess() }
                    } else {
                        Completable.fromAction { onError(DuplicatedAccountException()) }
                    }
                }
                .onErrorResumeNext {
                    Completable.fromAction {
                        platformRepository.addOrUpdateSavings(
                            PlatformEnum.SAVINGS,
                            name,
                            interests.toBigDecimal(),
                            Frequency.valueOf(frequency),
                            recurringAmount.toBigDecimal(),
                            Frequency.valueOf(recurringFrequency),
                            edit
                        )
                    }
                        .andThen { onSuccess() }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .delay(1, TimeUnit.SECONDS)
                .subscribe({}, { onError(it) })
        )
    }

    // P2P with 2FA
    private fun loginTwoStep(
        platformEnum: PlatformEnum,
        email: String,
        password: String,
        frequentUpdates: Boolean
    ) {
        tfaRequired.value = Triple(platformEnum, Login(email, password), frequentUpdates)
    }

    // Results after 2FA
    fun onWebViewResult(
        loginStatus: Int,
        platformEnum: PlatformEnum,
        username: String,
        password: String,
        token: String?,
        edit: Boolean,
        frequentUpdates: Boolean
    ) {
        when (loginStatus) {
            BaseWebViewClient.SUCCESS -> generalLogin2Step(
                platformEnum,
                username,
                password,
                token!!,
                edit,
                frequentUpdates
            )
            BaseWebViewClient.FAILURE -> onError(AuthenticationFailedException())
            BaseWebViewClient.NETWORK -> onError(IOException())
        }
    }

    private fun onNextP2P(
        email: String,
        password: String,
        platformEnum: PlatformEnum,
        edit: Boolean,
        frequentUpdates: Boolean,
        platformDetails: PlatformDetails
    ) {
        compositeDisposable.add(
            Completable.fromAction {
                platformRepository.addOrUpdateP2p(
                    platformEnum,
                    email,
                    password,
                    edit,
                    frequentUpdates,
                    platformDetails
                )
            }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ onSuccess() }, { onError(it) })
        )
    }

    private fun onError(throwable: Throwable? = null) {
        throwable?.let {
            it.printStackTrace()
            when {
                it.isNoNetworkException() -> networkError.postValue(true)
                it is DuplicatedAccountException -> status.postValue(
                    Resource.error(
                        context.getString(
                            R.string.error_name_unique
                        )
                    )
                )
                it is AuthenticationFailedException -> status.postValue(
                    Resource.error(
                        context.getString(
                            R.string.wrong_username_password
                        )
                    )
                )
                else -> {
                    status.postValue(Resource.error(context.getString(R.string.generic_error_message)))
                    logger.log(it)
                }
            }
        }
    }

    private fun onSuccess() {
        status.postValue(Resource.success(null))
    }

    private fun setupEditUi(platform: Platform): Completable = Completable.fromAction {
        when (platform.type) {
            PlatformEnum.BETS,
            PlatformEnum.CASH,
            PlatformEnum.SAVINGS -> {
                savings.set(platform.name)
                interests.set(platform.interest.toString())
                recurringAmount.set(platform.recurringAmount.toString())
                editPlatform.postValue(platform)
            }
            else -> {
                email.set(platform.username)
                password.set(platform.password)
                frequentUpdates.set(platform.frequentUpdates)
            }
        }

        edit.set(true)
        button.set(R.string.save_button)
        this.platform.set(platform.type)
    }

    private fun setupNewUi(name: String): Completable = Completable.fromAction {
        val platformEnum = PlatformEnum.valueOf(name)
        platform.set(platformEnum)
        edit.set(false)
        button.set(R.string.login_button)
    }

    private fun deletePreferences(platformEnum: PlatformEnum, name: String): Completable =
        Completable.fromAction {
            if (platformEnum.platformTypeEnum == PlatformTypeEnum.P2P) {
                preferencesHelper.delete(platformEnum.prefLastUpdateDetails)
                preferencesHelper.delete(platformEnum.prefLastUpdateStatement)
                preferencesHelper.delete(platformEnum.prefLastFullStatementScan)
            } else {
                preferencesHelper.delete(platformEnum.prefLastUpdateDetails + name)
                preferencesHelper.delete(platformEnum.prefLastUpdateStatement + name)
                preferencesHelper.delete(platformEnum.prefLastFullStatementScan + name)
            }
        }

    private fun deleteStatementsPreferences(platformEnum: PlatformEnum, name: String): Completable =
        Completable.fromAction {
            if (platformEnum.platformTypeEnum == PlatformTypeEnum.P2P) {
                preferencesHelper.delete(platformEnum.prefLastUpdateStatement)
                preferencesHelper.delete(platformEnum.prefLastFullStatementScan)
            } else {
                preferencesHelper.delete(platformEnum.prefLastUpdateStatement + name)
                preferencesHelper.delete(platformEnum.prefLastFullStatementScan + name)
            }
        }
}
