package pt.tiagocarvalho.financetracker.ui.management

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.model.ManagementItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class ManagementViewModel @Inject constructor(
    private val platformRepository: PlatformRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger
) : BaseViewModel(), ManagementListener {

    val platforms: SingleLiveEvent<List<String>> = SingleLiveEvent()
    val login: SingleLiveEvent<Pair<String, String>> = SingleLiveEvent()
    val popup: SingleLiveEvent<String> = SingleLiveEvent()
    val popupPremium: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val status: MutableLiveData<Resource<String?>> = MutableLiveData()
    val accounts: MutableLiveData<List<ManagementItem>> = MutableLiveData()

    fun load() {
        compositeDisposable.add(
            platformRepository.getManagementPlatforms()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({ accounts.postValue(it) }, { logger.log(it) })
        )
    }

    override fun showLogin(name: String) {
        compositeDisposable.add(
            platformRepository.getPlatformByName(name)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { login.value = Pair(name, name) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { handleMaxPlatforms(name, name) })
        )
    }

    override fun showPopUp(name: String) {
        popup.value = name
    }

    fun showPlatformsDialog(platformEnum: PlatformEnum) {
        compositeDisposable.add(
            platformRepository.getPlatformsNamesByTypeAsSingle(platformEnum)
                .doOnSuccess { platforms.postValue(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe()
        )
    }

    fun handleMaxPlatforms(name: String, name1: String) {
        compositeDisposable.add(
            platformRepository.canLogin()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess {
                    if (it) {
                        login.value = Pair(name, name1)
                    } else {
                        popupPremium.value = true
                    }
                }
                .subscribe()
        )
    }
}

interface ManagementListener {

    fun showLogin(name: String)

    fun showPopUp(name: String)
}
