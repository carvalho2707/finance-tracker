package pt.tiagocarvalho.financetracker.ui.tweets

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class TweetsFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        logger: Logger,
        analyticsLogger: AnalyticsLogger,
        twitterUseCase: TwitterUseCase
    ): TweetsViewModel = TweetsViewModel(
        schedulerProvider,
        logger,
        analyticsLogger,
        twitterUseCase
    )
}
