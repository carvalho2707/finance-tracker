package pt.tiagocarvalho.financetracker.ui.tweets

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import pt.tiagocarvalho.financetracker.GlideApp
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ItemTweetsBinding
import pt.tiagocarvalho.financetracker.model.TweetItem
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter

class TweetsAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    val context: Context
) : DataBoundListAdapter<TweetItem, ItemTweetsBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<TweetItem>() {
        override fun areItemsTheSame(
            oldItem: TweetItem,
            newItem: TweetItem
        ) = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: TweetItem,
            newItem: TweetItem
        ) = oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemTweetsBinding = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.item_tweets,
        parent,
        false,
        dataBindingComponent
    )

    override fun bind(binding: ItemTweetsBinding, item: TweetItem) {
        binding.tweet = item
        if (item.bodyImageUrl != null) {
            GlideApp
                .with(context)
                .load(item.bodyImageUrl)
                .into(binding.bodyImage)

            binding.bodyImage.setOnClickListener {
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(item.bodyImageUrl)
                it.context.startActivity(i)
            }
        } else {
            binding.bodyImage.visibility = GONE
        }

        binding.icon.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(item.twitterAccountUrl)
            it.context.startActivity(i)
        }
        binding.cardView.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(item.tweetUrl)
            it.context.startActivity(i)
        }
    }
}
