package pt.tiagocarvalho.financetracker.ui.main

import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger

@HiltViewModel
class MainViewModel @Inject constructor(
    private val analyticsLogger: AnalyticsLogger,
    private val mainUseCase: MainUseCase,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    val showAds: SingleLiveEvent<Boolean> = SingleLiveEvent()

    init {
        handleShowAds()
    }

    private fun handleShowAds() {
        compositeDisposable.add(
            mainUseCase.canShowAds()
                .subscribeOn(schedulerProvider.io())
                .doOnSuccess { showAds.postValue(it) }
                .subscribe()
        )
    }

    fun onAdNotNeeded() {
        analyticsLogger.logAdPremiumNotNeeded()
    }

    fun onAdSuccessToLoad() {
        analyticsLogger.logAdSuccessToLoad()
    }

    fun onAdFailedToLoad(errorCode: Int) {
        analyticsLogger.logAdFailedToLoad(errorCode.toString())
    }
}
