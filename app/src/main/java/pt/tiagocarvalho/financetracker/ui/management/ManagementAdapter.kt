package pt.tiagocarvalho.financetracker.ui.management

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ItemManagementBinding
import pt.tiagocarvalho.financetracker.model.ManagementItem
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter

class ManagementAdapter(
    appExecutors: AppExecutors,
    private val managementListener: ManagementListener
) : DataBoundListAdapter<ManagementItem, ItemManagementBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<ManagementItem>() {
        override fun areItemsTheSame(oldItem: ManagementItem, newItem: ManagementItem): Boolean =
            oldItem.platform == newItem.platform

        override fun areContentsTheSame(oldItem: ManagementItem, newItem: ManagementItem): Boolean =
            oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemManagementBinding = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.item_management,
        parent,
        false
    )

    override fun bind(binding: ItemManagementBinding, item: ManagementItem) {
        binding.item = item
        binding.showCheck = item.owned && item.platform.platformTypeEnum == PlatformTypeEnum.P2P
        binding.showCount = item.owned && item.platform.platformTypeEnum != PlatformTypeEnum.P2P
        binding.root.setTag(
            R.id.tag_platform_type,
            item.platform.name
        ) // Used to identify clicked view

        binding.root.setOnClickListener {
            when (item.platform.platformTypeEnum) {
                PlatformTypeEnum.P2P -> managementListener.showLogin(item.platform.name)
                PlatformTypeEnum.SAVINGS,
                PlatformTypeEnum.GAMBLING,
                PlatformTypeEnum.CASH ->
                    if (item.owned) {
                        managementListener.showPopUp(item.platform.name)
                    } else {
                        managementListener.showLogin(item.platform.name)
                    }
            }
        }
    }
}
