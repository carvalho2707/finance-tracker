package pt.tiagocarvalho.financetracker.ui.auth.twino

import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.model.twino.TwinoLoginTfa
import pt.tiagocarvalho.p2p.services.retrofit.TwinoApi

class TwinoAuthUseCase(
    private val twinoApi: TwinoApi,
    private val logger: Logger
) {

    internal fun checkTfa(email: String) =
        twinoApi.checkTfa(email)
            .doOnError { logger.log(it) }

    fun loginTfa(email: String, password: String, code: String?) =
        twinoApi.loginTfa(TwinoLoginTfa(email, password, code))
            .doOnError { logger.log(it) }
}
