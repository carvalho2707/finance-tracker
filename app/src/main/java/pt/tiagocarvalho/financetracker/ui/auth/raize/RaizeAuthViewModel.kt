package pt.tiagocarvalho.financetracker.ui.auth.raize

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.auth.LoginResponse
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@HiltViewModel
class RaizeAuthViewModel @Inject constructor(
    private val raizeAuthUseCase: RaizeAuthUseCase,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    val visibility: MutableLiveData<Boolean> = MutableLiveData(false)
    val token: MutableLiveData<String> = MutableLiveData()
    val isPasswordError: MutableLiveData<Boolean> = MutableLiveData()
    val isCodeError: MutableLiveData<Boolean> = MutableLiveData()
    val isUnknownError: MutableLiveData<Boolean> = MutableLiveData()

    fun authenticate(email: String, password: String) {
        compositeDisposable.add(
            raizeAuthUseCase.login(email, password)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { visibility.value = false }
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { it.printStackTrace() })
        )
    }

    fun authenticateTfa(email: String, password: String, code: String) {
        compositeDisposable.add(
            raizeAuthUseCase.loginTfa(email, password, code)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun onAuthenticationSuccess(loginResponse: LoginResponse) {
        when {
            loginResponse.tfaActive -> visibility.value = true
            loginResponse.token != null -> token.value = loginResponse.token
            loginResponse.isPasswordError -> isPasswordError.value = true
            loginResponse.isCodeError -> isCodeError.value = true
            loginResponse.isUnknownError -> isUnknownError.value = true
            else -> return
        }
    }
}
