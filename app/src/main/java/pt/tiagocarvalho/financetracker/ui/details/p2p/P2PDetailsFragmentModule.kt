package pt.tiagocarvalho.financetracker.ui.details.p2p

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class P2PDetailsFragmentModule {

    @Provides
    fun provideViewModel(
        platformRepository: PlatformRepository,
        statementsRepository: StatementsRepository,
        preferencesHelper: PreferencesHelper,
        schedulerProvider: SchedulerProvider,
        logger: Logger,
        p2pDetailsUseCase: P2pDetailsUseCase
    ): P2PDetailsViewModel = P2PDetailsViewModel(
        platformRepository,
        statementsRepository,
        preferencesHelper,
        schedulerProvider,
        logger,
        p2pDetailsUseCase
    )
}
