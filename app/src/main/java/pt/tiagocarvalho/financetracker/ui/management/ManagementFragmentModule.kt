package pt.tiagocarvalho.financetracker.ui.management

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class ManagementFragmentModule {

    @Provides
    fun provideViewModel(
        platformRepository: PlatformRepository,
        schedulerProvider: SchedulerProvider,
        logger: Logger
    ): ManagementViewModel = ManagementViewModel(
        platformRepository,
        schedulerProvider,
        logger
    )
}
