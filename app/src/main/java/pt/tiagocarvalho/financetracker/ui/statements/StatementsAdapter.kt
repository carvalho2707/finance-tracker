package pt.tiagocarvalho.financetracker.ui.statements

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.DiffUtil
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ItemStatementBinding
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.StatementsStatus
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter

class StatementsAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    val context: Context,
    private val navController: NavController
) : DataBoundListAdapter<StatementsStatus, ItemStatementBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<StatementsStatus>() {
        override fun areItemsTheSame(
            oldItem: StatementsStatus,
            newItem: StatementsStatus
        ) = oldItem.name == newItem.name

        override fun areContentsTheSame(
            oldItem: StatementsStatus,
            newItem: StatementsStatus
        ) = oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemStatementBinding {
        val binding = DataBindingUtil.inflate<ItemStatementBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_statement,
            parent,
            false,
            dataBindingComponent
        )
        binding.root.setOnClickListener {
            binding.statement?.let {
                when (it.platform.platformTypeEnum) {
                    PlatformTypeEnum.P2P -> navController.navigate(
                        StatementsFragmentDirections.showP2P(
                            it.name
                        )
                    )
                    PlatformTypeEnum.SAVINGS -> navController.navigate(
                        StatementsFragmentDirections.showSavings(
                            it.name
                        )
                    )
                    PlatformTypeEnum.GAMBLING -> navController.navigate(
                        StatementsFragmentDirections.showGambling(
                            it.name
                        )
                    )
                    PlatformTypeEnum.CASH -> navController.navigate(
                        StatementsFragmentDirections.showCash(
                            it.name
                        )
                    )
                }
            }
        }
        return binding
    }

    override fun bind(binding: ItemStatementBinding, item: StatementsStatus) {
        binding.statement = item
    }
}
