package pt.tiagocarvalho.financetracker.ui.accounts

import io.reactivex.Single
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.DisplayMode
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.repository.PreferencesRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.ui.accounts.last_change.LastChangeInfo
import pt.tiagocarvalho.financetracker.ui.accounts.last_change.LastChangeItem
import pt.tiagocarvalho.financetracker.utils.AccountItemComparator
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal

class AccountsUseCase(
    private val accountsRepository: AccountsRepository,
    private val preferencesRepository: PreferencesRepository,
    private val refreshRepository: RefreshRepository,
    private val platformDetailsRepository: PlatformDetailsRepository
) {

    companion object {
        private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    }

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    internal fun loadDetails(
        shouldFetch: Boolean,
        loadedData: MutableMap<PlatformEnum, WebViewData>
    ): Single<AccountsInfo> {
        return Single.zip(
            accountsRepository.loadDetails(shouldFetch, loadedData),
            getPreferences(),
            { details: Resource<List<PlatformDetails>>, preferences: Preferences ->
                mapToAccountsInfo(
                    details,
                    preferences
                )
            }
        )
            .doOnSuccess { preferencesRepository.setUiReady() }
            .doOnSubscribe { preferencesRepository.resetUiReady() }
    }

    internal fun hasMaxPlatforms() = accountsRepository.hasMaxPlatforms()

    internal fun getTwoFactorPlatforms() = accountsRepository.getTwoFactorPlatforms()

    internal fun getPlatformWithCashDrag() = accountsRepository.getPlatformWithCashDrag()

    internal fun getAndUpdateLastSessionBalance() =
        platformDetailsRepository.getPlatforms()
            .flattenAsObservable { it }
            .flatMap { accountsRepository.getLastSessionChange(it, it.balance) }
            .toList()
            .map { changeInfo -> mapToLastChange(changeInfo) }

    private fun mapToLastChange(platforms: List<Pair<PlatformDetails, BigDecimal>>): LastChangeInfo {
        val display = DisplayMode.valueOf(preferencesRepository.getDisplayMode())
        var items = emptyList<LastChangeItem>().toMutableList()
        var totalSessionChange = BigDecimal.ZERO
        var totalDailyChange = BigDecimal.ZERO
        var totalBalance = BigDecimal.ZERO
        platforms.forEach {
            val details = it.first
            totalSessionChange = totalSessionChange.add(it.second)
            totalDailyChange = totalDailyChange.add(details.changeValue)
            totalBalance = totalBalance.add(details.balance)
            items.add(
                LastChangeItem(
                    details.platform.logoId,
                    details.name,
                    details.platformType != PlatformTypeEnum.P2P,
                    numberFormat.format(it.second.toDouble()),
                    it.second,
                    it.second.compareTo(BigDecimal.ZERO)
                )
            )
        }
        val dailyChange: BigDecimal
        val sessionChange: BigDecimal

        if (display == DisplayMode.ABSOLUTE) {
            dailyChange = totalDailyChange
            sessionChange = totalSessionChange
        } else {
            dailyChange = Utils.calculateChangePercentage(totalDailyChange, totalBalance)
            sessionChange = Utils.calculateChangePercentage(totalSessionChange, totalBalance)
        }

        items = items.filter { it.comparator != 0 }.toMutableList()
        items.sortByDescending { it.valueBigDecimal }

        return LastChangeInfo(
            items,
            dailyChange,
            sessionChange,
            display
        )
    }

    internal fun isRefreshDetailsNeeded(force: Boolean) =
        refreshRepository.isRefreshDetailsNeeded(force)

    private fun getPreferences(): Single<Preferences> = Single.create {
        val balancePref = preferencesRepository.getBalancePref()
        val variation = preferencesRepository.getVariationPref()
        val positiveVariation = preferencesRepository.getPositiveVariationPref()
        val sortOrderId = preferencesRepository.getSortOrderPref()
        val platformType = preferencesRepository.getFilterTypePref()
        val display = DisplayMode.valueOf(preferencesRepository.getDisplayMode())

        val preferences = Preferences(
            balancePref,
            variation,
            positiveVariation,
            sortOrderId,
            platformType,
            display
        )
        it.onSuccess(preferences)
    }

    private fun mapToAccountsInfo(
        detailsResponse: Resource<List<PlatformDetails>>,
        preferences: Preferences
    ): AccountsInfo {

        val status = detailsResponse.status
        val error = detailsResponse.message

        requireNotNull(detailsResponse.data)
        val balance = detailsResponse.data.sumByBigDecimal { it.balance }
        val list = detailsResponse.data
            .filter { account -> includeInResponse(preferences, account) }
            .map { account -> mapToAccountItem(account) }
            .sortedWith(AccountItemComparator(preferences.sortOrderId))

        return AccountsInfo(
            list,
            status,
            error,
            balance.setScale(2, RoundingMode.HALF_UP)
        )
    }

    private fun mapToAccountItem(account: PlatformDetails) = AccountItem(
        account.platform,
        account.name,
        account.balance.setScale(2, RoundingMode.HALF_UP),
        account.changeValue.setScale(2, RoundingMode.HALF_UP),
        account.changePercentage.setScale(2, RoundingMode.HALF_UP)
    )

    private fun includeInResponse(
        preferences: Preferences,
        account: PlatformDetails
    ): Boolean {
        if (preferences.balance && account.balance.compareTo(BigDecimal.ZERO) == 0) {
            return false
        }
        if (preferences.variation && account.changeValue.compareTo(BigDecimal.ZERO) == 0) {
            return false
        }
        if (preferences.positiveVariation && account.changeValue.compareTo(BigDecimal.ZERO) == 0) {
            return false
        }
        return preferences.platformType.any { it == account.platform.platformTypeEnum.name }
    }
}
