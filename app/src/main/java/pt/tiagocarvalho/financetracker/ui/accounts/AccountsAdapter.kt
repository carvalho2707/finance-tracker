package pt.tiagocarvalho.financetracker.ui.accounts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import java.math.BigDecimal.ZERO
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ItemAccountBinding
import pt.tiagocarvalho.financetracker.model.DisplayMode
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter
import pt.tiagocarvalho.financetracker.utils.isDayMode

class AccountsAdapter(
    appExecutors: AppExecutors,
    val context: Context,
    private val displayMode: DisplayMode,
    private val accountClickCallback: ((AccountItem) -> Unit)?
) : DataBoundListAdapter<AccountItem, ItemAccountBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<AccountItem>() {
        override fun areItemsTheSame(oldItem: AccountItem, newItem: AccountItem): Boolean =
            oldItem.name == newItem.name

        override fun areContentsTheSame(
            oldItem: AccountItem,
            newItem: AccountItem
        ): Boolean = oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemAccountBinding {
        val binding = DataBindingUtil.inflate<ItemAccountBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_account,
            parent,
            false
        )
        binding.root.setOnClickListener {
            binding.account?.let {
                accountClickCallback?.invoke(it)
            }
        }
        binding.displayMode = displayMode
        return binding
    }

    override fun bind(binding: ItemAccountBinding, item: AccountItem) {
        val ratio = item.changeValue.compareTo(ZERO)
        when {
            ratio > 0 -> binding.textColor = R.color.inwallet_green_500
            ratio < 0 -> binding.textColor = R.color.inwallet_red_500
            else ->
                if (context.isDayMode()) binding.textColor =
                    R.color.inwallet_grey_500 else binding.textColor = R.color.inwallet_grey_200
        }

        binding.account = item
    }
}
