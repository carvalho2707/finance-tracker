package pt.tiagocarvalho.financetracker.ui.statistics.forecast

import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import dagger.hilt.android.AndroidEntryPoint
import java.util.ArrayList
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentForecastBinding
import pt.tiagocarvalho.financetracker.model.ForecastStatistics
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Constants.MATERIAL_COLORS_500
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.charts.CurrencyValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.PointLabelCurrencyValueFormatter

@AndroidEntryPoint
class ForecastFragment : BaseFragment() {
    companion object {
        fun newInstance(): ForecastFragment = ForecastFragment()
    }

    private lateinit var binding: FragmentForecastBinding

    private val forecastViewModel: ForecastViewModel by viewModels()
    private var color = 0
    private var viewMode = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentForecastBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = forecastViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        forecastViewModel.status.observe(viewLifecycleOwner) { status ->
            if (status == Status.LOADING) {
                binding.contentScroll.visibility = View.GONE
                binding.noAccountsMessage.visibility = View.GONE
                binding.animationView.setAnimation(R.raw.loading)
                binding.animationView.repeatCount = -1
                binding.animationView.playAnimation()
            } else binding.animationView.repeatCount = 0
        }

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        color = Utils.themeAttributeToColorInt(android.R.attr.textColorPrimary, requireContext())
        createCompoundChart()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        forecastViewModel.statistics.observe(viewLifecycleOwner) {
            it?.let {
                if (it.show) {
                    binding.noAccountsMessage.visibility = View.GONE
                    binding.contentScroll.visibility = View.VISIBLE
                    updateUI(it)
                } else {
                    binding.contentScroll.visibility = View.GONE
                    binding.noAccountsMessage.visibility = View.VISIBLE
                }
            }
        }

        forecastViewModel.byYear.observe(viewLifecycleOwner) {
            showByYear()
        }

        forecastViewModel.byMonth.observe(viewLifecycleOwner) {
            showByMonth()
        }

        forecastViewModel.loadForecastStatistics()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> forecastViewModel.loadForecastStatistics()
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun updateUI(statistics: ForecastStatistics) {
        updateCompoundChart(
            statistics.monthlyCompoundProjection,
            statistics.yearlyCompoundProjection
        )
    }

    private fun createCompoundChart() {
        binding.lcP2pProj.description.isEnabled = false
        binding.lcP2pProj.setDrawGridBackground(false)
        binding.lcP2pProj.extraRightOffset = 10f

        binding.lcP2pProj.animateX(1500)
        binding.lcP2pProj.setTouchEnabled(false)

        binding.lcP2pProj.axisRight.setDrawGridLines(false)
        binding.lcP2pProj.axisRight.isEnabled = false
        binding.lcP2pProj.axisRight.setDrawAxisLine(false)
        binding.lcP2pProj.legend.isEnabled = true
        binding.lcP2pProj.legend.textColor = color

        val leftAxis = binding.lcP2pProj.axisLeft
        leftAxis.setLabelCount(8, false)
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = CurrencyValueFormatter()
        leftAxis.spaceTop = 15f
        leftAxis.textSize = 7f
        leftAxis.setDrawGridLines(false)
        leftAxis.textColor = color

        val xAxis = binding.lcP2pProj.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.isEnabled = true
        xAxis.textSize = 7f
        xAxis.textColor = color
        xAxis.setLabelCount(6, true)
        xAxis.setDrawGridLines(false)
    }

    private fun updateCompoundChart(
        monthlyCompoundProjection: SparseArray<Float>,
        yearlyCompoundProjection: SparseArray<Float>
    ) {
        val values = if (viewMode == 0) monthlyCompoundProjection else yearlyCompoundProjection
        val count =
            if (viewMode == 0) monthlyCompoundProjection.size() else yearlyCompoundProjection.size()

        val entries = ArrayList<Entry>()
        for (i in 0 until values.size()) {
            val value = values.get(values.keyAt(i))
            entries.add(Entry(i.toFloat(), value))
        }

        val ds1 = LineDataSet(entries, getString(R.string.balance_projection))
        ds1.color = MATERIAL_COLORS_500[0]
        ds1.setCircleColor(MATERIAL_COLORS_500[0])
        ds1.lineWidth = 2.5f
        ds1.valueTextSize = 7f
        ds1.circleRadius = 3f
        ds1.valueTextColor = color

        val sets = listOf<ILineDataSet>(ds1)
        val lineData = LineData(sets)
        lineData.setValueFormatter(PointLabelCurrencyValueFormatter(count))
        lineData.setValueTextSize(7f)

        binding.lcP2pProj.data = lineData

        lineData.notifyDataChanged()
        binding.lcP2pProj.notifyDataSetChanged()
        binding.lcP2pProj.invalidate()
    }

    private fun showByYear() {
        viewMode = 1
        updateCompoundChart(
            forecastViewModel.statistics.value!!.monthlyCompoundProjection,
            forecastViewModel.statistics.value!!.yearlyCompoundProjection
        )
    }

    private fun showByMonth() {
        viewMode = 0
        updateCompoundChart(
            forecastViewModel.statistics.value!!.monthlyCompoundProjection,
            forecastViewModel.statistics.value!!.yearlyCompoundProjection
        )
    }

    override fun onToolbarItemClicked(menuItem: MenuItem) = false
}
