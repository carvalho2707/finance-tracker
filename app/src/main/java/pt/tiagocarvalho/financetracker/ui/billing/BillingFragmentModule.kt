package pt.tiagocarvalho.financetracker.ui.billing

import android.app.Application
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
class BillingFragmentModule {

    @Provides
    fun provideViewModel(
        application: Application
    ): BillingViewModel = BillingViewModel(
        application
    )
}
