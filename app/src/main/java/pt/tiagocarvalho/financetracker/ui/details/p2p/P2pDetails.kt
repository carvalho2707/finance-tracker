package pt.tiagocarvalho.financetracker.ui.details.p2p

import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.DataItem
import pt.tiagocarvalho.financetracker.model.Status

data class P2pDetails(
    val status: Status,
    val balance: BigDecimal,
    val hasIssues: Boolean,
    val statements: List<PlatformStatement>,
    val dataList: List<DataItem>
)
