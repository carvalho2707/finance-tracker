package pt.tiagocarvalho.financetracker.ui.statistics

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.databinding.FragmentStatisticsBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.financetracker.webview.BaseWebViewNavigator
import pt.tiagocarvalho.p2p.api.model.Login

@AndroidEntryPoint
class StatisticsFragment : BaseFragment(), BaseWebViewNavigator {

    private lateinit var binding: FragmentStatisticsBinding

    private val statisticsViewModel: StatisticsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentStatisticsBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerToolbarWithNavigation(binding.toolbar)

        setupUI()

        statisticsViewModel.status.observe(viewLifecycleOwner) {
            handleStatus(it)
        }

        statisticsViewModel.tfaRequired.observe(viewLifecycleOwner) {
            if (AuthDialogUtils.belongsToAuthDialog(it.type)) {
                loadAuthDialog(it)
            } else {
                loadWebView(it)
            }
        }

        statisticsViewModel.loadRefreshNeeded(false)
    }

    private fun setupUI() {
        binding.viewPager.adapter = StatisticsViewPagerAdapter(this)

        TabLayoutMediator(binding.tabs, binding.viewPager) { tab, position ->
            tab.text = getTabTitle(position)
        }.attach()
    }

    private fun getTabTitle(position: Int): CharSequence? = when (position) {
        0 -> context?.getString(R.string.fragment_statistics_general)
        1 -> context?.getString(R.string.fragment_statistics_returns)
        2 -> context?.getString(R.string.fragment_statistics_p2p)
        3 -> context?.getString(R.string.fragment_statistics_forecast)
        else -> throw RuntimeException()
    }

    private fun handleStatus(status: Status?) {
        if (status == Status.LOADING) {
            binding.webView.visibility = View.GONE
            binding.tabs.visibility = View.GONE
            binding.viewPager.visibility = View.GONE
            binding.layoutLoading.visibility = View.VISIBLE
        } else {
            binding.tabs.visibility = View.VISIBLE
            binding.viewPager.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
            binding.layoutLoading.visibility = View.GONE
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(login: Platform) {
        binding.layoutLoading.visibility = View.INVISIBLE
        binding.webView.visibility = View.VISIBLE
        val client = BaseWebViewClient.getClient(
            requireContext(),
            layoutInflater,
            login.type,
            Login(login.username!!, login.password!!),
            this,
            isLogin = false,
            frequentUpdates = true
        )
        binding.webView.webViewClient = client

        val webSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.userAgentString = Constants.USER_AGENT_VALUE

        binding.webView.loadUrl(client.getInitialUrl())
    }

    private fun loadAuthDialog(platform: Platform) {
        AuthDialogUtils.showAuthDialogFragment(
            platform.type,
            platform.username!!,
            platform.password!!,
            this@StatisticsFragment
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val platform = PlatformEnum.valueOf(data?.getStringExtra(PLATFORM)!!)
        if (resultCode == SUCCESS) {
            statisticsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)!!))
        } else if (resultCode == INVALID_USERNAME_PASSWORD_CODE ||
            resultCode == UNKNOWN_ERROR_CODE
        ) {
            setupUI()
        } else if (resultCode == AuthDialogUtils.CANCEL) {
            statisticsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)))
        }
    }

    override fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String?
    ) = Unit

    override fun onError(platformEnum: PlatformEnum, errorCode: Int) {
        if (errorCode == 0) {
            setupUI()
        }
    }

    override fun loadInfo(webViewData: WebViewData?) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            binding.webView.visibility = View.GONE
            binding.tabs.visibility = View.GONE
            binding.layoutLoading.visibility = View.VISIBLE
        }
        statisticsViewModel.loadNextAuth(webViewData)
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean =
        if (menuItem.itemId == R.id.menu_settings) {
            findNavController().navigate(MainDirections.actionGlobalSettings())
            true
        } else super.onOptionsItemSelected(menuItem)
}
