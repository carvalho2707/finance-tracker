package pt.tiagocarvalho.financetracker.ui.settings

import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.Intent.ACTION_SEND
import android.content.Intent.ACTION_SENDTO
import android.content.Intent.EXTRA_EMAIL
import android.content.Intent.EXTRA_SUBJECT
import android.content.Intent.EXTRA_TEXT
import android.net.Uri
import android.os.Build.BRAND
import android.os.Build.MANUFACTURER
import android.os.Build.MODEL
import android.os.Build.VERSION.RELEASE
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.viewModels
import androidx.preference.EditTextPreference
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.util.Locale
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.model.DisplayMode
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_DAY_NIGHT_OPTION
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_DISPLAY_MODE
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_IDLE_MONEY_THRESHOLD
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger

@AndroidEntryPoint
class SettingsFragment : PreferenceFragmentCompat() {

    @Inject
    lateinit var analyticsLogger: AnalyticsLogger

    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    private val settingsViewModel: SettingsViewModel by viewModels()
    private val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault())

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.pref_general)

        setupDayNight()
        setupAppVersion()
        setupSendFeedback()
        setupSupportedCurrencies()
        setupNextPlatforms()
        setupNetworkUsage()
        setupIdleMoneyThreshold()
        setupDisplayMode()
        setupTelegram()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        settingsViewModel.nextPlatforms.observe(viewLifecycleOwner) {
            updateNextPlatforms(it)
        }

        settingsViewModel.networkUsage.observe(viewLifecycleOwner) {
            updateNetworkUsage(it)
        }
    }

    private fun setupSendFeedback() {
        val sendFeedback: Preference? = findPreference("pref_send_feedback")
        sendFeedback?.setOnPreferenceClickListener {
            var body: String =
                context?.packageManager?.getPackageInfo(
                    requireContext().packageName,
                    0
                )?.versionName!!
            body =
                "\n\n-----------------------------\nPlease don't remove this information\n Device OS: Android \n Device OS version: " +
                RELEASE + "\n App Version: " + body + "\n Device Brand: " + BRAND +
                "\n Device Model: " + MODEL + "\n Device Manufacturer: " + MANUFACTURER

            val selectorIntent = Intent(ACTION_SENDTO)
            selectorIntent.data = Uri.parse("mailto:")

            val emailIntent = Intent(ACTION_SEND).apply {
                putExtra(EXTRA_EMAIL, arrayOf("tiago.s.carvalho1992@gmail.com"))
                putExtra(EXTRA_SUBJECT, "Query from inWallet app")
                putExtra(EXTRA_TEXT, body)
                selector = selectorIntent
            }
            context?.startActivity(Intent.createChooser(emailIntent, "Choose email client"))
            true
        }
    }

    private fun setupTelegram() {
        val telegram: Preference? = findPreference("pref_text_telegram")
        telegram?.setOnPreferenceClickListener {
            startBrowserActivity(Uri.parse("https://t.me/inWalletOfficial"))
            false
        }
    }

    private fun startBrowserActivity(uri: Uri) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, uri))
        } catch (exception: ActivityNotFoundException) {
            exception.printStackTrace()
            view?.let {
                Snackbar.make(it, R.string.generic_error_message, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun setupSupportedCurrencies() {
        val supportedCurrencies: Preference? = findPreference("pref_supported_currencies")
        supportedCurrencies?.setOnPreferenceClickListener {

            analyticsLogger.logSupportedCurrencies()
            val mySnackbar = Snackbar.make(
                requireView(), "More currencies will be supported soon", Snackbar.LENGTH_LONG
            )
            mySnackbar.show()

            true
        }
    }

    private fun setupIdleMoneyThreshold() {
        val idleMoneyThreshold: EditTextPreference? = findPreference(PREF_IDLE_MONEY_THRESHOLD)
        idleMoneyThreshold?.apply {

            setOnBindEditTextListener {
                it.inputType = InputType.TYPE_CLASS_NUMBER
                it.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(7))
            }

            setOnPreferenceChangeListener { _, newValue ->
                val value = newValue.toString().ifEmpty { "0" }.toInt()
                idleMoneyThreshold.summary = "${numberFormat.format(value)} €"
                preferencesHelper.put(PREF_IDLE_MONEY_THRESHOLD, value)
                true
            }
        }

        val currentValue = preferencesHelper.get(PREF_IDLE_MONEY_THRESHOLD, 10)
        idleMoneyThreshold?.summary = "$currentValue €"
    }

    private fun setupDisplayMode() {
        val idleMoneyThreshold: ListPreference? = findPreference(PREF_DISPLAY_MODE)
        idleMoneyThreshold?.setOnPreferenceChangeListener { _, newValue ->
            idleMoneyThreshold.summary = DisplayMode.valueOf(newValue.toString()).summary
            preferencesHelper.put(PREF_DISPLAY_MODE, newValue.toString())
            true
        }

        val displayMode =
            DisplayMode.valueOf(preferencesHelper.get(PREF_DISPLAY_MODE, DisplayMode.ABSOLUTE.name))
        idleMoneyThreshold?.summary = displayMode.summary
    }

    private fun setupAppVersion() {
        val version: Preference? = findPreference("pref_app_version")
        version?.summary =
            context?.packageManager?.getPackageInfo(requireContext().packageName, 0)?.versionName
    }

    private fun setupDayNight() {
        val listPreference: ListPreference? = findPreference(PREF_DAY_NIGHT_OPTION)
        listPreference?.summaryProvider = ListPreference.SimpleSummaryProvider.getInstance()
        listPreference?.onPreferenceChangeListener =
            Preference.OnPreferenceChangeListener { _, newValue ->
                AppCompatDelegate.setDefaultNightMode((newValue as String).toInt())
                true
            }
    }

    private fun setupNextPlatforms() {
        settingsViewModel.loadNextPlatforms()
    }

    private fun setupNetworkUsage() {
        settingsViewModel.loadNetworkUsage()
    }

    private fun updateNextPlatforms(value: String) {
        val version: Preference? = findPreference("pref_next_platforms")
        version?.summary = value
    }

    private fun updateNetworkUsage(value: String) {
        val version: Preference? = findPreference("pref_network_usage")
        version?.summary = value
    }
}
