package pt.tiagocarvalho.financetracker.ui.auth

data class LoginResponse(
    val tfaActive: Boolean = false,
    val cookies: Map<String, String>? = null,
    val token: String? = null,
    val secondaryToken: String? = null,
    val isPasswordError: Boolean = false,
    val isCodeError: Boolean = false,
    val isUnknownError: Boolean = false
)
