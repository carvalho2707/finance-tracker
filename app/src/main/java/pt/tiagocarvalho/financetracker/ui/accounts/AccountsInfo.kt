package pt.tiagocarvalho.financetracker.ui.accounts

import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.Status

data class AccountsInfo(
    val itemList: List<AccountItem>,
    val status: Status,
    val error: String?,
    val balance: BigDecimal
) {
    constructor() : this(
        emptyList(),
        Status.LOADING,
        null,
        BigDecimal.ZERO
    )
}
