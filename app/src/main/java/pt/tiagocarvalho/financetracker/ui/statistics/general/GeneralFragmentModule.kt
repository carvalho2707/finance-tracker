package pt.tiagocarvalho.financetracker.ui.statistics.general

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class GeneralFragmentModule {

    @Provides
    fun provideViewModel(
        platformDetailsRepository: PlatformDetailsRepository,
        schedulerProvider: SchedulerProvider,
        logger: Logger
    ): GeneralViewModel = GeneralViewModel(platformDetailsRepository, schedulerProvider, logger)
}
