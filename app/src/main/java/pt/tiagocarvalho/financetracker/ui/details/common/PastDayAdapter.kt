package pt.tiagocarvalho.financetracker.ui.details.common

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.databinding.ItemPastDayBinding
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter

class PastDayAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    val context: Context
) : DataBoundListAdapter<PlatformStatement, ItemPastDayBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<PlatformStatement>() {
        override fun areItemsTheSame(
            oldItem: PlatformStatement,
            newItem: PlatformStatement
        ) = oldItem.label == newItem.label

        override fun areContentsTheSame(
            oldItem: PlatformStatement,
            newItem: PlatformStatement
        ) = oldItem == newItem
    }
) {

    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun createBinding(parent: ViewGroup): ItemPastDayBinding = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.item_past_day,
        parent,
        false,
        dataBindingComponent
    )

    override fun bind(binding: ItemPastDayBinding, item: PlatformStatement) {
        binding.statement = item

        val color = when (item.type) {
            StatementType.DEPOSIT -> R.color.inwallet_green_500
            StatementType.WITHDRAWAL -> R.color.inwallet_red_500
            StatementType.INTEREST -> R.color.inwallet_green_500
            StatementType.FEE -> R.color.inwallet_red_500
            StatementType.INVESTMENT -> Utils.themeAttributeToColorRes(
                R.attr.colorOnSurface,
                context
            )
            StatementType.PRINCIPAL -> Utils.themeAttributeToColorRes(
                R.attr.colorOnSurface,
                context
            )
            StatementType.PRINCIPAL_FIX_NEGATIVE -> Utils.themeAttributeToColorRes(
                R.attr.colorOnSurface,
                context
            )
            StatementType.BONUS -> R.color.inwallet_green_500
            StatementType.DEPOSIT_CANCELED -> Utils.themeAttributeToColorRes(
                R.attr.colorOnSurface,
                context
            )
            StatementType.WITHDRAWAL_CANCELED -> Utils.themeAttributeToColorRes(
                R.attr.colorOnSurface,
                context
            )
            StatementType.REFERRAL -> R.color.inwallet_green_500
            StatementType.CANCELED -> Utils.themeAttributeToColorRes(
                R.attr.colorOnSurface,
                context
            )
        }

        binding.tvStatementAmount.setTextColor(ContextCompat.getColor(context, color))
        binding.tvStatementAmount.text = numberFormat.format(item.amount)
    }

    fun getItemNew(position: Int): PlatformStatement = super.getItem(position)
}
