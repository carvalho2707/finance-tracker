package pt.tiagocarvalho.financetracker.ui.alerts

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Single
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.AlertItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.repository.AccountsRepository
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository

class AlertsUseCase(
    private val platformDetailsRepository: PlatformDetailsRepository,
    private val refreshRepository: RefreshRepository,
    private val accountsRepository: AccountsRepository
) {

    internal fun loadAlerts(): Single<List<AlertItem>> =
        platformDetailsRepository.getPlatformDetailsWithCashDrag()
            .map { mapToListAlertItem(it) }

    internal fun refreshDetails(loadedData: MutableMap<PlatformEnum, WebViewData>): Completable =
        refreshRepository.refreshDetails(loadedData)

    private fun mapToListAlertItem(detailsList: List<PlatformDetails>): List<AlertItem> =
        detailsList.map { mapToAlertItem(it) }

    private fun mapToAlertItem(platformDetails: PlatformDetails): AlertItem = AlertItem(
        platformDetails.platform.logoId,
        platformDetails.available ?: BigDecimal.ZERO,
        platformDetails.platform.name,
        platformDetails.platform.url
    )

    internal fun getTwoFactorPlatforms(): Maybe<List<Platform>> =
        accountsRepository.getTwoFactorPlatforms()

    internal fun isRefreshDetailsNeeded(force: Boolean): Single<Boolean> =
        refreshRepository.isRefreshDetailsNeeded(force)
}
