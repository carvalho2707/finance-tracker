package pt.tiagocarvalho.financetracker.ui.management.request_platform

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class RequestPlatformFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        context: Context,
        logger: Logger
    ): RequestPlatformViewModel = RequestPlatformViewModel(
        schedulerProvider,
        context,
        logger
    )
}
