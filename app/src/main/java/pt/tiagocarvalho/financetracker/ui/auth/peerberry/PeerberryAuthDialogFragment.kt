package pt.tiagocarvalho.financetracker.ui.auth.peerberry

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.viewModels
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.DialogAuthBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.CANCEL
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.FREQUENT_UPDATES
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PASSWORD
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.REQUEST_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.USERNAME
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.getTextFromClipboard
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.isValidPaste
import pt.tiagocarvalho.financetracker.ui.base.BaseDialogFragment
import pt.tiagocarvalho.financetracker.utils.executeAfter

@AndroidEntryPoint
class PeerberryAuthDialogFragment : BaseDialogFragment() {

    companion object {

        @JvmStatic
        fun
        newInstance(
            username: String,
            password: String,
            frequentUpdates: Boolean = false
        ): PeerberryAuthDialogFragment {
            val fragment = PeerberryAuthDialogFragment()
            val bundle = Bundle().apply {
                putString(USERNAME, username)
                putString(PASSWORD, password)
                putBoolean(FREQUENT_UPDATES, frequentUpdates)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    private val username: String by lazy {
        if (requireArguments().containsKey(USERNAME)) {
            requireArguments().getString(USERNAME)!!
        } else {
            throw IllegalArgumentException("username not found")
        }
    }

    private val password: String by lazy {
        if (requireArguments().containsKey(PASSWORD)) {
            requireArguments().getString(PASSWORD)!!
        } else {
            throw IllegalArgumentException("password not found")
        }
    }

    private val frequentUpdates: Boolean by lazy {
        if (requireArguments().containsKey(FREQUENT_UPDATES)) {
            requireArguments().getBoolean(FREQUENT_UPDATES)
        } else {
            throw IllegalArgumentException("frequentUpdates not found")
        }
    }

    private lateinit var binding: DialogAuthBinding

    private val peerberryAuthViewModel: PeerberryAuthViewModel by viewModels()
    private var tfaToken: String? = null
    private var visibility = ObservableBoolean(false)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        isCancelable = false
        binding = DialogAuthBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            visibility = this@PeerberryAuthDialogFragment.visibility
            title = "Peerberry"
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        peerberryAuthViewModel.tfaDialog.observe(viewLifecycleOwner) {
            tfaToken = it
        }

        peerberryAuthViewModel.authToken.observe(viewLifecycleOwner) {
            closeWithStatus(SUCCESS, it)
        }

        peerberryAuthViewModel.visibility.observe(viewLifecycleOwner) {
            visibility.set(it)
        }

        peerberryAuthViewModel.isPasswordError.observe(viewLifecycleOwner) {
            if (it) closeWithStatus(INVALID_USERNAME_PASSWORD_CODE, null)
            else closeWithStatus(UNKNOWN_ERROR_CODE, null)
        }

        peerberryAuthViewModel.invalidCode.observe(viewLifecycleOwner) {
            binding.code.error = getString(R.string.auth_dialog_invalid_code)
        }

        binding.executeAfter {
            lifecycleOwner = viewLifecycleOwner
        }

        binding.inputTwoFactor.setOnClickListener {
            if (isValidPaste(getClipboardManager())) {
                val text = getTextFromClipboard(getClipboardManager())
                (it as TextInputEditText).setText(text)
            }
        }

        binding.inputTwoFactor.setOnFocusChangeListener { v: View, hasFocus ->
            if (hasFocus && isValidPaste(getClipboardManager())) {
                val text = getTextFromClipboard(getClipboardManager())
                (v as TextInputEditText).setText(text)
            }
        }

        binding.btnSubmit.setOnClickListener {
            if (binding.inputTwoFactor.text?.isEmpty() == true || binding.inputTwoFactor.text?.length ?: 0 < 6) {
                binding.code.error = "required"
            } else {
                peerberryAuthViewModel.authenticateTfa(
                    tfaToken!!,
                    binding.inputTwoFactor.text.toString()
                )
            }
        }

        binding.btnResend.setOnClickListener {
            binding.code.error = null
            binding.inputTwoFactor.text = null
            peerberryAuthViewModel.authenticate(username, password)
        }

        binding.btnCancel.setOnClickListener {
            closeWithStatus(CANCEL, null)
        }

        if (showsDialog) {
            (requireDialog() as AlertDialog).setView(binding.root)
        }

        peerberryAuthViewModel.authenticate(username, password)
    }

    private fun closeWithStatus(resultCode: Int, token: String?) {
        val intent = Intent().apply {
            putExtra(TOKEN, token)
            putExtra(USERNAME, username)
            putExtra(PASSWORD, password)
            putExtra(FREQUENT_UPDATES, frequentUpdates)
            putExtra(PLATFORM, PlatformEnum.PEERBERRY.name)
        }
        targetFragment?.onActivityResult(REQUEST_CODE, resultCode, intent)
        dismiss()
    }
}
