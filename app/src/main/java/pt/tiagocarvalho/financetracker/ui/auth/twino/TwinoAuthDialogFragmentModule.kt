package pt.tiagocarvalho.financetracker.ui.auth.twino

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class TwinoAuthDialogFragmentModule {

    @Provides
    fun provideViewModel(
        twinoAuthUseCase: TwinoAuthUseCase,
        schedulerProvider: SchedulerProvider
    ) = TwinoAuthViewModel(
        twinoAuthUseCase,
        schedulerProvider
    )
}
