package pt.tiagocarvalho.financetracker.ui.login

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.repository.RefreshRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class LoginFragmentModule {

    @Provides
    fun provideViewModel(
        platformRepository: PlatformRepository,
        schedulerProvider: SchedulerProvider,
        logger: Logger,
        context: Context,
        platformDetailsRepository: PlatformDetailsRepository,
        statementsRepository: StatementsRepository,
        preferencesHelper: PreferencesHelper,
        refreshRepository: RefreshRepository
    ): LoginViewModel = LoginViewModel(
        platformRepository,
        schedulerProvider,
        logger,
        context,
        platformDetailsRepository,
        statementsRepository,
        preferencesHelper,
        refreshRepository
    )
}
