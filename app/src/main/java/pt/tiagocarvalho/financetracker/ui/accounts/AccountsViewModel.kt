package pt.tiagocarvalho.financetracker.ui.accounts

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import java.util.EnumMap
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent

@HiltViewModel
class AccountsViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val accountsUseCase: AccountsUseCase
) : BaseViewModel() {

    var info: ObservableField<AccountsInfo> = ObservableField(AccountsInfo())
    var error: ObservableField<String> = ObservableField()

    var status: MutableLiveData<Status> = MutableLiveData()
    val accounts: MutableLiveData<List<AccountItem>> = MutableLiveData()
    var tfaRequired = MutableLiveData<Platform?>()
    val cashDragAlerts: MutableLiveData<Int> = MutableLiveData()
    val showFloatActionButton: MutableLiveData<Boolean> = MutableLiveData(true)
    val showErrorDialog: SingleLiveEvent<Boolean> = SingleLiveEvent()

    private var requiredPlatforms: MutableList<Platform> = mutableListOf()
    private var loadedData: MutableMap<PlatformEnum, WebViewData> =
        EnumMap(PlatformEnum::class.java)

    private var forceLiveData: Boolean = false

    fun handleFloatActionButton() {
        compositeDisposable.add(
            accountsUseCase.hasMaxPlatforms()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess {
                    if (it.not()) {
                        showFloatActionButton.postValue(it.not())
                    }
                }
                .subscribe()
        )
    }

    fun loadRefreshNeeded(force: Boolean) {
        compositeDisposable.add(
            accountsUseCase.isRefreshDetailsNeeded(force)
                .delay(1, TimeUnit.SECONDS)
                .doOnSuccess {
                    if (it) {
                        loadTwoFactorAccounts(force)
                    } else {
                        loadAccounts(force, loadedData)
                    }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .subscribe()
        )
    }

    private fun loadTwoFactorAccounts(force: Boolean) {
        compositeDisposable.add(
            accountsUseCase.getTwoFactorPlatforms()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            loadAuth(it, force)
                        } else {
                            loadAccounts(force, loadedData)
                        }
                    },
                    { it.printStackTrace() },
                    { loadAccounts(force, loadedData) }
                )
        )
    }

    fun loadNextAuth(webViewData: WebViewData?) {
        webViewData?.let {
            loadedData[webViewData.platform] = it
        }
        if (requiredPlatforms.size > 0) {
            tfaRequired.postValue(requiredPlatforms.first())
            requiredPlatforms.removeAt(0)
        } else {
            requiredPlatforms = mutableListOf()
            tfaRequired = MutableLiveData()
            loadAccounts(forceLiveData, loadedData)
            loadedData = EnumMap(PlatformEnum::class.java)
        }
    }

    private fun loadAuth(
        accounts: List<Platform>,
        force: Boolean
    ) {
        compositeDisposable.add(
            Observable.timer(2, TimeUnit.SECONDS)
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .observeOn(schedulerProvider.ui())
                .subscribe({ onAuthAccounts(accounts, force) }, { it.printStackTrace() })
        )
    }

    fun loadAccounts(shouldFetch: Boolean, loadedData: MutableMap<PlatformEnum, WebViewData>) {
        compositeDisposable.add(
            accountsUseCase.loadDetails(shouldFetch, loadedData)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Status.LOADING) }
                .doOnSuccess { onReceiveAccounts(it) }
                .subscribe({ }, { it.printStackTrace() })
        )
    }

    private fun onReceiveAccounts(accountInfo: AccountsInfo) {
        handleAlerts()
        updateUi(accountInfo)

        if (accountInfo.status == Status.ERROR) {
            error.set(accountInfo.error ?: "Unknown error")
        }
    }

    private fun updateUi(accountInfo: AccountsInfo) {
        status.postValue(accountInfo.status)

        if (accountInfo.itemList.isNotEmpty()) {
            info.set(accountInfo)
        }
        accounts.value = accountInfo.itemList
    }

    private fun handleAlerts() {
        compositeDisposable.add(
            accountsUseCase.getPlatformWithCashDrag()
                .doOnSuccess { cashDragAlerts.postValue(it) }
                .observeOn(schedulerProvider.ui())
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun onAuthAccounts(
        accounts: List<Platform>,
        force: Boolean
    ) {
        forceLiveData = force
        requiredPlatforms = accounts.toMutableList()
        tfaRequired.value = requiredPlatforms.first()
        requiredPlatforms.removeAt(0)
    }

    fun showErrorDialog() {
        showErrorDialog.value = true
    }
}
