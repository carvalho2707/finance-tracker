package pt.tiagocarvalho.financetracker.ui.billing

import androidx.databinding.BindingAdapter
import com.google.android.material.textview.MaterialTextView

@BindingAdapter("skuTitle")
fun setSkuTitle(
    view: MaterialTextView,
    title: String
) {
    val name = title.let {
        // In most cases subscription title is followed by app name in (), remove it.
        val indexOfAppName = it.indexOf("(")
        if (indexOfAppName != -1) {
            it.substring(0, indexOfAppName)
        } else {
            it
        }
    }
    view.text = name
}
