package pt.tiagocarvalho.financetracker.ui.details.p2p

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Completable
import io.reactivex.Single
import java.math.BigDecimal
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.DataItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.repository.PlatformRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.api.model.Login

@HiltViewModel
class P2PDetailsViewModel @Inject constructor(
    private val platformRepository: PlatformRepository,
    private val statementsRepository: StatementsRepository,
    private val preferencesHelper: PreferencesHelper,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger,
    private val p2pDetailsUseCase: P2pDetailsUseCase
) : BaseViewModel() {

    companion object {
        private const val UNKNOWN_ERROR = "Unknown Error"
    }

    var statusObservable: ObservableField<Status> = ObservableField()
    var error: ObservableField<String> = ObservableField()
    val balance: ObservableField<BigDecimal> = ObservableField(BigDecimal.ZERO)

    val statements: MutableLiveData<List<PlatformStatement>> = MutableLiveData()
    val dataList: MutableLiveData<List<DataItem>> = MutableLiveData()
    val issues: SingleLiveEvent<Boolean> = SingleLiveEvent()
    var status: MutableLiveData<Status> = MutableLiveData()
    var tfaRequired: MutableLiveData<Login> = MutableLiveData()
    val showErrorDialog: SingleLiveEvent<Boolean> = SingleLiveEvent()

    private var isNetworkError = false

    private fun loadAuthIfNeeded(name: String, shouldFetch: Boolean) {
        compositeDisposable.add(
            platformRepository.getPlatformByName(name)
                .delay(1, TimeUnit.SECONDS)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess {
                    if (it.twofactor && it.type != PlatformEnum.BONDORA) {
                        tfaRequired.value = Login(it.username!!, it.password!!)
                    } else {
                        loadInfo(shouldFetch, WebViewData(it.type))
                    }
                }
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .subscribe()
        )
    }

    fun loadInfo(name: String, shouldFetch: Boolean) {
        val platform = PlatformEnum.valueOf(name)
        if (shouldFetch || p2pDetailsUseCase.shouldRefreshDailyStatements(name)) {
            loadAuthIfNeeded(name, shouldFetch)
        } else {
            loadInfo(shouldFetch, WebViewData(platform))
        }
    }

    fun loadInfo(shouldFetch: Boolean, webViewData: WebViewData) {
        compositeDisposable.add(
            Single.just(true)
                .observeOn(schedulerProvider.io())
                .flatMapObservable {
                    p2pDetailsUseCase.loadDetails(
                        shouldFetch,
                        webViewData.platform.name,
                        webViewData.data
                    )
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }.subscribe(
                    {
                        status.postValue(it.status)
                        statusObservable.set(it.status)
                        balance.set(it.balance)
                        statements.postValue(it.statements)
                        dataList.postValue(it.dataList)
                        if (it.hasIssues) {
                            issues.value = true
                        }
                    },
                    {
                        isNetworkError = false
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        error.set(it.message ?: UNKNOWN_ERROR)
                        logger.log(it)
                    }
                )
        )
    }

    fun loadInfoOffline(platformEnum: PlatformEnum) {
        compositeDisposable.add(
            p2pDetailsUseCase.loadDetailsOffline(platformEnum)
                .flatMap { p2pDetailsUseCase.loadDetailsOffline(platformEnum) }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }.subscribe(
                    {
                        isNetworkError = true
                        balance.set(it.balance)
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        statements.postValue(it.statements)
                    },
                    {
                        isNetworkError = false
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        error.set(it.message ?: UNKNOWN_ERROR)
                        logger.log(it)
                    }
                )
        )
    }

    fun showErrorDialog() {
        showErrorDialog.value = isNetworkError
    }

    fun resetStatements(name: String) {
        val platform = PlatformEnum.valueOf(name)
        compositeDisposable.add(
            statementsRepository.deleteStatements(name)
                .delay(1, TimeUnit.SECONDS)
                .andThen(deleteStatementsPreferences(platform))
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        loadAuthIfNeeded(name, false)
                    },
                    {
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        error.set(it.message ?: UNKNOWN_ERROR)
                        logger.log(it)
                    }
                )
        )
    }

    private fun deleteStatementsPreferences(platformEnum: PlatformEnum): Completable =
        Completable.fromAction {
            preferencesHelper.delete(platformEnum.prefLastUpdateStatement)
            preferencesHelper.delete(platformEnum.prefLastFullStatementScan)
        }
}
