package pt.tiagocarvalho.financetracker.ui.accounts.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnLayout
import androidx.core.view.marginBottom
import androidx.core.view.updateLayoutParams
import androidx.core.view.updatePaddingRelative
import androidx.fragment.app.viewModels
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.databinding.FragmentAccountsFilterBinding
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.doOnApplyWindowInsets
import pt.tiagocarvalho.financetracker.utils.lerp
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_COLLAPSED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_EXPANDED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_HIDDEN

@AndroidEntryPoint
class AccountsFilterFragment : BaseFragment() {

    private lateinit var binding: FragmentAccountsFilterBinding
    private lateinit var behavior: BottomSheetBehavior<*>

    private val accountsFilterViewModel: AccountsFilterViewModel by viewModels()
    private val contentFadeInterpolator = LinearOutSlowInInterpolator()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentAccountsFilterBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = accountsFilterViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        // Pad the bottom of the RecyclerView so that the content scrolls up above the nav bar
        binding.recyclerviewFilter.doOnApplyWindowInsets { v, insets, padding ->
            v.updatePaddingRelative(bottom = padding.bottom + insets.systemWindowInsetBottom)
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        behavior = BottomSheetBehavior.from(binding.filterSheet)

        // Update the peek and margins so that it scrolls and rests within sys ui
        val peekHeight = behavior.peekHeight
        val marginBottom = binding.root.marginBottom
        binding.root.doOnApplyWindowInsets { v, insets, _ ->
            val gestureInsets = insets.systemGestureInsets
            // Update the peek height so that it is above the navigation bar
            behavior.peekHeight = gestureInsets.bottom + peekHeight

            v.updateLayoutParams<ViewGroup.MarginLayoutParams> {
                bottomMargin = marginBottom + insets.systemWindowInsetTop
            }
        }

        binding.sortChips.setOnCheckedChangeListener { _, checkedId ->
            // Handle child Chip checked/unchecked
            accountsFilterViewModel.updateSortOrderId(checkedId)
        }

        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                updateFilterContentsAlpha(slideOffset)
            }
        })

        binding.collapseArrow.setOnClickListener {
            behavior.state = if (behavior.skipCollapsed) STATE_HIDDEN else STATE_COLLAPSED
        }

        // This fragment is in the layout of a parent fragment, so its view hierarchy is restored
        // when the parent's hierarchy is restored. However, the dispatch order seems to traverse
        // child fragments first, meaning the views we care about have not actually been restored
        // when onViewStateRestored is called (otherwise we would do this there).
        binding.filterSheet.doOnLayout {
            val slideOffset = when (behavior.state) {
                STATE_EXPANDED -> 1f
                STATE_COLLAPSED -> 0f
                else /*BottomSheetBehavior.STATE_HIDDEN*/ -> -1f
            }
            updateFilterContentsAlpha(slideOffset)
        }

        behavior.state = STATE_HIDDEN

        accountsFilterViewModel.loadPreferences()
    }

    private fun updateFilterContentsAlpha(slideOffset: Float) {
        // Otherwise we just show the header view
        accountsFilterViewModel.headerAlpha.set(1f)
        // Due to the content view being visible below the navigation bar, we apply a short alpha
        // transition
        accountsFilterViewModel.recyclerviewAlpha.set(
            lerp(
                ALPHA_CONTENT_START_ALPHA, ALPHA_CONTENT_END_ALPHA,
                contentFadeInterpolator.getInterpolation(slideOffset)
            )
        )
    }

    override fun onToolbarItemClicked(menuItem: MenuItem) = false

    companion object {
        // Threshold for when the filter list content reach maximum alpha. Should be a value between
        // 0 and [ALPHA_CHANGEOVER], inclusive.
        private const val ALPHA_CONTENT_END_ALPHA = 1f

        // Threshold for when the filter list content should starting changing alpha state
        // This should be a value between 0 and 1, coinciding with a point between the bottom
        // sheet's collapsed (0) and expanded (1) states.
        private const val ALPHA_CONTENT_START_ALPHA = 0.2f
    }
}
