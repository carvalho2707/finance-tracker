package pt.tiagocarvalho.financetracker.ui.auth.robocash

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@HiltViewModel
class RobocashAuthViewModel @Inject constructor(
    private val robocashAuthUseCase: RobocashAuthUseCase,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    val visibility: MutableLiveData<Boolean> = MutableLiveData(false)
    val cookies: MutableLiveData<Map<String, String>> = MutableLiveData()
    val isPasswordError: MutableLiveData<Boolean> = MutableLiveData()
    val isCodeError: MutableLiveData<Boolean> = MutableLiveData()
    val isError: MutableLiveData<Boolean> = MutableLiveData()

    fun authenticate(email: String, password: String) {
        compositeDisposable.add(
            robocashAuthUseCase.login(email, password)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { visibility.value = false }
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { it.printStackTrace() })
        )
    }

    fun authenticateTfa(code: String) {
        compositeDisposable.add(
            robocashAuthUseCase.loginTfa(code)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun onAuthenticationSuccess(loginResponse: LoginResponse) {
        when {
            loginResponse.isPasswordError -> isPasswordError.value = true
            loginResponse.tfaActive -> visibility.value = true
            loginResponse.isCodeError -> isCodeError.value = true
            loginResponse.tfaActive.not() && loginResponse.cookies != null ->
                cookies.value =
                    loginResponse.cookies
            else -> isError.value = true
        }
    }
}
