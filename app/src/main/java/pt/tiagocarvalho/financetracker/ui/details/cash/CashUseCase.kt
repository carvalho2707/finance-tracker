package pt.tiagocarvalho.financetracker.ui.details.cash

import android.content.Context
import android.net.Uri
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashStatement
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.repository.CashRepository
import pt.tiagocarvalho.financetracker.utils.CsvUtils
import pt.tiagocarvalho.financetracker.utils.toDate

class CashUseCase(
    val cashRepository: CashRepository,
    val context: Context
) {

    internal fun importCsv(uri: Uri, name: String) =
        CsvUtils.readCsvFromUri(uri, context)
            .map { parseCsvToSavingsStatement(name, it) }
            .flatMapCompletable { statements ->
                cashRepository.deleteAllByName(name)
                    .andThen(cashRepository.triggerFetchCompletable(name))
                    .andThen(cashRepository.insert(statements))
            }

    private fun parseCsvToSavingsStatement(
        name: String,
        entries: List<String>
    ) = entries.map { line ->
        val list = line.split(",")
        CashStatement(
            null,
            name,
            list[0].toDouble(),
            list[1].toDate(),
            StatementType.valueOf(list[2])
        )
    }
}
