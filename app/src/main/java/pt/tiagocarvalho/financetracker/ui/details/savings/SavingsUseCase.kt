package pt.tiagocarvalho.financetracker.ui.details.savings

import android.content.Context
import android.net.Uri
import io.reactivex.Completable
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsStatement
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.repository.SavingsRepository
import pt.tiagocarvalho.financetracker.utils.CsvUtils
import pt.tiagocarvalho.financetracker.utils.toDate

class SavingsUseCase(
    val savingsRepository: SavingsRepository,
    val context: Context
) {

    internal fun importCsv(uri: Uri, name: String): Completable =
        CsvUtils.readCsvFromUri(uri, context)
            .map { parseCsvToSavingsStatement(name, it) }
            .flatMapCompletable { statements ->
                savingsRepository.deleteAllByName(name)
                    .andThen(savingsRepository.triggerFetchCompletable(name))
                    .andThen(savingsRepository.insert(statements))
            }

    private fun parseCsvToSavingsStatement(
        name: String,
        entries: List<String>
    ): List<SavingsStatement> = entries.map { line ->
        val list = line.split(",")
        SavingsStatement(
            null,
            name,
            list[0].toDouble(),
            list[1].toDate(),
            StatementType.valueOf(list[2])
        )
    }
}
