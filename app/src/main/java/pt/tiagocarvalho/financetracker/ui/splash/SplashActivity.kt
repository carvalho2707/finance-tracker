package pt.tiagocarvalho.financetracker.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import dagger.hilt.android.AndroidEntryPoint
import java.util.concurrent.TimeUnit
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ActivitySplashBinding
import pt.tiagocarvalho.financetracker.ui.main.MainActivity
import pt.tiagocarvalho.financetracker.ui.onboarding.OnboardingActivity

@AndroidEntryPoint
class SplashActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashBinding
    private val viewModel: SplashViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash)

        binding.apply {
            this.viewModel = this@SplashActivity.viewModel
            lifecycleOwner = this@SplashActivity
        }

        lifecycle.addObserver(viewModel)

        subscribeUI()
    }

    private fun subscribeUI() {
        viewModel.singleLiveEvent.observe(
            this,
            {
                if (it) goToMainActivity()
                else goToOnboarding()
            }
        )
    }

    override fun onStart() {
        super.onStart()
        Handler().postDelayed(viewModel::start, TimeUnit.SECONDS.toMillis(1))
    }

    private fun goToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun goToOnboarding() {
        startActivity(Intent(this, OnboardingActivity::class.java))
    }
}
