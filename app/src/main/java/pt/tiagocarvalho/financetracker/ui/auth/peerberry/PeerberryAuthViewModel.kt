package pt.tiagocarvalho.financetracker.ui.auth.peerberry

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.p2p.services.model.peerberry.LoginResponse

@HiltViewModel
class PeerberryAuthViewModel @Inject constructor(
    private val peerberryAuthUseCase: PeerberryAuthUseCase,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    val visibility: MutableLiveData<Boolean> = MutableLiveData(false)
    val tfaDialog: MutableLiveData<String> = MutableLiveData()
    val authToken: MutableLiveData<String> = MutableLiveData()
    val isPasswordError: MutableLiveData<Boolean> = MutableLiveData()
    val invalidCode: MutableLiveData<Boolean> = MutableLiveData()

    fun authenticate(email: String, password: String) {
        compositeDisposable.add(
            peerberryAuthUseCase.login(email, password)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { visibility.value = false }
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { it.printStackTrace() })
        )
    }

    fun authenticateTfa(tfaToken: String, code: String) {
        compositeDisposable.add(
            peerberryAuthUseCase.loginTfa(tfaToken, code)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { onAuthenticationTfaSuccess(it) }
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun onAuthenticationSuccess(loginResponse: LoginResponse) {
        when {
            loginResponse.tfaIsActive == true -> {
                visibility.value = true
                tfaDialog.value = loginResponse.tfaToken!!
            }
            loginResponse.accessToken != null -> authToken.value = loginResponse.accessToken
            else -> isPasswordError.value = loginResponse.passwordError == true
        }
    }

    private fun onAuthenticationTfaSuccess(loginResponse: LoginResponse) {
        when {
            loginResponse.accessToken != null -> authToken.value = loginResponse.accessToken
            loginResponse.invalidCode == true -> invalidCode.value = true
            else -> isPasswordError.value = false
        }
    }
}
