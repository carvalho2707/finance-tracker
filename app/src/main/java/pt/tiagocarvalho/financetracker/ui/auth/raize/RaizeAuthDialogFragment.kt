package pt.tiagocarvalho.financetracker.ui.auth.raize

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.ObservableBoolean
import androidx.fragment.app.viewModels
import com.google.android.material.textfield.TextInputEditText
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.DialogAuthBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.FREQUENT_UPDATES
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PASSWORD
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.REQUEST_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.USERNAME
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.getTextFromClipboard
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.isValidPaste
import pt.tiagocarvalho.financetracker.ui.base.BaseDialogFragment
import pt.tiagocarvalho.financetracker.utils.executeAfter

@AndroidEntryPoint
class RaizeAuthDialogFragment : BaseDialogFragment() {

    companion object {

        @JvmStatic
        fun newInstance(
            username: String,
            password: String,
            frequentUpdates: Boolean = false
        ): RaizeAuthDialogFragment {
            val fragment =
                RaizeAuthDialogFragment()
            val bundle = Bundle().apply {
                putString(USERNAME, username)
                putString(PASSWORD, password)
                putBoolean(FREQUENT_UPDATES, frequentUpdates)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    private val username: String by lazy {
        if (requireArguments().containsKey(USERNAME)) {
            requireArguments().getString(USERNAME)!!
        } else {
            throw IllegalArgumentException("username not found")
        }
    }

    private val password: String by lazy {
        if (requireArguments().containsKey(PASSWORD)) {
            requireArguments().getString(PASSWORD)!!
        } else {
            throw IllegalArgumentException("password not found")
        }
    }

    private val frequentUpdates: Boolean by lazy {
        if (requireArguments().containsKey(FREQUENT_UPDATES)) {
            requireArguments().getBoolean(FREQUENT_UPDATES)
        } else {
            throw IllegalArgumentException("frequentUpdates not found")
        }
    }

    private lateinit var binding: DialogAuthBinding

    private val raizeAuthViewModel: RaizeAuthViewModel by viewModels()
    private var visibility = ObservableBoolean(false)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        isCancelable = false
        binding = DialogAuthBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
            visibility = this@RaizeAuthDialogFragment.visibility
            btnResend.visibility = View.INVISIBLE
            title = "Raize"
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        raizeAuthViewModel.token.observe(viewLifecycleOwner) {
            closeWithStatus(SUCCESS, it)
        }

        raizeAuthViewModel.visibility.observe(viewLifecycleOwner) {
            visibility.set(it)
        }

        raizeAuthViewModel.isPasswordError.observe(viewLifecycleOwner) {
            if (it) closeWithStatus(INVALID_USERNAME_PASSWORD_CODE, null)
        }

        raizeAuthViewModel.isUnknownError.observe(viewLifecycleOwner) {
            if (it) closeWithStatus(UNKNOWN_ERROR_CODE, null)
        }

        raizeAuthViewModel.isCodeError.observe(viewLifecycleOwner) {
            if (it) binding.code.error = getString(R.string.auth_dialog_invalid_code)
        }

        binding.executeAfter {
            lifecycleOwner = viewLifecycleOwner
        }

        binding.inputTwoFactor.setOnClickListener {
            if (isValidPaste(getClipboardManager())) {
                val text = getTextFromClipboard(getClipboardManager())
                (it as TextInputEditText).setText(text)
            }
        }

        binding.inputTwoFactor.setOnFocusChangeListener { v: View, hasFocus ->
            if (hasFocus && isValidPaste(getClipboardManager())) {
                val text = getTextFromClipboard(getClipboardManager())
                (v as TextInputEditText).setText(text)
            }
        }

        binding.btnSubmit.setOnClickListener {
            if (binding.inputTwoFactor.text?.isEmpty() == true || binding.inputTwoFactor.text?.length ?: 0 < 6) {
                binding.code.error = "required"
            } else {
                raizeAuthViewModel.authenticateTfa(
                    username,
                    password,
                    binding.inputTwoFactor.text.toString()
                )
            }
        }

        binding.btnCancel.setOnClickListener {
            closeWithStatus(AuthDialogUtils.CANCEL, null)
        }

        if (showsDialog) {
            (requireDialog() as AlertDialog).setView(binding.root)
        }

        raizeAuthViewModel.authenticate(username, password)
    }

    private fun closeWithStatus(resultCode: Int, token: String?) {
        val intent = Intent().apply {
            putExtra(TOKEN, token)
            putExtra(USERNAME, username)
            putExtra(PASSWORD, password)
            putExtra(FREQUENT_UPDATES, frequentUpdates)
            putExtra(PLATFORM, PlatformEnum.RAIZE.name)
        }
        targetFragment?.onActivityResult(REQUEST_CODE, resultCode, intent)
        dismiss()
    }
}
