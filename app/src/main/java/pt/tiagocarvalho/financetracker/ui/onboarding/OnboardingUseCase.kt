package pt.tiagocarvalho.financetracker.ui.onboarding

import io.reactivex.Completable
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.utils.Preferences

class OnboardingUseCase(
    private val preferencesHelper: PreferencesHelper
) {

    internal fun getItems(): Single<List<OnboardingItem>> = Single.create {
        val list = listOf(
            OnboardingItem(
                R.string.onboarding_1_title,
                R.string.onboarding_1_body,
                R.raw.onboarding_welcome
            ),
            OnboardingItem(
                R.string.onboarding_2_title,
                R.string.onboarding_2_body,
                R.raw.onboarding_wallet
            ),
            OnboardingItem(
                R.string.onboarding_3_title,
                R.string.onboarding_3_body,
                R.raw.onboarding_statistics
            )
        )
        it.onSuccess(list)
    }

    fun markOnboardingSeen(): Completable = Completable.fromAction {
        preferencesHelper.put(Preferences.PREF_RATER_SEEN_ONBOARDING, true)
    }
}
