package pt.tiagocarvalho.financetracker.ui.tweets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.TwitterAccount
import pt.tiagocarvalho.financetracker.databinding.FragmentDataBindingComponent
import pt.tiagocarvalho.financetracker.databinding.FragmentTweetsBinding
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.AppExecutors

@AndroidEntryPoint
class TweetsFragment : BaseFragment() {

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentTweetsBinding

    private val tweetsViewModel: TweetsViewModel by viewModels()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentTweetsBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = tweetsViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tweetsAdapter = TweetsAdapter(dataBindingComponent, appExecutors, requireContext())
        binding.rvTweets.adapter = tweetsAdapter

        registerToolbarWithNavigation(binding.toolbar)

        tweetsViewModel.status.observe(viewLifecycleOwner) { status ->
            handleStatus(status)
        }

        tweetsViewModel.errorType.observe(viewLifecycleOwner) {
            if (it == 0) {
                binding.layoutNoInternet.visibility = View.VISIBLE
            } else {
                binding.layoutGenericError.visibility = View.VISIBLE
            }
        }

        tweetsViewModel.tweets.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.rvTweets.visibility = View.GONE
                binding.noTweetsMessage.visibility = View.VISIBLE
            } else {
                scheduleAnimation(tweetsAdapter)
                tweetsAdapter.submitList(it)
            }
        }

        tweetsViewModel.twitterChips.observe(viewLifecycleOwner) {
            showTwitterAccounts(it)
        }

        tweetsViewModel.filter.observe(viewLifecycleOwner) {
            hideFilter()
        }

        tweetsViewModel.loadTweets()
    }

    private fun handleStatus(status: Status?) {
        when (status) {
            Status.LOADING -> {
                binding.cvFilter.visibility = View.GONE
                binding.layoutNoInternet.visibility = View.GONE
                binding.layoutGenericError.visibility = View.GONE
                binding.noTweetsMessage.visibility = View.GONE
                binding.rvTweets.visibility = View.GONE
                binding.animationView.visibility = View.VISIBLE
            }
            Status.ERROR -> {
                binding.cvFilter.visibility = View.GONE
                binding.noTweetsMessage.visibility = View.GONE
                binding.layoutNoInternet.visibility = View.GONE
                binding.layoutGenericError.visibility = View.GONE
                binding.rvTweets.visibility = View.GONE
                binding.animationView.visibility = View.GONE
            }
            else -> {
                binding.cvFilter.visibility = View.GONE
                binding.layoutNoInternet.visibility = View.GONE
                binding.layoutGenericError.visibility = View.GONE
                binding.noTweetsMessage.visibility = View.GONE
                binding.animationView.visibility = View.GONE
                binding.rvTweets.visibility = View.VISIBLE
            }
        }
    }

    private fun hideFilter() {
        binding.cvFilter.visibility = View.GONE
    }

    private fun showTwitterAccounts(twitterAccounts: List<TwitterAccount>) {
        binding.chipGroup.removeAllViews()
        twitterAccounts.forEach {
            val chip = layoutInflater.inflate(
                R.layout.layout_chip_choice,
                binding.chipGroup,
                false
            ) as Chip
            chip.isChecked = it.active
            chip.text = it.name
            chip.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked.not() && binding.chipGroup.checkedChipIds.size == 0) {
                    chip.isChecked = true
                } else {
                    tweetsViewModel.updateTwitterAccount(it.name, isChecked)
                }
            }
            binding.chipGroup.addView(chip)
        }
        binding.cvFilter.visibility = View.VISIBLE
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_refresh -> tweetsViewModel.loadTweets()
            R.id.menu_filter ->
                if (binding.cvFilter.visibility == View.VISIBLE) {
                    binding.cvFilter.visibility = View.GONE
                } else {
                    tweetsViewModel.loadTwitterAccounts()
                }
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun scheduleAnimation(adapter: TweetsAdapter) {
        val controller =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        binding.rvTweets.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        binding.rvTweets.scheduleLayoutAnimation()
    }
}
