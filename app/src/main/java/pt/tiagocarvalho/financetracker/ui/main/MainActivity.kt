package pt.tiagocarvalho.financetracker.ui.main

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.View
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ActivityMainBinding
import pt.tiagocarvalho.financetracker.utils.AppRater
import pt.tiagocarvalho.financetracker.utils.Constants.MAIN_AD_ID
import pt.tiagocarvalho.financetracker.utils.Constants.TEST_AD_ID
import pt.tiagocarvalho.financetracker.utils.ForceUpdateChecker

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), ForceUpdateChecker.OnUpdateNeededListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    lateinit var drawer: DrawerLayout

    private val mainViewModel: MainViewModel by viewModels()
    private var currentNavId = NAV_ID_NONE

    private val adSize: AdSize
        get() {
            val display = windowManager.defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)

            val density = outMetrics.density

            var adWidthPixels = binding.adContainer.width.toFloat()
            if (adWidthPixels == 0f) {
                adWidthPixels = outMetrics.widthPixels.toFloat()
            }

            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(this, adWidth)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.lifecycleOwner = this

        drawer = binding.drawerLayout

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.my_nav_host_fragment) as NavHostFragment

        navController = navHostFragment.navController
        navController.addOnDestinationChangedListener { _, destination, _ ->
            val isTopLevelDestination = TOP_LEVEL_DESTINATIONS.contains(destination.id)
            val lockMode = if (isTopLevelDestination) {
                DrawerLayout.LOCK_MODE_UNLOCKED
            } else {
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED
            }
            drawer.setDrawerLockMode(lockMode)
        }

        binding.navView.setupWithNavController(navController)

        if (savedInstanceState == null) {
            // default to showing Home
            val initialNavId = intent.getIntExtra(EXTRA_NAVIGATION_ID, R.id.accounts)
            binding.navView.setCheckedItem(initialNavId) // doesn't trigger listener
            navigateTo(initialNavId)
        }

        lifecycle.addObserver(mainViewModel)

        ForceUpdateChecker.with(this).onUpdateNeeded(this).check()
        AppRater.appLaunched(this)
        updateDrawer(0)

        mainViewModel.showAds.observe(this) {
            if (it) loadAd()
            else mainViewModel.onAdNotNeeded()
        }
    }

    private fun navigateTo(navId: Int) {
        if (navId == currentNavId) {
            return // user tapped the current item
        }
        navController.navigate(navId)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentNavId = binding.navView.checkedItem?.itemId ?: NAV_ID_NONE
    }

    private fun loadAd() {
        val adView = AdView(this)
        adView.setBackgroundColor(Color.TRANSPARENT)
        if (BuildConfig.DEBUG) {
            adView.adUnitId = TEST_AD_ID
        } else {
            adView.adUnitId = MAIN_AD_ID
        }

        binding.adContainer.addView(adView)
        adView.adSize = adSize
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        adView.adListener = object : AdListener() {
            override fun onAdLoaded() {
                mainViewModel.onAdSuccessToLoad()
                binding.adContainer.visibility = View.VISIBLE
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                mainViewModel.onAdFailedToLoad(errorCode)
                binding.adContainer.visibility = View.GONE
            }
        }
    }

    override fun onUpdateNeeded(updateUrl: String) {
        val dialog = MaterialAlertDialogBuilder(this)
            .setTitle(getString(R.string.new_version_title))
            .setMessage(getString(R.string.new_version_message))
            .setPositiveButton(getString(R.string.new_version_update_button)) { _, _ ->
                redirectStore(
                    updateUrl
                )
            }
            .setNegativeButton(getString(R.string.new_version_negative_button)) { _, _ -> finish() }
            .setCancelable(false)
            .create()
        dialog.show()
    }

    private fun redirectStore(updateUrl: String) {
        Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl))
            .apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }.let {
                startActivity(it)
            }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun registerToolbarWithNavigation(toolbar: Toolbar) {
        val appBarConfiguration = AppBarConfiguration(TOP_LEVEL_DESTINATIONS, drawer)
        toolbar.setupWithNavController(navController, appBarConfiguration)
    }

    fun updateDrawer(notificationCount: Int) {
        val alertsTv = binding.navView.menu.findItem(R.id.alerts).actionView as TextView
        if (notificationCount > 0) {
            alertsTv.apply {
                gravity = Gravity.CENTER_VERTICAL
                setTypeface(null, Typeface.BOLD)
                setTextColor(resources.getColor(R.color.inwallet_red_500))
                text = notificationCount.toString()
            }
        } else {
            alertsTv.text = ""
        }
    }

    override fun onSupportNavigateUp() = navController.navigateUp(binding.drawerLayout)

    companion object {
        /** Key for an int extra defining the initial navigation target. */
        const val EXTRA_NAVIGATION_ID = "extra.NAVIGATION_ID"

        private const val NAV_ID_NONE = -1

        private val TOP_LEVEL_DESTINATIONS = setOf(
            R.id.accounts,
            R.id.alerts,
            R.id.statistics,
            R.id.statements,
            R.id.management,
            R.id.tweets,
            R.id.billing,
            R.id.settings
        )
    }
}
