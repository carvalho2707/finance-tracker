package pt.tiagocarvalho.financetracker.ui.details.cash

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.CashRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class CashFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        cashRepository: CashRepository,
        statementsRepository: StatementsRepository,
        preferencesHelper: PreferencesHelper,
        context: Context,
        logger: Logger,
        cashUseCase: CashUseCase
    ) = CashViewModel(
        schedulerProvider,
        cashRepository,
        statementsRepository,
        preferencesHelper,
        context,
        logger,
        cashUseCase
    )
}
