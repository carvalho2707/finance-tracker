package pt.tiagocarvalho.financetracker.ui.alerts

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.R

@BindingAdapter("alert")
fun setAlertMessage(view: TextView, value: BigDecimal) {
    val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault()).apply {
        minimumFractionDigits = 1
        maximumFractionDigits = 2
        currency = Currency.getInstance("EUR")
    }

    view.text = view.resources.getString(
        R.string.alert_available_body,
        numberFormat.format(value.toDouble())
    )
}
