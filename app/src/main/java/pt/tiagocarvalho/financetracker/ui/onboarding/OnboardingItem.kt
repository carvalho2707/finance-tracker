package pt.tiagocarvalho.financetracker.ui.onboarding

import androidx.annotation.RawRes
import androidx.annotation.StringRes

data class OnboardingItem(
    @StringRes val title: Int,
    @StringRes val body: Int,
    @RawRes val icon: Int
)
