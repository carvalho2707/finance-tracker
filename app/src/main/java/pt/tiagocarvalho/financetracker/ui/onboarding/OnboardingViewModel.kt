package pt.tiagocarvalho.financetracker.ui.onboarding

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent

@HiltViewModel
class OnboardingViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val onboardingUseCase: OnboardingUseCase
) : BaseViewModel() {

    val items: MutableLiveData<List<OnboardingItem>> = MutableLiveData()
    val navigateToMainActivity: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun loadItems() {
        compositeDisposable.add(
            onboardingUseCase.getItems()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { items.value = it }
                .subscribe()
        )
    }

    fun markOnboardingSeen() {
        compositeDisposable.add(
            onboardingUseCase.markOnboardingSeen()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnComplete { navigateToMainActivity.value = true }
                .subscribe()
        )
    }
}
