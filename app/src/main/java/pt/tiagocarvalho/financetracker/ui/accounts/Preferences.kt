package pt.tiagocarvalho.financetracker.ui.accounts

import pt.tiagocarvalho.financetracker.model.DisplayMode

data class Preferences(
    val balance: Boolean,
    val variation: Boolean,
    val positiveVariation: Boolean,
    val sortOrderId: Int,
    val platformType: MutableSet<String>,
    val display: DisplayMode
)
