package pt.tiagocarvalho.financetracker.ui.login

import android.animation.Animator
import android.annotation.SuppressLint
import android.content.Intent
import android.content.Intent.ACTION_VIEW
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.layout_no_internet.view.*
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentLoginBinding
import pt.tiagocarvalho.financetracker.model.Frequency
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.CANCEL
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.FREQUENT_UPDATES
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PASSWORD
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.USERNAME
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.postponeEnterTransition
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.financetracker.webview.BaseWebViewNavigator
import pt.tiagocarvalho.p2p.api.model.Login

@AndroidEntryPoint
class LoginFragment : BaseFragment(), BaseWebViewNavigator {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var platformEnum: PlatformEnum

    private val loginViewModel: LoginViewModel by viewModels()
    private val params by navArgs<LoginFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentLoginBinding.inflate(inflater, container, false)

        sharedElementEnterTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.account_enter)
        // Delay the enter transition until speaker image has loaded.
        postponeEnterTransition(500L)

        startPostponedEnterTransition()

        binding.apply {
            viewModel = loginViewModel
            lifecycleOwner = viewLifecycleOwner
        }
        binding.toolbar.setOnMenuItemClickListener { item -> onOptionsItemSelected(item) }

        registerToolbarWithNavigation(binding.toolbar)

        setupUI()

        loginViewModel.status.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    Utils.hideKeyboard(requireContext(), view)
                    showLoading()
                }
                Status.SUCCESS -> showSuccess()
                Status.ERROR -> showError(it.message)
            }
        }

        loginViewModel.networkError.observe(viewLifecycleOwner) { handleNetworkError() }

        loginViewModel.tfaRequired.observe(viewLifecycleOwner) { result ->
            result?.let {
                platformEnum = it.first
                if (AuthDialogUtils.belongsToAuthDialog(it.first)) {
                    loadAuthDialog(it.first, it.second, it.third)
                } else {
                    loadWebView(it.second, it.third)
                }
            }
        }

        loginViewModel.signUp.observe(viewLifecycleOwner) {
            it?.let {
                val intent = Intent(ACTION_VIEW, Uri.parse(it))
                startActivity(intent)
            }
        }

        loginViewModel.login.observe(viewLifecycleOwner) { login() }

        loginViewModel.editPlatform.observe(viewLifecycleOwner) { platform ->
            platform?.let {
                binding.inputFrequency.setText(it.interestsFrequency?.name, false)
                binding.inputRecurringFrequency.setText(it.recurringFrequency?.name, false)
            }
        }

        loginViewModel.setup(params.name, params.platform)

        return binding.root
    }

    private fun handleNetworkError() {
        binding.layoutNoInternet.apply {
            visibility = VISIBLE
            no_internet_animation.repeatCount = 0
            no_internet_animation.addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) = Unit

                override fun onAnimationEnd(animation: Animator?) {
                    binding.layoutNoInternet.visibility = GONE
                    binding.body.visibility = VISIBLE
                    binding.tvError.visibility = VISIBLE
                    binding.tvError.text = getString(R.string.no_network_message)
                }

                override fun onAnimationCancel(animation: Animator?) = Unit

                override fun onAnimationStart(animation: Animator?) {
                    binding.animationView.visibility = GONE
                    binding.body.visibility = GONE
                    binding.tvError.visibility = GONE
                }
            })
            no_internet_animation.playAnimation()
        }
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean =
        if (menuItem.itemId == R.id.menu_settings) {
            findNavController().navigate(MainDirections.actionGlobalSettings())
            true
        } else super.onOptionsItemSelected(menuItem)

    private fun setupUI() {
        val platform = PlatformEnum.valueOf(params.platform)

        if (platform == PlatformEnum.SAVINGS) {
            binding.inputSavings.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
            val adapter = ArrayAdapter(
                requireContext(),
                R.layout.material_spinner_item,
                Frequency.values()
            )
            binding.inputFrequency.setAdapter(adapter)
            val adapter2 = ArrayAdapter(
                requireContext(),
                R.layout.material_spinner_item,
                Frequency.values()
            )
            binding.inputRecurringFrequency.setAdapter(adapter2)
        }

        if (platform == PlatformEnum.SAVINGS ||
            platform == PlatformEnum.BETS ||
            platform == PlatformEnum.CASH
        ) {
            binding.inputSavings.filters = arrayOf<InputFilter>(InputFilter.AllCaps())
        }

        binding.inputEmail.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )

        binding.inputPassword.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )
        binding.inputSavings.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )
        binding.inputFrequency.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )
        binding.inputInterests.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )
        binding.inputRecurringAmount.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )
        binding.inputRecurringFrequency.addTextChangedListener(
            afterTextChanged = {
                cleanErrors()
            }
        )
    }

    fun login() {
        val p2pValid = validateP2P()
        val savingsValid = validateSavings()
        val gamblingValid = validateGambling()
        val cashValid = validateCash()
        if (isLoginValid(p2pValid, savingsValid, gamblingValid, cashValid)) {
            loginViewModel.login(
                params.platform,
                binding.inputEmail.editableText.toString(),
                binding.inputPassword.editableText.toString(),
                binding.inputSavings.editableText.toString().trim(),
                binding.inputInterests.editableText.toString(),
                binding.inputFrequency.editableText.toString(),
                binding.inputRecurringAmount.editableText.toString(),
                binding.inputRecurringFrequency.editableText.toString(),
                binding.switchInclude.isChecked
            )
        }
    }

    private fun isLoginValid(
        p2pValid: Boolean,
        savingsValid: Boolean,
        gamblingValid: Boolean,
        cashValid: Boolean
    ) = p2pValid && savingsValid && gamblingValid && cashValid

    private fun validateP2P(): Boolean {
        val platform = PlatformEnum.valueOf(params.platform).platformTypeEnum
        if (platform != PlatformTypeEnum.P2P) {
            return true
        }

        var valid = true
        val email = binding.inputEmail.text.toString()
        val password = binding.inputPassword.text.toString()
        if (email.isBlank()) {
            binding.tilEmail.error = "Required"
            valid = false
        } else {
            binding.tilEmail.error = null
        }

        if (password.isBlank()) {
            binding.tilPassword.error = "Required"
            valid = false
        } else {
            binding.tilPassword.error = null
        }
        return valid
    }

    private fun validateSavings(): Boolean {
        val platform = PlatformEnum.valueOf(params.platform).platformTypeEnum
        if (platform != PlatformTypeEnum.SAVINGS) {
            return true
        }
        var valid = true
        val name = binding.inputSavings.text.toString()
        val interests = binding.inputInterests.text.toString()
        val frequency = binding.inputFrequency.text.toString()
        val recurringAmount = binding.inputRecurringAmount.text.toString()
        val recurringFrequency = binding.inputRecurringFrequency.text.toString()
        if (name.isBlank()) {
            binding.tilSavings.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilSavings.error = null
        }

        if (interests.isBlank()) {
            binding.tilInterests.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilInterests.error = null
        }

        if (frequency.isBlank()) {
            binding.tilFrequency.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilFrequency.error = null
        }

        if (recurringAmount.isBlank()) {
            binding.tilRecurringAmount.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilRecurringAmount.error = null
        }

        if (recurringFrequency.isBlank()) {
            binding.tilRecurringFrequency.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilRecurringFrequency.error = null
        }
        return valid
    }

    private fun validateGambling(): Boolean {
        val platform = PlatformEnum.valueOf(params.platform).platformTypeEnum
        if (platform != PlatformTypeEnum.GAMBLING) {
            return true
        }
        var valid = true
        val name = binding.inputSavings.text.toString()
        if (name.isBlank()) {
            binding.tilSavings.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilSavings.error = null
        }

        return valid
    }

    private fun validateCash(): Boolean {
        val platform = PlatformEnum.valueOf(params.platform).platformTypeEnum
        if (platform != PlatformTypeEnum.CASH) {
            return true
        }
        var valid = true
        val name = binding.inputSavings.text.toString()
        if (name.isBlank()) {
            binding.tilSavings.error = getString(R.string.required_field)
            valid = false
        } else {
            binding.tilSavings.error = null
        }

        return valid
    }

    private fun showLoading() {
        binding.body.visibility = GONE
        binding.tvError.visibility = GONE
        binding.layoutNoInternet.visibility = GONE
        binding.animationView.apply {
            visibility = VISIBLE
            setAnimation(R.raw.loading)
            speed = 0.9f
            repeatCount = -1
            playAnimation()
        }
    }

    private fun showSuccess() {
        binding.body.visibility = GONE
        binding.animationView.apply {
            visibility = VISIBLE
            setAnimation(R.raw.success)
            speed = 0.75f
            repeatCount = 0
            addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) = Unit

                override fun onAnimationEnd(animation: Animator?) {
                    findNavController().navigateUp()
                }

                override fun onAnimationCancel(animation: Animator?) = Unit

                override fun onAnimationStart(animation: Animator?) = Unit
            })
            playAnimation()
        }
    }

    private fun showError(message: String?) {
        binding.animationView.setAnimation(R.raw.error)
        binding.animationView.apply {
            speed = 1.0f
            visibility = VISIBLE
            repeatCount = 0
            addAnimatorListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) = Unit

                override fun onAnimationEnd(animation: Animator?) {
                    binding.animationView.visibility = GONE
                    binding.body.visibility = VISIBLE
                    message?.let {
                        binding.tvError.visibility = VISIBLE
                        binding.tvError.text = it
                    }
                }

                override fun onAnimationCancel(animation: Animator?) = Unit

                override fun onAnimationStart(animation: Animator?) = Unit
            })
            playAnimation()
        }
    }

    override fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String?
    ) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            binding.animationView.visibility = VISIBLE
            binding.webView.visibility = GONE
        }

        when (platformEnum) {
            PlatformEnum.BONDORA,
            PlatformEnum.MINTOS,
            PlatformEnum.GRUPEER,
            PlatformEnum.CROWDESTOR -> loginViewModel.onWebViewResult(
                loginStatus,
                platformEnum,
                username,
                password,
                token,
                false,
                frequentUpdates
            )
            else -> return
        }
    }

    override fun loadInfo(webViewData: WebViewData?) = Unit

    override fun onError(platformEnum: PlatformEnum, errorCode: Int) = Unit

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(
        login: Login,
        frequentUpdates: Boolean
    ) {
        val client =
            BaseWebViewClient.getClient(
                requireContext(),
                layoutInflater,
                platformEnum,
                login,
                this,
                true,
                frequentUpdates
            )

        binding.animationView.visibility = View.INVISIBLE
        binding.webView.apply {
            visibility = VISIBLE
            webViewClient = client
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            settings.userAgentString = Constants.USER_AGENT_VALUE
            loadUrl(client.getInitialUrl())
        }
    }

    private fun loadAuthDialog(platformEnum: PlatformEnum, login: Login, frequentUpdates: Boolean) {
        AuthDialogUtils.showAuthDialogFragment(
            platformEnum,
            login.username,
            login.password,
            this@LoginFragment,
            frequentUpdates
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            SUCCESS -> loginViewModel.onWebViewResult(
                BaseWebViewClient.SUCCESS,
                platformEnum,
                data?.getStringExtra(USERNAME)!!,
                data.getStringExtra(PASSWORD)!!,
                data.getStringExtra(TOKEN)!!,
                false, // TODO change
                data.getBooleanExtra(FREQUENT_UPDATES, false)
            )
            INVALID_USERNAME_PASSWORD_CODE -> showError(getString(R.string.wrong_username_password))
            UNKNOWN_ERROR_CODE -> showError(getString(R.string.generic_error_message))
            CANCEL -> showError(getString(R.string.wrong_username_password))
        }
    }

    private fun cleanErrors() {
        with(binding) {
            tilEmail.error = null
            tilPassword.error = null
            tilSavings.error = null
            tilFrequency.error = null
            tilInterests.error = null
            tilRecurringAmount.error = null
            tilRecurringFrequency.error = null
            tvError.visibility = GONE
            tvError.text = ""
        }
    }
}
