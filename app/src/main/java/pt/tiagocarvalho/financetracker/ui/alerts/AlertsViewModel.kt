package pt.tiagocarvalho.financetracker.ui.alerts

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import java.util.EnumMap
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.model.AlertItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.isNoNetworkException
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class AlertsViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger,
    private val alertsUseCase: AlertsUseCase
) : BaseViewModel() {

    var status: MutableLiveData<Resource<List<AlertItem>>> = MutableLiveData()
    var tfaRequired: MutableLiveData<Platform> = MutableLiveData()

    private var requiredPlatforms: MutableList<Platform> = mutableListOf()
    private var loadedData: MutableMap<PlatformEnum, WebViewData> =
        EnumMap(PlatformEnum::class.java)

    fun loadRefreshNeeded(force: Boolean) {
        compositeDisposable.add(
            alertsUseCase.isRefreshDetailsNeeded(force)
                .doOnSuccess {
                    if (it) {
                        loadTwoFactorPlatforms()
                    } else {
                        loadAlerts()
                    }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { status.postValue(Resource.loading()) }
                .subscribe()
        )
    }

    fun loadNextAuth(webViewData: WebViewData?) {
        webViewData?.let {
            loadedData[it.platform] = it
        }
        if (requiredPlatforms.size > 0) {
            tfaRequired.postValue(requiredPlatforms.first())
            requiredPlatforms.removeAt(0)
        } else {
            requiredPlatforms = mutableListOf()
            tfaRequired = MutableLiveData()
            refreshDetails()
        }
    }

    private fun loadTwoFactorPlatforms() {
        compositeDisposable.add(
            alertsUseCase.getTwoFactorPlatforms()
                .subscribeOn(schedulerProvider.io())
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            requiredPlatforms = it.toMutableList()
                            tfaRequired.postValue(requiredPlatforms.first())
                            requiredPlatforms.removeAt(0)
                        } else {
                            refreshDetails()
                        }
                    },
                    { onError(it) },
                    { refreshDetails() }
                )
        )
    }

    private fun onError(throwable: Throwable) {
        if (throwable.isNoNetworkException()) {
            status.postValue(Resource.error("0"))
        } else {
            logger.log(throwable)
            status.postValue(Resource.error("1"))
        }
    }

    private fun refreshDetails() {
        compositeDisposable.add(
            alertsUseCase.refreshDetails(loadedData)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    { loadAlerts() },
                    { onError(it) }
                )
        )
    }

    private fun loadAlerts() {
        compositeDisposable.add(
            alertsUseCase.loadAlerts()
                .doOnSubscribe { status.postValue(Resource.loading()) }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { status.postValue(Resource.success(it)) }
                .subscribe({}, { onError(it) })
        )
    }
}
