package pt.tiagocarvalho.financetracker.ui.details.gambling

import android.content.Context
import android.net.Uri
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Completable
import java.io.IOException
import java.text.ParseException
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.CsvUtils
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class GamblingViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val gamblingRepository: GamblingRepository,
    private val statementsRepository: StatementsRepository,
    private val preferencesHelper: PreferencesHelper,
    private val context: Context,
    private val logger: Logger,
    private val gamblingUseCase: GamblingUseCase
) : BaseViewModel() {

    var status: MutableLiveData<Status> = MutableLiveData()
    var invalidFormat: MutableLiveData<Boolean> = MutableLiveData()
    val info: MutableLiveData<List<PlatformStatement>> = MutableLiveData()
    val navigate: SingleLiveEvent<Boolean> = SingleLiveEvent()

    val details: ObservableField<PlatformDetails> =
        ObservableField(PlatformDetails(PlatformEnum.PEERBERRY))
    var statusObservable: ObservableField<Status> = ObservableField()

    fun loadInfo(shouldFetch: Boolean, name: String) {
        compositeDisposable.add(
            gamblingRepository.loadDetails(shouldFetch, name)
                .flatMap { platformDetails ->
                    gamblingRepository.loadStatements(shouldFetch, name)
                        .map { Pair(platformDetails, it) }
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .delay(1, TimeUnit.SECONDS)
                .subscribe(
                    {
                        details.set(it.first.data)
                        status.postValue(Status.SUCCESS) // TODO
                        statusObservable.set(Status.SUCCESS)
                        info.postValue(it.second)
                    },
                    {
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        logger.log(it)
                    }
                )
        )
    }

    fun removeStatement(name: String, id: String) {
        compositeDisposable.add(
            Completable.fromAction {
                gamblingRepository.removeStatement(id, name)
            }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        loadInfo(true, name)
                    },
                    {
                        logger.log(it)
                    }
                )
        )
    }

    fun importCsv(uri: Uri, name: String) {
        compositeDisposable.add(
            gamblingUseCase.importCsv(uri, name)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { onSubscribe() }
                .subscribe({ loadInfo(true, name) }, { onImportError(it) })
        )
    }

    fun exportCsv(uri: Uri, name: String) {
        compositeDisposable.add(
            Completable.fromAction {
                val list = gamblingRepository.loadStatementsSync(name).map { it.toStringCSV() }
                CsvUtils.writeToCsv(uri, list, context)
            }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe { onSubscribe() }
                .subscribe({ onSuccess() }, { onError(it) })
        )
    }

    fun addTransactions() {
        navigate.value = true
    }

    fun clearInvalidFormatError() {
        invalidFormat.value = false
    }

    private fun onSuccess() {
        status.postValue(Status.SUCCESS)
        statusObservable.set(Status.SUCCESS)
    }

    private fun onError(it: Throwable) {
        logger.log(it)
        status.postValue(Status.ERROR)
        statusObservable.set(Status.ERROR)
    }

    private fun onImportError(it: Throwable) {
        logger.log(it)
        if (it is IOException || it is ParseException) {
            invalidFormat.value = true
        }
        status.postValue(Status.ERROR)
        statusObservable.set(Status.ERROR)
    }

    private fun onSubscribe() {
        status.postValue(Status.LOADING)
        statusObservable.set(Status.LOADING)
    }

    fun resetStatements(name: String) {
        compositeDisposable.add(
            statementsRepository.deleteStatements(name)
                .andThen(deleteStatementsPreferences(name))
                .delay(1, TimeUnit.SECONDS)
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(
                    {
                        loadInfo(false, name)
                    },
                    {
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                        logger.log(it)
                    }
                )
        )
    }

    private fun deleteStatementsPreferences(name: String) = Completable.fromAction {
        preferencesHelper.delete(PlatformEnum.BETS.prefLastUpdateStatement + name)
        preferencesHelper.delete(PlatformEnum.BETS.prefLastFullStatementScan + name)
    }
}
