package pt.tiagocarvalho.financetracker.ui.accounts.last_change

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ItemLastChangeBinding
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter
import pt.tiagocarvalho.financetracker.utils.isDayMode

class LastChangeAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    val context: Context
) : DataBoundListAdapter<LastChangeItem, ItemLastChangeBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<LastChangeItem>() {
        override fun areItemsTheSame(
            oldItem: LastChangeItem,
            newItem: LastChangeItem
        ) = oldItem.name == newItem.name

        override fun areContentsTheSame(
            oldItem: LastChangeItem,
            newItem: LastChangeItem
        ) = oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemLastChangeBinding = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.item_last_change,
        parent,
        false,
        dataBindingComponent
    )

    override fun bind(binding: ItemLastChangeBinding, item: LastChangeItem) {
        binding.item = item
        val color = when {
            item.comparator > 0 -> R.color.inwallet_green_500
            item.comparator == 0 -> if (context.isDayMode()) R.color.inwallet_grey_500 else R.color.inwallet_grey_200
            else -> R.color.inwallet_red_500
        }
        binding.change.setTextColor(context.resources.getColor(color))
    }
}
