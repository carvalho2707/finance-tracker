package pt.tiagocarvalho.financetracker.ui.statistics.p2p

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class P2pFragmentModule {

    @Provides
    fun provideViewModel(
        platformDetailsRepository: PlatformDetailsRepository,
        schedulerProvider: SchedulerProvider,
        logger: Logger
    ): P2pViewModel = P2pViewModel(platformDetailsRepository, schedulerProvider, logger)
}
