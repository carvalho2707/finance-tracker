package pt.tiagocarvalho.financetracker.ui.statistics.forecast

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import java.math.BigDecimal
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.model.ForecastStatistics
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class ForecastViewModel @Inject constructor(
    private val platformDetailsRepository: PlatformDetailsRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger
) : BaseViewModel() {

    var yearly: ObservableField<BigDecimal> = ObservableField(BigDecimal.ZERO)
    var monthly: ObservableField<BigDecimal> = ObservableField(BigDecimal.ZERO)
    var daily: ObservableField<BigDecimal> = ObservableField(BigDecimal.ZERO)
    var secondly: ObservableField<BigDecimal> = ObservableField(BigDecimal.ZERO)
    var statusObservable: ObservableField<Status> = ObservableField()

    var status: MutableLiveData<Status> = MutableLiveData()
    val statistics: MutableLiveData<ForecastStatistics> = MutableLiveData()
    val byYear: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val byMonth: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun loadForecastStatistics() {
        compositeDisposable.add(
            platformDetailsRepository.loadForecastStatistics()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .subscribe(
                    {
                        status.postValue(Status.SUCCESS) // TODO
                        statusObservable.set(Status.SUCCESS)
                        statistics.postValue(it)
                        yearly.set(it.yearly)
                        monthly.set(it.monthly)
                        daily.set(it.daily)
                        secondly.set(it.secondly)
                    },
                    {
                        logger.log(it)
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                    }
                )
        )
    }

    fun setShowByYear() {
        byYear.value = true
    }

    fun setShowByMonth() {
        byMonth.value = true
    }
}
