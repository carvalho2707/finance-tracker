package pt.tiagocarvalho.financetracker.ui.details.cash

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.Intent.ACTION_CREATE_DOCUMENT
import android.content.Intent.ACTION_OPEN_DOCUMENT
import android.content.Intent.CATEGORY_OPENABLE
import android.content.Intent.EXTRA_TITLE
import android.graphics.Canvas
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.LEFT
import androidx.recyclerview.widget.ItemTouchHelper.RIGHT
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentCashBinding
import pt.tiagocarvalho.financetracker.databinding.FragmentDataBindingComponent
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.ui.details.cash.CashFragment.Constants.READ_REQUEST_CODE
import pt.tiagocarvalho.financetracker.ui.details.cash.CashFragment.Constants.WRITE_REQUEST_CODE
import pt.tiagocarvalho.financetracker.ui.details.common.PastDayAdapter
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.smoothSnapToPosition

@AndroidEntryPoint
class CashFragment : BaseFragment() {

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var pastDayAdapter: PastDayAdapter
    private lateinit var binding: FragmentCashBinding

    private val viewModel: CashViewModel by viewModels()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    private val params by navArgs<CashFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentCashBinding.inflate(inflater, container, false)

        binding.apply {
            viewModel = this@CashFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
            name = params.name
            var isToolbarShown = true
            var scrollRange = -1
            appbar.addOnOffsetChangedListener(
                AppBarLayout.OnOffsetChangedListener { barLayout, verticalOffset ->
                    if (scrollRange == -1) {
                        scrollRange = barLayout?.totalScrollRange!!
                    }
                    if (scrollRange + verticalOffset == 0) {
                        toolbarTitle.visibility = View.VISIBLE
                        isToolbarShown = true
                    } else if (isToolbarShown) {
                        toolbarTitle.visibility = View.GONE
                        isToolbarShown = false
                    }
                }
            )

            toolbar.setNavigationOnClickListener { view ->
                view.findNavController().navigateUp()
            }
        }

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pastDayAdapter = PastDayAdapter(
            dataBindingComponent,
            appExecutors,
            requireContext()
        )

        registerToolbarWithNavigation(binding.toolbar)

        viewModel.status.observe(viewLifecycleOwner) { status ->
            if (status == Status.LOADING) {
                binding.animationView.setAnimation(R.raw.loading)
                binding.animationView.repeatCount = -1
                binding.animationView.playAnimation()
            } else binding.animationView.repeatCount = 0
        }

        viewModel.invalidFormat.observe(viewLifecycleOwner) {
            if (it) showInvalidFormatAlert()
        }

        binding.rvSavingDetails.apply {
            addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
            adapter = pastDayAdapter
        }

        val callback = getSimpleCallback()
        val itemTouchHelper = ItemTouchHelper(callback)
        itemTouchHelper.attachToRecyclerView(binding.rvSavingDetails)

        viewModel.info.observe(viewLifecycleOwner) { statements ->
            scheduleAnimation(pastDayAdapter)
            pastDayAdapter.submitList(statements)
        }

        viewModel.navigate.observe(viewLifecycleOwner) {
            showAddTransaction()
        }

        viewModel.loadInfo(false, params.name)
    }

    override fun onResume() {
        super.onResume()
        binding.rvSavingDetails.smoothSnapToPosition(0)
    }

    private fun getSimpleCallback(): ItemTouchHelper.Callback =
        object : ItemTouchHelper.SimpleCallback(0, RIGHT or LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val entry = pastDayAdapter.getItemNew(position)

                if (direction == RIGHT) {
                    findNavController().navigate(
                        CashFragmentDirections.showAddTransaction(
                            params.name,
                            PlatformEnum.CASH.name,
                            entry.label
                        )
                    )
                } else if (direction == LEFT) {
                    viewModel.removeStatement(params.name, entry.label)
                }
            }

            override fun onChildDraw(
                c: Canvas,
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                dX: Float,
                dY: Float,
                actionState: Int,
                isCurrentlyActive: Boolean
            ) {
                RecyclerViewSwipeDecorator.Builder(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
                    .addSwipeLeftActionIcon(android.R.drawable.ic_menu_delete)
                    .addSwipeRightActionIcon(android.R.drawable.ic_menu_edit)
                    .create()
                    .decorate()
                super.onChildDraw(
                    c,
                    recyclerView,
                    viewHolder,
                    dX,
                    dY,
                    actionState,
                    isCurrentlyActive
                )
            }
        }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_refresh -> viewModel.loadInfo(true, params.name)
            R.id.menu_import -> showImportAlertDialog()
            R.id.menu_export -> createFileIntent("export" + params.name + ".csv")
            R.id.menu_reset_statements -> viewModel.resetStatements(params.name)
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun showImportAlertDialog() {
        with(MaterialAlertDialogBuilder(requireContext())) {
            setMessage(getString(R.string.dialog_warning_import_body))
            setPositiveButton(R.string.yes) { dialog, _ ->
                openSelectFileIntent()
                dialog.dismiss()
            }
            setNegativeButton(R.string.cancel) { dialog, _ ->
                dialog.dismiss()
            }
            show()
        }
    }

    private fun showInvalidFormatAlert() {
        with(MaterialAlertDialogBuilder(requireContext())) {
            setTitle(getString(R.string.dialog_warning_invalid_format_title))
            setMessage(getString(R.string.dialog_warning_invalid_format_body))
            setPositiveButton(R.string.ok) { dialog, _ ->
                viewModel.clearInvalidFormatError()
                dialog.dismiss()
            }
            show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == READ_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                val uri = data.data!!
                viewModel.importCsv(uri, params.name)
            }
        } else if (requestCode == WRITE_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                val uri = data.data!!
                viewModel.exportCsv(uri, params.name)
            }
        }
    }

    private fun openSelectFileIntent() {
        val intent = Intent(ACTION_OPEN_DOCUMENT)
        intent.addCategory(CATEGORY_OPENABLE)
        intent.type = "text/csv"
        startActivityForResult(intent, READ_REQUEST_CODE)
    }

    private fun createFileIntent(fileName: String) {
        val intent = Intent(ACTION_CREATE_DOCUMENT)
        intent.addCategory(CATEGORY_OPENABLE)
        intent.type = "text/csv"
        intent.putExtra(EXTRA_TITLE, fileName)
        startActivityForResult(intent, WRITE_REQUEST_CODE)
    }

    private fun showAddTransaction() {
        findNavController().navigate(
            CashFragmentDirections.showAddTransaction(
                params.name,
                PlatformEnum.CASH.name,
                null
            )
        )
    }

    object Constants {
        internal const val READ_REQUEST_CODE = 42
        internal const val WRITE_REQUEST_CODE = 43
    }

    private fun scheduleAnimation(adapter: PastDayAdapter) {
        val controller =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        binding.rvSavingDetails.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        binding.rvSavingDetails.scheduleLayoutAnimation()
    }
}
