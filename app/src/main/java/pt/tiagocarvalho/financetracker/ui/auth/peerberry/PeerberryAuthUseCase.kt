package pt.tiagocarvalho.financetracker.ui.auth.peerberry

import com.google.gson.Gson
import io.reactivex.Single
import java.util.UUID
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.model.peerberry.Errors
import pt.tiagocarvalho.p2p.services.model.peerberry.LoginResponse
import pt.tiagocarvalho.p2p.services.model.peerberry.TfaRequest
import pt.tiagocarvalho.p2p.services.retrofit.PeerberryApi
import retrofit2.HttpException

class PeerberryAuthUseCase(
    private val peerberryApi: PeerberryApi,
    private val logger: Logger
) {

    internal fun login(email: String, password: String): Single<LoginResponse> =
        peerberryApi.login(email, password, UUID.randomUUID().toString())
            .onErrorReturn { mapErrorLogin(it) }

    fun loginTfa(tfaToken: String, code: String) = peerberryApi.loginTfa(TfaRequest(code, tfaToken))
        .onErrorReturn { mapErrorLoginTfa(it) }

    private fun mapErrorLogin(throwable: Throwable) = when (throwable) {
        is HttpException -> {
            if (throwable.code() == 401) {
                val gson = Gson()
                val body = throwable.response()?.errorBody()?.charStream()
                val error = gson.fromJson(body, Errors::class.java)
                LoginResponse(passwordError = (error.errors.password != null || error.errors.email != null))
            } else {
                logger.log(throwable)
                LoginResponse(unknownError = true)
            }
        }
        else -> {
            logger.log(throwable)
            LoginResponse(unknownError = true)
        }
    }

    private fun mapErrorLoginTfa(throwable: Throwable) = when (throwable) {
        is HttpException -> {
            if (throwable.response()?.code() == 403) {
                LoginResponse(invalidCode = true)
            } else {
                val body = throwable.response()?.errorBody()?.string()
                if (body != null) {
                    logger.log(body)
                } else {
                    logger.log(throwable)
                }
                LoginResponse(unknownError = true)
            }
        }
        else -> {
            logger.log(throwable)
            LoginResponse(unknownError = true)
        }
    }
}
