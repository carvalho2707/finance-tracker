package pt.tiagocarvalho.financetracker.ui.details.gambling

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.GamblingRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class GamblingFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        gamblingRepository: GamblingRepository,
        statementsRepository: StatementsRepository,
        preferencesHelper: PreferencesHelper,
        context: Context,
        logger: Logger,
        gamblingUseCase: GamblingUseCase
    ) = GamblingViewModel(
        schedulerProvider,
        gamblingRepository,
        statementsRepository,
        preferencesHelper,
        context,
        logger,
        gamblingUseCase
    )
}
