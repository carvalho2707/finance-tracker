package pt.tiagocarvalho.financetracker.ui.statistics.p2p

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.model.P2pStatistics
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.repository.PlatformDetailsRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class P2pViewModel @Inject constructor(
    private val platformDetailsRepository: PlatformDetailsRepository,
    private val schedulerProvider: SchedulerProvider,
    private val logger: Logger
) : BaseViewModel() {

    var totalPlatform: ObservableField<String> = ObservableField("0")
    var ongoingLoans: ObservableField<String> = ObservableField("0")
    var totalStatements: ObservableField<String> = ObservableField("0")
    var statusObservable: ObservableField<Status> = ObservableField()

    var status: MutableLiveData<Status> = MutableLiveData()
    val statistics: MutableLiveData<P2pStatistics> = MutableLiveData()

    fun loadP2pStatistics() {
        compositeDisposable.add(
            platformDetailsRepository.loadP2pStatistics()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnNext { }
                .doOnSubscribe {
                    status.postValue(Status.LOADING)
                    statusObservable.set(Status.LOADING)
                }
                .subscribe(
                    {
                        status.postValue(Status.SUCCESS) // TODO
                        statusObservable.set(Status.SUCCESS)
                        statistics.postValue(it)
                        totalPlatform.set(it.total.toString())
                        ongoingLoans.set(it.ongoingInvestments.toString())
                        totalStatements.set(it.totalStatements.toString())
                    },
                    {
                        logger.log(it)
                        status.postValue(Status.ERROR)
                        statusObservable.set(Status.ERROR)
                    }
                )
        )
    }
}
