package pt.tiagocarvalho.financetracker.ui.premium

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel

@HiltViewModel
class PremiumOnlyViewModel @Inject constructor() : BaseViewModel() {

    val navigateToBilling: MutableLiveData<Boolean> = MutableLiveData()
    val closeDialog: MutableLiveData<Boolean> = MutableLiveData()

    fun showBilling() {
        navigateToBilling.value = true
    }

    fun back() {
        closeDialog.value = true
    }
}
