package pt.tiagocarvalho.financetracker.ui.alerts

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.databinding.FragmentAlertsBinding
import pt.tiagocarvalho.financetracker.databinding.FragmentDataBindingComponent
import pt.tiagocarvalho.financetracker.model.AlertItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.financetracker.webview.BaseWebViewNavigator
import pt.tiagocarvalho.p2p.api.model.Login

@AndroidEntryPoint
class AlertsFragment : BaseFragment(), BaseWebViewNavigator {

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentAlertsBinding

    private val alertsViewModel: AlertsViewModel by viewModels()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentAlertsBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = alertsViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = AlertsAdapter(
            dataBindingComponent,
            appExecutors,
            requireContext(),
            findNavController()
        )
        binding.rvAlerts.adapter = adapter

        registerToolbarWithNavigation(binding.toolbar)

        alertsViewModel.status.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> {
                    binding.webView.visibility = GONE
                    binding.rvAlerts.visibility = GONE
                    binding.noAlertsMessage.visibility = GONE
                    binding.noAlertsAnimation.visibility = GONE
                    binding.layoutNoInternet.visibility = GONE
                    binding.layoutGenericError.visibility = GONE
                    binding.layoutLoading.visibility = VISIBLE
                }
                Status.SUCCESS -> {
                    handleAlerts(it.data!!, adapter)
                }
                Status.ERROR -> {
                    binding.webView.visibility = GONE
                    binding.layoutLoading.visibility = GONE
                    binding.noAlertsAnimation.visibility = GONE
                    binding.rvAlerts.visibility = GONE
                    binding.noAlertsMessage.visibility = GONE
                    handleError(it.message?.toInt() ?: 1)
                }
            }
        }

        alertsViewModel.tfaRequired.observe(viewLifecycleOwner) {
            if (AuthDialogUtils.belongsToAuthDialog(it.type)) {
                loadAuthDialog(it)
            } else {
                loadWebView(it)
            }
        }

        alertsViewModel.loadRefreshNeeded(false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(login: Platform) {
        binding.layoutLoading.visibility = View.INVISIBLE
        binding.webView.visibility = VISIBLE
        val client = BaseWebViewClient.getClient(
            requireContext(),
            layoutInflater,
            login.type,
            Login(login.username!!, login.password!!),
            this,
            isLogin = false,
            frequentUpdates = true
        )
        binding.webView.webViewClient = client

        val webSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.userAgentString = Constants.USER_AGENT_VALUE

        binding.webView.loadUrl(client.getInitialUrl())
    }

    private fun loadAuthDialog(platform: Platform) {
        AuthDialogUtils.showAuthDialogFragment(
            platform.type,
            platform.username!!,
            platform.password!!,
            this@AlertsFragment
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val platform = PlatformEnum.valueOf(data?.getStringExtra(PLATFORM)!!)
        if (resultCode == SUCCESS) {
            alertsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)!!))
        } else if (resultCode == INVALID_USERNAME_PASSWORD_CODE ||
            resultCode == UNKNOWN_ERROR_CODE
        ) {
            handleError(1)
        } else if (resultCode == AuthDialogUtils.CANCEL) {
            alertsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)))
        }
    }

    override fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String?
    ) = Unit

    override fun loadInfo(webViewData: WebViewData?) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            binding.webView.visibility = GONE
            binding.layoutLoading.visibility = VISIBLE
        }
        alertsViewModel.loadNextAuth(webViewData)
    }

    override fun onError(platformEnum: PlatformEnum, errorCode: Int) {
        handleError(errorCode)
    }

    private fun handleAlerts(
        alerts: List<AlertItem>,
        adapter: AlertsAdapter
    ) {
        binding.webView.visibility = GONE
        binding.layoutLoading.visibility = GONE
        binding.layoutNoInternet.visibility = GONE
        binding.layoutGenericError.visibility = GONE
        if (alerts.isEmpty()) {
            binding.noAlertsMessage.visibility = VISIBLE
            binding.noAlertsAnimation.visibility = VISIBLE
            binding.rvAlerts.visibility = GONE
        } else {
            binding.noAlertsMessage.visibility = GONE
            binding.noAlertsAnimation.visibility = GONE
            binding.rvAlerts.visibility = VISIBLE
            scheduleAnimation(adapter)
            adapter.submitList(alerts)
        }
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean =
        if (menuItem.itemId == R.id.menu_settings) {
            findNavController().navigate(MainDirections.actionGlobalSettings())
            true
        } else super.onOptionsItemSelected(menuItem)

    private fun handleError(errorCode: Int) {
        binding.webView.visibility = GONE
        binding.layoutLoading.visibility = GONE
        binding.noAlertsAnimation.visibility = GONE
        binding.rvAlerts.visibility = GONE
        binding.noAlertsMessage.visibility = GONE

        if (errorCode == 0) {
            binding.layoutNoInternet.visibility = VISIBLE
            binding.layoutGenericError.visibility = GONE
        } else {
            binding.layoutNoInternet.visibility = GONE
            binding.layoutGenericError.visibility = VISIBLE
        }
    }

    private fun scheduleAnimation(adapter: AlertsAdapter) {
        val controller =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        binding.rvAlerts.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        binding.rvAlerts.scheduleLayoutAnimation()
    }
}
