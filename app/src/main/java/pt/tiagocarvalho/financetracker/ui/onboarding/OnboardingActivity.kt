package pt.tiagocarvalho.financetracker.ui.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.animation.AccelerateInterpolator
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ActivityOnboardingBinding
import pt.tiagocarvalho.financetracker.ui.main.MainActivity

@AndroidEntryPoint
class OnboardingActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnboardingBinding

    private val onboardingViewModel: OnboardingViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_onboarding)

        binding.lifecycleOwner = this

        val onboardingAdapter = OnboardingViewPagerAdapter()

        binding.viewPager.apply {
            adapter = onboardingAdapter
            registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    if (position == 2) {
                        binding.btnGetStarted.animate().alpha(1f).setDuration(500)
                            .setInterpolator(AccelerateInterpolator()).start()
                    }
                }
            })
        }

        binding.btnGetStarted.setOnClickListener {
            onboardingViewModel.markOnboardingSeen()
        }

        subscribeUi(onboardingAdapter)

        TabLayoutMediator(binding.tabs, binding.viewPager) { _, _ ->
        }.attach()
    }

    private fun subscribeUi(adapter: OnboardingViewPagerAdapter) {
        onboardingViewModel.items.observe(
            this,
            {
                adapter.submitList(it)
            }
        )

        onboardingViewModel.navigateToMainActivity.observe(
            this,
            {
                goToMainActivity()
            }
        )

        onboardingViewModel.loadItems()
    }

    private fun goToMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
    }
}
