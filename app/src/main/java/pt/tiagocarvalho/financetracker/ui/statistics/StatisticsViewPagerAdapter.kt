package pt.tiagocarvalho.financetracker.ui.statistics

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import pt.tiagocarvalho.financetracker.ui.statistics.forecast.ForecastFragment
import pt.tiagocarvalho.financetracker.ui.statistics.general.GeneralFragment
import pt.tiagocarvalho.financetracker.ui.statistics.p2p.P2pFragment
import pt.tiagocarvalho.financetracker.ui.statistics.returns.ReturnsFragment

class StatisticsViewPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        0 to { GeneralFragment.newInstance() },
        1 to { ReturnsFragment.newInstance() },
        2 to { P2pFragment.newInstance() },
        3 to { ForecastFragment.newInstance() }
    )

    override fun getItemCount() = tabFragmentsCreators.size

    override fun createFragment(position: Int): Fragment =
        tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()
}
