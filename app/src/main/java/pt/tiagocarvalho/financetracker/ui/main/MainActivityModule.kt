package pt.tiagocarvalho.financetracker.ui.main

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger

@InstallIn(ActivityComponent::class)
@Module
class MainActivityModule {

    @Provides
    fun provideViewModel(
        analyticsLogger: AnalyticsLogger,
        mainUseCase: MainUseCase,
        schedulerProvider: SchedulerProvider
    ): MainViewModel = MainViewModel(analyticsLogger, mainUseCase, schedulerProvider)
}
