package pt.tiagocarvalho.financetracker.ui.accounts.last_change

import androidx.databinding.ObservableField
import androidx.databinding.ObservableFloat
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.accounts.AccountsUseCase
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@HiltViewModel
class LastChangeViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val accountsUseCase: AccountsUseCase,
    private val preferencesHelper: PreferencesHelper
) : BaseViewModel() {

    var recyclerviewAlpha = ObservableFloat(1f)
    var lastChangeStatus = ObservableField(Status.LOADING)

    val lastChangeItems: MutableLiveData<List<LastChangeItem>> = MutableLiveData()
    val lastChangeInfo: ObservableField<LastChangeInfo> = ObservableField(LastChangeInfo())

    fun handleLastSessionChange() {
        if (preferencesHelper.get(LastChangeFragment.UI_READY, false)) {
            compositeDisposable.add(
                accountsUseCase.getAndUpdateLastSessionBalance()
                    .doOnSubscribe { lastChangeStatus.set(Status.LOADING) }
                    .subscribeOn(schedulerProvider.io())
                    .doOnSuccess {
                        lastChangeInfo.set(it)
                        lastChangeItems.postValue(it.items)
                        lastChangeStatus.set(Status.SUCCESS)
                    }
                    .doOnError { it.printStackTrace() }
                    .subscribe()
            )
        } else {
            lastChangeStatus.set(Status.LOADING)
        }
    }
}
