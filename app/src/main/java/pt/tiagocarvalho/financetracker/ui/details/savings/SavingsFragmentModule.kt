package pt.tiagocarvalho.financetracker.ui.details.savings

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.repository.SavingsRepository
import pt.tiagocarvalho.financetracker.repository.StatementsRepository
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@InstallIn(FragmentComponent::class)
@Module
class SavingsFragmentModule {

    @Provides
    fun provideViewModel(
        schedulerProvider: SchedulerProvider,
        savingsRepository: SavingsRepository,
        statementsRepository: StatementsRepository,
        preferencesHelper: PreferencesHelper,
        context: Context,
        logger: Logger,
        savingsUseCase: SavingsUseCase
    ) = SavingsViewModel(
        schedulerProvider,
        savingsRepository,
        statementsRepository,
        preferencesHelper,
        context,
        logger,
        savingsUseCase
    )
}
