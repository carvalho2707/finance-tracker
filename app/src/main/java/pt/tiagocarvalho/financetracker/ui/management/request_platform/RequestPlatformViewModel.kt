package pt.tiagocarvalho.financetracker.ui.management.request_platform

import android.content.Context
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Single
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.billing.localdb.LocalBillingDb
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.SingleLiveEvent
import pt.tiagocarvalho.financetracker.utils.log.Logger

@HiltViewModel
class RequestPlatformViewModel @Inject constructor(
    private val schedulerProvider: SchedulerProvider,
    private val context: Context,
    private val logger: Logger
) : BaseViewModel() {

    val name: ObservableField<String> = ObservableField()
    val premium: MutableLiveData<Boolean> = MutableLiveData()

    val back: SingleLiveEvent<Boolean> = SingleLiveEvent()
    val navigateToBilling: SingleLiveEvent<Boolean> = SingleLiveEvent()

    fun load() {
        compositeDisposable.add(
            Single.create<Boolean> {
                val premium = LocalBillingDb.getInstance(
                    context
                ).entitlementsDao().getPremium()
                val isEntitled = (premium != null && premium.level == 3) || BuildConfig.DEBUG
                it.onSuccess(isEntitled)
            }
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { premium.value = it }
                .subscribe({}, {})
        )
    }

    fun back() {
        back.value = true
    }

    fun request() {
        logger.logPlatformRequest(name.get().orEmpty())
        Toast.makeText(context, "Request Sent \uD83D\uDD25", Toast.LENGTH_LONG).show()
        name.set("")
        back()
    }
}
