package pt.tiagocarvalho.financetracker.ui.statistics.returns

import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Build.VERSION_CODES.O
import android.os.Bundle
import android.util.SparseArray
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.MPPointF
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.ArrayList
import java.util.Calendar
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentReturnsBinding
import pt.tiagocarvalho.financetracker.model.GainsStatistics
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Constants.MATERIAL_COLORS_500
import pt.tiagocarvalho.financetracker.utils.MonthEnum
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.charts.CurrencyValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.MonthValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.PercentageValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.PlatformValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.StringValueFormatter

@AndroidEntryPoint
class ReturnsFragment : BaseFragment() {

    companion object {
        fun newInstance(): ReturnsFragment = ReturnsFragment()
    }

    private lateinit var binding: FragmentReturnsBinding

    private var color = 0
    private val returnsViewModel: ReturnsViewModel by viewModels()
    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentReturnsBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = returnsViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        returnsViewModel.status.observe(viewLifecycleOwner) { status ->
            if (status == Status.LOADING) {
                binding.contentScroll.visibility = View.GONE
                binding.noAccountsMessage.visibility = View.GONE
                binding.animationView.setAnimation(R.raw.loading)
                binding.animationView.repeatCount = -1
                binding.animationView.playAnimation()
            } else binding.animationView.repeatCount = 0
        }

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        color = Utils.themeAttributeToColorInt(android.R.attr.textColorPrimary, requireContext())
        createChartGainsByMonth()
        createChartCurrentMonthGains()
        createChartGainsByYear()
        createChartCurrentYearGains()
        createChartGainsByPlatform()
        createChartGainsByType()
        createChartXirrByName()
        createChartRoiByName()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        returnsViewModel.statistics.observe(viewLifecycleOwner) {
            it?.let {
                if (it.show) {
                    binding.noAccountsMessage.visibility = View.GONE
                    binding.contentScroll.visibility = View.VISIBLE
                    updateUI(it)
                } else {
                    binding.contentScroll.visibility = View.GONE
                    binding.noAccountsMessage.visibility = View.VISIBLE
                }
            }
        }

        returnsViewModel.loadReturnStatistics()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_refresh -> returnsViewModel.loadReturnStatistics()
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun updateUI(statistics: GainsStatistics) {
        updateChartsGainsByMonth(statistics.gainsByMonth)
        updateChartCurrentMonthGains(statistics.currentMonthByName)
        updateChartsGainsByYear(statistics.gainsByYear)
        updateChartCurrentYearGains(statistics.currentYearByName)
        updateChartGainsByPlatform(statistics.gainsByPlatform)
        updateChartGainsByType(statistics.gainsByType)
        updateChartXirrByName(statistics.xirByName)
        updateChartRoiByName(statistics.roiByName)
    }

    override fun onToolbarItemClicked(menuItem: MenuItem) = false

    private fun createChartGainsByMonth() {

        binding.lcGainsByMonth.description.isEnabled = false
        binding.lcGainsByMonth.setDrawGridBackground(false)
        binding.lcGainsByMonth.animateX(1500)
        binding.lcGainsByMonth.setTouchEnabled(true)
        binding.lcGainsByMonth.axisRight.setDrawGridLines(false)
        binding.lcGainsByMonth.axisRight.isEnabled = false
        binding.lcGainsByMonth.axisRight.setDrawAxisLine(false)
        binding.lcGainsByMonth.legend.isEnabled = true
        binding.lcGainsByMonth.legend.textColor = color

        val leftAxis = binding.lcGainsByMonth.axisLeft
        leftAxis.setLabelCount(8, false)
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = CurrencyValueFormatter()
        leftAxis.spaceTop = 15f
        leftAxis.textSize = 7f
        leftAxis.setDrawGridLines(false)
        leftAxis.textColor = color

        val xAxis = binding.lcGainsByMonth.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.isEnabled = true
        xAxis.textSize = 7f
        xAxis.textColor = color
        xAxis.setDrawAxisLine(false)
        xAxis.labelRotationAngle = -45f
        xAxis.valueFormatter = MonthValueFormatter()
        xAxis.setLabelCount(12, true)
        xAxis.setDrawGridLines(false)
    }

    private fun updateChartsGainsByMonth(gainsByMonth: SparseArray<Float>) {

        val entries = ArrayList<Entry>()
        for (i in 0 until gainsByMonth.size() - 1) {
            val value = gainsByMonth.get(gainsByMonth.keyAt(i))
            entries.add(Entry(i.toFloat(), value))
        }

        val ds1 = LineDataSet(entries, getString(R.string.returns_by_month))
        ds1.color = MATERIAL_COLORS_500[0]
        ds1.setCircleColor(MATERIAL_COLORS_500[0])
        ds1.lineWidth = 2.5f
        ds1.valueTextSize = 7f
        ds1.circleRadius = 3f
        ds1.valueTextColor = color

        val sets = listOf<ILineDataSet>(ds1)
        val lineData = LineData(sets)
        lineData.setValueFormatter(CurrencyValueFormatter())
        lineData.setValueTextSize(7f)

        binding.lcGainsByMonth.data = lineData

        lineData.notifyDataChanged()
        binding.lcGainsByMonth.notifyDataSetChanged()
        binding.lcGainsByMonth.invalidate()
    }

    private fun createChartGainsByYear() {
        binding.lcGainsByYear.description.isEnabled = false
        binding.lcGainsByYear.setDrawGridBackground(false)

        binding.lcGainsByYear.animateX(1500)
        binding.lcGainsByYear.setTouchEnabled(true)

        binding.lcGainsByYear.axisRight.setDrawGridLines(false)
        binding.lcGainsByYear.axisRight.isEnabled = false
        binding.lcGainsByYear.axisRight.setDrawAxisLine(false)
        binding.lcGainsByYear.legend.isEnabled = true
        binding.lcGainsByYear.legend.textColor = color

        val leftAxis = binding.lcGainsByYear.axisLeft
        leftAxis.setLabelCount(8, false)
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = CurrencyValueFormatter()
        leftAxis.spaceTop = 15f
        leftAxis.textSize = 7f
        leftAxis.setDrawGridLines(false)
        leftAxis.textColor = color

        val xAxis = binding.lcGainsByYear.xAxis
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.valueFormatter = StringValueFormatter()
        xAxis.setDrawAxisLine(false)
        xAxis.labelRotationAngle = -45f
        xAxis.isEnabled = true
        xAxis.textColor = color
        xAxis.textSize = 7f
        xAxis.setDrawGridLines(false)
    }

    private fun updateChartsGainsByYear(gainsByYear: MutableMap<Int, Float>) {
        gainsByYear.size

        val entries = ArrayList<Entry>()
        for (entry in gainsByYear) {
            entries.add(Entry(entry.key.toFloat(), entry.value))
        }

        val ds1 = LineDataSet(entries, getString(R.string.returns_by_year))
        ds1.color = MATERIAL_COLORS_500[0]
        ds1.setCircleColor(MATERIAL_COLORS_500[0])
        ds1.lineWidth = 2.5f
        ds1.valueTextSize = 7f
        ds1.circleRadius = 3f
        ds1.valueTextColor = color

        val sets = listOf<ILineDataSet>(ds1)
        val lineData = LineData(sets)
        lineData.setValueFormatter(CurrencyValueFormatter())
        lineData.setValueTextSize(7f)

        binding.lcGainsByYear.data = lineData

        val xAxis = binding.lcGainsByYear.xAxis
        if (entries.size > 0) {
            xAxis.axisMinimum = entries[0].x
        }
        xAxis.setLabelCount(entries.size, true)

        lineData.notifyDataChanged()
        binding.lcGainsByYear.notifyDataSetChanged()
        binding.lcGainsByYear.invalidate()
    }

    private fun createChartCurrentMonthGains() {
        binding.pcCurrentMonthByName.setUsePercentValues(true)
        binding.pcCurrentMonthByName.description.isEnabled = false
        binding.pcCurrentMonthByName.dragDecelerationFrictionCoef = 0.95f
        binding.pcCurrentMonthByName.isDrawHoleEnabled = true
        binding.pcCurrentMonthByName.setTransparentCircleAlpha(0)
        binding.pcCurrentMonthByName.setHoleColor(Color.TRANSPARENT)
        binding.pcCurrentMonthByName.holeRadius = 50f
        binding.pcCurrentMonthByName.transparentCircleRadius = 61f
        binding.pcCurrentMonthByName.setCenterTextSize(10f)
        binding.pcCurrentMonthByName.setDrawCenterText(true)
        binding.pcCurrentMonthByName.legend.isEnabled = true
        binding.pcCurrentMonthByName.legend.textColor = color
        binding.pcCurrentMonthByName.legend.isWordWrapEnabled = true
        binding.pcCurrentMonthByName.legend.textSize = 7f
        binding.pcCurrentMonthByName.rotationAngle = 0f
        binding.pcCurrentMonthByName.isRotationEnabled = true
        binding.pcCurrentMonthByName.setExtraOffsets(0f, 0f, 0f, -5f)
        binding.pcCurrentMonthByName.isHighlightPerTapEnabled = true

        val month =
            MonthEnum.valueOfByCalendarValue(Calendar.getInstance().get(Calendar.MONTH)).displayName

        binding.pcCurrentMonthByName.centerText =
            String.format(getString(R.string.current_month_returns_distribution), month)
        binding.pcCurrentMonthByName.setCenterTextSize(10f)
        binding.pcCurrentMonthByName.setCenterTextColor(
            Utils.themeAttributeToColorRes(
                R.attr.colorPrimary,
                requireContext()
            )
        )
        binding.pcCurrentMonthByName.setCenterTextTypeface(Typeface.DEFAULT_BOLD)
        binding.pcCurrentMonthByName.setOnChartValueSelectedListener(object :
                OnChartValueSelectedListener {
                override fun onValueSelected(e: Entry, h: Highlight) {
                    val pieEntry = e as PieEntry
                    val text = pieEntry.label + ":    " + numberFormat.format(pieEntry.value.toDouble())
                    val mySnackbar =
                        Snackbar.make(binding.pcCurrentMonthByName, text, Snackbar.LENGTH_LONG)
                    mySnackbar.show()
                }

                override fun onNothingSelected() = Unit
            })
    }

    private fun updateChartCurrentMonthGains(currentMonthGains: MutableMap<String, BigDecimal>) {
        val entries = ArrayList<PieEntry>()

        for (entry in currentMonthGains.entries) {
            entries.add(
                PieEntry(
                    entry.value.setScale(2, RoundingMode.HALF_UP).toFloat(),
                    entry.key
                )
            )
        }

        val dataSet = PieDataSet(entries, null)

        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        dataSet.valueTextSize = 7f
        dataSet.colors = MATERIAL_COLORS_500
        dataSet.valueTextColor = color

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(binding.pcCurrentMonthByName))
        data.setValueTextSize(7f)
        data.setValueTextColor(color)

        binding.pcCurrentMonthByName.data = data
        binding.pcCurrentMonthByName.setDrawEntryLabels(false)
        binding.pcCurrentMonthByName.highlightValues(null)
        binding.pcCurrentMonthByName.animateY(1400, Easing.EaseInOutQuad)
        binding.pcCurrentMonthByName.setEntryLabelColor(color)
        binding.pcCurrentMonthByName.setEntryLabelTextSize(7f)
        binding.pcCurrentMonthByName.invalidate()
    }

    private fun createChartCurrentYearGains() {
        binding.pcCurrentYearByName.setUsePercentValues(true)
        binding.pcCurrentYearByName.description.isEnabled = false
        binding.pcCurrentYearByName.dragDecelerationFrictionCoef = 0.95f
        binding.pcCurrentYearByName.isDrawHoleEnabled = true
        binding.pcCurrentYearByName.setTransparentCircleAlpha(0)
        binding.pcCurrentYearByName.setHoleColor(Color.TRANSPARENT)
        binding.pcCurrentYearByName.holeRadius = 50f
        binding.pcCurrentYearByName.transparentCircleRadius = 61f
        binding.pcCurrentYearByName.setCenterTextSize(10f)
        binding.pcCurrentYearByName.setDrawCenterText(true)
        binding.pcCurrentYearByName.legend.isEnabled = true
        binding.pcCurrentYearByName.legend.textColor = color
        binding.pcCurrentYearByName.legend.textSize = 7f
        binding.pcCurrentYearByName.legend.isWordWrapEnabled = true
        binding.pcCurrentYearByName.rotationAngle = 0f
        binding.pcCurrentYearByName.isRotationEnabled = true
        binding.pcCurrentYearByName.setExtraOffsets(0f, 0f, 0f, -5f)
        binding.pcCurrentYearByName.isHighlightPerTapEnabled = true

        val year = Calendar.getInstance().get(Calendar.YEAR)
        binding.pcCurrentYearByName.centerText =
            String.format(getString(R.string.current_year_returns_distribution), year)
        binding.pcCurrentYearByName.setCenterTextSize(10f)
        binding.pcCurrentYearByName.setCenterTextColor(
            Utils.themeAttributeToColorRes(
                R.attr.colorPrimary,
                requireContext()
            )
        )
        binding.pcCurrentYearByName.setCenterTextTypeface(Typeface.DEFAULT_BOLD)
        binding.pcCurrentYearByName.setOnChartValueSelectedListener(object :
                OnChartValueSelectedListener {
                override fun onValueSelected(e: Entry, h: Highlight) {
                    val pieEntry = e as PieEntry
                    val text = pieEntry.label + ":    " + numberFormat.format(pieEntry.value.toDouble())
                    val mySnackbar =
                        Snackbar.make(binding.pcCurrentYearByName, text, Snackbar.LENGTH_LONG)
                    mySnackbar.show()
                }

                override fun onNothingSelected() = Unit
            })
    }

    private fun updateChartCurrentYearGains(currentYearGains: MutableMap<String, BigDecimal>) {
        val entries = ArrayList<PieEntry>()

        for (entry in currentYearGains.entries) {
            entries.add(
                PieEntry(
                    entry.value.setScale(2, RoundingMode.HALF_UP).toFloat(),
                    entry.key
                )
            )
        }

        val dataSet = PieDataSet(entries, null)

        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        dataSet.valueTextSize = 7f
        dataSet.colors = MATERIAL_COLORS_500
        dataSet.valueTextColor = color

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(binding.pcCurrentYearByName))
        data.setValueTextSize(7f)
        data.setValueTextColor(color)

        binding.pcCurrentYearByName.data = data
        binding.pcCurrentYearByName.setDrawEntryLabels(false)
        binding.pcCurrentYearByName.highlightValues(null)
        binding.pcCurrentYearByName.animateY(1400, Easing.EaseInOutQuad)
        binding.pcCurrentYearByName.setEntryLabelColor(color)
        binding.pcCurrentYearByName.setEntryLabelTextSize(7f)
        binding.pcCurrentYearByName.invalidate()
    }

    private fun createChartGainsByType() {
        binding.hcGainsByType.setTouchEnabled(false)
        binding.hcGainsByType.isDoubleTapToZoomEnabled = false
        binding.hcGainsByType.axisLeft.isEnabled = false
        binding.hcGainsByType.axisLeft.setDrawGridLines(false)
        binding.hcGainsByType.axisLeft.setDrawLabels(false)
        binding.hcGainsByType.axisRight.setDrawGridLines(false)
        binding.hcGainsByType.axisRight.setDrawAxisLine(false)
        binding.hcGainsByType.axisRight.textColor = color
        binding.hcGainsByType.extraLeftOffset = -10f

        val description = Description()
        description.text = getString(R.string.gains_by_type)
        description.xOffset = -10f
        description.textColor = color
        description.textSize = 7f

        binding.hcGainsByType.description = description
        binding.hcGainsByType.legend.isEnabled = false
    }

    private fun updateChartGainsByType(gainsByType: MutableMap<PlatformTypeEnum, BigDecimal>) {
        var max = 0.0f
        val values = arrayOfNulls<PlatformTypeEnum>(gainsByType.size)
        val yVals = ArrayList<BarEntry>()
        var hasNegative = false

        var i = 1
        for ((key, value1) in gainsByType.entries) {
            val value = value1.toFloat()
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = key
            if (value < 0f) {
                hasNegative = true
            }
            i++
            if (value > max) {
                max = value
            }
        }
        max *= 1.1f

        val set1 = BarDataSet(yVals, getString(R.string.gains_by_type))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = CurrencyValueFormatter()
        set1.valueTextSize = 7f
        set1.valueTextColor = color
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)

        if (!hasNegative) {
            binding.hcGainsByType.axisLeft.axisMinimum = 0f
            binding.hcGainsByType.axisRight.axisMinimum = 0f
            binding.hcGainsByType.axisLeft.axisMaximum = max
            binding.hcGainsByType.axisRight.axisMaximum = max
        }

        val xl = binding.hcGainsByType.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.valueFormatter = PlatformValueFormatter(values)
        xl.granularity = 1f
        xl.textSize = 7f
        xl.labelCount = gainsByType.size
        xl.setDrawAxisLine(false)
        xl.setDrawGridLines(false)
        xl.textColor = color
        binding.hcGainsByType.data = data

        binding.hcGainsByType.axisLeft.textSize = 7f
        binding.hcGainsByType.axisRight.textSize = 7f

        binding.hcGainsByType.animateY(2500)
        binding.hcGainsByType.invalidate()
    }

    private fun createChartGainsByPlatform() {
        binding.hcGainsByPlatform.setTouchEnabled(false)
        binding.hcGainsByPlatform.setPinchZoom(false)
        binding.hcGainsByPlatform.isDoubleTapToZoomEnabled = false
        binding.hcGainsByPlatform.extraLeftOffset = -10f
        binding.hcGainsByPlatform.extraTopOffset = -10f
        binding.hcGainsByPlatform.axisLeft.isEnabled = false
        binding.hcGainsByPlatform.axisLeft.setDrawGridLines(false)
        binding.hcGainsByPlatform.axisLeft.setDrawLabels(false)

        binding.hcGainsByPlatform.axisRight.setDrawGridLines(false)
        binding.hcGainsByPlatform.axisRight.setDrawAxisLine(false)
        binding.hcGainsByPlatform.axisLeft.textSize = 7f
        binding.hcGainsByPlatform.axisRight.textSize = 7f
        binding.hcGainsByPlatform.axisRight.textColor = color

        val description = Description()
        description.text = getString(R.string.gains_by_platform)
        description.xOffset = -10f
        description.textColor = color
        description.textSize = 7f

        binding.hcGainsByPlatform.description = description
        binding.hcGainsByPlatform.legend.isEnabled = false
    }

    private fun updateChartGainsByPlatform(gainsByPlatform: MutableMap<String, BigDecimal>) {
        val values = arrayOfNulls<String>(gainsByPlatform.size)
        val yVals = ArrayList<BarEntry>()
        var hasNegative = false

        var i = 1
        for ((key, value1) in gainsByPlatform.entries) {
            val value = value1.toFloat()
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = key
            if (value < 0f) {
                hasNegative = true
            }
            i++
        }

        val xl = binding.hcGainsByPlatform.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.valueFormatter = PlatformValueFormatter(values)
        xl.granularity = 1f
        xl.setDrawAxisLine(false)
        xl.setDrawGridLines(false)
        xl.textColor = color
        xl.labelCount = values.size
        xl.textSize = 7f

        val set1 = BarDataSet(yVals, getString(R.string.gains_by_platform))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = CurrencyValueFormatter()
        set1.valueTextSize = 7f
        set1.valueTextColor = color

        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)
        data.setValueTextSize(7f)

        if (!hasNegative) {
            binding.hcGainsByPlatform.axisLeft.axisMinimum = 0f
            binding.hcGainsByPlatform.axisRight.axisMinimum = 0f
        }

        binding.hcGainsByPlatform.data = data
        binding.hcGainsByPlatform.animateY(2500)
        binding.hcGainsByPlatform.invalidate()
    }

    private fun createChartXirrByName() {
        if (Build.VERSION.SDK_INT < O) {
            binding.hcXirrByName.visibility = View.GONE
            binding.divider7.visibility = View.GONE
            return
        }

        binding.hcXirrByName.setDrawValueAboveBar(false)
        binding.hcXirrByName.description.isEnabled = false
        binding.hcXirrByName.setTouchEnabled(false)
        binding.hcXirrByName.setPinchZoom(false)
        binding.hcXirrByName.isDoubleTapToZoomEnabled = false
        binding.hcXirrByName.extraLeftOffset = -5f
        binding.hcXirrByName.extraTopOffset = -10f
        binding.hcXirrByName.setDrawGridBackground(false)
        binding.hcXirrByName.axisRight.setDrawGridLines(false)
        binding.hcXirrByName.axisRight.isEnabled = false
        binding.hcXirrByName.axisRight.setDrawAxisLine(false)

        val leftAxis = binding.hcXirrByName.axisLeft
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = PercentFormatter()
        leftAxis.spaceTop = 15f
        leftAxis.textSize = 7f
        leftAxis.setDrawGridLines(false)
        leftAxis.textColor = color
        leftAxis.axisMinimum = 0f

        val l = binding.hcXirrByName.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.form = Legend.LegendForm.NONE
        l.formSize = 9f
        l.textSize = 7f
        l.xEntrySpace = 4f
        l.textColor = color
    }

    private fun updateChartXirrByName(gainsByPlatform: MutableMap<String, Float>) {
        if (Build.VERSION.SDK_INT < O) {
            return
        }

        val values = arrayOfNulls<String>(gainsByPlatform.size)
        val yVals = ArrayList<BarEntry>()

        var i = 1
        for (entry in gainsByPlatform.entries) {
            val value = entry.value
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = entry.key
            i++
        }

        val set1 = BarDataSet(yVals, getString(R.string.xirr_comparison))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = PercentageValueFormatter()
        set1.valueTextSize = 7f
        set1.valueTextColor = color
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)

        val xl = binding.hcXirrByName.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.granularity = 1f
        xl.textSize = 7f
        xl.textColor = color
        xl.labelCount = values.size
        xl.labelRotationAngle = -45f
        xl.setDrawGridLines(false)
        xl.setDrawAxisLine(false)
        xl.valueFormatter = PlatformValueFormatter(values)
        binding.hcXirrByName.data = data

        binding.hcXirrByName.animateY(2500)
        binding.hcXirrByName.invalidate()
    }

    private fun createChartRoiByName() {
        binding.hcRoiByName.setDrawBarShadow(false)
        binding.hcRoiByName.setDrawValueAboveBar(false)
        binding.hcRoiByName.description.isEnabled = false
        binding.hcRoiByName.setTouchEnabled(false)
        binding.hcRoiByName.setPinchZoom(false)
        binding.hcRoiByName.isDoubleTapToZoomEnabled = false
        binding.hcRoiByName.extraLeftOffset = -5f
        binding.hcRoiByName.extraTopOffset = -10f
        binding.hcRoiByName.setDrawGridBackground(false)
        binding.hcRoiByName.axisRight.setDrawGridLines(false)
        binding.hcRoiByName.axisRight.isEnabled = false
        binding.hcRoiByName.axisRight.setDrawAxisLine(false)

        val leftAxis = binding.hcRoiByName.axisLeft
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = PercentFormatter()
        leftAxis.textSize = 7f
        leftAxis.textColor = color
        leftAxis.spaceTop = 15f
        leftAxis.setDrawGridLines(false)
        leftAxis.axisMinimum = 0f

        val l = binding.hcRoiByName.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.form = Legend.LegendForm.NONE
        l.formSize = 9f
        l.textSize = 7f
        l.xEntrySpace = 4f
        l.textColor = color
    }

    private fun updateChartRoiByName(roiByPlatform: MutableMap<String, Float>) {
        val values = arrayOfNulls<String>(roiByPlatform.size)
        val yVals = ArrayList<BarEntry>()

        var i = 1
        for (entry in roiByPlatform.entries) {
            val value = entry.value
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = entry.key
            i++
        }

        val set1 = BarDataSet(yVals, getString(R.string.annualized_roi))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = PercentageValueFormatter()
        set1.valueTextColor = color
        set1.valueTextSize = 7f
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)

        val xl = binding.hcRoiByName.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.granularity = 1f
        xl.textSize = 7f
        xl.textColor = color
        xl.labelCount = values.size
        xl.labelRotationAngle = -45f
        xl.setDrawGridLines(false)
        xl.setDrawAxisLine(false)
        xl.valueFormatter = PlatformValueFormatter(values)
        binding.hcRoiByName.data = data

        binding.hcRoiByName.animateY(2500)
        binding.hcRoiByName.invalidate()
    }
}
