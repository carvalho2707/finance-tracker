package pt.tiagocarvalho.financetracker.ui.alerts

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.recyclerview.widget.DiffUtil
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.ItemAlertBinding
import pt.tiagocarvalho.financetracker.model.AlertItem
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter

class AlertsAdapter(
    private val dataBindingComponent: DataBindingComponent,
    appExecutors: AppExecutors,
    val context: Context,
    private val navController: NavController
) : DataBoundListAdapter<AlertItem, ItemAlertBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<AlertItem>() {
        override fun areItemsTheSame(
            oldItem: AlertItem,
            newItem: AlertItem
        ) = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: AlertItem,
            newItem: AlertItem
        ) = oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemAlertBinding = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.item_alert,
        parent,
        false,
        dataBindingComponent
    )

    override fun bind(binding: ItemAlertBinding, item: AlertItem) {
        binding.item = item
        binding.btnBrowse.setOnClickListener {
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(item.url)
            context.startActivity(i)
        }
        binding.btnDetails.setOnClickListener {
            navController.navigate(AlertsFragmentDirections.showP2P(item.name))
        }
    }
}
