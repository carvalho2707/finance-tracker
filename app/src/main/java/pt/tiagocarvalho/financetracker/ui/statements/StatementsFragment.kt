package pt.tiagocarvalho.financetracker.ui.statements

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.databinding.DataBindingComponent
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.databinding.FragmentDataBindingComponent
import pt.tiagocarvalho.financetracker.databinding.FragmentStatementsBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.INVALID_USERNAME_PASSWORD_CODE
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.PLATFORM
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.SUCCESS
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.TOKEN
import pt.tiagocarvalho.financetracker.ui.auth.AuthDialogUtils.UNKNOWN_ERROR_CODE
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.webview.BaseWebViewClient
import pt.tiagocarvalho.financetracker.webview.BaseWebViewNavigator
import pt.tiagocarvalho.p2p.api.model.Login

@AndroidEntryPoint
class StatementsFragment : BaseFragment(), BaseWebViewNavigator {

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentStatementsBinding

    private val statementsViewModel: StatementsViewModel by viewModels()
    private var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentStatementsBinding.inflate(inflater, container, false)
        binding.apply {
            lifecycleOwner = viewLifecycleOwner
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = StatementsAdapter(
            dataBindingComponent,
            appExecutors,
            requireContext(),
            findNavController()
        )

        registerToolbarWithNavigation(binding.toolbar)

        binding.rvStatements.adapter = adapter

        statementsViewModel.status.observe(viewLifecycleOwner) {
            when (it) {
                Status.LOADING -> {
                    binding.layoutGenericError.visibility = View.GONE
                    binding.layoutNoInternet.visibility = View.GONE
                    binding.noStatementsMessage.visibility = View.GONE
                    binding.rvStatements.visibility = View.GONE
                    binding.layoutLoading.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.layoutGenericError.visibility = View.GONE
                    binding.layoutNoInternet.visibility = View.GONE
                    binding.noStatementsMessage.visibility = View.GONE
                    binding.layoutLoading.visibility = View.GONE
                    binding.rvStatements.visibility = View.VISIBLE
                }
                else -> {
                    binding.layoutGenericError.visibility = View.GONE
                    binding.layoutNoInternet.visibility = View.GONE
                    binding.noStatementsMessage.visibility = View.GONE
                    binding.layoutLoading.visibility = View.GONE
                    binding.rvStatements.visibility = View.GONE
                }
            }
        }

        statementsViewModel.errorType.observe(viewLifecycleOwner) {
            if (it == 0) {
                binding.layoutGenericError.visibility = View.GONE
                binding.layoutNoInternet.visibility = View.VISIBLE
            } else {
                binding.layoutNoInternet.visibility = View.GONE
                binding.layoutGenericError.visibility = View.VISIBLE
            }
        }

        statementsViewModel.statements.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                binding.noStatementsMessage.visibility = View.VISIBLE
                binding.layoutLoading.visibility = View.GONE
                binding.rvStatements.visibility = View.GONE
            } else {
                scheduleAnimation(adapter)
                adapter.submitList(it)
            }
        }

        statementsViewModel.tfaRequired.observe(viewLifecycleOwner) {
            if (AuthDialogUtils.belongsToAuthDialog(it.type)) {
                loadAuthDialog(it)
            } else {
                loadWebView(it)
            }
        }

        statementsViewModel.loadRefreshNeeded()
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadWebView(login: Platform) {
        binding.layoutLoading.visibility = View.INVISIBLE
        binding.webView.visibility = View.VISIBLE
        val client = BaseWebViewClient.getClient(
            requireContext(),
            layoutInflater,
            login.type,
            Login(login.username!!, login.password!!),
            this,
            isLogin = false,
            frequentUpdates = true
        )
        binding.webView.webViewClient = client

        val webSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.userAgentString = Constants.USER_AGENT_VALUE

        binding.webView.loadUrl(client.getInitialUrl())
    }

    private fun loadAuthDialog(platform: Platform) {
        AuthDialogUtils.showAuthDialogFragment(
            platform.type,
            platform.username!!,
            platform.password!!,
            this@StatementsFragment
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val platform = PlatformEnum.valueOf(data?.getStringExtra(PLATFORM)!!)
        if (resultCode == SUCCESS) {
            statementsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)))
        } else if (resultCode == INVALID_USERNAME_PASSWORD_CODE ||
            resultCode == UNKNOWN_ERROR_CODE
        ) {
            binding.layoutLoading.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
            statementsViewModel.loadStatements()
        } else if (resultCode == AuthDialogUtils.CANCEL) {
            statementsViewModel.loadNextAuth(WebViewData(platform, data.getStringExtra(TOKEN)))
        }
    }

    override fun loadInfo(webViewData: WebViewData?) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            binding.layoutLoading.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
        }
        statementsViewModel.loadNextAuth(webViewData)
    }

    override fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String?
    ) = Unit

    override fun onError(platformEnum: PlatformEnum, errorCode: Int) {
        if (errorCode == 0) {
            binding.layoutLoading.visibility = View.VISIBLE
            binding.webView.visibility = View.GONE
            statementsViewModel.loadStatements()
        }
    }

    private fun scheduleAnimation(adapter: StatementsAdapter) {
        val controller =
            AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
        binding.rvStatements.layoutAnimation = controller
        adapter.notifyDataSetChanged()
        binding.rvStatements.scheduleLayoutAnimation()
    }
}
