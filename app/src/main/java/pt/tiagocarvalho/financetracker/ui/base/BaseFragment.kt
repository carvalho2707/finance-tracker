package pt.tiagocarvalho.financetracker.ui.base

import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import pt.tiagocarvalho.financetracker.ui.main.MainActivity

abstract class BaseFragment : Fragment() {

    fun registerToolbarWithNavigation(toolbar: Toolbar) {
        requireActivity().let {
            (it as MainActivity).registerToolbarWithNavigation(toolbar)
        }

        toolbar.setOnMenuItemClickListener { item -> onToolbarItemClicked(item) }
    }

    abstract fun onToolbarItemClicked(menuItem: MenuItem): Boolean
}
