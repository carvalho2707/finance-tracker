package pt.tiagocarvalho.financetracker.ui.accounts.filter

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableFloat
import androidx.databinding.ObservableInt
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.Observable
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.repository.PreferencesRepository
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.addOnPropertyChanged

@HiltViewModel
class AccountsFilterViewModel @Inject constructor(
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel() {

    var headerAlpha = ObservableFloat(1f)
    var recyclerviewAlpha = ObservableFloat(1f)

    var withBalance: ObservableBoolean = ObservableBoolean(false)
    var withVariation: ObservableBoolean = ObservableBoolean(false)
    var withPositiveVariation: ObservableBoolean = ObservableBoolean(false)
    var p2p: ObservableBoolean = ObservableBoolean(true)
    var savings: ObservableBoolean = ObservableBoolean(true)
    var gambling: ObservableBoolean = ObservableBoolean(true)
    var cash: ObservableBoolean = ObservableBoolean(true)
    var sortOrderId: ObservableInt = ObservableInt()

    init {
        addOnPropertyChangedCallback()
    }

    private fun addOnPropertyChangedCallback() {
        withBalance.addOnPropertyChanged { saveFilters() }
        withVariation.addOnPropertyChanged { saveFilters() }
        withPositiveVariation.addOnPropertyChanged { saveFilters() }
        p2p.addOnPropertyChanged { saveFilters() }
        savings.addOnPropertyChanged { saveFilters() }
        savings.addOnPropertyChanged { saveFilters() }
        gambling.addOnPropertyChanged { saveFilters() }
        cash.addOnPropertyChanged { saveFilters() }
        sortOrderId.addOnPropertyChanged { saveFilters() }
    }

    fun loadPreferences() {
        compositeDisposable.add(
            Observable.fromCallable { preferencesRepository.getFilterPreference() }
                .doOnNext {
                    withBalance.set(it.withBalance)
                    withVariation.set(it.withVariation)
                    withPositiveVariation.set(it.withPositiveVariation)
                    p2p.set(it.filterArray.contains(PlatformTypeEnum.P2P.name))
                    savings.set(it.filterArray.contains(PlatformTypeEnum.SAVINGS.name))
                    gambling.set(it.filterArray.contains(PlatformTypeEnum.GAMBLING.name))
                    cash.set(it.filterArray.contains(PlatformTypeEnum.CASH.name))
                    sortOrderId.set(it.sortOrderId)
                }
                .subscribe()
        )
    }

    fun restorePreferences() {
        preferencesRepository.restorePreferences()
        loadPreferences()
    }

    private fun saveFilters() {
        compositeDisposable.add(
            preferencesRepository.saveFilters(
                Filter(
                    withBalance = withBalance.get(),
                    withVariation = withVariation.get(),
                    withPositiveVariation = withPositiveVariation.get(),
                    filterArray = getFilterArray(),
                    sortOrderId = sortOrderId.get()
                )
            )
                .subscribe()
        )
    }

    private fun getFilterArray(): MutableSet<String> = emptySet<String>().toMutableSet().apply {
        if (p2p.get()) {
            this.add(PlatformTypeEnum.P2P.name)
        }

        if (savings.get()) {
            this.add(PlatformTypeEnum.SAVINGS.name)
        }

        if (gambling.get()) {
            this.add(PlatformTypeEnum.GAMBLING.name)
        }

        if (cash.get()) {
            this.add(PlatformTypeEnum.CASH.name)
        }
    }

    fun updateSortOrderId(checkedId: Int) {
        when (checkedId) {
            R.id.chip0 -> sortOrderId.set(0)
            R.id.chip1 -> sortOrderId.set(1)
            R.id.chip2 -> sortOrderId.set(2)
            R.id.chip3 -> sortOrderId.set(3)
            R.id.chip4 -> sortOrderId.set(4)
            R.id.chip5 -> sortOrderId.set(5)
            R.id.chip6 -> sortOrderId.set(6)
            R.id.chip7 -> sortOrderId.set(7)
        }
    }
}

data class Filter(
    val withBalance: Boolean,
    val withVariation: Boolean,
    val withPositiveVariation: Boolean,
    val sortOrderId: Int,
    val filterArray: MutableSet<String>
)
