package pt.tiagocarvalho.financetracker.ui.billing

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.billing.localdb.AugmentedSkuDetails
import pt.tiagocarvalho.financetracker.databinding.ItemSkuDetailsBinding
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.adapters.DataBoundListAdapter

class SkuDetailsAdapter(
    appExecutors: AppExecutors,
    val context: Context,
    private val clickCallback: ((AugmentedSkuDetails) -> Unit)?
) : DataBoundListAdapter<AugmentedSkuDetails, ItemSkuDetailsBinding>(
    appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<AugmentedSkuDetails>() {
        override fun areItemsTheSame(
            oldItem: AugmentedSkuDetails,
            newItem: AugmentedSkuDetails
        ) = oldItem.sku == newItem.sku

        override fun areContentsTheSame(
            oldItem: AugmentedSkuDetails,
            newItem: AugmentedSkuDetails
        ) = oldItem == newItem
    }
) {
    override fun createBinding(parent: ViewGroup): ItemSkuDetailsBinding = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context),
        R.layout.item_sku_details,
        parent,
        false
    )

    override fun bind(binding: ItemSkuDetailsBinding, item: AugmentedSkuDetails) {
        binding.root.setOnClickListener { clickCallback?.invoke(item) }
        binding.item = item
    }
}
