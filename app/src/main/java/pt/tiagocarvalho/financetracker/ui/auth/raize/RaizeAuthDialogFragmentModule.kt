package pt.tiagocarvalho.financetracker.ui.auth.raize

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider

@InstallIn(FragmentComponent::class)
@Module
class RaizeAuthDialogFragmentModule {

    @Provides
    fun provideViewModel(
        raizeAuthUseCase: RaizeAuthUseCase,
        schedulerProvider: SchedulerProvider
    ) = RaizeAuthViewModel(
        raizeAuthUseCase,
        schedulerProvider
    )
}
