package pt.tiagocarvalho.financetracker.ui.accounts.last_change

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.model.DisplayMode
import pt.tiagocarvalho.financetracker.utils.isDayMode

@BindingAdapter(value = ["lastChange", "displayMode"])
fun showHide(view: TextView, value: BigDecimal, displayMode: DisplayMode) {
    val color = when {
        value > BigDecimal.ZERO -> R.color.inwallet_green_500
        value.compareTo(BigDecimal.ZERO) == 0 -> if (view.context.isDayMode()) R.color.inwallet_grey_500 else R.color.inwallet_grey_200
        else -> R.color.inwallet_red_500
    }

    if (displayMode == DisplayMode.ABSOLUTE) {
        val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
        numberFormat.currency = Currency.getInstance("EUR")
        view.text = numberFormat.format(value.toDouble())
    } else {
        val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault())
        view.text = numberFormat.format(value.toDouble()) + " %"
    }

    view.setTextColor(view.resources.getColor(color))
}
