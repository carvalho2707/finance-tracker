package pt.tiagocarvalho.financetracker.ui.accounts.last_change

import androidx.annotation.DrawableRes
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.DisplayMode

data class LastChangeInfo(
    val items: List<LastChangeItem>,
    val dailyChange: BigDecimal,
    val lastSession: BigDecimal,
    val displayMode: DisplayMode
) {
    constructor() : this(
        emptyList(),
        BigDecimal.ZERO,
        BigDecimal.ZERO,
        DisplayMode.ABSOLUTE
    )
}

data class LastChangeItem(
    @DrawableRes val logo: Int,
    val name: String,
    val showName: Boolean,
    val value: String,
    val valueBigDecimal: BigDecimal,
    val comparator: Int
)
