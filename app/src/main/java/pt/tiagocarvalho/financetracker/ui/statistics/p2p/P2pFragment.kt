package pt.tiagocarvalho.financetracker.ui.statistics.p2p

import android.graphics.Color
import android.graphics.Typeface.DEFAULT_BOLD
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.MPPointF
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.NumberFormat
import java.util.ArrayList
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentP2pBinding
import pt.tiagocarvalho.financetracker.model.P2pStatistics
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.utils.Constants.MATERIAL_COLORS_500
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.charts.CurrencyValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.IntValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.PercentageValueFormatter
import pt.tiagocarvalho.financetracker.utils.charts.PlatformValueFormatter

@AndroidEntryPoint
class P2pFragment : BaseFragment() {

    companion object {
        fun newInstance(): P2pFragment = P2pFragment()
    }

    private lateinit var binding: FragmentP2pBinding

    private val p2pViewModel: P2pViewModel by viewModels()
    private var color = 0
    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = FragmentP2pBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = p2pViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        p2pViewModel.status.observe(viewLifecycleOwner) { status ->
            if (status == Status.LOADING) {
                binding.contentScroll.visibility = View.GONE
                binding.noAccountsMessage.visibility = View.GONE
                binding.animationView.setAnimation(R.raw.loading)
                binding.animationView.repeatCount = -1
                binding.animationView.playAnimation()
            } else binding.animationView.repeatCount = 0
        }

        setupUI()

        return binding.root
    }

    private fun setupUI() {
        color = Utils.themeAttributeToColorInt(android.R.attr.textColorPrimary, requireContext())
        createChartByName()
        createBarChartInterestComparison()
        createBarChartStatements()
        createChartCumulativeInvestments()
        createChartPrincipalReceived()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        p2pViewModel.statistics.observe(viewLifecycleOwner) {
            it?.let {
                if (it.show) {
                    binding.noAccountsMessage.visibility = View.GONE
                    binding.contentScroll.visibility = View.VISIBLE
                    updateUI(it)
                } else {
                    binding.contentScroll.visibility = View.GONE
                    binding.noAccountsMessage.visibility = View.VISIBLE
                }
            }
        }

        p2pViewModel.loadP2pStatistics()
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_refresh -> p2pViewModel.loadP2pStatistics()
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun updateUI(statistics: P2pStatistics) {
        updateChartByName(statistics.byName)
        updateBarChartInterestComparison(statistics.interestComparison)
        updateBarChartStatement(statistics.statementsByName)
        updateChartCumulativeInvestments(statistics.cumulativeInvestments)
        updateChartPrincipalReceived(statistics.cumulativePrincipal)
    }

    private fun createChartByName() {
        binding.pcByP2p.setUsePercentValues(true)
        binding.pcByP2p.description.isEnabled = false
        binding.pcByP2p.dragDecelerationFrictionCoef = 0.95f
        binding.pcByP2p.isDrawHoleEnabled = true
        binding.pcByP2p.setTransparentCircleAlpha(0)
        binding.pcByP2p.setHoleColor(Color.TRANSPARENT)
        binding.pcByP2p.holeRadius = 50f
        binding.pcByP2p.transparentCircleRadius = 61f
        binding.pcByP2p.setDrawCenterText(true)
        binding.pcByP2p.legend.isEnabled = true
        binding.pcByP2p.legend.isWordWrapEnabled = true
        binding.pcByP2p.legend.textColor = color
        binding.pcByP2p.legend.textSize = 7f
        binding.pcByP2p.rotationAngle = 0f
        binding.pcByP2p.isRotationEnabled = true
        binding.pcByP2p.setExtraOffsets(0f, 0f, 0f, -5f)
        binding.pcByP2p.isHighlightPerTapEnabled = true
        binding.pcByP2p.centerText = getString(R.string.p2p_allocation)
        binding.pcByP2p.setCenterTextSize(10f)
        binding.pcByP2p.setCenterTextColor(
            Utils.themeAttributeToColorRes(
                R.attr.colorPrimary,
                requireContext()
            )
        )
        binding.pcByP2p.setCenterTextTypeface(DEFAULT_BOLD)
        binding.pcByP2p.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            override fun onValueSelected(e: Entry, h: Highlight) {
                val pieEntry = e as PieEntry
                val text = pieEntry.label + ":    " + numberFormat.format(pieEntry.value.toDouble())
                val mySnackbar = Snackbar.make(binding.pcByP2p, text, Snackbar.LENGTH_LONG)
                mySnackbar.show()
            }

            override fun onNothingSelected() = Unit
        })
    }

    private fun updateChartByName(byName: Map<PlatformEnum, BigDecimal>) {
        val entries = ArrayList<PieEntry>()

        for (entry in byName.entries) {
            entries.add(
                PieEntry(
                    entry.value.setScale(2, RoundingMode.HALF_UP).toFloat(),
                    entry.key.toString()
                )
            )
        }

        val dataSet = PieDataSet(entries, null)

        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        dataSet.valueTextSize = 7f
        dataSet.colors = MATERIAL_COLORS_500
        dataSet.valueTextColor = color

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(binding.pcByP2p))
        data.setValueTextSize(7f)
        data.setValueTextColor(color)

        binding.pcByP2p.data = data
        binding.pcByP2p.setDrawEntryLabels(false)
        binding.pcByP2p.highlightValues(null)
        binding.pcByP2p.animateY(1400, Easing.EaseInOutQuad)
        binding.pcByP2p.setEntryLabelColor(color)
        binding.pcByP2p.setEntryLabelTextSize(7f)
        binding.pcByP2p.invalidate()
    }

    private fun createBarChartInterestComparison() {
        binding.bcInterestComparison.setDrawBarShadow(false)
        binding.bcInterestComparison.setDrawValueAboveBar(false)
        binding.bcInterestComparison.description.isEnabled = false
        binding.bcInterestComparison.setTouchEnabled(false)
        binding.bcInterestComparison.setPinchZoom(false)
        binding.bcInterestComparison.isDoubleTapToZoomEnabled = false
        binding.bcInterestComparison.extraLeftOffset = -5f
        binding.bcInterestComparison.extraTopOffset = -10f
        binding.bcInterestComparison.setDrawGridBackground(false)
        binding.bcInterestComparison.axisRight.setDrawGridLines(false)
        binding.bcInterestComparison.axisRight.isEnabled = false
        binding.bcInterestComparison.axisRight.setDrawAxisLine(false)

        val leftAxis = binding.bcInterestComparison.axisLeft
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = PercentFormatter()
        leftAxis.spaceTop = 15f
        leftAxis.textSize = 7f
        leftAxis.setDrawGridLines(false)
        leftAxis.axisMinimum = 0f
        leftAxis.textColor = color

        val l = binding.bcInterestComparison.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.form = Legend.LegendForm.NONE
        l.formSize = 9f
        l.textSize = 7f
        l.xEntrySpace = 4f
        l.textColor = color
    }

    private fun createBarChartStatements() {
        binding.bcStatementsComparison.setDrawBarShadow(false)
        binding.bcStatementsComparison.setDrawValueAboveBar(false)
        binding.bcStatementsComparison.description.isEnabled = false
        binding.bcStatementsComparison.setTouchEnabled(false)
        binding.bcStatementsComparison.setPinchZoom(false)
        binding.bcStatementsComparison.isDoubleTapToZoomEnabled = false
        binding.bcStatementsComparison.extraLeftOffset = -5f
        binding.bcStatementsComparison.extraTopOffset = -10f
        binding.bcStatementsComparison.setDrawGridBackground(false)
        binding.bcStatementsComparison.axisRight.setDrawGridLines(false)
        binding.bcStatementsComparison.axisRight.isEnabled = false
        binding.bcStatementsComparison.axisRight.setDrawAxisLine(false)

        val leftAxis = binding.bcStatementsComparison.axisLeft
        leftAxis.setDrawAxisLine(false)
        leftAxis.valueFormatter = IntValueFormatter(hideZero = false)
        leftAxis.spaceTop = 15f
        leftAxis.textSize = 7f
        leftAxis.setDrawGridLines(false)
        leftAxis.axisMinimum = 0f
        leftAxis.textColor = color

        val l = binding.bcStatementsComparison.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.form = Legend.LegendForm.NONE
        l.formSize = 9f
        l.textSize = 7f
        l.xEntrySpace = 4f
        l.textColor = color
    }

    private fun updateBarChartInterestComparison(interestComparison: Map<PlatformEnum, BigDecimal>) {
        val values = arrayOfNulls<String>(interestComparison.size)
        val yVals = ArrayList<BarEntry>()

        var i = 1
        for (entry in interestComparison.entries) {
            val value = entry.value.toFloat()
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = entry.key.toString()
            i++
        }

        val set1 = BarDataSet(yVals, getString(R.string.interests_comparison))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = PercentageValueFormatter()
        set1.valueTextSize = 7f
        set1.valueTextColor = color
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)

        val xl = binding.bcInterestComparison.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.granularity = 1f
        xl.textSize = 7f
        xl.textColor = color
        xl.labelRotationAngle = -45f
        xl.labelCount = values.size
        xl.setDrawGridLines(false)
        xl.setDrawAxisLine(false)
        xl.valueFormatter = PlatformValueFormatter(values)
        binding.bcInterestComparison.data = data

        binding.bcInterestComparison.animateY(2500)
        binding.bcInterestComparison.invalidate()
    }

    private fun updateBarChartStatement(statementsByName: Map<PlatformEnum, Int>) {
        val values = arrayOfNulls<String>(statementsByName.size)
        val yVals = ArrayList<BarEntry>()

        var i = 1
        for (entry in statementsByName.entries) {
            val value = entry.value.toFloat()
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = entry.key.toString()
            i++
        }

        val set1 = BarDataSet(yVals, getString(R.string.statements_counter))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = IntValueFormatter(hideZero = true)
        set1.valueTextSize = 7f
        set1.valueTextColor = color

        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f

        val xl = binding.bcStatementsComparison.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.granularity = 1f
        xl.textSize = 7f
        xl.labelCount = values.size
        xl.labelRotationAngle = -45f
        xl.setDrawGridLines(false)
        xl.setDrawAxisLine(false)
        xl.valueFormatter = PlatformValueFormatter(values)
        xl.textColor = color
        binding.bcStatementsComparison.data = data

        binding.bcStatementsComparison.animateY(2500)
        binding.bcStatementsComparison.invalidate()
    }

    private fun createChartCumulativeInvestments() {
        binding.cumulativeInvestments.setTouchEnabled(false)
        binding.cumulativeInvestments.isDoubleTapToZoomEnabled = false
        binding.cumulativeInvestments.axisLeft.isEnabled = false
        binding.cumulativeInvestments.axisLeft.setDrawGridLines(false)
        binding.cumulativeInvestments.axisLeft.setDrawLabels(false)
        binding.cumulativeInvestments.axisRight.setDrawGridLines(false)
        binding.cumulativeInvestments.axisRight.setDrawAxisLine(false)
        binding.cumulativeInvestments.axisRight.textColor = color
        binding.cumulativeInvestments.extraLeftOffset = -10f

        val description = Description()
        description.text = getString(R.string.cumulative_investments)
        description.xOffset = -10f
        description.textColor = color
        description.textSize = 7f

        binding.cumulativeInvestments.description = description
        binding.cumulativeInvestments.legend.isEnabled = false
    }

    private fun updateChartCumulativeInvestments(investments: MutableMap<String, Double>) {
        var max = 0.0f
        val values = arrayOfNulls<String>(investments.size)
        val yVals = ArrayList<BarEntry>()
        var hasNegative = false

        var i = 1
        for ((key, value1) in investments.entries) {
            val value = value1.toFloat()
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = key
            if (value < 0f) {
                hasNegative = true
            }
            i++
            if (value > max) {
                max = value
            }
        }
        max *= 1.1f

        val set1 = BarDataSet(yVals, getString(R.string.cumulative_investments))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = CurrencyValueFormatter()
        set1.valueTextSize = 7f
        set1.valueTextColor = color
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)

        if (!hasNegative) {
            binding.cumulativeInvestments.axisLeft.axisMinimum = 0f
            binding.cumulativeInvestments.axisRight.axisMinimum = 0f
            binding.cumulativeInvestments.axisLeft.axisMaximum = max
            binding.cumulativeInvestments.axisRight.axisMaximum = max
        }

        val xl = binding.cumulativeInvestments.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.granularity = 1f
        xl.textSize = 7f
        xl.valueFormatter = PlatformValueFormatter(values)
        xl.labelCount = investments.size
        xl.setDrawAxisLine(false)
        xl.setDrawGridLines(false)
        xl.textColor = color
        binding.cumulativeInvestments.data = data

        binding.cumulativeInvestments.axisLeft.textSize = 7f
        binding.cumulativeInvestments.axisRight.textSize = 7f

        binding.cumulativeInvestments.animateY(2500)
        binding.cumulativeInvestments.invalidate()
    }

    private fun createChartPrincipalReceived() {
        binding.principalReceived.setTouchEnabled(false)
        binding.principalReceived.isDoubleTapToZoomEnabled = false
        binding.principalReceived.axisLeft.isEnabled = false
        binding.principalReceived.axisLeft.setDrawGridLines(false)
        binding.principalReceived.axisLeft.setDrawLabels(false)
        binding.principalReceived.axisRight.setDrawGridLines(false)
        binding.principalReceived.axisRight.setDrawAxisLine(false)
        binding.principalReceived.axisRight.textColor = color
        binding.principalReceived.extraLeftOffset = -10f

        val description = Description()
        description.text = getString(R.string.principal_received)
        description.xOffset = -10f
        description.textColor = color
        description.textSize = 7f

        binding.principalReceived.description = description
        binding.principalReceived.legend.isEnabled = false
    }

    private fun updateChartPrincipalReceived(principal: MutableMap<String, Double>) {
        var max = 0.0f
        val values = arrayOfNulls<String>(principal.size)
        val yVals = ArrayList<BarEntry>()
        var hasNegative = false

        var i = 1
        for ((key, value1) in principal.entries) {
            val value = value1.toFloat()
            yVals.add(BarEntry(i.toFloat(), value))
            values[i - 1] = key
            if (value < 0f) {
                hasNegative = true
            }
            i++
            if (value > max) {
                max = value
            }
        }
        max *= 1.1f

        val set1 = BarDataSet(yVals, getString(R.string.principal_received))
        set1.colors = MATERIAL_COLORS_500
        set1.valueFormatter = CurrencyValueFormatter()
        set1.valueTextSize = 7f
        set1.valueTextColor = color
        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(set1)

        val data = BarData(dataSets)
        data.barWidth = 0.8f
        data.setValueTextSize(7f)

        if (!hasNegative) {
            binding.principalReceived.axisLeft.axisMinimum = 0f
            binding.principalReceived.axisRight.axisMinimum = 0f
            binding.principalReceived.axisLeft.axisMaximum = max
            binding.principalReceived.axisRight.axisMaximum = max
        }

        val xl = binding.principalReceived.xAxis
        xl.position = XAxis.XAxisPosition.BOTTOM
        xl.labelCount = principal.size
        xl.valueFormatter = PlatformValueFormatter(values)
        xl.granularity = 1f
        xl.textSize = 7f
        xl.setDrawAxisLine(false)
        xl.setDrawGridLines(false)
        xl.textColor = color
        binding.principalReceived.data = data

        binding.principalReceived.axisLeft.textSize = 7f
        binding.principalReceived.axisRight.textSize = 7f

        binding.principalReceived.animateY(2500)
        binding.principalReceived.invalidate()
    }
}
