package pt.tiagocarvalho.financetracker.ui.management

import android.animation.Animator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.forEach
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionInflater
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.MainDirections
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.databinding.FragmentManagementBinding
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.ui.base.BaseFragment
import pt.tiagocarvalho.financetracker.ui.premium.PremiumOnlyDialogFragment
import pt.tiagocarvalho.financetracker.utils.AppExecutors
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.postponeEnterTransition
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_COLLAPSED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_EXPANDED
import pt.tiagocarvalho.financetracker.widget.BottomSheetBehavior.Companion.STATE_HIDDEN
import timber.log.Timber

@AndroidEntryPoint
class ManagementFragment : BaseFragment() {

    @Inject
    lateinit var schedulerProvider: SchedulerProvider

    @Inject
    lateinit var appExecutors: AppExecutors

    private lateinit var binding: FragmentManagementBinding
    private lateinit var managementAdapter: ManagementAdapter
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>

    private val managementViewModel: ManagementViewModel by viewModels()
    private var platform: PlatformEnum? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            onBackPressed()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = FragmentManagementBinding.inflate(inflater, container, false)
        binding.apply {
            viewModel = managementViewModel
            lifecycleOwner = viewLifecycleOwner
        }

        sharedElementReturnTransition =
            TransitionInflater.from(context).inflateTransition(R.transition.account_enter)
        // Delay the enter transition until speaker image has loaded.
        postponeEnterTransition(500L)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        managementAdapter = ManagementAdapter(appExecutors, managementViewModel)

        binding.rvManagement.addItemDecoration(
            DividerItemDecoration(
                context,
                LinearLayoutManager.VERTICAL
            )
        )
        binding.rvManagement.apply {
            adapter = managementAdapter
            (itemAnimator as DefaultItemAnimator).run {
                supportsChangeAnimations = false
                addDuration = 160L
                moveDuration = 160L
                changeDuration = 160L
                removeDuration = 120L
            }
        }

        registerToolbarWithNavigation(binding.toolbar)

        bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.filter_sheet))

        bottomSheetBehavior.isHideable = false
        bottomSheetBehavior.skipCollapsed = false
        if (bottomSheetBehavior.state == STATE_COLLAPSED) {
            bottomSheetBehavior.state = STATE_HIDDEN
        }

        managementViewModel.platforms.observe(viewLifecycleOwner) {
            showPlatformsDialog(it)
        }

        managementViewModel.accounts.observe(viewLifecycleOwner) {
            managementAdapter.submitList(it)
        }

        managementViewModel.status.observe(viewLifecycleOwner) {
            when (it.status) {
                Status.LOADING -> showLoading()
                Status.SUCCESS -> showSuccess()
                Status.ERROR -> showError()
            }
        }

        managementViewModel.login.observe(viewLifecycleOwner) {
            showLogin(it.first, it.second)
        }

        managementViewModel.popupPremium.observe(viewLifecycleOwner) {
            showPopUpPremium()
        }

        managementViewModel.popup.observe(viewLifecycleOwner) {
            showPlatformsDialog(PlatformEnum.valueOf(it))
        }
    }

    override fun onResume() {
        super.onResume()

        managementViewModel.load()
    }

    private fun findSpeakerHeadshot(speakers: ViewGroup, platformType: String): View {
        speakers.forEach {
            if (it.getTag(R.id.tag_platform_type) == platformType) {
                return it.findViewById(R.id.platform_logo_headshot)
            }
        }
        Timber.e("Could not find view for speaker id $platformType")
        return speakers
    }

    override fun onToolbarItemClicked(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.menu_settings -> findNavController().navigate(MainDirections.actionGlobalSettings())
            R.id.menu_info ->
                if (bottomSheetBehavior.state == STATE_EXPANDED) {
                    bottomSheetBehavior.state = STATE_COLLAPSED
                } else {
                    bottomSheetBehavior.state = STATE_EXPANDED
                }
            else -> return super.onOptionsItemSelected(menuItem)
        }
        return true
    }

    private fun showLogin(name: String, platformType: String) {
        val sharedElement = findSpeakerHeadshot(binding.rvManagement, platformType)
        val extras = FragmentNavigatorExtras(sharedElement to sharedElement.transitionName)
        findNavController().navigate(
            ManagementFragmentDirections.showLogin(name, platformType),
            extras
        )
    }

    private fun showPlatformsDialog(platformEnum: PlatformEnum) {
        platform = platformEnum
        managementViewModel.showPlatformsDialog(platformEnum)
    }

    private fun showPopUpPremium() {
        PremiumOnlyDialogFragment.newInstance(
            R.string.premium_title_add_account,
            R.string.premium_subtitle_add_account
        ).show(
            requireActivity().supportFragmentManager,
            "dialog_only_premium"
        )
    }

    private fun showPlatformsDialog(platforms: List<String>) {
        if (platforms.isEmpty()) {
            // This means there are no accounts (Savings, Gambling or Cash) in the user's portfolio
            managementViewModel.handleMaxPlatforms(platform!!.name, platform!!.name)
        } else {
            val builder = MaterialAlertDialogBuilder(requireContext())
            builder.setTitle(getString(R.string.select_account))
            builder.setPositiveButton(R.string.label_new) { dialog, _ ->
                // This means the user want's to create a new account
                managementViewModel.handleMaxPlatforms("EMPTY", platform!!.name)
                dialog.dismiss()
            }
            builder.setItems(platforms.toTypedArray()) { dialog, which ->
                // This means the user want's to edit an existing account
                showLogin(platforms[which], platform!!.name)
                dialog.dismiss()
            }
            builder.show()
        }
    }

    private fun showLoading() {
        binding.rvManagement.visibility = View.GONE
        binding.animationView.visibility = View.VISIBLE
        binding.animationView.setAnimation(R.raw.loading)
        binding.animationView.speed = 0.9f
        binding.animationView.repeatCount = -1
        binding.animationView.playAnimation()
    }

    private fun showSuccess() {
        binding.animationView.visibility = View.GONE
        binding.rvManagement.visibility = View.VISIBLE
        binding.animationView.setAnimation(R.raw.success)
        binding.animationView.speed = 0.75f
        binding.animationView.repeatCount = 0
        binding.animationView.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) = Unit

            override fun onAnimationEnd(animation: Animator?) {
                findNavController().navigateUp()
            }

            override fun onAnimationCancel(animation: Animator?) = Unit

            override fun onAnimationStart(animation: Animator?) = Unit
        })
        binding.animationView.playAnimation()
    }

    private fun showError() {
        binding.rvManagement.visibility = View.GONE
        binding.animationView.setAnimation(R.raw.error)
        binding.animationView.speed = 1.0f
        binding.animationView.repeatCount = 0
        binding.animationView.visibility = View.VISIBLE
        binding.animationView.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) = Unit

            override fun onAnimationEnd(animation: Animator?) {
                binding.animationView.visibility = View.GONE
                binding.rvManagement.visibility = View.VISIBLE
            }

            override fun onAnimationCancel(animation: Animator?) = Unit

            override fun onAnimationStart(animation: Animator?) = Unit
        })
        binding.animationView.playAnimation()
    }

    private fun onBackPressed(): Boolean {
        if (::bottomSheetBehavior.isInitialized && bottomSheetBehavior.state == STATE_EXPANDED) {
            // collapse or hide the sheet
            if (bottomSheetBehavior.isHideable && bottomSheetBehavior.skipCollapsed) {
                bottomSheetBehavior.state = STATE_HIDDEN
            } else {
                bottomSheetBehavior.state = STATE_COLLAPSED
            }
            return true
        }
        findNavController().navigateUp()
        return false
    }
}
