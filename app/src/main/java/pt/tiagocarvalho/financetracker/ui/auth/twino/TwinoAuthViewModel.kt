package pt.tiagocarvalho.financetracker.ui.auth.twino

import androidx.lifecycle.MutableLiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.ui.base.BaseViewModel
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.p2p.services.model.twino.LoginResponse
import retrofit2.Response

@HiltViewModel
class TwinoAuthViewModel @Inject constructor(
    private val twinoAuthUseCase: TwinoAuthUseCase,
    private val schedulerProvider: SchedulerProvider
) : BaseViewModel() {

    val visibility: MutableLiveData<Boolean> = MutableLiveData(false)
    val cookie: MutableLiveData<String> = MutableLiveData()
    val isError: MutableLiveData<Boolean> = MutableLiveData()

    fun authenticate(email: String, password: String) {
        compositeDisposable.add(
            twinoAuthUseCase.checkTfa(email)
                .subscribeOn(schedulerProvider.io())
                .doOnSubscribe { visibility.postValue(false) }
                .doOnSuccess {
                    if (it) visibility.postValue(true)
                    else authenticateNotTfa(email, password)
                }
                .subscribe({}, { isError.postValue(true) })
        )
    }

    private fun authenticateNotTfa(email: String, password: String) {
        compositeDisposable.add(
            twinoAuthUseCase.loginTfa(email, password, null)
                .subscribeOn(schedulerProvider.io())
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { isError.postValue(true) })
        )
    }

    fun authenticateTfa(email: String, password: String, code: String) {
        compositeDisposable.add(
            twinoAuthUseCase.loginTfa(email, password, code)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .doOnSuccess { onAuthenticationSuccess(it) }
                .subscribe({}, { isError.postValue(true) })
        )
    }

    private fun onAuthenticationSuccess(response: Response<LoginResponse>) {
        val body = response.body()
        body?.let {
            if (it.success) {
                val sessionId =
                    response.headers().get("Set-Cookie")?.split(";")?.first()?.split("=")?.last()
                cookie.postValue(sessionId)
            } else {
                isError.postValue(true)
            }
        } ?: isError.postValue(true)
    }
}
