package pt.tiagocarvalho.financetracker.widget

import android.content.Context
import android.graphics.Rect
import android.util.TypedValue
import android.view.View
import androidx.annotation.DimenRes
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.State

class SpacingItemDecoration(
    context: Context,
    @DimenRes spacingId: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: State) {
        super.getItemOffsets(outRect, view, parent, state)

        val screenWidth =
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_PX, parent.measuredWidth.toFloat(),
                parent.context.resources
                    .displayMetrics
            )
                .toInt()

        val viewWidth = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 100f,
            parent.context.resources
                .displayMetrics
        )
            .toInt()

        val emptySpace = screenWidth - 2 * viewWidth

        outRect.left = emptySpace / 7
    }
}
