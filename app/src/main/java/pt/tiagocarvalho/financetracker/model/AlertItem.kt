package pt.tiagocarvalho.financetracker.model

import androidx.annotation.DrawableRes
import java.math.BigDecimal

data class AlertItem(
    @DrawableRes val id: Int,
    val value: BigDecimal,
    val name: String,
    val url: String
)
