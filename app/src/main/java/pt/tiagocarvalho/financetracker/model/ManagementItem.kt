package pt.tiagocarvalho.financetracker.model

data class ManagementItem(
    val platform: PlatformEnum,
    var owned: Boolean,
    var total: Int?
)
