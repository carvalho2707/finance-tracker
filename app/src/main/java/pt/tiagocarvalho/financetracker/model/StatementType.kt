package pt.tiagocarvalho.financetracker.model

import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementType

enum class StatementType(val label: String) {

    DEPOSIT("Deposit"),
    WITHDRAWAL("Withdrawal"),
    INTEREST("Interest Received"),
    FEE("Fee"),
    INVESTMENT("Investment in Loan"),
    PRINCIPAL("Principal Received"),
    PRINCIPAL_FIX_NEGATIVE("Principal correction"),
    BONUS("Bonus"),
    REFERRAL("Referral"),
    DEPOSIT_CANCELED("Deposit Canceled"),
    WITHDRAWAL_CANCELED("Withdrawal Canceled"),
    CANCELED("Canceled");

    companion object {
        fun getStatementType(thirdPartyStatementType: ThirdPartyStatementType) =
            values().find { it.name == thirdPartyStatementType.name }.takeIf { it != null }
                ?: throw ThirdPartyException("Invalid label ${thirdPartyStatementType.name}")

        fun getValues(): Array<StatementType> =
            arrayOf(DEPOSIT, WITHDRAWAL, INTEREST, FEE, BONUS)

        fun getDepositsWithdrawals(): List<StatementType> = listOf(DEPOSIT, WITHDRAWAL)

        fun getChangeValues(): List<StatementType> = listOf(INTEREST, FEE, BONUS, REFERRAL)
    }
}
