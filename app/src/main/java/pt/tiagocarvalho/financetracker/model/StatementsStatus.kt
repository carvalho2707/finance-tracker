package pt.tiagocarvalho.financetracker.model

data class StatementsStatus(
    val platform: PlatformEnum,
    val name: String,
    val count: Int,
    val lastUpdate: String,
    val showName: Boolean
)
