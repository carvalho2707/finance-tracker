package pt.tiagocarvalho.financetracker.model

import android.util.SparseArray
import java.math.BigDecimal

data class GainsStatistics(
    val gainsByPlatform: MutableMap<String, BigDecimal>,
    val gainsByType: MutableMap<PlatformTypeEnum, BigDecimal>,
    val xirByName: MutableMap<String, Float> = HashMap(),
    val roiByName: MutableMap<String, Float> = HashMap(),
    val gainsByMonth: SparseArray<Float>,
    val gainsByYear: MutableMap<Int, Float>,
    val currentMonthByName: MutableMap<String, BigDecimal>,
    val currentYearByName: MutableMap<String, BigDecimal>,
    val show: Boolean
)
