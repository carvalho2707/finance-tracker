package pt.tiagocarvalho.financetracker.model

import java.util.ArrayList

enum class PlatformTypeEnum {
    P2P,
    SAVINGS,
    GAMBLING,
    CASH;

    companion object {

        fun valuesString(): Array<String> {
            val values = ArrayList<String>()
            for (platformTypeEnum in values()) {
                values.add(platformTypeEnum.toString())
            }

            return values.toTypedArray()
        }
    }
}
