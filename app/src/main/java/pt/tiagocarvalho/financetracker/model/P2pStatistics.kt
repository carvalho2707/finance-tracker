package pt.tiagocarvalho.financetracker.model

import java.math.BigDecimal

data class P2pStatistics(
    val total: Int,
    val ongoingInvestments: Int,
    val totalStatements: Int,
    val interestComparison: Map<PlatformEnum, BigDecimal>,
    val byName: Map<PlatformEnum, BigDecimal>,
    val statementsByName: Map<PlatformEnum, Int>,
    val cumulativeInvestments: MutableMap<String, Double>,
    val cumulativePrincipal: MutableMap<String, Double>,
    val show: Boolean
)
