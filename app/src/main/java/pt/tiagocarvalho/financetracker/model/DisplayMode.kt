package pt.tiagocarvalho.financetracker.model

enum class DisplayMode constructor(val summary: String) {

    ABSOLUTE("Absolute"),
    PERCENTAGE("Percentage")
}
