package pt.tiagocarvalho.financetracker.model

import java.math.BigDecimal

data class GamblingInfo(
    var name: String,
    var type: PlatformEnum,
    var totalDeposits: BigDecimal,
    var totalWithdrawals: BigDecimal,
    var totalBalance: BigDecimal,
    var interest: BigDecimal,
    var profit: BigDecimal,
    var serviceFees: BigDecimal,
    var bonus: BigDecimal,
    var changePercentage: BigDecimal = BigDecimal.ZERO,
    var changeValue: BigDecimal = BigDecimal.ZERO
)
