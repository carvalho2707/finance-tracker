package pt.tiagocarvalho.financetracker.model

data class DataItem(
    val label: Int,
    val value: String
)
