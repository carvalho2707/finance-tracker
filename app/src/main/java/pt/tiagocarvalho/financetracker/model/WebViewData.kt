package pt.tiagocarvalho.financetracker.model

data class WebViewData(
    val platform: PlatformEnum,
    val data: String? = null,
    val page: String? = null
)
