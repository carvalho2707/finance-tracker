package pt.tiagocarvalho.financetracker.model

import java.math.BigDecimal

data class GeneralStatistics(
    val balanceByType: MutableMap<String, BigDecimal>,
    val deposits: BigDecimal,
    val withdrawals: BigDecimal,
    val fees: BigDecimal,
    val interests: BigDecimal,
    val referrals: BigDecimal,
    val cashback: BigDecimal,
    val show: Boolean
)
