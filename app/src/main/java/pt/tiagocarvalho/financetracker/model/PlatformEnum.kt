package pt.tiagocarvalho.financetracker.model

import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum.GAMBLING
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum.P2P

enum class PlatformEnum constructor(
    val platformTypeEnum: PlatformTypeEnum,
    val url: String,
    val referralUrl: String,
    val logoId: Int,
    val twofactor: Boolean,
    val prefLastUpdateDetails: String,
    val prefLastUpdateStatement: String,
    val prefLastFullStatementScan: String
) {
    MINTOS(
        P2P,
        "https://www.mintos.com",
        "https://www.mintos.com/en/l/ref/ES8BJH",
        R.drawable.logo_mintos,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.mintos",
        "pt.tiagocarvalho.financetracker.last.update.statements.mintos",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.mintos"
    ),
    ROBOCASH(
        P2P,
        "https://robo.cash",
        "https://robo.cash",
        R.drawable.logo_robocash,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.robocash",
        "pt.tiagocarvalho.financetracker.last.update.statements.robocash",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.robocash"
    ),
    SAVINGS(
        PlatformTypeEnum.SAVINGS,
        "",
        "",
        R.drawable.logo_savings,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.savings_",
        "pt.tiagocarvalho.financetracker.last.update.statements.savings_",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.savings_"
    ),
    PEERBERRY(
        P2P,
        "https://peerberry.com",
        "https://peerberry.com",
        R.drawable.logo_peerberry,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.peerberry",
        "pt.tiagocarvalho.financetracker.last.update.statements.peerberry",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.peerberry"
    ),
    GRUPEER(
        P2P,
        "https://www.grupeer.com",
        "https://www.grupeer.com/join-grupeer?invited_by=grp_1001603",
        R.drawable.logo_grupeer,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.grupeer",
        "pt.tiagocarvalho.financetracker.last.update.statements.grupeer",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.grupeer"
    ),
    RAIZE(
        P2P,
        "https://www.raize.pt/login",
        "https://www.raize.pt/signup/QkKYK7ei7",
        R.drawable.logo_raize,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.raize",
        "pt.tiagocarvalho.financetracker.last.update.statements.raize",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.raize"
    ),
    BETS(
        GAMBLING,
        "",
        "",
        R.drawable.logo_gambling,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.bets_",
        "pt.tiagocarvalho.financetracker.last.update.statements.bets_",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.bets_"
    ),
    BONDORA(
        P2P,
        "https://bondora.com/",
        "https://bondora.com/ref/tiagoc16",
        R.drawable.logo_bondora,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.bondora",
        "pt.tiagocarvalho.financetracker.last.update.statements.bondora",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.bondora"
    ),
    TWINO(
        P2P,
        "https://www.twino.eu/en/",
        "https://www.twino.eu/en/",
        R.drawable.logo_twino,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.twino",
        "pt.tiagocarvalho.financetracker.last.update.statements.twino",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.twino"
    ),
    CROWDESTOR(
        P2P,
        "https://crowdestor.com/en/home/",
        "https://crowdestor.com/en/invite/friend/CR6351",
        R.drawable.logo_crowdestor,
        true,
        "pt.tiagocarvalho.financetracker.last.update.details.crowdestor",
        "pt.tiagocarvalho.financetracker.last.update.statements.crowdestor",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.crowdestor"
    ),
    ESTATEGURU(
        P2P,
        "https://estateguru.co/",
        "https://estateguru.co/en/investor-referral/?switch=en&userPromotionCode=EGU71354",
        R.drawable.logo_estateguru,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.estateguru",
        "pt.tiagocarvalho.financetracker.last.update.statements.estateguru",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.estateguru"
    ),
    CASH(
        PlatformTypeEnum.CASH,
        "",
        "",
        R.drawable.logo_cash,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.cash_",
        "pt.tiagocarvalho.financetracker.last.update.statements.cash_",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.cash_"
    ),
    IUVO(
        P2P,
        "https://www.iuvo-group.com/",
        "https://www.iuvo-group.com/",
        R.drawable.logo_iuvo,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.iuvo",
        "pt.tiagocarvalho.financetracker.last.update.statements.iuvo",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.iuvo"
    ),
    LENDERMARKET(
        P2P,
        "https://www.lendermarket.com/",
        "https://www.lendermarket.com/",
        R.drawable.logo_lendermarket,
        false,
        "pt.tiagocarvalho.financetracker.last.update.details.lendermarket",
        "pt.tiagocarvalho.financetracker.last.update.statements.lendermarket",
        "pt.tiagocarvalho.financetracker.last.full.statements.scan.lendermarket"
    );

    companion object {

        fun filterByPlatformTypeEnum(platformTypeEnum: PlatformTypeEnum): List<PlatformEnum> =
            values().filter { it.platformTypeEnum == platformTypeEnum }
    }
}
