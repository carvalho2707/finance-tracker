package pt.tiagocarvalho.financetracker.model

enum class Frequency {

    DAILY,
    WEEKLY,
    MONTHLY,
    QUARTERLY,
    BIANNUALLY,
    ANNUALLY,
    NONE,
    UNKNOWN
}
