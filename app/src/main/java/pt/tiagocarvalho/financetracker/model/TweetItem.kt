package pt.tiagocarvalho.financetracker.model

data class TweetItem(
    val id: String,
    val iconUrl: String,
    val title: String,
    val date: String,
    val body: String,
    val bodyImageUrl: String?,
    val twitterAccountUrl: String,
    val tweetUrl: String
)
