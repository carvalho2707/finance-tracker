package pt.tiagocarvalho.financetracker.model

import android.util.SparseArray
import java.math.BigDecimal

data class ForecastStatistics(
    val yearly: BigDecimal,
    val monthly: BigDecimal,
    val daily: BigDecimal,
    val secondly: BigDecimal,
    val monthlyCompoundProjection: SparseArray<Float>,
    val yearlyCompoundProjection: SparseArray<Float>,
    val show: Boolean
)
