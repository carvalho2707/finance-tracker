package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import pt.tiagocarvalho.financetracker.model.GamblingInfo

interface GamblingService {

    fun getDetails(name: String): Single<GamblingInfo>
}
