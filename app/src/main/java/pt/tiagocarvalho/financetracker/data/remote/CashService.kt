package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import pt.tiagocarvalho.financetracker.model.CashInfo

interface CashService {

    fun getDetails(name: String): Single<CashInfo>
}
