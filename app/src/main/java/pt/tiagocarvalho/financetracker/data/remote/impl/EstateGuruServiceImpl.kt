package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.EstateGuruService
import pt.tiagocarvalho.p2p.api.EstateGuruAPI
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class EstateGuruServiceImpl : EstateGuruService {

    override fun getDetails(login: Login): Single<ThirdPartyDetails> = try {
        val response = EstateGuruAPI.getDetails(login)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(
        username: String,
        password: String,
        date: Date?
    ): Single<ThirdPartyStatementModel> = try {
        val login = Login(username, password)
        val response = EstateGuruAPI.getStatements(date, login)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getDailyStatements(
        username: String,
        password: String
    ): Single<ThirdPartyStatementModel> = try {
        val login = Login(username, password)
        val response = EstateGuruAPI.getTodayStatements(login)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
