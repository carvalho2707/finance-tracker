package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.Index
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.Frequency
import pt.tiagocarvalho.financetracker.model.PlatformEnum

@Entity(
    tableName = "platform",
    primaryKeys = ["name"],
    indices = [
        Index("name"),
        Index("type")
    ]
)
data class Platform(
    var name: String,
    var type: PlatformEnum,

    // P2P Related
    val username: String?,
    val password: String?,
    var twofactor: Boolean,
    var frequentUpdates: Boolean,

    // Savings Related
    val interest: BigDecimal?,
    var interestsFrequency: Frequency?,
    val recurringAmount: BigDecimal = BigDecimal.ZERO,
    val recurringFrequency: Frequency?
)
