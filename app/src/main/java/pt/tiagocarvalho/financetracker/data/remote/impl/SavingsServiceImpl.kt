package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsStatement
import pt.tiagocarvalho.financetracker.data.remote.SavingsService
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.SavingsInfo
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.utils.Utils

class SavingsServiceImpl(
    private val savingsStatementDao: SavingsStatementDao,
    private val savingsAccountDao: SavingsAccountDao
) : SavingsService {

    override fun getDetails(name: String): Single<SavingsInfo> = Single.create { emitter ->
        try {
            val savingsAccount = savingsAccountDao.getByName(name)
            val statements = savingsStatementDao.getAllByName(name)
            var balance = 0.0
            var deposits = 0.0
            var withdrawals = 0.0
            var fees = 0.0
            var interests = 0.0
            var bonus = 0.0

            statements.forEach {
                when (it.type) {
                    StatementType.DEPOSIT -> {
                        deposits += it.amount
                        balance += it.amount
                    }
                    StatementType.WITHDRAWAL -> {
                        withdrawals += it.amount
                        balance -= it.amount
                    }
                    StatementType.INTEREST -> {
                        interests += it.amount
                        balance += it.amount
                    }
                    StatementType.FEE -> {
                        fees += it.amount
                        balance -= it.amount
                    }
                    StatementType.BONUS -> {
                        bonus += it.amount
                        balance += it.amount
                    }
                    StatementType.INVESTMENT,
                    StatementType.PRINCIPAL,
                    StatementType.DEPOSIT_CANCELED,
                    StatementType.REFERRAL,
                    StatementType.PRINCIPAL_FIX_NEGATIVE,
                    StatementType.WITHDRAWAL_CANCELED -> {
                    }
                }
            }

            val profit = (interests + bonus - fees).toBigDecimal()

            val change = getDailyChange(name)
            val changePercentage =
                Utils.calculateChangePercentage(change, balance.toBigDecimal())

            val savingsInfo = SavingsInfo(
                name = name,
                type = PlatformEnum.SAVINGS,
                totalDeposits = deposits.toBigDecimal(),
                totalWithdrawals = withdrawals.toBigDecimal(),
                profit = profit,
                interest = interests.toBigDecimal(),
                totalBalance = balance.toBigDecimal(),
                netAnnualReturn = savingsAccount.interest,
                serviceFees = fees.toBigDecimal(),
                interestFrequency = savingsAccount.interestsFrequency,
                recurringAmount = savingsAccount.recurringAmount,
                recurringFrequency = savingsAccount.recurringFrequency,
                bonus = bonus.toBigDecimal(),
                changeValue = change,
                changePercentage = changePercentage

            )
            emitter.onSuccess(savingsInfo)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    private fun getDailyChange(name: String): BigDecimal {
        var total = 0.0
        savingsStatementDao.getDailyStatementsByName(
            name,
            Utils.getTodayDate(),
            Utils.getEndDate()
        )
            .filter { isBalanceRelated(it) }
            .forEach {
                when (it.type) {
                    StatementType.DEPOSIT, StatementType.INTEREST, StatementType.BONUS -> total += it.amount
                    StatementType.WITHDRAWAL, StatementType.FEE -> total -= it.amount
                    else -> BigDecimal.ZERO
                }
            }
        return total.toBigDecimal()
    }

    private fun isBalanceRelated(savingsStatement: SavingsStatement): Boolean =
        savingsStatement.type == StatementType.DEPOSIT ||
            savingsStatement.type == StatementType.WITHDRAWAL ||
            savingsStatement.type == StatementType.INTEREST ||
            savingsStatement.type == StatementType.FEE ||
            savingsStatement.type == StatementType.BONUS
}
