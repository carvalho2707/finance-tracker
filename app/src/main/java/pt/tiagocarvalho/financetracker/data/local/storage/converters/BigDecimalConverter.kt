package pt.tiagocarvalho.financetracker.data.local.storage.converters

import androidx.room.TypeConverter
import java.math.BigDecimal
import java.math.BigDecimal.ZERO

object BigDecimalConverter {

    @TypeConverter
    @JvmStatic
    fun fromString(value: String?): BigDecimal =
        if (value == null || value.isBlank()) ZERO else BigDecimal(value)

    @TypeConverter
    @JvmStatic
    fun toString(bigDecimal: BigDecimal?): String? = bigDecimal?.toPlainString()
}
