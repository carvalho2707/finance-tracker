package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashStatement
import pt.tiagocarvalho.financetracker.data.remote.CashService
import pt.tiagocarvalho.financetracker.model.CashInfo
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.p2p.services.utils.Utils

class CashServiceImpl(
    private val cashStatementDao: CashStatementDao
) : CashService {

    override fun getDetails(name: String): Single<CashInfo> = Single.create { emitter ->
        try {
            val statements = cashStatementDao.getAllByName(name)
            var balance = 0.0
            var deposits = 0.0
            var withdrawals = 0.0
            var fees = 0.0
            var interests = 0.0
            var bonus = 0.0

            statements.forEach {
                when (it.type) {
                    StatementType.DEPOSIT -> {
                        deposits += it.amount
                        balance += it.amount
                    }
                    StatementType.WITHDRAWAL -> {
                        withdrawals += it.amount
                        balance -= it.amount
                    }
                    StatementType.INTEREST -> {
                        interests += it.amount
                        balance += it.amount
                    }
                    StatementType.FEE -> {
                        fees += it.amount
                        balance -= it.amount
                    }
                    StatementType.BONUS -> {
                        bonus += it.amount
                        balance += it.amount
                    }
                    StatementType.INVESTMENT,
                    StatementType.PRINCIPAL,
                    StatementType.DEPOSIT_CANCELED,
                    StatementType.REFERRAL,
                    StatementType.WITHDRAWAL_CANCELED -> {
                    }
                }
            }

            val profit = (interests + bonus - fees).toBigDecimal()

            val change = getDailyChange(name)
            val changePercentage =
                Utils.calculateChangePercentage(change, balance.toBigDecimal())

            val cashInfo = CashInfo(
                name = name,
                type = PlatformEnum.CASH,
                totalDeposits = deposits.toBigDecimal(),
                totalWithdrawals = withdrawals.toBigDecimal(),
                profit = profit,
                interest = interests.toBigDecimal(),
                serviceFees = fees.toBigDecimal(),
                bonus = bonus.toBigDecimal(),
                totalBalance = balance.toBigDecimal(),
                changeValue = change,
                changePercentage = changePercentage
            )
            emitter.onSuccess(cashInfo)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    private fun isBalanceRelated(cashStatement: CashStatement): Boolean =
        cashStatement.type == StatementType.DEPOSIT ||
            cashStatement.type == StatementType.WITHDRAWAL ||
            cashStatement.type == StatementType.INTEREST ||
            cashStatement.type == StatementType.FEE ||
            cashStatement.type == StatementType.BONUS

    private fun getDailyChange(name: String): BigDecimal {
        var total = 0.0
        cashStatementDao.getDailyStatementsByName(
            name,
            pt.tiagocarvalho.financetracker.utils.Utils.getTodayDate(),
            pt.tiagocarvalho.financetracker.utils.Utils.getEndDate()
        )
            .filter { isBalanceRelated(it) }
            .forEach {
                when (it.type) {
                    StatementType.DEPOSIT, StatementType.INTEREST, StatementType.BONUS -> total += it.amount
                    StatementType.WITHDRAWAL, StatementType.FEE -> total -= it.amount
                    else -> BigDecimal.ZERO
                }
            }
        return total.toBigDecimal()
    }
}
