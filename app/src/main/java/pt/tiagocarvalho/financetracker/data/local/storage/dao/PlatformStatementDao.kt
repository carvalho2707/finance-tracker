package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.StatementType

@Dao
interface PlatformStatementDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStatementsCompletable(platforms: List<PlatformStatement>): Completable

    @Query("SELECT * FROM platform_statement WHERE name = :name and date >= :today")
    fun getDailyStatementsByNameAsObservable(
        name: String,
        today: Date
    ): Observable<List<PlatformStatement>>

    @Query("SELECT * FROM platform_statement WHERE name = :name ORDER BY date desc")
    fun getAllByNameOrderByDateDescAsObservable(name: String): Observable<List<PlatformStatement>>

    @Query("SELECT * FROM platform_statement WHERE name = :name ORDER BY date desc")
    fun getAllByNameOrderByDateDescAsSingle(name: String): Single<List<PlatformStatement>>

    @Query("DELETE FROM platform_statement WHERE label = :label")
    fun deleteByLabel(label: String)

    @Query("DELETE FROM platform_statement WHERE name = :name")
    fun deleteByName(name: String)

    @Query("SELECT count(*) FROM platform_statement WHERE name = :name")
    fun getStatementsCount(name: String): Int

    @Query("SELECT * FROM platform_statement WHERE name = :name and type in (:types) ORDER BY date ASC")
    fun getAllByNameAndTypeInOrderByDateAsc(
        name: String,
        types: List<StatementType>
    ): List<PlatformStatement>

    @Query("SELECT * FROM platform_statement WHERE name = :name and type = :type")
    fun getAllByNameAndType(
        name: String,
        type: StatementType
    ): List<PlatformStatement>

    @Query(" SELECT * FROM platform_statement WHERE date >= :year and type in (:types)")
    fun getByYear(year: Date, types: List<StatementType>): List<PlatformStatement>

    @Query(" SELECT * FROM platform_statement where type in (:types)")
    fun getAllByTypes(types: List<StatementType>): List<PlatformStatement>

    @Query("DELETE FROM platform_statement WHERE name = :name")
    fun deleteByNameCompletable(name: String): Completable

    @Query("DELETE FROM platform_statement WHERE name = :name and type = :statementType")
    fun deleteByNameAndType(name: String, statementType: StatementType): Completable
}
