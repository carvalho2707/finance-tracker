package pt.tiagocarvalho.financetracker.data.local.storage.converters

import androidx.room.TypeConverter
import pt.tiagocarvalho.financetracker.model.PlatformEnum

object PlatformEnumConverter {

    @TypeConverter
    @JvmStatic
    fun toString(platformEnum: PlatformEnum?) = platformEnum?.name

    @TypeConverter
    @JvmStatic
    fun toPlatformEnum(platformEnum: String?): PlatformEnum? =
        platformEnum?.let(PlatformEnum::valueOf)
}
