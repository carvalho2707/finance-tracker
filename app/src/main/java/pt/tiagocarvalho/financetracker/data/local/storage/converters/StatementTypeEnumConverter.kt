package pt.tiagocarvalho.financetracker.data.local.storage.converters

import androidx.room.TypeConverter
import pt.tiagocarvalho.financetracker.model.StatementType

object StatementTypeEnumConverter {

    @TypeConverter
    @JvmStatic
    fun toString(statementType: StatementType?) = statementType?.name

    @TypeConverter
    @JvmStatic
    fun toStatementType(statementType: String?): StatementType? =
        statementType?.let(StatementType::valueOf)
}
