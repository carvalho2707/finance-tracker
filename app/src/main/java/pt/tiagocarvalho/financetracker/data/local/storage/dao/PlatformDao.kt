package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.model.PlatformEnum

@Dao
interface PlatformDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg platform: Platform)

    @Update
    fun update(vararg platform: Platform)

    @Query("SELECT * FROM platform")
    fun getPlatformsAsSingle(): Single<List<Platform>>

    @Query("SELECT * FROM platform")
    fun getPlatformsAsObservable(): Observable<List<Platform>>

    @Query("SELECT * FROM platform WHERE frequentUpdates = 1 ")
    fun getActivePlatforms(): Single<List<Platform>>

    @Query("SELECT * FROM platform")
    fun getPlatforms(): List<Platform>

    @Query("SELECT * FROM platform WHERE name = :name")
    fun getPlatformByNameAsSingle(name: String): Single<Platform>

    @Query("SELECT * FROM platform WHERE name = :name")
    fun getPlatformByNameAsMaybe(name: String): Maybe<Platform>

    @Query("SELECT * FROM platform WHERE type = :type")
    fun getPlatformsByTypeAsSingle(type: PlatformEnum): Single<List<Platform>>

    @Query("SELECT * FROM platform WHERE name = :name")
    fun getPlatformByName(name: String): Platform?

    @Query("SELECT name FROM platform WHERE type = :type")
    fun getPlatformsNamesByTypeAsSingle(type: PlatformEnum): Single<List<String>>

    @Query("SELECT * FROM platform WHERE name = :name")
    fun getPlatformByNameAsObservable(name: String): Observable<Platform>

    @Query("DELETE FROM platform WHERE name = :name")
    fun deleteByName(name: String): Completable

    @Query("SELECT * FROM platform WHERE twofactor = 1 and frequentUpdates = 1")
    fun getPlatformsWithTwoFactorAndActive(): Maybe<List<Platform>>

    @Query("SELECT count(*) FROM platform WHERE type in (:types)")
    fun countAccountsByType(types: List<PlatformEnum>): Single<Int>

    @Query("SELECT count(*) FROM platform")
    fun countAccounts(): Single<Int>

    @Query("SELECT * FROM platform WHERE type in (:types)")
    fun getPlatformsByTypeIn(types: List<PlatformEnum>): Single<List<Platform>>
}
