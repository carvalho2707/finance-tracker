package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.storage.model.TwitterAccount

@Dao
interface TwitterAccountDao {

    @Query("SELECT * FROM twitter_account")
    fun getTwitterAccounts(): Single<List<TwitterAccount>>

    @Query("SELECT * FROM twitter_account where active != 0")
    fun getEnabledTwitterAccounts(): Single<List<TwitterAccount>>

    @Update
    fun update(vararg twitterAccount: TwitterAccount): Completable
}
