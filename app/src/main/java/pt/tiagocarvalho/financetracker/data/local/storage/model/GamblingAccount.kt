package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.Index

@Entity(
    tableName = "gambling_account",
    primaryKeys = ["name"],
    indices = [
        Index("name")
    ]
)
data class GamblingAccount(
    val name: String
)
