package pt.tiagocarvalho.financetracker.data.local.storage.converters

import androidx.room.TypeConverter
import java.util.Date

object DateConverter {

    @TypeConverter
    @JvmStatic
    fun toDate(dateLong: Long?): Date? = if (dateLong == null) null else Date(dateLong)

    @TypeConverter
    @JvmStatic
    fun fromDate(date: Date?): Long? = date?.time
}
