package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingAccount

@Dao
interface GamblingAccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg gamblingAccount: GamblingAccount)

    @Update
    fun update(vararg gamblingAccount: GamblingAccount)
}
