package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.Frequency
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum

@Entity(
    tableName = "platform_details",
    primaryKeys = ["name"],
    foreignKeys = [
        ForeignKey(
            entity = Platform::class,
            parentColumns = arrayOf("name"),
            childColumns = arrayOf("name"),
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("name")
    ]
)
class PlatformDetails(

    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "platform") var platform: PlatformEnum,
    @ColumnInfo(name = "platform_type") var platformType: PlatformTypeEnum,
    @ColumnInfo(name = "total_deposits") var deposits: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "total_withdrawals") var withdrawals: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "total_balance") var balance: BigDecimal = BigDecimal.ZERO,
    @ColumnInfo(name = "available_funds") var available: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "invested_funds") var invested: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "net_annual_return") var netAnnualReturn: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "interest") var interest: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "late_payment_fees") var latePaymentFees: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "bad_Debpt") var badDebt: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "secondary_market_transactions") var secondaryMarketTransactions: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "service_fees") var serviceFees: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "campaign_rewards") var campaignRewards: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "my_investments") var myInvestments: Int? = 0,
    @ColumnInfo(name = "current_total") var currentTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "grace_period_total") var gracePeriodTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "one_to_fifteen_total") var oneToFifteenLateTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "sixteen_to_thirty_total") var sixteenToThirtyLateTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "thirty_to_sixty_total") var thirtyToSixtyLateTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "more_than_sixty_total") var moreThanSixtyLateTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "default_total") var defaultInvestmentTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "bad_debt_total") var badDebtTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "total_number") var totalInvestmentsNumber: Int? = 0,
    @ColumnInfo(name = "current_number") var currentNumber: Int? = 0,
    @ColumnInfo(name = "grace_number") var gracePeriodNumber: Int? = 0,
    @ColumnInfo(name = "one_to_fifteen_number") var oneToFifteenLateNumber: Int? = 0,
    @ColumnInfo(name = "sixteen_to_thirty_number") var sixteenToThirtyLateNumber: Int? = 0,
    @ColumnInfo(name = "thirty_to_sixty_number") var thirtyToSixtyLateNumber: Int? = 0,
    @ColumnInfo(name = "more_sixty_number") var moreThanSixtyLateNumber: Int? = 0,
    @ColumnInfo(name = "default_number") var defaultInvestmentNumber: Int? = 0,
    @ColumnInfo(name = "bad_debt_number") var badDebtNumber: Int? = 0,
    @ColumnInfo(name = "pending_interests") var pendingInterests: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "interest_frequency") var interestFrequency: Frequency? = Frequency.UNKNOWN,
    @ColumnInfo(name = "recurring_amount") var recurringAmount: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "recurring_frequency") var recurringFrequency: Frequency? = Frequency.UNKNOWN,
    @ColumnInfo(name = "cashback") var cashback: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "taxes") var taxes: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "currency_fluctuations") var currencyFluctuations: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "write_off") var writeOff: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "pending_payments") var pendingPayments: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "cumulative_invested") var cumulativeInvested: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "cumulative_principal") var cumulativePrincipal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "profit") var profit: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "late_total") var lateTotal: BigDecimal? = BigDecimal.ZERO,
    @ColumnInfo(name = "late_count") var lateNumber: Int? = 0,
    @ColumnInfo(name = "change_percentage") var changePercentage: BigDecimal = BigDecimal.ZERO,
    @ColumnInfo(name = "change_value") var changeValue: BigDecimal = BigDecimal.ZERO

) {
    constructor(platformEnum: PlatformEnum) : this(
        platformEnum.name,
        platformEnum,
        platformEnum.platformTypeEnum
    )
}
