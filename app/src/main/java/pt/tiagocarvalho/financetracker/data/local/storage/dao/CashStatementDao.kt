package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashStatement
import pt.tiagocarvalho.financetracker.model.StatementType

@Dao
interface CashStatementDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg statement: CashStatement)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(statements: List<CashStatement>): Completable

    @Update
    fun update(vararg statement: CashStatement)

    @Query("SELECT * FROM cash_statement WHERE id = :id")
    fun getById(id: Long): CashStatement

    @Query("SELECT * FROM cash_statement WHERE id = :id")
    fun getByIdAsObservable(id: Long): Observable<CashStatement>

    @Query("SELECT * FROM cash_statement WHERE name = :name")
    fun getAllByNameAsObservable(name: String): Observable<List<CashStatement>>

    @Query("SELECT * FROM cash_statement WHERE name = :name")
    fun getAllByNameAsSingle(name: String): Single<List<CashStatement>>

    @Query("SELECT * FROM cash_statement WHERE name = :name")
    fun getAllByName(name: String): List<CashStatement>

    @Query("SELECT * FROM cash_statement WHERE name = :name ORDER BY date desc")
    fun getAllByNameOrderByDateDesc(name: String): List<CashStatement>

    @Query("SELECT * FROM cash_statement WHERE name = :name and date >= :today and date < :endOfToday")
    fun getDailyStatementsByName(
        name: String,
        today: Date,
        endOfToday: Date
    ): List<CashStatement>

    @Query("DELETE FROM cash_statement WHERE id = :id")
    fun deleteById(id: Long)

    @Query("DELETE FROM cash_statement WHERE name = :name")
    fun deleteAllByName(name: String): Completable

    @Query("DELETE FROM cash_statement WHERE name = :name and type = :statementType")
    fun deleteByNameAndType(name: String, statementType: StatementType): Completable
}
