package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.Index

@Entity(
    tableName = "twitter_account",
    primaryKeys = ["name"],
    indices = [
        Index("name")
    ]
)
data class TwitterAccount(
    var name: String,
    val active: Boolean
)
