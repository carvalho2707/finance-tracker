package pt.tiagocarvalho.financetracker.data.local.storage.converters

import androidx.room.TypeConverter
import pt.tiagocarvalho.financetracker.model.Frequency

object InterestsFrequencyEnumConverter {

    @TypeConverter
    @JvmStatic
    fun toString(frequency: Frequency?) = frequency?.name ?: Frequency.UNKNOWN.name

    @TypeConverter
    @JvmStatic
    fun toInterestFrequency(frequency: String?): Frequency? = frequency?.let(Frequency::valueOf)
}
