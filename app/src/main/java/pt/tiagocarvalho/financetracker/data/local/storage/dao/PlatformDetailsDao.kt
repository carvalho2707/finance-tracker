package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum

@Dao
interface PlatformDetailsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCompletable(vararg platform: PlatformDetails): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg platform: PlatformDetails)

    @Query("SELECT * FROM platform_details WHERE name = :name")
    fun getDetailsByNameAsSingle(name: String): Single<PlatformDetails>

    @Query("SELECT * FROM platform_details WHERE name = :name")
    fun getDetailsByNameAsObservable(name: String): Observable<PlatformDetails>

    @Query("SELECT * FROM platform_details")
    fun getAll(): List<PlatformDetails>

    @Query("SELECT * FROM platform_details")
    fun getAllAsSingle(): Single<List<PlatformDetails>>

    @Query("SELECT * FROM platform_details where platform_type = :type")
    fun getAllByPlatformTypeEnum(type: PlatformTypeEnum): List<PlatformDetails>

    @Query("DELETE FROM platform_details where name = :name")
    fun deleteByName(name: String): Completable
}
