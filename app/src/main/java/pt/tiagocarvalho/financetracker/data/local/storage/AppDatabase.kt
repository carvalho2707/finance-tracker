package pt.tiagocarvalho.financetracker.data.local.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import pt.tiagocarvalho.financetracker.data.local.storage.converters.BigDecimalConverter
import pt.tiagocarvalho.financetracker.data.local.storage.converters.DateConverter
import pt.tiagocarvalho.financetracker.data.local.storage.converters.InterestsFrequencyEnumConverter
import pt.tiagocarvalho.financetracker.data.local.storage.converters.PlatformEnumConverter
import pt.tiagocarvalho.financetracker.data.local.storage.converters.PlatformTypeEnumConverter
import pt.tiagocarvalho.financetracker.data.local.storage.converters.StatementTypeEnumConverter
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.TwitterAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashAccount
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashStatement
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingAccount
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingStatement
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsAccount
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsStatement
import pt.tiagocarvalho.financetracker.data.local.storage.model.TwitterAccount

@Database(
    entities = [
        Platform::class,
        PlatformDetails::class,
        PlatformStatement::class,
        SavingsAccount::class,
        GamblingAccount::class,
        SavingsStatement::class,
        GamblingStatement::class,
        TwitterAccount::class,
        CashAccount::class,
        CashStatement::class
    ],
    version = 14,
    exportSchema = true
)
@TypeConverters(
    DateConverter::class,
    BigDecimalConverter::class,
    PlatformEnumConverter::class,
    PlatformTypeEnumConverter::class,
    StatementTypeEnumConverter::class,
    InterestsFrequencyEnumConverter::class
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun platformDao(): PlatformDao
    abstract fun platformDetailsDao(): PlatformDetailsDao
    abstract fun platformStatementDao(): PlatformStatementDao
    abstract fun savingsAccountDao(): SavingsAccountDao
    abstract fun gamblingAccountDao(): GamblingAccountDao
    abstract fun savingsStatementDao(): SavingsStatementDao
    abstract fun gamblingStatementDao(): GamblingStatementDao
    abstract fun twitterAccountDao(): TwitterAccountDao
    abstract fun cashAccountDao(): CashAccountDao
    abstract fun cashStatementDao(): CashStatementDao

    companion object {
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE platform ADD COLUMN twofactor INTEGER NOT NULL DEFAULT 0")
            }
        }

        val MIGRATION_2_3: Migration = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE savings_account ADD COLUMN interestsFrequency TEXT NOT NULL DEFAULT 'UNKNOWN'")
                database.execSQL("ALTER TABLE savings_account ADD COLUMN recurringAmount TEXT NOT NULL DEFAULT '0'")
                database.execSQL("ALTER TABLE savings_account ADD COLUMN recurringFrequency TEXT NOT NULL DEFAULT 'UNKNOWN'")
                database.execSQL("ALTER TABLE platform ADD COLUMN interestsFrequency TEXT DEFAULT 'UNKNOWN'")
                database.execSQL("ALTER TABLE platform ADD COLUMN recurringAmount TEXT NOT NULL DEFAULT '0'")
                database.execSQL("ALTER TABLE platform ADD COLUMN recurringFrequency TEXT DEFAULT 'UNKNOWN'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN interest_frequency TEXT DEFAULT 'UNKNOWN'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN recurring_amount TEXT DEFAULT '0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN recurring_frequency TEXT DEFAULT 'UNKNOWN'")
            }
        }

        val MIGRATION_3_4: Migration = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DELETE FROM platform_statement")
            }
        }

        val MIGRATION_4_5: Migration = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DELETE FROM platform_statement where name = 'GRUPEER'")
            }
        }

        val MIGRATION_5_6: Migration = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("UPDATE platform SET twofactor = 1 WHERE (name = 'MINTOS' OR name = 'RAIZE')")
            }
        }

        val MIGRATION_6_7: Migration = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS `twitter_account` (`name` TEXT NOT NULL, `active` INTEGER NOT NULL, PRIMARY KEY(`name`))")
                database.execSQL("CREATE INDEX IF NOT EXISTS `index_twitter_account_name` ON `twitter_account` (`name`)")
                database.execSQL("INSERT INTO twitter_account VALUES('MintosPlatform', 1)")
                database.execSQL("INSERT INTO twitter_account VALUES('Grupeer_com', 1)")
                database.execSQL("INSERT INTO twitter_account VALUES('PeerBerry', 1)")
                database.execSQL("INSERT INTO twitter_account VALUES('Robocash1', 1)")
                database.execSQL("INSERT INTO twitter_account VALUES('p2pMillionaire', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('ExploreP2P', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('Bondora_com', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('TWINO_eu', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('Viventor_P2P', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('Zopa', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('Seedrs', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('FASTINVEST_COM', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('Estate_Guru', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('Crowdestate_eu', 0)")
                database.execSQL("INSERT INTO twitter_account VALUES('VIAINVEST_', 0)")
            }
        }

        val MIGRATION_7_8: Migration = object : Migration(7, 8) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE platform_details ADD COLUMN cashback TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN taxes TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN currency_fluctuations TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN write_off TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN pending_payments TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN cumulative_invested TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN cumulative_principal TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN profit TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN late_total TEXT DEFAULT '0.0'")
                database.execSQL("ALTER TABLE platform_details ADD COLUMN late_count INTEGER DEFAULT 0")
            }
        }

        val MIGRATION_8_9: Migration = object : Migration(8, 9) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE IF NOT EXISTS `cash_account` (`name` TEXT NOT NULL, PRIMARY KEY(`name`))")
                database.execSQL("CREATE INDEX IF NOT EXISTS `index_cash_account_name` ON `cash_account` (`name`)")
                database.execSQL("CREATE TABLE IF NOT EXISTS `cash_statement` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `amount` REAL NOT NULL, `date` INTEGER NOT NULL, `type` TEXT NOT NULL, FOREIGN KEY(`name`) REFERENCES `platform`(`name`) ON UPDATE NO ACTION ON DELETE CASCADE )")
                database.execSQL("CREATE INDEX IF NOT EXISTS `index_cash_statement_id` ON `cash_statement` (`id`)")
                database.execSQL("CREATE INDEX IF NOT EXISTS `index_cash_statement_name` ON `cash_statement` (`name`)")
                database.execSQL("CREATE INDEX IF NOT EXISTS `index_cash_statement_date` ON `cash_statement` (`date`)")
            }
        }

        val MIGRATION_9_10: Migration = object : Migration(9, 10) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE platform ADD COLUMN frequentUpdates INTEGER NOT NULL DEFAULT 1")
            }
        }

        val MIGRATION_10_11: Migration = object : Migration(10, 11) {
            override fun migrate(database: SupportSQLiteDatabase) {
                val tableNameTemp = "platform_details_tmp"
                val tableName = "platform_details"

                database.execSQL(
                    "CREATE TABLE IF NOT EXISTS `$tableNameTemp` (`name` TEXT NOT NULL, `platform` TEXT NOT NULL, `platform_type` TEXT NOT NULL, `total_deposits` TEXT, `total_withdrawals` TEXT, `total_balance` TEXT NOT NULL, `available_funds` TEXT, `invested_funds` TEXT, `net_annual_return` TEXT, `interest` TEXT, `late_payment_fees` TEXT, `bad_Debpt` TEXT, `secondary_market_transactions` TEXT, `service_fees` TEXT, `campaign_rewards` TEXT,  `my_investments` INTEGER, `current_total` TEXT, `grace_period_total` TEXT, `one_to_fifteen_total` TEXT, `sixteen_to_thirty_total` TEXT, `thirty_to_sixty_total` TEXT, `more_than_sixty_total` TEXT, `default_total` TEXT, `bad_debt_total` TEXT, `total_number` INTEGER, `current_number` INTEGER, `grace_number` INTEGER, `one_to_fifteen_number` INTEGER, `sixteen_to_thirty_number` INTEGER, `thirty_to_sixty_number` INTEGER, `more_sixty_number` INTEGER, `default_number` INTEGER, `bad_debt_number` INTEGER, `pending_interests` TEXT, `interest_frequency` TEXT, `recurring_amount` TEXT, `recurring_frequency` TEXT, `cashback` TEXT, `taxes` TEXT, `currency_fluctuations` TEXT, `write_off` TEXT, `pending_payments` TEXT, `cumulative_invested` TEXT, `cumulative_principal` TEXT, `profit` TEXT, `late_total` TEXT, `late_count` INTEGER, `change_percentage` TEXT NOT NULL, `change_value` TEXT NOT NULL, PRIMARY KEY(`name`), FOREIGN KEY(`name`) REFERENCES `platform`(`name`) ON UPDATE NO ACTION ON DELETE CASCADE )"
                )
                database.execSQL(
                    "INSERT INTO $tableNameTemp (name , platform , platform_type , total_deposits , total_withdrawals , total_balance , available_funds , invested_funds , net_annual_return , interest , late_payment_fees , bad_Debpt , secondary_market_transactions , service_fees , campaign_rewards , my_investments , current_total , grace_period_total , one_to_fifteen_total , sixteen_to_thirty_total , thirty_to_sixty_total , more_than_sixty_total , default_total , bad_debt_total , total_number , current_number , grace_number, one_to_fifteen_number , sixteen_to_thirty_number , thirty_to_sixty_number , more_sixty_number , default_number , bad_debt_number , pending_interests , interest_frequency , recurring_amount , recurring_frequency , cashback , taxes , currency_fluctuations , write_off , pending_payments , cumulative_invested , cumulative_principal , profit , late_total , late_count , change_percentage , change_value)  SELECT name , platform , platform_type , total_deposits , total_withdrawals , total_balance , available_funds , invested_funds , net_annual_return , interest , late_payment_fees , bad_Debpt , secondary_market_transactions , service_fees , campaign_rewards , my_investments , current_total , grace_period_total , one_to_fifteen_total , sixteen_to_thirty_total , thirty_to_sixty_total , more_than_sixty_total , default_total , bad_debt_total , total_number , current_number , grace_number, one_to_fifteen_number , sixteen_to_thirty_number , thirty_to_sixty_number , more_sixty_number , default_number , bad_debt_number , pending_interests , interest_frequency , recurring_amount , recurring_frequency , cashback , taxes , currency_fluctuations , write_off , pending_payments , cumulative_invested , cumulative_principal , profit , late_total , late_count , 0.0, 0.0 FROM $tableName"
                )
                database.execSQL("DROP TABLE $tableName")
                database.execSQL("ALTER TABLE $tableNameTemp RENAME TO $tableName")
                database.execSQL("CREATE INDEX IF NOT EXISTS `index_platform_details_name` ON `$tableName` (`name`)")
                database.execSQL("DROP TABLE `account`")
            }
        }

        val MIGRATION_11_12: Migration = object : Migration(11, 12) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DELETE FROM platform_statement where name = 'ESTATEGURU'")
            }
        }

        val MIGRATION_12_13: Migration = object : Migration(12, 13) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("UPDATE platform SET twofactor = 1 WHERE name in ('ROBOCASH', 'PEERBERRY', 'TWINO', 'CROWDESTOR', 'RAIZE', 'MINTOS', 'GRUPEER')")
            }
        }

        val MIGRATION_13_14: Migration = object : Migration(13, 14) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DELETE FROM platform_statement where name = 'MINTOS'")
                database.execSQL("DELETE FROM platform_details where name = 'MINTOS'")
                database.execSQL("DELETE FROM platform where name = 'MINTOS'")
            }
        }
    }
}
