package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

interface GrupeerService {

    fun getDetails(cookies: String): Single<ThirdPartyDetails>

    fun getStatements(cookies: String, date: Date?): Single<ThirdPartyStatementModel>

    fun getDailyStatements(cookies: String): Single<ThirdPartyStatementModel>
}
