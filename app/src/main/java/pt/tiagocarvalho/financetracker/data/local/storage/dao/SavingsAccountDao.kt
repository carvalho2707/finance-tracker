package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsAccount

@Dao
interface SavingsAccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg savingsAccount: SavingsAccount)

    @Update
    fun update(vararg savingsAccount: SavingsAccount)

    @Query("SELECT * FROM savings_account WHERE name = :name")
    fun getByName(name: String): SavingsAccount
}
