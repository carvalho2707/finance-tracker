package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.GrupeerService
import pt.tiagocarvalho.p2p.api.GrupeerAPI
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class GrupeerServiceImpl : GrupeerService {

    override fun getDetails(cookies: String): Single<ThirdPartyDetails> = try {
        val response = GrupeerAPI.getDetails(cookies)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(cookies: String, date: Date?): Single<ThirdPartyStatementModel> =
        try {
            val response = GrupeerAPI.getStatements(cookies, date)
            Single.just(response)
        } catch (e: Exception) {
            Single.error(e)
        }

    override fun getDailyStatements(cookies: String): Single<ThirdPartyStatementModel> = try {
        val response = GrupeerAPI.getTodayStatements(cookies)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
