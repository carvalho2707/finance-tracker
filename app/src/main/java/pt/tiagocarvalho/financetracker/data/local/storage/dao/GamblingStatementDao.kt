package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingStatement
import pt.tiagocarvalho.financetracker.model.StatementType

@Dao
interface GamblingStatementDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg gamblingStatement: GamblingStatement)

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(statements: List<GamblingStatement>): Completable

    @Update
    fun update(vararg gamblingStatement: GamblingStatement)

    @Query("SELECT * FROM gambling_statement WHERE id = :id")
    fun getById(id: Long): GamblingStatement

    @Query("SELECT * FROM gambling_statement WHERE id = :id")
    fun getByIdAsObservable(id: Long): Observable<GamblingStatement>

    @Query("SELECT * FROM gambling_statement WHERE name = :name")
    fun getAllByNameAsObservable(name: String): Observable<List<GamblingStatement>>

    @Query("SELECT * FROM gambling_statement WHERE name = :name")
    fun getAllByNameAsSingle(name: String): Single<List<GamblingStatement>>

    @Query("SELECT * FROM gambling_statement WHERE name = :name")
    fun getAllByName(name: String): List<GamblingStatement>

    @Query("SELECT * FROM gambling_statement WHERE name = :name ORDER BY date desc")
    fun getAllByNameOrderByDateDesc(name: String): List<GamblingStatement>

    @Query("SELECT * FROM gambling_statement WHERE name = :name and date >= :today and date < :endOfToday")
    fun getDailyStatementsByName(
        name: String,
        today: Date,
        endOfToday: Date
    ): List<GamblingStatement>

    @Query("DELETE FROM gambling_statement WHERE id = :id")
    fun deleteById(id: Long)

    @Query("DELETE FROM gambling_statement WHERE name = :name")
    fun deleteAllByName(name: String): Completable

    @Query("DELETE FROM gambling_statement WHERE name = :name and type = :statementType")
    fun deleteByNameAndType(name: String, statementType: StatementType): Completable
}
