package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingStatement
import pt.tiagocarvalho.financetracker.data.remote.GamblingService
import pt.tiagocarvalho.financetracker.model.GamblingInfo
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.utils.Utils

class GamblingServiceImpl(
    private val gamblingStatementDao: GamblingStatementDao
) : GamblingService {

    override fun getDetails(name: String): Single<GamblingInfo> = Single.create { emitter ->
        try {
            val statements = gamblingStatementDao.getAllByName(name)
            var balance = 0.0
            var deposits = 0.0
            var withdrawals = 0.0
            var fees = 0.0
            var interests = 0.0
            var bonus = 0.0

            statements.forEach {
                when (it.type) {
                    StatementType.DEPOSIT -> {
                        deposits += it.amount
                        balance += it.amount
                    }
                    StatementType.WITHDRAWAL -> {
                        withdrawals += it.amount
                        balance -= it.amount
                    }
                    StatementType.INTEREST -> {
                        interests += it.amount
                        balance += it.amount
                    }
                    StatementType.FEE -> {
                        fees += it.amount
                        balance -= it.amount
                    }
                    StatementType.BONUS -> {
                        bonus += it.amount
                        balance += it.amount
                    }
                    StatementType.INVESTMENT,
                    StatementType.PRINCIPAL,
                    StatementType.DEPOSIT_CANCELED,
                    StatementType.REFERRAL,
                    StatementType.WITHDRAWAL_CANCELED -> {
                    }
                }
            }

            val profit = (interests + bonus - fees).toBigDecimal()

            val change = getDailyChange(name)
            val changePercentage =
                Utils.calculateChangePercentage(change, balance.toBigDecimal())

            val gamblingInfo = GamblingInfo(
                name = name,
                type = PlatformEnum.BETS,
                totalDeposits = deposits.toBigDecimal(),
                totalWithdrawals = withdrawals.toBigDecimal(),
                interest = interests.toBigDecimal(),
                profit = profit,
                totalBalance = balance.toBigDecimal(),
                serviceFees = fees.toBigDecimal(),
                bonus = bonus.toBigDecimal(),
                changeValue = change,
                changePercentage = changePercentage
            )
            emitter.onSuccess(gamblingInfo)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    private fun getDailyChange(name: String): BigDecimal {
        var total = 0.0
        gamblingStatementDao.getDailyStatementsByName(
            name,
            Utils.getTodayDate(),
            Utils.getEndDate()
        )
            .filter { isBalanceRelated(it) }
            .forEach {
                when (it.type) {
                    StatementType.DEPOSIT, StatementType.INTEREST, StatementType.BONUS -> total += it.amount
                    StatementType.WITHDRAWAL, StatementType.FEE -> total -= it.amount
                    else -> BigDecimal.ZERO
                }
            }
        return total.toBigDecimal()
    }

    private fun isBalanceRelated(gamblingStatement: GamblingStatement): Boolean =
        gamblingStatement.type == StatementType.DEPOSIT ||
            gamblingStatement.type == StatementType.WITHDRAWAL ||
            gamblingStatement.type == StatementType.INTEREST ||
            gamblingStatement.type == StatementType.FEE ||
            gamblingStatement.type == StatementType.BONUS
}
