package pt.tiagocarvalho.financetracker.data.local.storage.converters

import androidx.room.TypeConverter
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum

object PlatformTypeEnumConverter {

    @TypeConverter
    @JvmStatic
    fun toString(platformTypeEnum: PlatformTypeEnum?) = platformTypeEnum?.name

    @TypeConverter
    @JvmStatic
    fun toPlatformTypeEnum(platformTypeEnum: String?): PlatformTypeEnum? =
        platformTypeEnum?.let(PlatformTypeEnum::valueOf)
}
