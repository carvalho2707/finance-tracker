package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

interface TwinoService {

    fun getDetails(sessionId: String): Single<ThirdPartyDetails>

    fun getStatements(
        sessionId: String,
        date: Date?
    ): Single<ThirdPartyStatementModel>

    fun getDailyStatements(sessionId: String): Single<ThirdPartyStatementModel>
}
