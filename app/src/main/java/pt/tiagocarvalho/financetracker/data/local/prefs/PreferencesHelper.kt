package pt.tiagocarvalho.financetracker.data.local.prefs

/**
 * Contract specification for a preferences helper.
 */
interface PreferencesHelper {

    fun clear()

    fun put(key: String, value: String)

    fun put(key: String, value: Int)

    fun put(key: String, value: Float)

    fun put(key: String, value: Boolean)

    fun put(key: String, value: Long)

    fun put(key: String, value: MutableSet<String>)

    fun get(key: String, defaultValue: String): String

    fun get(key: String): String?

    fun get(key: String, defaultValue: Int): Int

    fun get(key: String, defaultValue: Float): Float

    fun get(key: String, defaultValue: Boolean): Boolean

    fun get(key: String, defaultValue: Long): Long

    fun get(key: String, defaultValue: MutableSet<String>): MutableSet<String>

    fun delete(key: String)
}
