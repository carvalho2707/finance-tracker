package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.TwinoService
import pt.tiagocarvalho.p2p.api.TwinoAPI
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class TwinoServiceImpl : TwinoService {

    override fun getDetails(sessionId: String): Single<ThirdPartyDetails> = try {
        val response = TwinoAPI.getDetails(sessionId)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(
        sessionId: String,
        date: Date?
    ): Single<ThirdPartyStatementModel> = try {
        val response = TwinoAPI.getStatements(date, sessionId)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getDailyStatements(
        sessionId: String
    ): Single<ThirdPartyStatementModel> = try {
        val response = TwinoAPI.getTodayStatements(sessionId)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
