package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.RaizeService
import pt.tiagocarvalho.p2p.api.RaizeAPI
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class RaizeServiceImpl : RaizeService {

    override fun getDetails(token: String): Single<ThirdPartyDetails> = try {
        val response = RaizeAPI.getDetails(token)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(
        token: String,
        date: Date?
    ): Single<ThirdPartyStatementModel> = try {
        val response = RaizeAPI.getStatements(token, date)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getDailyStatements(
        token: String
    ): Single<ThirdPartyStatementModel> = try {
        val response = RaizeAPI.getTodayStatements(token)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
