package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.Index

@Entity(
    tableName = "cash_account",
    primaryKeys = ["name"],
    indices = [
        Index("name")
    ]
)
data class CashAccount(
    val name: String
)
