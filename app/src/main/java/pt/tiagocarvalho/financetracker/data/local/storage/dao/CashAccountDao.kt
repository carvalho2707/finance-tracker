package pt.tiagocarvalho.financetracker.data.local.storage.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashAccount

@Dao
interface CashAccountDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg cashAccount: CashAccount)

    @Update
    fun update(vararg cashAccount: CashAccount)

    @Query("SELECT * FROM cash_account WHERE name = :name")
    fun getByName(name: String): CashAccount
}
