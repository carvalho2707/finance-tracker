package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.MintosService
import pt.tiagocarvalho.p2p.api.MintosAPI
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class MintosServiceImpl : MintosService {

    override fun getDetails(
        cookies: String
    ): Single<ThirdPartyDetails> =
        try {
            val response = MintosAPI.getDetails(cookies)
            Single.just(response)
        } catch (e: Exception) {
            Single.error(e)
        }

    override fun getStatements(
        cookies: String,
        date: Date?
    ): Single<ThirdPartyStatementModel> = try {
        val response = MintosAPI.getStatements(date, cookies)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getDailyStatements(
        cookies: String
    ): Single<ThirdPartyStatementModel> = try {
        val response = MintosAPI.getTodayStatements(cookies)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
