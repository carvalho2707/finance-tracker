package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.Date
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.utils.toStringPattern

@Entity(
    tableName = "savings_statement",
    foreignKeys = [
        ForeignKey(
            entity = Platform::class,
            parentColumns = arrayOf("name"),
            childColumns = arrayOf("name"),
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("id"),
        Index("name"),
        Index("date")
    ]
)
data class SavingsStatement(
    @PrimaryKey(autoGenerate = true) val id: Long? = null,
    var name: String,
    var amount: Double,
    var date: Date,
    var type: StatementType
) {
    fun toStringCSV(): String = "$amount,${date.toStringPattern()},$type"
}
