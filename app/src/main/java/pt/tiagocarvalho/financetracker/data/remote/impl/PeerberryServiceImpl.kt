package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.PeerberryService
import pt.tiagocarvalho.p2p.api.PeerberryAPI
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class PeerberryServiceImpl : PeerberryService {

    override fun getDetails(token: String): Single<ThirdPartyDetails> = try {
        val response = PeerberryAPI.getDetails(token)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(
        token: String,
        date: Date?
    ): Single<ThirdPartyStatementModel> = try {
        val response = PeerberryAPI.getStatements(token, date)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getDailyStatements(
        token: String
    ): Single<ThirdPartyStatementModel> = try {
        val response = PeerberryAPI.getTodayStatements(token)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
