package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import java.util.Date
import pt.tiagocarvalho.financetracker.model.StatementType

@Entity(
    tableName = "platform_statement",
    primaryKeys = ["label"],
    foreignKeys = [
        ForeignKey(
            entity = Platform::class,
            parentColumns = arrayOf("name"),
            childColumns = arrayOf("name"),
            onDelete = ForeignKey.CASCADE
        )
    ],
    indices = [
        Index("name"),
        Index("label"),
        Index("date")
    ]
)
data class PlatformStatement(
    var name: String,
    var amount: Double,
    var date: Date,
    var type: StatementType,
    var label: String
)
