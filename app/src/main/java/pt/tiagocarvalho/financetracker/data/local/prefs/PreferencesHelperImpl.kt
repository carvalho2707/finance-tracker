package pt.tiagocarvalho.financetracker.data.local.prefs

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Implementation of the [PreferencesHelper].
 */
@Singleton
class PreferencesHelperImpl @Inject
constructor(private val mSharedPreferences: SharedPreferences) : PreferencesHelper {

    override fun clear() {
        mSharedPreferences.edit().clear().apply()
    }

    override fun put(key: String, value: String) {
        mSharedPreferences.edit().putString(key, value).apply()
    }

    override fun put(key: String, value: Int) {
        mSharedPreferences.edit().putInt(key, value).apply()
    }

    override fun put(key: String, value: Float) {
        mSharedPreferences.edit().putFloat(key, value).apply()
    }

    override fun put(key: String, value: Long) {
        mSharedPreferences.edit().putLong(key, value).apply()
    }

    override fun put(key: String, value: Boolean) {
        mSharedPreferences.edit().putBoolean(key, value).apply()
    }

    override fun put(key: String, value: MutableSet<String>) {
        mSharedPreferences.edit().putStringSet(key, value).apply()
    }

    override fun get(key: String, defaultValue: String) =
        mSharedPreferences.getString(key, defaultValue)!!

    override fun get(key: String): String? = mSharedPreferences.getString(key, null)

    override fun get(key: String, defaultValue: Int) = mSharedPreferences.getInt(key, defaultValue)

    override fun get(key: String, defaultValue: Float) =
        mSharedPreferences.getFloat(key, defaultValue)

    override fun get(key: String, defaultValue: Boolean) =
        mSharedPreferences.getBoolean(key, defaultValue)

    override fun get(key: String, defaultValue: Long) =
        mSharedPreferences.getLong(key, defaultValue)

    override fun get(key: String, defaultValue: MutableSet<String>): MutableSet<String> =
        mSharedPreferences.getStringSet(key, defaultValue)!!

    override fun delete(key: String) {
        mSharedPreferences.edit().remove(key).apply()
    }
}
