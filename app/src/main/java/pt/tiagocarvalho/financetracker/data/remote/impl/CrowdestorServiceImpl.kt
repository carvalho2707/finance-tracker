package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.data.remote.CrowdestorService
import pt.tiagocarvalho.p2p.api.CrowdestorAPI
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class CrowdestorServiceImpl : CrowdestorService {

    override fun getDetails(cookies: String): Single<ThirdPartyDetails> = try {
        val response = CrowdestorAPI.getDetails(cookies)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(cookies: String, date: Date?): Single<ThirdPartyStatementModel> =
        try {
            val response = CrowdestorAPI.getStatements(date, cookies)
            Single.just(response)
        } catch (e: Exception) {
            Single.error(e)
        }

    override fun getDailyStatements(
        cookies: String
    ): Single<ThirdPartyStatementModel> = try {
        val response = CrowdestorAPI.getTodayStatements(cookies)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
