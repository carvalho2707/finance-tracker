package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

interface PeerberryService {

    fun getDetails(token: String): Single<ThirdPartyDetails>

    fun getStatements(token: String, date: Date?): Single<ThirdPartyStatementModel>

    fun getDailyStatements(token: String): Single<ThirdPartyStatementModel>
}
