package pt.tiagocarvalho.financetracker.data.remote.impl

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.data.remote.BondoraService
import pt.tiagocarvalho.p2p.api.BondoraAPI
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

class BondoraServiceImpl : BondoraService {

    override fun getDetails(
        token: String,
        username: String,
        password: String
    ): Single<ThirdPartyDetails> = try {
        val login = Login(username, password)
        val response = BondoraAPI.getInstance(
            BuildConfig.BONDORA_CLIENT_ID,
            BuildConfig.BONDORA_CLIENT_SECRET
        ).getDetails(token, login)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getStatements(token: String, date: Date?): Single<ThirdPartyStatementModel> = try {
        val response = BondoraAPI.getInstance(
            BuildConfig.BONDORA_CLIENT_ID,
            BuildConfig.BONDORA_CLIENT_SECRET
        ).getStatements(date, token)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }

    override fun getDailyStatements(token: String): Single<ThirdPartyStatementModel> = try {
        val response = BondoraAPI.getInstance(
            BuildConfig.BONDORA_CLIENT_ID,
            BuildConfig.BONDORA_CLIENT_SECRET
        ).getTodayStatements(token)
        Single.just(response)
    } catch (e: Exception) {
        Single.error(e)
    }
}
