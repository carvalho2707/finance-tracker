package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

interface BondoraService {

    fun getDetails(token: String, username: String, password: String): Single<ThirdPartyDetails>

    fun getStatements(token: String, date: Date?): Single<ThirdPartyStatementModel>

    fun getDailyStatements(token: String): Single<ThirdPartyStatementModel>
}
