package pt.tiagocarvalho.financetracker.data.local.storage.model

import androidx.room.Entity
import androidx.room.Index
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.Frequency

@Entity(
    tableName = "savings_account",
    primaryKeys = ["name"],
    indices = [
        Index("name")
    ]
)
data class SavingsAccount(
    var name: String,
    var interest: BigDecimal,
    var interestsFrequency: Frequency,
    val recurringAmount: BigDecimal = BigDecimal.ZERO,
    val recurringFrequency: Frequency
)
