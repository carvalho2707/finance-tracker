package pt.tiagocarvalho.financetracker.data.remote

import io.reactivex.Single
import java.util.Date
import pt.tiagocarvalho.p2p.api.model.Login
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatementModel

interface LenderMarketService {

    fun getDetails(login: Login): Single<ThirdPartyDetails>

    fun getStatements(
        username: String,
        password: String,
        date: Date?
    ): Single<ThirdPartyStatementModel>

    fun getDailyStatements(
        username: String,
        password: String
    ): Single<ThirdPartyStatementModel>
}
