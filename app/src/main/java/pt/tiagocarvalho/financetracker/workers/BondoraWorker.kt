package pt.tiagocarvalho.financetracker.workers

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import io.reactivex.Single
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.repository.BondoraRepository
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.Logger
import timber.log.Timber

@HiltWorker
class BondoraWorker @AssistedInject constructor(
    @Assisted appContext: Context,
    @Assisted workerParams: WorkerParameters,
    private val bondoraRepository: BondoraRepository,
    private val platformDao: PlatformDao,
    private val logger: Logger,
    private val analyticsLogger: AnalyticsLogger
) : RxWorker(appContext, workerParams) {

    companion object {
        const val WORKER_PERIOD: Long = 4
        const val WORKER_TAG = "BondoraWorkerTag"
        const val UNIQUE_WORKER_NAME = "BondoraWorker"
    }

    override fun createWork(): Single<Result> {
        Timber.e("BondoraWorker started")
        return Single.create {
            val platform = platformDao.getPlatformByName(PlatformEnum.BONDORA.name)
            if (platform == null) {
                Timber.e("BondoraWorker success no platform")
                it.onSuccess(Result.success())
                return@create
            }
            try {
                bondoraRepository.syncDetails(true).blockingAwait()
                bondoraRepository.syncStatements(true).blockingAwait()
            } catch (e: Exception) {
                Timber.e("BondoraWorker error", e)
                logger.log(e)
                analyticsLogger.logWorkerStatus(false)
                it.onSuccess(Result.retry())
                return@create
            }
            Timber.e("BondoraWorker success")
            analyticsLogger.logWorkerStatus(true)
            it.onSuccess(Result.success())
            return@create
        }
    }
}
