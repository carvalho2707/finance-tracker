package pt.tiagocarvalho.financetracker.webview

import android.annotation.TargetApi
import android.content.ClipDescription
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.view.LayoutInflater
import android.webkit.CookieManager
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import kotlin.concurrent.thread
import okhttp3.OkHttpClient
import okhttp3.Request
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.p2p.api.model.Login

class CrowdestorWebViewClient(
    private val context: Context?,
    private val layoutInflater: LayoutInflater,
    private val login: Login,
    private val listener: BaseWebViewNavigator,
    private val isLogin: Boolean,
    private val frequentUpdates: Boolean
) : BaseWebViewClient() {

    private var isTfaActive = false
    private var tfaSubmitted = false
    private val clipboardManager =
        context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

    @SuppressWarnings("deprecation")
    override fun onReceivedError(
        view: WebView?,
        errorCode: Int,
        description: String?,
        failingUrl: String?
    ) {
        if (errorCode == -2) {
            if (isLogin) {
                listener.onLoginResult(NETWORK, login.username, login.password, frequentUpdates)
            } else {
                listener.onError(PlatformEnum.CROWDESTOR, 0)
            }
        } else {
            super.onReceivedError(view, errorCode, description, failingUrl)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError?
    ) {
        if (error?.errorCode == -2) {
            if (isLogin) {
                listener.onLoginResult(NETWORK, login.username, login.password, frequentUpdates)
            } else {
                listener.onError(PlatformEnum.CROWDESTOR, 0)
            }
        } else {
            super.onReceivedError(view, request, error)
        }
    }

    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        return if (request?.url.toString() == "https://crowdestor.com/en/clients/dashboard") {
            handleRequestViaOkHttp(request?.url.toString())
            super.shouldOverrideUrlLoading(view, request)
        } else {
            super.shouldOverrideUrlLoading(view, request)
        }
    }

    private fun handleRequestViaOkHttp(url: String) {
        val httpClient = OkHttpClient()
        thread {
            try {
                val request = Request.Builder().url(url).build()
                print("Request: $request")
                val response = httpClient.newCall(request).execute()
                println("Response: " + response.headers.toString())

                if (response.header("refresh") == "0;url=https://crowdestor.com/en/clients/auth2f") {
                    isTfaActive = true
                } else {
                    tfaSubmitted = true
                }
            } catch (e: Exception) {
            }
        }
    }

    override fun shouldInterceptRequest(
        view: WebView?,
        request: WebResourceRequest?
    ): WebResourceResponse? {

        if (request?.url.toString() == "https://crowdestor.com/en/clients/dashboard") {
            return super.shouldInterceptRequest(view, request)
        }
        return super.shouldInterceptRequest(view, request)
    }

    override fun onPageFinished(view: WebView, url: String) {
        super.onPageFinished(view, url)
        if (url == "https://crowdestor.com/en/account") {
            val js =
                "javascript:" +
                    "document.getElementById('login_identity').value = '" + login.username + "';" +
                    "document.querySelector('input[name=login_password]').value='" + login.password + "';" +
                    "document.querySelector('input[value=Login]').click();"
            loadUrl(js, view)
        } else if (url == "https://crowdestor.com/en/clients/auth2f") {
            showTwoFactorDialog(view)
        } else if (url == "https://crowdestor.com/en/clients/dashboard" && tfaSubmitted) {
            val cookies = CookieManager.getInstance().getCookie(url)
            if (isLogin) {
                listener.onLoginResult(
                    SUCCESS,
                    login.username,
                    login.password,
                    frequentUpdates,
                    cookies
                )
            } else {
                listener.loadInfo(WebViewData(PlatformEnum.CROWDESTOR, cookies))
            }
        }
    }

    private fun showTwoFactorDialog(view: WebView) {
        // create an alert builder
        val builder = MaterialAlertDialogBuilder(context!!)
        builder.setTitle("Two-factor authentication")
        // set the custom layout
        val customLayout = layoutInflater.inflate(R.layout.dialog_two_factor, null)
        builder.setView(customLayout)
        builder.setCancelable(false)
        // add a button
        builder.setPositiveButton("OK") { _, _ ->
            // send data from the AlertDialog to the Activity
            val editText = customLayout.findViewById(R.id.input_two_factor) as TextInputEditText
            val code = editText.text.toString()
            val js = "javascript:" +
                "document.querySelector('input[name=auth_code]').value='" + code + "';" +
                "document.querySelector('button[type=submit]').click();"
            loadUrl(js, view)
            tfaSubmitted = true
        }
        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()

        val editText = customLayout.findViewById<TextInputEditText>(R.id.input_two_factor)
        editText.setOnClickListener {
            if (isValidPaste()) {
                val text = getTextFromClipboard()
                (it as TextInputEditText).setText(text)
            }
        }

        editText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus && isValidPaste()) {
                val text = getTextFromClipboard()
                editText.setText(text)
            }
        }
    }

    private fun isValidPaste(): Boolean =
        if (clipboardManager.primaryClipDescription?.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN) == true) {
            val text = getTextFromClipboard()
            text != null && TextUtils.isDigitsOnly(text) && text.length == 6
        } else {
            false
        }

    private fun getTextFromClipboard() = clipboardManager.primaryClip?.getItemAt(0)
        ?.text?.toString()
        ?.replace(Regex("[^\\d.]"), "")

    override fun getInitialUrl() = "https://crowdestor.com/en/account"
}
