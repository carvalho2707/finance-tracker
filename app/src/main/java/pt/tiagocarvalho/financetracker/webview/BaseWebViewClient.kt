package pt.tiagocarvalho.financetracker.webview

import android.content.Context
import android.view.LayoutInflater
import android.webkit.WebView
import android.webkit.WebViewClient
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.p2p.api.model.Login

abstract class BaseWebViewClient : WebViewClient() {

    abstract fun getInitialUrl(): String

    companion object {

        fun getClient(
            context: Context?,
            layoutInflater: LayoutInflater,
            platformEnum: PlatformEnum,
            login: Login,
            navigator: BaseWebViewNavigator,
            isLogin: Boolean,
            frequentUpdates: Boolean
        ): BaseWebViewClient = when (platformEnum) {
            PlatformEnum.MINTOS -> MintosWebViewClient(
                context,
                layoutInflater,
                login,
                navigator,
                isLogin,
                frequentUpdates
            )
            PlatformEnum.BONDORA -> BondoraWebViewClient(
                login,
                navigator,
                frequentUpdates
            )
            PlatformEnum.GRUPEER -> GrupeerWebViewClient(
                context,
                layoutInflater,
                login,
                navigator,
                isLogin,
                frequentUpdates
            )
            PlatformEnum.CROWDESTOR -> CrowdestorWebViewClient(
                context,
                layoutInflater,
                login,
                navigator,
                isLogin,
                frequentUpdates
            )
            else -> throw UnsupportedOperationException("Unknown platform enum: $platformEnum")
        }

        const val SUCCESS = 0
        const val FAILURE = 1
        const val NETWORK = 2
    }

    protected fun loadUrl(url: String, view: WebView) {
        view.evaluateJavascript(url) { }
    }
}
