package pt.tiagocarvalho.financetracker.webview

import android.annotation.TargetApi
import android.content.ClipDescription
import android.content.ClipboardManager
import android.content.Context
import android.os.Build
import android.text.TextUtils
import android.view.LayoutInflater
import android.webkit.CookieManager
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputEditText
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.p2p.api.model.Login

class GrupeerWebViewClient(
    private val context: Context?,
    private val layoutInflater: LayoutInflater,
    private val login: Login,
    private val listener: BaseWebViewNavigator,
    private val isLogin: Boolean,
    private val frequentUpdates: Boolean
) : BaseWebViewClient() {

    private var inputFilled = false
    private val clipboardManager =
        context!!.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager

    @SuppressWarnings("deprecation")
    override fun onReceivedError(
        view: WebView?,
        errorCode: Int,
        description: String?,
        failingUrl: String?
    ) {
        if (errorCode == -2) {
            if (isLogin) {
                listener.onLoginResult(NETWORK, login.username, login.password, frequentUpdates)
            } else {
                listener.onError(PlatformEnum.GRUPEER, 0)
            }
        } else {
            super.onReceivedError(view, errorCode, description, failingUrl)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError?
    ) {
        if (error?.errorCode == -2) {
            if (isLogin) {
                listener.onLoginResult(NETWORK, login.username, login.password, frequentUpdates)
            } else {
                listener.onError(PlatformEnum.GRUPEER, 0)
            }
        } else {
            super.onReceivedError(view, request, error)
        }
    }

    override fun onPageFinished(view: WebView, url: String) {
        super.onPageFinished(view, url)
        if (url.startsWith("https://www.grupeer.com/login")) {
            if (inputFilled) {
                showTwoFactorDialog(view)
            } else {
                val js =
                    "javascript:" +
                        "document.getElementById('email').value = '" + login.username + "';" +
                        "document.getElementById('password').value='" + login.password + "';" +
                        "document.getElementsByClassName('btn button-blue w-250')[0].click()"
                loadUrl(js, view)
                inputFilled = true
            }
        } else if (url == "https://www.grupeer.com/" || url.contains("overview")) {
            val cookies = CookieManager.getInstance().getCookie(url)
            if (isLogin) {
                listener.onLoginResult(
                    SUCCESS,
                    login.username,
                    login.password,
                    frequentUpdates,
                    cookies
                )
            } else {
                listener.loadInfo(WebViewData(PlatformEnum.GRUPEER, cookies))
            }
        }
    }

    private fun showTwoFactorDialog(view: WebView) {
        // create an alert builder
        val builder = MaterialAlertDialogBuilder(context!!)
        builder.setTitle("Two-factor authentication")
        // set the custom layout
        val customLayout = layoutInflater.inflate(R.layout.dialog_two_factor, null)
        builder.setView(customLayout)
        builder.setCancelable(false)
        // add a button
        builder.setPositiveButton("OK") { _, _ ->
            // send data from the AlertDialog to the Activity
            val editText = customLayout.findViewById(R.id.input_two_factor) as TextInputEditText
            val code = editText.text.toString()
            val js = "javascript:" +
                "document.getElementById('google_2fa').value = '" + code + "';" +
                "document.getElementsByClassName('btn button-blue w-250')[0].click()"
            loadUrl(js, view)
        }
        // create and show the alert dialog
        val dialog = builder.create()
        dialog.show()

        val editText = customLayout.findViewById<TextInputEditText>(R.id.input_two_factor)
        editText.setOnClickListener {
            if (isValidPaste()) {
                val text = getTextFromClipboard()
                (it as TextInputEditText).setText(text)
            }
        }

        editText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus && isValidPaste()) {
                val text = getTextFromClipboard()
                editText.setText(text)
            }
        }
    }

    private fun isValidPaste(): Boolean =
        if (clipboardManager.primaryClipDescription?.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN) == true) {
            val text = getTextFromClipboard()
            text != null && TextUtils.isDigitsOnly(text) && text.length == 6
        } else {
            false
        }

    private fun getTextFromClipboard() = clipboardManager.primaryClip?.getItemAt(0)
        ?.text?.toString()
        ?.replace(Regex("[^\\d.]"), "")

    override fun getInitialUrl() = "https://www.grupeer.com/login"
}
