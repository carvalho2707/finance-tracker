package pt.tiagocarvalho.financetracker.webview

import android.annotation.TargetApi
import android.net.Uri
import android.os.Build
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import java.net.URLDecoder
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.p2p.api.model.Login

class BondoraWebViewClient(
    private val login: Login,
    private val listener: BaseWebViewNavigator,
    private val frequentUpdates: Boolean
) : BaseWebViewClient() {

    @SuppressWarnings("deprecation")
    override fun onReceivedError(
        view: WebView?,
        errorCode: Int,
        description: String?,
        failingUrl: String?
    ) {
        if (errorCode == -2) {
            listener.onLoginResult(NETWORK, login.username, login.password, frequentUpdates)
        } else {
            super.onReceivedError(view, errorCode, description, failingUrl)
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest?,
        error: WebResourceError?
    ) {
        if (error?.errorCode == -2) {
            listener.onLoginResult(NETWORK, login.username, login.password, frequentUpdates)
        } else {
            super.onReceivedError(view, request, error)
        }
    }

    override fun onPageFinished(view: WebView, url: String?) {
        super.onPageFinished(view, url)

        if (url == null) {
            return
        }

        val urlDecoded = URLDecoder.decode(url, "UTF-8")

        if (urlDecoded.endsWith(INPUT_URL)) {
            var js =
                "javascript:document.getElementsByClassName('form-control js-login-email')[0].value = '" +
                    login.username + "';document.getElementsByName('Password')[0].value='" +
                    login.password + "';"
            loadUrl(js, view)

            js =
                "javascript:document.getElementsByClassName('btn btn--lg btn--primary btn-block')[0].click()"
            loadUrl(js, view)
        } else if (url == FORM_ACCEPT_URL) {
            val js =
                "javascript:document.getElementsByName('cancel')[1].click()"
            loadUrl(js, view)
        }
    }

    override fun shouldOverrideUrlLoading(webView: WebView?, url: String): Boolean =
        shouldOverrideUrlLoading(url, webView)

    @TargetApi(Build.VERSION_CODES.N)
    override fun shouldOverrideUrlLoading(webView: WebView?, request: WebResourceRequest): Boolean {
        val uri: Uri = request.url
        return shouldOverrideUrlLoading(uri.toString(), webView)
    }

    private fun shouldOverrideUrlLoading(url: String, webView: WebView?): Boolean {
        if (url.startsWith(REDIRECT_URL)) {
            val uri = Uri.parse(url)
            val code = uri.getQueryParameter("code")
            code?.let {
                listener.onLoginResult(
                    SUCCESS,
                    login.username,
                    login.password,
                    frequentUpdates,
                    it
                )
            }
        } else {
            webView?.loadUrl(url)
        }
        return true // Returning True means that application wants to leave the current WebView and handle the url itself, otherwise return false.
    }

    override fun getInitialUrl() = "https://www.bondora.com/oauth/authorize" +
        "?response_type=code" +
        "&client_id=" + BuildConfig.BONDORA_CLIENT_ID +
        "&state=xyz" +
        "&scope=ReportCreate%20BidsRead%20Investments%20ReportRead" +
        "&redirect_uri=https%3A%2F%2Ftestetiago.com%3A8080%2Ftest"

    companion object {
        private const val INPUT_URL =
            "login?ReturnUrl=/oauth/authorize?response_type=code&client_id=" +
                BuildConfig.BONDORA_CLIENT_ID + "&state=xyz&scope=ReportCreate%20BidsRead%20Investments%20ReportRead" +
                "&redirect_uri=https%3A%2F%2Ftestetiago.com%3A8080%2Ftest&response_type=code&client_id=" +
                BuildConfig.BONDORA_CLIENT_ID + "&state=xyz&scope=ReportCreate BidsRead Investments ReportRead&redirect_uri=https://testetiago.com:8080/test"
        private const val FORM_ACCEPT_URL =
            "https://www.bondora.com/oauth/authorize?response_type=code&client_id=" + BuildConfig.BONDORA_CLIENT_ID + "&state=xyz&scope=ReportCreate%20BidsRead%20Investments%20ReportRead&redirect_uri=https://testetiago.com:8080/test"
        private const val REDIRECT_URL = "https://testetiago.com:8080/test"
    }
}
