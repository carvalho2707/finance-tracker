package pt.tiagocarvalho.financetracker.webview

import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.WebViewData

interface BaseWebViewNavigator {

    fun onLoginResult(
        loginStatus: Int,
        username: String,
        password: String,
        frequentUpdates: Boolean,
        token: String? = null
    )

    fun loadInfo(webViewData: WebViewData?)

    fun onError(platformEnum: PlatformEnum, errorCode: Int)
}
