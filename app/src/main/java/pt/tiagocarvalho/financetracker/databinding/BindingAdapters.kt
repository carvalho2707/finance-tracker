/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package pt.tiagocarvalho.financetracker.databinding

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale
import pt.tiagocarvalho.financetracker.R
import pt.tiagocarvalho.financetracker.model.DisplayMode

/**
 * Data Binding adapters specific to the app.
 */
object BindingAdapters {
    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    private const val BALANCE_ANIMATION_DURATION = 500L

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    @JvmStatic
    @BindingAdapter("visibleGone")
    fun showHide(view: View, show: Boolean) {
        view.visibility = if (show) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("textBalanceAnimated")
    fun textBalanceAnimated(view: TextView, value: BigDecimal) {
        if (value == BigDecimal.ZERO) {
            view.text = numberFormat.format(0.0)
            return
        }
        val valueAnimator = ValueAnimator.ofFloat(0.00f, value.toFloat())
        valueAnimator.duration = BALANCE_ANIMATION_DURATION
        valueAnimator.addUpdateListener { valueAnimator1 ->
            view.text = valueAnimator1.animatedValue.toString()
        }
        valueAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                view.text = numberFormat.format(value.toDouble())
            }
        })
        valueAnimator.start()
    }

    @JvmStatic
    @BindingAdapter(value = ["value", "percentage", "displayMode", "showAsSubtitle"])
    fun textVariation(
        view: TextView,
        value: BigDecimal,
        percentage: BigDecimal,
        displayMode: DisplayMode,
        showAsSubtitle: Boolean
    ) {
        val total: String
        val textId: Int

        if (displayMode == DisplayMode.ABSOLUTE) {
            total = numberFormat.format(value.toDouble())
            textId = if (showAsSubtitle) {
                R.string.accounts_variation_value
            } else {
                R.string.item_variation_value
            }
        } else {
            total = numberFormat.format(percentage.toDouble())
            textId = if (showAsSubtitle) {
                R.string.accounts_variation_percentage
            } else {
                R.string.item_variation_percentage
            }
        }
        val text = view.resources.getText(textId).toString()
        view.text = String.format(text, total)
    }

    @JvmStatic
    @BindingAdapter("textCurrency")
    fun textCurrency(view: TextView, value: BigDecimal) {
        view.text = numberFormat.format(value.toDouble())
    }

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImageResource(imageView: ImageView, @DrawableRes resource: Int) {
        imageView.setImageResource(resource)
    }
}
