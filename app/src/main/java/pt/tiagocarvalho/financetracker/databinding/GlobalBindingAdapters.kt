package pt.tiagocarvalho.financetracker.databinding

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("visibleInvisible")
fun visibleInvisible(view: View, show: Boolean) {
    view.visibility = if (show) View.VISIBLE else View.INVISIBLE
}
