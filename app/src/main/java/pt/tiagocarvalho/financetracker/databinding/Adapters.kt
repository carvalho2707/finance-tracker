package pt.tiagocarvalho.financetracker.databinding

import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter

@BindingAdapter("visibleGone")
fun showHide(view: View, show: Boolean) {
    view.visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("imageRes")
fun setImageResource(imageView: ImageView, @DrawableRes resource: Int) {
    imageView.setImageResource(resource)
}
