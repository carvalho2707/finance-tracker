package pt.tiagocarvalho.financetracker.databinding

import android.content.Intent
import android.net.Uri
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.URLSpan
import android.util.Patterns
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.GlideApp

class FragmentBindingAdapters @Inject constructor(val fragment: Fragment) {

    @BindingAdapter("android:textDate")
    fun setTextDate(view: TextView, date: Date) {
        val format = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        val formatted = format.format(date)
        view.text = formatted
    }

    @BindingAdapter("android:imageUrl")
    fun setImageUrl(view: ImageView, url: String?) {
        GlideApp.with(view.context).load(url).placeholder(android.R.drawable.ic_menu_help)
            .error(android.R.drawable.ic_menu_help).into(view)
    }

    /**
     * Enables click support for a TextView from a [fullText] String, which one containing one or multiple URLs.
     */
    @BindingAdapter("android:textWithUrl")
    fun setTextWithLinkSupport(
        textView: TextView,
        fullText: String
    ) {
        val spannable = SpannableString(fullText)
        val matcher = Patterns.WEB_URL.matcher(spannable)
        while (matcher.find()) {
            val url = spannable.toString().substring(matcher.start(), matcher.end())
            val urlSpan = object : URLSpan(fullText) {
                override fun onClick(widget: View) {
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    textView.context.startActivity(i)
                }
            }
            spannable.setSpan(
                urlSpan,
                matcher.start(),
                matcher.end(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        textView.text = spannable
        textView.movementMethod = LinkMovementMethod.getInstance() // Make link clickable
    }
}
