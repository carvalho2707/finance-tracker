package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.StatementsStatus
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.utils.toStringPattern

@Singleton
class StatementsRepository @Inject constructor(
    private val platformDao: PlatformDao,
    private val platformStatementDao: PlatformStatementDao,
    private val preferencesHelper: PreferencesHelper
) {

    fun loadStatements(): Observable<List<StatementsStatus>> = Observable.create { emitter ->
        try {
            val list = platformDao.getPlatforms()
                .map {
                    val count = platformStatementDao.getStatementsCount(it.name)
                    val lastUpdateDate: Long =
                        if (it.type.platformTypeEnum == PlatformTypeEnum.P2P) {
                            preferencesHelper.get(it.type.prefLastFullStatementScan, 0L)
                        } else {
                            preferencesHelper.get(
                                it.type.prefLastFullStatementScan + it.name,
                                0L
                            )
                        }
                    StatementsStatus(
                        it.type,
                        it.name,
                        count,
                        mapToLastUpdate(lastUpdateDate),
                        it.type == PlatformEnum.SAVINGS || it.type == PlatformEnum.BETS || it.type == PlatformEnum.CASH
                    )
                }
                .sortedByDescending { it.count }
            emitter.onNext(list)
        } catch (exception: Exception) {
            emitter.onError(exception)
        }
    }

    fun deleteStatements(name: String): Completable =
        platformStatementDao.deleteByNameCompletable(name)

    private fun mapToLastUpdate(ts: Long): String {
        return if (ts == 0L) "Never" else ts.toStringPattern(Constants.STATEMENT_DATE_PATTERN)
    }
}
