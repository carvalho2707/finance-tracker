package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.utils.Utils

@Singleton
class RefreshRepository @Inject constructor(
    private val grupeerRepository: GrupeerRepository,
    private val mintosRepository: MintosRepository,
    private val peerberryRepository: PeerberryRepository,
    private val raizeRepository: RaizeRepository,
    private val robocashRepository: RobocashRepository,
    private val savingsRepository: SavingsRepository,
    private val gamblingRepository: GamblingRepository,
    private val bondoraRepository: BondoraRepository,
    private val twinoRepository: TwinoRepository,
    private val crowdestorRepository: CrowdestorRepository,
    private val estateGuruRepository: EstateGuruRepository,
    private val iuvoRepository: IuvoRepository,
    private val lenderMarketRepository: LenderMarketRepository,
    private val platformDetailsDao: PlatformDetailsDao,
    private val platformStatementDao: PlatformStatementDao
) {

    fun refreshDetails(loadedData: MutableMap<PlatformEnum, WebViewData>): Completable =
        Completable.mergeArray(
            grupeerRepository.syncDetails(loadedData[PlatformEnum.GRUPEER]?.data),
            mintosRepository.syncDetails(loadedData[PlatformEnum.MINTOS]?.data),
            peerberryRepository.syncDetails(loadedData[PlatformEnum.PEERBERRY]?.data),
            raizeRepository.syncDetails(loadedData[PlatformEnum.RAIZE]?.data),
            robocashRepository.syncDetails(loadedData[PlatformEnum.ROBOCASH]?.data),
            bondoraRepository.syncDetails(false),
            crowdestorRepository.syncDetails(loadedData[PlatformEnum.CROWDESTOR]?.data),
            twinoRepository.syncDetails(loadedData[PlatformEnum.TWINO]?.data),
            estateGuruRepository.syncDetails(),
            iuvoRepository.syncDetails(),
            lenderMarketRepository.syncDetails(),
            refreshFullStatements(loadedData)
        )

    fun refreshFullStatements(loadedData: MutableMap<PlatformEnum, WebViewData>): Completable =
        Completable.mergeArray(
            grupeerRepository.syncStatements(loadedData[PlatformEnum.GRUPEER]?.data),
            mintosRepository.syncStatements(loadedData[PlatformEnum.MINTOS]?.data),
            peerberryRepository.syncStatements(loadedData[PlatformEnum.PEERBERRY]?.data),
            raizeRepository.syncStatements(loadedData[PlatformEnum.RAIZE]?.data),
            robocashRepository.syncStatements(loadedData[PlatformEnum.ROBOCASH]?.data),
            bondoraRepository.syncStatements(false),
            crowdestorRepository.syncStatements(loadedData[PlatformEnum.CROWDESTOR]?.data),
            twinoRepository.syncStatements(loadedData[PlatformEnum.TWINO]?.data),
            estateGuruRepository.syncStatements(),
            iuvoRepository.syncStatements(),
            lenderMarketRepository.syncStatements()
        )

    fun isRefreshDetailsNeeded(force: Boolean): Single<Boolean> {
        val observables = listOf(
            Single.just(force),
            grupeerRepository.isRefreshDetailsNeeded(),
            mintosRepository.isRefreshDetailsNeeded(),
            peerberryRepository.isRefreshDetailsNeeded(),
            raizeRepository.isRefreshDetailsNeeded(),
            robocashRepository.isRefreshDetailsNeeded(),
            savingsRepository.isRefreshDetailsNeeded(),
            gamblingRepository.isRefreshDetailsNeeded(),
            bondoraRepository.isRefreshDetailsNeeded(),
            crowdestorRepository.isRefreshDetailsNeeded(),
            twinoRepository.isRefreshDetailsNeeded(),
            estateGuruRepository.isRefreshDetailsNeeded(),
            iuvoRepository.isRefreshDetailsNeeded(),
            lenderMarketRepository.isRefreshDetailsNeeded()
        )
        return Single.zip(
            observables
        ) { it.any { any -> any as Boolean } }
    }

    fun isRefreshStatementsNeeded(): Single<Boolean> {
        val observables = listOf(
            grupeerRepository.isRefreshStatementsNeeded(),
            mintosRepository.isRefreshStatementsNeeded(),
            peerberryRepository.isRefreshStatementsNeeded(),
            raizeRepository.isRefreshStatementsNeeded(),
            robocashRepository.isRefreshStatementsNeeded(),
            bondoraRepository.isRefreshStatementsNeeded(),
            crowdestorRepository.isRefreshStatementsNeeded(),
            twinoRepository.isRefreshStatementsNeeded(),
            estateGuruRepository.isRefreshStatementsNeeded(),
            iuvoRepository.isRefreshStatementsNeeded(),
            lenderMarketRepository.isRefreshStatementsNeeded()
        )
        return Single.zip(
            observables
        ) { it.any { any -> any as Boolean } }
    }

    fun shouldRefreshDailyStatements(name: String): Boolean = when (PlatformEnum.valueOf(name)) {
        PlatformEnum.MINTOS -> mintosRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.ROBOCASH -> robocashRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.PEERBERRY -> peerberryRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.GRUPEER -> grupeerRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.RAIZE -> raizeRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.BONDORA -> bondoraRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.TWINO -> twinoRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.CROWDESTOR -> crowdestorRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.ESTATEGURU -> estateGuruRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.IUVO -> iuvoRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.LENDERMARKET -> lenderMarketRepository.shouldRefreshDailyStatements(false)
        PlatformEnum.BETS,
        PlatformEnum.SAVINGS,
        PlatformEnum.CASH -> false
    }

    fun loadDetails(
        shouldFetch: Boolean,
        name: String,
        data: String? = null
    ): Single<Resource<PlatformDetails>> = when (PlatformEnum.valueOf(name)) {
        PlatformEnum.MINTOS -> mintosRepository.loadDetails(shouldFetch, data)
        PlatformEnum.ROBOCASH -> robocashRepository.loadDetails(shouldFetch, data)
        PlatformEnum.PEERBERRY -> peerberryRepository.loadDetails(shouldFetch, data)
        PlatformEnum.GRUPEER -> grupeerRepository.loadDetails(shouldFetch, data)
        PlatformEnum.RAIZE -> raizeRepository.loadDetails(shouldFetch, data)
        PlatformEnum.BONDORA -> bondoraRepository.loadDetails(true)
        PlatformEnum.TWINO -> twinoRepository.loadDetails(shouldFetch, data)
        PlatformEnum.CROWDESTOR -> crowdestorRepository.loadDetails(shouldFetch, data)
        PlatformEnum.ESTATEGURU -> estateGuruRepository.loadDetails(shouldFetch)
        PlatformEnum.IUVO -> iuvoRepository.loadDetails(shouldFetch)
        PlatformEnum.LENDERMARKET -> lenderMarketRepository.loadDetails(shouldFetch)
        PlatformEnum.BETS,
        PlatformEnum.SAVINGS,
        PlatformEnum.CASH -> throw IllegalArgumentException()
    }

    fun loadDetailsOffline(
        platformEnum: PlatformEnum
    ): Single<Resource<PlatformDetails>> = when (platformEnum) {
        PlatformEnum.MINTOS,
        PlatformEnum.RAIZE,
        PlatformEnum.GRUPEER,
        PlatformEnum.ROBOCASH,
        PlatformEnum.PEERBERRY,
        PlatformEnum.TWINO,
        PlatformEnum.BONDORA ->
            platformDetailsDao.getDetailsByNameAsSingle(platformEnum.name)
                .map {
                    Resource.success(
                        it
                    )
                }
        else -> throw IllegalArgumentException()
    }

    fun loadStatements(
        shouldFetch: Boolean,
        name: String,
        data: String? = null
    ): Observable<Pair<Boolean, List<PlatformStatement>>> = when (PlatformEnum.valueOf(name)) {
        PlatformEnum.MINTOS -> mintosRepository.loadStatements(shouldFetch, data)
        PlatformEnum.ROBOCASH -> robocashRepository.loadStatements(shouldFetch, data)
        PlatformEnum.PEERBERRY -> peerberryRepository.loadStatements(shouldFetch, data)
        PlatformEnum.GRUPEER -> grupeerRepository.loadStatements(shouldFetch, data)
        PlatformEnum.RAIZE -> raizeRepository.loadStatements(shouldFetch, data)
        PlatformEnum.BONDORA -> bondoraRepository.loadStatements(false)
        PlatformEnum.TWINO -> twinoRepository.loadStatements(shouldFetch, data)
        PlatformEnum.CROWDESTOR -> crowdestorRepository.loadStatements(shouldFetch, data)
        PlatformEnum.ESTATEGURU -> estateGuruRepository.loadStatements(shouldFetch)
        PlatformEnum.IUVO -> iuvoRepository.loadStatements(shouldFetch)
        PlatformEnum.LENDERMARKET -> lenderMarketRepository.loadStatements(shouldFetch)
        PlatformEnum.BETS,
        PlatformEnum.SAVINGS,
        PlatformEnum.CASH -> throw IllegalArgumentException()
    }

    fun loadStatementsOffline(
        platformEnum: PlatformEnum
    ): Observable<Pair<Boolean, List<PlatformStatement>>> = when (platformEnum) {
        PlatformEnum.MINTOS,
        PlatformEnum.RAIZE,
        PlatformEnum.GRUPEER,
        PlatformEnum.PEERBERRY,
        PlatformEnum.TWINO,
        PlatformEnum.ROBOCASH,
        PlatformEnum.BONDORA -> platformStatementDao.getDailyStatementsByNameAsObservable(
            platformEnum.name,
            Utils.getTodayDate()
        ).map { Pair(false, it) }
        else -> throw IllegalArgumentException()
    }

    fun login(
        platform: PlatformEnum,
        email: String,
        password: String,
        data: String = ""
    ): Single<PlatformDetails> = when (platform) {
        PlatformEnum.MINTOS -> mintosRepository.login(data)
        PlatformEnum.ROBOCASH -> robocashRepository.login(data)
        PlatformEnum.PEERBERRY -> peerberryRepository.login(data)
        PlatformEnum.GRUPEER -> grupeerRepository.login(data)
        PlatformEnum.RAIZE -> raizeRepository.login(data)
        PlatformEnum.BONDORA -> bondoraRepository.login(data, email, password)
        PlatformEnum.TWINO -> twinoRepository.login(data)
        PlatformEnum.CROWDESTOR -> crowdestorRepository.login(data)
        PlatformEnum.ESTATEGURU -> estateGuruRepository.login(email, password)
        PlatformEnum.IUVO -> iuvoRepository.login(email, password)
        PlatformEnum.LENDERMARKET -> lenderMarketRepository.login(email, password)
        PlatformEnum.BETS,
        PlatformEnum.SAVINGS,
        PlatformEnum.CASH -> throw IllegalArgumentException()
    }
}
