package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.data.remote.RaizeService
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.utils.Utils.getTodayDate
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLoggerImpl
import pt.tiagocarvalho.financetracker.utils.log.Logger

@Singleton
class RaizeRepository @Inject constructor(
    private val raizeService: RaizeService,
    override val platformDao: PlatformDao,
    private val platformDetailsDao: PlatformDetailsDao,
    private val platformStatementDao: PlatformStatementDao,
    override val preferencesHelper: PreferencesHelper,
    override val logger: Logger,
    private val analyticsLogger: AnalyticsLogger
) : BaseRepository(preferencesHelper, logger, platformDao, PlatformEnum.RAIZE) {

    fun login(token: String): Single<PlatformDetails> = raizeService.getDetails(token)
        .doOnSuccess {
            analyticsLogger.logLogin(
                this.platform
            )
        }
        .map { convertThirdPartyDetails(it) }

    fun loadDetails(force: Boolean, token: String?): Single<Resource<PlatformDetails>> =
        if (shouldRefreshDetails(force) && token != null) {
            raizeService.getDetails(token)
                .doOnSuccess {
                    analyticsLogger.logDetailsRefresh(
                        AnalyticsLoggerImpl.REFRESH_REMOTE,
                        this.platform
                    )
                }
                .map { convertThirdPartyDetails(it) }
                .flatMap {
                    platformDetailsDao.insertCompletable(it)
                        .doOnComplete { updateLastDetailsTime() }
                        .andThen(platformDetailsDao.getDetailsByNameAsSingle(platform.name))
                        .map { t -> Resource.success(t) }
                }
                .onErrorResumeNext { t: Throwable ->
                    Completable.fromAction { logger.log(t) }
                        .andThen(platformDetailsDao.getDetailsByNameAsSingle(platform.name))
                        .map { r ->
                            Resource.error(
                                t::class.java.canonicalName ?: "Something went wrong :(",
                                r
                            )
                        }
                }
        } else {
            Single.fromCallable {
                analyticsLogger.logDetailsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
                .flatMap {
                    platformDetailsDao.getDetailsByNameAsSingle(platform.name)
                        .map { Resource.success(it) }
                }
        }

    fun syncDetails(token: String?): Completable =
        if (shouldRefreshDetails(false) && token != null) {
            platformDao.getPlatformByNameAsMaybe(platform.name)
                .flatMapCompletable {
                    if (it.frequentUpdates) {
                        raizeService.getDetails(token)
                            .doOnSuccess {
                                analyticsLogger.logDetailsRefresh(
                                    AnalyticsLoggerImpl.REFRESH_REMOTE,
                                    this.platform
                                )
                            }
                            .map { info -> convertThirdPartyDetails(info) }
                            .flatMapCompletable { details ->
                                platformDetailsDao.insertCompletable(details)
                                    .doOnComplete { updateLastDetailsTime() }
                            }
                    } else {
                        Completable.fromAction {
                            analyticsLogger.logDetailsRefresh(
                                AnalyticsLoggerImpl.REFRESH_LOCAL,
                                this.platform
                            )
                        }
                    }
                }
                .doOnError { logger.log(it) }
                .onErrorComplete()
        } else {
            Completable.fromAction {
                analyticsLogger.logDetailsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
        }

    fun syncStatements(token: String?): Completable = if (shouldRefreshFullStatements()) {
        val date = getLastStatementUpdateDate()
        platformDao.getPlatformByNameAsMaybe(platform.name)
            .flatMapCompletable { platform ->
                if (platform.frequentUpdates) {
                    raizeService.getStatements(token!!, date)
                        .doOnSuccess {
                            analyticsLogger.logFullStatementsRefresh(
                                AnalyticsLoggerImpl.REFRESH_REMOTE,
                                this.platform
                            )
                        }
                        .doOnSuccess { logInvalidStatements(it.issues) }
                        .map { model ->
                            Pair(
                                model.issues.isNotEmpty(),
                                model.statements.map(this::convertStatement)
                            )
                        }
                        .flatMapCompletable {
                            platformStatementDao.insertStatementsCompletable(it.second)
                                .doOnComplete { if (it.first.not()) updateLastFullStatementsTime() }
                        }
                } else {
                    Completable.fromAction {
                        analyticsLogger.logFullStatementsRefresh(
                            AnalyticsLoggerImpl.REFRESH_LOCAL,
                            this.platform
                        )
                    }
                }
            }
            .doOnError { logger.log(it) }
            .onErrorComplete()
    } else {
        Completable.fromAction {
            analyticsLogger.logFullStatementsRefresh(
                AnalyticsLoggerImpl.REFRESH_LOCAL,
                this.platform
            )
        }
    }

    fun loadStatements(
        force: Boolean,
        token: String?
    ): Observable<Pair<Boolean, List<PlatformStatement>>> =
        if (shouldRefreshDailyStatements(force) && token != null) {
            raizeService.getDailyStatements(token)
                .doOnSuccess {
                    analyticsLogger.logStatementsRefresh(
                        AnalyticsLoggerImpl.REFRESH_REMOTE,
                        this.platform
                    )
                }
                .doOnSuccess { logInvalidStatements(it.issues) }
                .map { model ->
                    Pair(
                        model.issues.isNotEmpty(),
                        model.statements.map(this::convertStatement)
                    )
                }
                .flatMapObservable { result ->
                    platformStatementDao.insertStatementsCompletable(result.second)
                        .doOnComplete { if (!result.first) updateLastStatementsTime() }
                        .andThen(
                            platformStatementDao.getDailyStatementsByNameAsObservable(
                                platform.name, getTodayDate()
                            ).map { statements -> Pair(result.first, statements) }
                        )
                }
                .onErrorResumeNext { t: Throwable ->
                    Completable.fromAction { logger.log(t) }
                        .andThen(
                            platformStatementDao.getDailyStatementsByNameAsObservable(
                                platform.name,
                                getTodayDate()
                            )
                        )
                        .map { Pair(true, it) }
                }
        } else {
            Observable.fromCallable {
                analyticsLogger.logStatementsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
                .flatMap {
                    platformStatementDao.getDailyStatementsByNameAsObservable(
                        platform.name,
                        getTodayDate()
                    )
                        .map { Pair(false, it) }
                }
        }
}
