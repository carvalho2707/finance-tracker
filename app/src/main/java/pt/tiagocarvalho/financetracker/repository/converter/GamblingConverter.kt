package pt.tiagocarvalho.financetracker.repository.converter

import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.GamblingInfo
import pt.tiagocarvalho.financetracker.model.PlatformEnum

object GamblingConverter {

    fun convert(gamblingAccount: GamblingInfo): PlatformDetails = PlatformDetails(
        name = gamblingAccount.name,
        platform = PlatformEnum.BETS,
        platformType = PlatformEnum.BETS.platformTypeEnum,
        balance = gamblingAccount.totalBalance,
        deposits = gamblingAccount.totalDeposits,
        withdrawals = gamblingAccount.totalWithdrawals,
        profit = gamblingAccount.profit,
        interest = gamblingAccount.interest,
        serviceFees = gamblingAccount.serviceFees,
        campaignRewards = gamblingAccount.bonus,
        changeValue = gamblingAccount.changeValue,
        changePercentage = gamblingAccount.changePercentage
    )
}
