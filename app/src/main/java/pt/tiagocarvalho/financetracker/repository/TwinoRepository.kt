package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.data.remote.TwinoService
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLoggerImpl
import pt.tiagocarvalho.financetracker.utils.log.Logger

@Singleton
class TwinoRepository @Inject constructor(
    private val twinoService: TwinoService,
    override val platformDao: PlatformDao,
    private val platformDetailsDao: PlatformDetailsDao,
    private val platformStatementDao: PlatformStatementDao,
    override val preferencesHelper: PreferencesHelper,
    override val logger: Logger,
    private val analyticsLogger: AnalyticsLogger
) : BaseRepository(preferencesHelper, logger, platformDao, PlatformEnum.TWINO) {

    fun login(sessionId: String): Single<PlatformDetails> =
        twinoService.getDetails(sessionId)
            .doOnSuccess {
                analyticsLogger.logLogin(
                    this.platform
                )
            }
            .map { convertThirdPartyDetails(it) }

    fun loadDetails(force: Boolean, sessionId: String?): Single<Resource<PlatformDetails>> =
        platformDao.getPlatformByNameAsSingle(platform.name)
            .flatMap { platform ->
                if (shouldRefreshDetails(force) && sessionId != null) {
                    twinoService.getDetails(sessionId)
                        .doOnSuccess {
                            analyticsLogger.logDetailsRefresh(
                                AnalyticsLoggerImpl.REFRESH_REMOTE,
                                this.platform
                            )
                        }
                        .map { convertThirdPartyDetails(it) }
                        .flatMap {
                            platformDetailsDao.insertCompletable(it)
                                .doOnComplete { updateLastDetailsTime() }
                                .andThen(platformDetailsDao.getDetailsByNameAsSingle(platform.name))
                                .map { t -> Resource.success(t) }
                        }
                        .onErrorResumeNext { t: Throwable ->
                            Completable.fromAction { logger.log(t) }
                                .andThen(platformDetailsDao.getDetailsByNameAsSingle(platform.name))
                                .map { r ->
                                    Resource.error(
                                        t::class.java.canonicalName ?: "Something went wrong :(",
                                        r
                                    )
                                }
                        }
                } else {
                    Single.fromCallable {
                        analyticsLogger.logDetailsRefresh(
                            AnalyticsLoggerImpl.REFRESH_LOCAL,
                            this.platform
                        )
                    }
                        .flatMap {
                            platformDetailsDao.getDetailsByNameAsSingle(platform.name)
                                .map { Resource.success(it) }
                        }
                }
            }

    fun syncDetails(sessionId: String?): Completable =
        if (shouldRefreshDetails(false) && sessionId != null) {
            platformDao.getPlatformByNameAsMaybe(platform.name)
                .flatMapCompletable {
                    if (it.frequentUpdates) {
                        twinoService.getDetails(sessionId)
                            .doOnSuccess {
                                analyticsLogger.logDetailsRefresh(
                                    AnalyticsLoggerImpl.REFRESH_REMOTE,
                                    this.platform
                                )
                            }
                            .map { info -> convertThirdPartyDetails(info) }
                            .flatMapCompletable { details ->
                                platformDetailsDao.insertCompletable(details)
                                    .doOnComplete { updateLastDetailsTime() }
                            }
                    } else {
                        Completable.fromAction {
                            analyticsLogger.logDetailsRefresh(
                                AnalyticsLoggerImpl.REFRESH_LOCAL,
                                this.platform
                            )
                        }
                    }
                }
                .doOnError { logger.log(it) }
                .onErrorComplete()
        } else {
            Completable.fromAction {
                analyticsLogger.logDetailsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
        }

    fun syncStatements(sessionId: String?): Completable =
        if (shouldRefreshFullStatements() && sessionId != null) {
            val date = getLastStatementUpdateDate()
            platformDao.getPlatformByNameAsMaybe(platform.name)
                .flatMapCompletable { platform ->
                    if (platform.frequentUpdates) {
                        twinoService.getStatements(sessionId, date)
                            .doOnSuccess {
                                analyticsLogger.logFullStatementsRefresh(
                                    AnalyticsLoggerImpl.REFRESH_REMOTE,
                                    this.platform
                                )
                            }
                            .doOnSuccess { logInvalidStatements(it.issues) }
                            .map { model ->
                                Pair(
                                    model.issues.isNotEmpty(),
                                    model.statements.map(this::convertStatement)
                                )
                            }
                            .flatMapCompletable {
                                platformStatementDao.insertStatementsCompletable(it.second)
                                    .doOnComplete { if (it.first.not()) updateLastFullStatementsTime() }
                            }
                    } else {
                        Completable.fromAction {
                            analyticsLogger.logFullStatementsRefresh(
                                AnalyticsLoggerImpl.REFRESH_LOCAL,
                                this.platform
                            )
                        }
                    }
                }
                .doOnError { logger.log(it) }
                .onErrorComplete()
        } else {
            Completable.fromAction {
                analyticsLogger.logFullStatementsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
        }

    fun loadStatements(
        force: Boolean,
        sessionId: String?
    ): Observable<Pair<Boolean, List<PlatformStatement>>> =
        platformDao.getPlatformByNameAsObservable(platform.name)
            .flatMap { platform ->
                if (shouldRefreshDailyStatements(force) && sessionId != null) {
                    twinoService.getDailyStatements(sessionId)
                        .doOnSuccess {
                            analyticsLogger.logStatementsRefresh(
                                AnalyticsLoggerImpl.REFRESH_REMOTE,
                                this.platform
                            )
                        }
                        .doOnSuccess { logInvalidStatements(it.issues) }
                        .map { model ->
                            Pair(
                                model.issues.isNotEmpty(),
                                model.statements.map(this::convertStatement)
                            )
                        }
                        .flatMapObservable { result ->
                            platformStatementDao.insertStatementsCompletable(result.second)
                                .doOnComplete { if (!result.first) updateLastStatementsTime() }
                                .andThen(
                                    platformStatementDao.getDailyStatementsByNameAsObservable(
                                        platform.name, Utils.getTodayDate()
                                    ).map { statements -> Pair(result.first, statements) }
                                )
                        }
                        .onErrorResumeNext { t: Throwable ->
                            Completable.fromAction { logger.log(t) }
                                .andThen(
                                    platformStatementDao.getDailyStatementsByNameAsObservable(
                                        platform.name,
                                        Utils.getTodayDate()
                                    )
                                )
                                .map { Pair(true, it) }
                        }
                } else {
                    Observable.fromCallable {
                        analyticsLogger.logStatementsRefresh(
                            AnalyticsLoggerImpl.REFRESH_LOCAL,
                            this.platform
                        )
                    }
                        .flatMap {
                            platformStatementDao.getDailyStatementsByNameAsObservable(
                                platform.name,
                                Utils.getTodayDate()
                            )
                                .map { Pair(false, it) }
                        }
                }
            }
}
