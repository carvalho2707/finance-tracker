package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.storage.dao.TwitterAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.TwitterAccount

@Singleton
class TwitterAccountRepository @Inject constructor(
    private val twitterAccountDao: TwitterAccountDao
) {

    fun getTwitterAccounts(): Single<List<TwitterAccount>> = twitterAccountDao.getTwitterAccounts()

    fun getEnabledTwitterAccounts(): Single<List<TwitterAccount>> =
        twitterAccountDao.getEnabledTwitterAccounts()

    fun updateTwitterAccount(twitterAccount: TwitterAccount): Completable =
        twitterAccountDao.update(twitterAccount)
}
