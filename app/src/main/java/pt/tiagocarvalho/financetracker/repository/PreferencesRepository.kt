package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.model.DisplayMode
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.ui.accounts.filter.Filter
import pt.tiagocarvalho.financetracker.ui.accounts.last_change.LastChangeFragment
import pt.tiagocarvalho.financetracker.utils.Preferences.KEY_FILTER_BALANCE
import pt.tiagocarvalho.financetracker.utils.Preferences.KEY_FILTER_POSITIVE_VARIATION
import pt.tiagocarvalho.financetracker.utils.Preferences.KEY_FILTER_TYPE
import pt.tiagocarvalho.financetracker.utils.Preferences.KEY_FILTER_VARIATION
import pt.tiagocarvalho.financetracker.utils.Preferences.KEY_SORT_ORDER
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_DISPLAY_MODE

@Singleton
class PreferencesRepository @Inject constructor(
    private val preferencesHelper: PreferencesHelper
) {

    fun getBalancePref(): Boolean = preferencesHelper.get(KEY_FILTER_BALANCE, false)
    fun getVariationPref(): Boolean = preferencesHelper.get(KEY_FILTER_VARIATION, false)
    fun getSortOrderPref(): Int = preferencesHelper.get(KEY_SORT_ORDER, 7)

    fun getPositiveVariationPref(): Boolean =
        preferencesHelper.get(KEY_FILTER_POSITIVE_VARIATION, false)

    fun getFilterTypePref(): MutableSet<String> =
        preferencesHelper.get(
            KEY_FILTER_TYPE,
            PlatformTypeEnum.valuesString().toMutableSet()
        )

    fun getDisplayMode(): String =
        preferencesHelper.get(PREF_DISPLAY_MODE, DisplayMode.ABSOLUTE.name)

    fun restorePreferences() {
        preferencesHelper.put(KEY_FILTER_BALANCE, false)
        preferencesHelper.put(KEY_FILTER_VARIATION, false)
        preferencesHelper.put(KEY_FILTER_POSITIVE_VARIATION, false)
        preferencesHelper.put(KEY_SORT_ORDER, 7)
        preferencesHelper.put(KEY_FILTER_TYPE, PlatformTypeEnum.valuesString().toMutableSet())
    }

    fun getFilterPreference(): Filter {
        val balance = preferencesHelper.get(KEY_FILTER_BALANCE, false)
        val variation = preferencesHelper.get(KEY_FILTER_VARIATION, false)
        val positiveVariation = preferencesHelper.get(KEY_FILTER_POSITIVE_VARIATION, false)
        val sortId = preferencesHelper.get(KEY_SORT_ORDER, 7)
        val type = preferencesHelper.get(
            KEY_FILTER_TYPE,
            PlatformTypeEnum.valuesString().toMutableSet()
        )

        return Filter(balance, variation, positiveVariation, sortId, type)
    }

    fun saveFilters(filter: Filter): Completable = Completable.fromAction {
        preferencesHelper.put(KEY_FILTER_POSITIVE_VARIATION, filter.withPositiveVariation)
        preferencesHelper.put(KEY_FILTER_BALANCE, filter.withBalance)
        preferencesHelper.put(KEY_FILTER_VARIATION, filter.withVariation)
        preferencesHelper.put(KEY_SORT_ORDER, filter.sortOrderId)
        preferencesHelper.put(KEY_FILTER_TYPE, filter.filterArray)
    }

    fun resetUiReady() {
        preferencesHelper.put(LastChangeFragment.UI_READY, false)
    }

    fun setUiReady() {
        preferencesHelper.put(LastChangeFragment.UI_READY, true)
    }
}
