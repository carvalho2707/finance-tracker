package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.data.remote.BondoraService
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.utils.Constants.DETAILS_REFRESH_PERIODICITY_LONG
import pt.tiagocarvalho.financetracker.utils.Constants.DETAILS_REFRESH_WORKER
import pt.tiagocarvalho.financetracker.utils.Constants.STATEMENTS_REFRESH_PERIODICITY_LONG
import pt.tiagocarvalho.financetracker.utils.Preferences.BONDORA_TOKEN
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLoggerImpl
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.api.BondoraAPI

@Singleton
class BondoraRepository @Inject constructor(
    private val bondoraService: BondoraService,
    override val platformDao: PlatformDao,
    private val platformDetailsDao: PlatformDetailsDao,
    private val platformStatementDao: PlatformStatementDao,
    override val preferencesHelper: PreferencesHelper,
    override val logger: Logger,
    private val analyticsLogger: AnalyticsLogger
) : BaseRepository(preferencesHelper, logger, platformDao, PlatformEnum.BONDORA) {

    fun login(code: String, email: String, password: String): Single<PlatformDetails> =
        BondoraAPI.getInstance(
            BuildConfig.BONDORA_CLIENT_ID,
            BuildConfig.BONDORA_CLIENT_SECRET
        ).authenticate(code)
            .flatMap { token ->
                Completable.fromAction { preferencesHelper.put(BONDORA_TOKEN, token) }
                    .toSingleDefault(true)
                    .flatMap {
                        bondoraService.getDetails(token, email, password)
                            .doOnSuccess {
                                analyticsLogger.logLogin(
                                    this.platform
                                )
                            }
                            .map { convertThirdPartyDetails(it) }
                    }
            }

    fun loadDetails(force: Boolean): Single<Resource<PlatformDetails>> =
        platformDao.getPlatformByNameAsSingle(platform.name)
            .flatMap { platform ->
                if (shouldRefreshDetails(force)) {
                    val token = preferencesHelper.get(BONDORA_TOKEN, "")
                    bondoraService.getDetails(token, platform.username!!, platform.password!!)
                        .doOnSuccess {
                            analyticsLogger.logDetailsRefresh(
                                AnalyticsLoggerImpl.REFRESH_REMOTE,
                                this.platform
                            )
                        }
                        .map { convertThirdPartyDetails(it) }
                        .flatMap {
                            platformDetailsDao.insertCompletable(it)
                                .doOnComplete { updateLastDetailsTime() }
                                .andThen(platformDetailsDao.getDetailsByNameAsSingle(platform.name))
                                .map { t -> Resource.success(t) }
                        }
                        .onErrorResumeNext { t: Throwable ->
                            Completable.fromAction { logger.log(t) }
                                .andThen(platformDetailsDao.getDetailsByNameAsSingle(platform.name))
                                .map { r ->
                                    Resource.error(
                                        t::class.java.canonicalName ?: "Something went wrong :(",
                                        r
                                    )
                                }
                        }
                } else {
                    Single.fromCallable {
                        analyticsLogger.logDetailsRefresh(
                            AnalyticsLoggerImpl.REFRESH_LOCAL,
                            this.platform
                        )
                    }
                        .flatMap {
                            platformDetailsDao.getDetailsByNameAsSingle(platform.name)
                                .map { Resource.success(it) }
                        }
                }
            }

    fun syncDetails(worker: Boolean): Completable {
        val shouldRefresh = if (worker) {
            shouldRefreshDetailsWorker()
        } else {
            shouldRefreshDetails(false)
        }
        return if (shouldRefresh) {
            val token = preferencesHelper.get(BONDORA_TOKEN, "")
            platformDao.getPlatformByNameAsMaybe(platform.name)
                .flatMapCompletable {
                    if (it.frequentUpdates) {
                        bondoraService.getDetails(token, it.username!!, it.password!!)
                            .doOnSuccess {
                                analyticsLogger.logDetailsRefresh(
                                    AnalyticsLoggerImpl.REFRESH_REMOTE,
                                    this.platform
                                )
                            }
                            .map { info -> convertThirdPartyDetails(info) }
                            .flatMapCompletable { details ->
                                platformDetailsDao.insertCompletable(details)
                                    .doOnComplete { updateLastDetailsTime() }
                            }
                    } else {
                        Completable.fromAction {
                            analyticsLogger.logDetailsRefresh(
                                AnalyticsLoggerImpl.REFRESH_LOCAL,
                                this.platform
                            )
                        }
                    }
                }
                .doOnError { logger.log(it) }
                .onErrorComplete()
        } else {
            Completable.fromAction {
                analyticsLogger.logDetailsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
        }
    }

    fun syncStatements(worker: Boolean): Completable {
        val shouldRefresh = if (worker) {
            shouldRefreshFullStatementsWorker()
        } else {
            shouldRefreshFullStatements()
        }
        return if (shouldRefresh) {
            val token = preferencesHelper.get(BONDORA_TOKEN, "")
            val date = getLastStatementUpdateDate()
            platformDao.getPlatformByNameAsMaybe(platform.name)
                .flatMapCompletable { platform ->
                    if (platform.frequentUpdates) {
                        bondoraService.getStatements(token, date)
                            .doOnSuccess {
                                analyticsLogger.logFullStatementsRefresh(
                                    AnalyticsLoggerImpl.REFRESH_REMOTE,
                                    this.platform
                                )
                            }
                            .doOnSuccess { logInvalidStatements(it.issues) }
                            .map { model ->
                                Pair(
                                    model.issues.isNotEmpty(),
                                    model.statements.map(this::convertStatement)
                                )
                            }
                            .flatMapCompletable {
                                platformStatementDao.insertStatementsCompletable(it.second)
                                    .doOnComplete { if (it.first.not()) updateLastFullStatementsTime() }
                            }
                    } else {
                        Completable.fromAction {
                            analyticsLogger.logFullStatementsRefresh(
                                AnalyticsLoggerImpl.REFRESH_LOCAL,
                                this.platform
                            )
                        }
                    }
                }
                .doOnError { logger.log(it) }
                .onErrorComplete()
        } else {
            Completable.fromAction {
                analyticsLogger.logFullStatementsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }
        }
    }

    fun loadStatements(force: Boolean): Observable<Pair<Boolean, List<PlatformStatement>>> =
        platformDao.getPlatformByNameAsObservable(platform.name)
            .flatMap { platform ->
                if (shouldRefreshDailyStatements(force)) {
                    val token = preferencesHelper.get(BONDORA_TOKEN, "")
                    bondoraService.getDailyStatements(token)
                        .doOnSuccess {
                            analyticsLogger.logStatementsRefresh(
                                AnalyticsLoggerImpl.REFRESH_REMOTE,
                                this.platform
                            )
                        }
                        .doOnSuccess { logInvalidStatements(it.issues) }
                        .map { model ->
                            Pair(
                                model.issues.isNotEmpty(),
                                model.statements.map(this::convertStatement)
                            )
                        }
                        .flatMapObservable { result ->
                            platformStatementDao.insertStatementsCompletable(result.second)
                                .doOnComplete { if (!result.first) updateLastStatementsTime() }
                                .andThen(
                                    platformStatementDao.getDailyStatementsByNameAsObservable(
                                        platform.name, Utils.getTodayDate()
                                    ).map { statements -> Pair(result.first, statements) }
                                )
                        }
                        .onErrorResumeNext { t: Throwable ->
                            Completable.fromAction { logger.log(t) }
                                .andThen(
                                    platformStatementDao.getDailyStatementsByNameAsObservable(
                                        platform.name,
                                        Utils.getTodayDate()
                                    )
                                )
                                .map { Pair(true, it) }
                        }
                } else {
                    Observable.fromCallable {
                        analyticsLogger.logStatementsRefresh(
                            AnalyticsLoggerImpl.REFRESH_LOCAL,
                            this.platform
                        )
                    }
                        .flatMap {
                            platformStatementDao.getDailyStatementsByNameAsObservable(
                                platform.name,
                                Utils.getTodayDate()
                            )
                                .map { Pair(false, it) }
                        }
                }
            }

    override fun isRefreshDetailsNeeded(): Single<Boolean> =
        platformDao.getPlatformByNameAsSingle(platform.name)
            .flatMap {
                Single.just(shouldRefreshDetails(false))
            }
            .onErrorResumeNext(Single.just(false))

    override fun isRefreshStatementsNeeded(): Single<Boolean> =
        platformDao.getPlatformByNameAsSingle(platform.name)
            .flatMap {
                Single.just(shouldRefreshFullStatements())
            }
            .onErrorResumeNext(Single.just(false))

    override fun shouldRefreshDetails(force: Boolean): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateDetails, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(DETAILS_REFRESH_PERIODICITY_LONG) || force
    }

    private fun shouldRefreshDetailsWorker(): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateDetails, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(DETAILS_REFRESH_WORKER)
    }

    override fun shouldRefreshDailyStatements(force: Boolean): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateStatement, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(
            STATEMENTS_REFRESH_PERIODICITY_LONG
        ) || force
    }

    override fun shouldRefreshFullStatements(): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastFullStatementScan, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.FULL_STATEMENTS_REFRESH_PERIODICITY)
    }

    private fun shouldRefreshFullStatementsWorker(): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastFullStatementScan, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.FULL_STATEMENTS_REFRESH_PERIODICITY_WORKER)
    }
}
