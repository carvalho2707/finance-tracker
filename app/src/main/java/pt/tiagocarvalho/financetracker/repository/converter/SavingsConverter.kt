package pt.tiagocarvalho.financetracker.repository.converter

import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.SavingsInfo

object SavingsConverter {

    fun convert(savingsAccount: SavingsInfo): PlatformDetails = PlatformDetails(
        name = savingsAccount.name,
        platform = PlatformEnum.SAVINGS,
        platformType = PlatformEnum.SAVINGS.platformTypeEnum,
        balance = savingsAccount.totalBalance,
        netAnnualReturn = savingsAccount.netAnnualReturn,
        deposits = savingsAccount.totalDeposits,
        withdrawals = savingsAccount.totalWithdrawals,
        profit = savingsAccount.profit,
        interest = savingsAccount.interest,
        serviceFees = savingsAccount.serviceFees,
        interestFrequency = savingsAccount.interestFrequency,
        recurringAmount = savingsAccount.recurringAmount,
        recurringFrequency = savingsAccount.recurringFrequency,
        campaignRewards = savingsAccount.bonus,
        changeValue = savingsAccount.changeValue,
        changePercentage = savingsAccount.changePercentage
    )
}
