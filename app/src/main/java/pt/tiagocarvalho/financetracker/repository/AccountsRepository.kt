package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.model.Status
import pt.tiagocarvalho.financetracker.model.WebViewData
import pt.tiagocarvalho.financetracker.utils.Preferences
import pt.tiagocarvalho.financetracker.utils.isNoNetworkException
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.services.utils.sumByBigDecimal

@Singleton
class AccountsRepository @Inject constructor(
    private val platformDao: PlatformDao,
    private val platformDetailsDao: PlatformDetailsDao,
    private val preferencesHelper: PreferencesHelper,
    private val grupeerRepository: GrupeerRepository,
    private val mintosRepository: MintosRepository,
    private val peerberryRepository: PeerberryRepository,
    private val raizeRepository: RaizeRepository,
    private val robocashRepository: RobocashRepository,
    private val savingsRepository: SavingsRepository,
    private val gamblingRepository: GamblingRepository,
    private val bondoraRepository: BondoraRepository,
    private val twinoRepository: TwinoRepository,
    private val crowdestorRepository: CrowdestorRepository,
    private val estateGuruRepository: EstateGuruRepository,
    private val cashRepository: CashRepository,
    private val iuvoRepository: IuvoRepository,
    private val lenderMarketRepository: LenderMarketRepository,
    private val logger: Logger,
    private val analyticsLogger: AnalyticsLogger,
    private val platformDetailsRepository: PlatformDetailsRepository
) {

    fun getTwoFactorPlatforms(): Maybe<List<Platform>> =
        platformDao.getPlatformsWithTwoFactorAndActive()

    fun loadDetails(
        force: Boolean,
        loadedData: MutableMap<PlatformEnum, WebViewData>
    ): Single<Resource<List<PlatformDetails>>> {
        return platformDao.getPlatformsAsSingle().toObservable()
            .flatMapIterable { platforms -> platforms }
            .flatMapSingle {
                when (it.type) {
                    PlatformEnum.GRUPEER -> grupeerRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.GRUPEER]?.data
                    )
                    PlatformEnum.MINTOS -> mintosRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.MINTOS]?.data
                    )
                    PlatformEnum.ROBOCASH -> robocashRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.ROBOCASH]?.data
                    )
                    PlatformEnum.SAVINGS -> savingsRepository.loadDetails(force, it.name)
                    PlatformEnum.CASH -> cashRepository.loadDetails(force, it.name)
                    PlatformEnum.PEERBERRY -> peerberryRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.PEERBERRY]?.data
                    )
                    PlatformEnum.RAIZE -> raizeRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.RAIZE]?.data
                    )
                    PlatformEnum.BETS -> gamblingRepository.loadDetails(force, it.name)
                    PlatformEnum.BONDORA -> bondoraRepository.loadDetails(false)
                    PlatformEnum.TWINO -> twinoRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.TWINO]?.data
                    )
                    PlatformEnum.CROWDESTOR -> crowdestorRepository.loadDetails(
                        force,
                        loadedData[PlatformEnum.CROWDESTOR]?.data
                    )
                    PlatformEnum.ESTATEGURU -> estateGuruRepository.loadDetails(force)
                    PlatformEnum.IUVO -> iuvoRepository.loadDetails(force)
                    PlatformEnum.LENDERMARKET -> lenderMarketRepository.loadDetails(force)
                }
            }
            .toList()
            .map { mapToSingleResource(it) }
            .doOnSuccess { logAnalytics(it) }
            .onErrorResumeNext { t: Throwable ->
                Completable.fromAction { logger.log(t) }
                    .andThen(platformDetailsDao.getAllAsSingle())
                    .map {
                        if (t.isNoNetworkException()) {
                            Resource.error("No internet connection", it)
                        } else {
                            Resource.error(t.message, it)
                        }
                    }
            }
    }

    private fun logAnalytics(accounts: Resource<List<PlatformDetails>>?) {
        analyticsLogger.logTotalAccounts(accounts?.data?.size ?: 0)
        val total = accounts?.data?.sumByBigDecimal { it.balance } ?: BigDecimal.ZERO
        analyticsLogger.logTotalBalance(total)
    }

    private fun mapToSingleResource(accounts: List<Resource<PlatformDetails>>): Resource<List<PlatformDetails>> {
        val result = emptyList<PlatformDetails>().toMutableList()
        var hasError = false
        var error: Throwable? = null
        var message: String? = null
        accounts.forEach {
            result.add(it.data!!)
            if (it.status != Status.SUCCESS) {
                hasError = true
                error = it.error
                message = it.message
            }
        }
        return if (hasError) {
            Resource.error(message, result, error)
        } else {
            Resource.success(result)
        }
    }

    fun getLastSessionChange(
        platformDetails: PlatformDetails,
        balance: BigDecimal
    ): Observable<Pair<PlatformDetails, BigDecimal>> = Observable.create {
        val lastSessionBalance =
            preferencesHelper.get(
                Preferences.LAST_SESSION_BALANCE_ACCOUNT + platformDetails.name,
                BigDecimal.ZERO.toPlainString()
            )
        val oldBalance = BigDecimal(lastSessionBalance)
        val diff = balance.subtract(oldBalance)

        preferencesHelper.put(
            Preferences.LAST_SESSION_BALANCE_ACCOUNT + platformDetails.name,
            balance.toPlainString()
        )
        it.onNext(Pair(platformDetails, diff))
        it.onComplete()
    }

    fun hasMaxPlatforms(): Single<Boolean> = platformDao.countAccountsByType(
        PlatformEnum.filterByPlatformTypeEnum(
            PlatformTypeEnum.P2P
        )
    )
        .map { it == PlatformEnum.filterByPlatformTypeEnum(PlatformTypeEnum.P2P).size }

    fun getPlatformWithCashDrag(): Single<Int> =
        platformDetailsRepository.getPlatformDetailsWithCashDrag()
            .map { it.size }
            .doOnError { logger.log(it) }
}
