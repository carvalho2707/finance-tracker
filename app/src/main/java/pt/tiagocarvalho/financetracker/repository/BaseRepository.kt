package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Single
import java.math.BigDecimal
import java.util.Date
import java.util.concurrent.TimeUnit
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.Frequency
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.utils.Constants
import pt.tiagocarvalho.financetracker.utils.log.Logger
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyStatement

open class BaseRepository(
    open val preferencesHelper: PreferencesHelper,
    open val logger: Logger,
    open val platformDao: PlatformDao,
    val platform: PlatformEnum
) {

    fun convertThirdPartyDetails(thirdPartyDetails: ThirdPartyDetails): PlatformDetails {
        val platformEnum = PlatformEnum.valueOf(thirdPartyDetails.name.name)
        return PlatformDetails(
            name = platformEnum.name,
            platform = platformEnum,
            platformType = platformEnum.platformTypeEnum,

            balance = thirdPartyDetails.balance,
            available = thirdPartyDetails.available,
            invested = thirdPartyDetails.invested,

            netAnnualReturn = thirdPartyDetails.netAnnualReturn,
            interest = thirdPartyDetails.interests,
            deposits = thirdPartyDetails.deposits,
            withdrawals = thirdPartyDetails.withdrawals,
            profit = thirdPartyDetails.profit,

            latePaymentFees = thirdPartyDetails.latePaymentFees,
            badDebt = thirdPartyDetails.badDebt,
            secondaryMarketTransactions = thirdPartyDetails.secondaryMarketTransactions,
            serviceFees = thirdPartyDetails.serviceFees,
            campaignRewards = thirdPartyDetails.campaigns,
            cashback = thirdPartyDetails.cashback,
            taxes = thirdPartyDetails.taxes,
            currencyFluctuations = thirdPartyDetails.currencyFluctuations,
            writeOff = thirdPartyDetails.writeOff,

            myInvestments = thirdPartyDetails.investments,
            currentTotal = thirdPartyDetails.currentTotal,
            gracePeriodTotal = thirdPartyDetails.gracePeriodTotal,
            oneToFifteenLateTotal = thirdPartyDetails.oneToFifteenLateTotal,
            sixteenToThirtyLateTotal = thirdPartyDetails.sixteenToThirtyLateTotal,
            thirtyToSixtyLateTotal = thirdPartyDetails.thirtyToSixtyLateTotal,
            moreThanSixtyLateTotal = thirdPartyDetails.moreThanSixtyLateTotal,
            defaultInvestmentTotal = thirdPartyDetails.defaultInvestmentTotal,
            badDebtTotal = thirdPartyDetails.badDebtTotal,
            lateTotal = thirdPartyDetails.lateTotal,

            currentNumber = thirdPartyDetails.currentNumber,
            gracePeriodNumber = thirdPartyDetails.gracePeriodNumber,
            oneToFifteenLateNumber = thirdPartyDetails.oneToFifteenLateNumber,
            sixteenToThirtyLateNumber = thirdPartyDetails.sixteenToThirtyLateNumber,
            thirtyToSixtyLateNumber = thirdPartyDetails.thirtyToSixtyLateNumber,
            moreThanSixtyLateNumber = thirdPartyDetails.moreThanSixtyLateNumber,
            defaultInvestmentNumber = thirdPartyDetails.defaultInvestmentNumber,
            badDebtNumber = thirdPartyDetails.badDebtNumber,
            lateNumber = thirdPartyDetails.lateNumber,

            pendingInterests = thirdPartyDetails.pendingInterests,
            pendingPayments = thirdPartyDetails.pendingPayments,

            cumulativeInvested = thirdPartyDetails.cumulativeInvested,
            cumulativePrincipal = thirdPartyDetails.cumulativePrincipal,

            totalInvestmentsNumber = 0,
            interestFrequency = Frequency.NONE,
            recurringAmount = BigDecimal.ZERO,
            recurringFrequency = Frequency.NONE,

            changeValue = thirdPartyDetails.changeValue,
            changePercentage = thirdPartyDetails.changePercentage

        )
    }

    fun convertStatement(statement: ThirdPartyStatement): PlatformStatement = PlatformStatement(
        statement.name.name,
        statement.amount.toDouble(),
        statement.date,
        StatementType.getStatementType(statement.type),
        statement.id
    )

    fun logInvalidStatements(issues: List<String>) {
        if (issues.isNotEmpty()) {
            logger.logMessages(issues)
        }
    }

    // P2P
    fun updateLastDetailsTime() {
        preferencesHelper.put(platform.prefLastUpdateDetails, System.currentTimeMillis())
    }

    fun updateLastStatementsTime() {
        preferencesHelper.put(platform.prefLastUpdateStatement, System.currentTimeMillis())
    }

    fun updateLastFullStatementsTime() {
        preferencesHelper.put(platform.prefLastFullStatementScan, System.currentTimeMillis())
    }

    // SAVINGS OR BETS
    fun updateLastDetailsTime(name: String) {
        preferencesHelper.put(
            platform.prefLastUpdateDetails + name,
            System.currentTimeMillis()
        )
    }

    fun updateLastStatementsTime(name: String) {
        preferencesHelper.put(
            platform.prefLastUpdateStatement + name,
            System.currentTimeMillis()
        )
    }

    fun updateLastFullStatementsScanTime(name: String) {
        preferencesHelper.put(
            platform.prefLastFullStatementScan + name,
            System.currentTimeMillis()
        )
    }

    // P2P
    open fun shouldRefreshDetails(force: Boolean): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateDetails, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.DETAILS_REFRESH_PERIODICITY) || force
    }

    open fun shouldRefreshDailyStatements(force: Boolean): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateStatement, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.STATEMENTS_REFRESH_PERIODICITY) || force
    }

    open fun shouldRefreshFullStatements(): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastFullStatementScan, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.FULL_STATEMENTS_REFRESH_PERIODICITY)
    }

    fun shouldRefreshDetails(
        force: Boolean,
        name: String
    ): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateDetails + name, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.DETAILS_REFRESH_PERIODICITY) || force
    }

    fun shouldRefreshDailyStatements(
        force: Boolean,
        name: String
    ): Boolean {
        val savedTime = preferencesHelper.get(platform.prefLastUpdateStatement + name, 0L)
        val currentTime = System.currentTimeMillis()
        return currentTime >= savedTime + TimeUnit.HOURS.toMillis(Constants.STATEMENTS_REFRESH_PERIODICITY) || force
    }

    fun getLastStatementUpdateDate(): Date? {
        val lastUpdate = preferencesHelper.get(platform.prefLastFullStatementScan, 0L)
        return if (lastUpdate == 0L) {
            null
        } else {
            Date(lastUpdate)
        }
    }

    open fun isRefreshDetailsNeeded(): Single<Boolean> =
        platformDao.getPlatformByNameAsSingle(platform.name)
            .flatMap {
                if (it.frequentUpdates.not()) {
                    Single.just(false)
                } else {
                    Single.just(shouldRefreshDetails(false))
                }
            }
            .onErrorResumeNext { Single.just(false) }

    open fun isRefreshStatementsNeeded(): Single<Boolean> =
        platformDao.getPlatformByNameAsSingle(platform.name)
            .flatMap {
                if (it.frequentUpdates.not()) {
                    Single.just(false)
                } else {
                    Single.just(shouldRefreshFullStatements())
                }
            }
            .onErrorResumeNext(Single.just(false))
}
