package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsStatementDao
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.utils.SchedulerProvider
import pt.tiagocarvalho.financetracker.utils.log.Logger

@Singleton
class UpdatesRepository @Inject constructor(
    private val preferencesHelper: PreferencesHelper,
    private val schedulerProvider: SchedulerProvider,
    private val platformDao: PlatformDao,
    private val platformDetailsDao: PlatformDetailsDao,
    private val platformStatementDao: PlatformStatementDao,
    private val savingsStatementDao: SavingsStatementDao,
    private val cashStatementDao: CashStatementDao,
    private val gamblingStatementDao: GamblingStatementDao,
    private val logger: Logger
) {

    val compositeDisposable = CompositeDisposable()

    fun handleUpdates() {
        // Fix estateguru bug in statements
        handleUpdate272()
        // Fix mintos statements being categorized as interests due to bug in mintos itself
        handleUpdate279()
        // Fix twino statements being wrong categorized
        handleUpdate287()
        // Added twofactor to grupeer
        handleUpdate293()
        // EstateGuru type issue
        handleUpdate300()
        // Remove referral type from local accounts
        handleUpdate305()
        // Remove referral type from local accounts
        handleUpdate320()
        // Disabled Mintos and Crowdestor
        handleUpdate323()
    }

    private fun handleUpdate323() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_323, false) }
                .filter { it.not() }
                .flatMapCompletable { update323() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate320() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_320, false) }
                .filter { it.not() }
                .flatMapCompletable { update320() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate305() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_305, false) }
                .filter { it.not() }
                .flatMapCompletable { update305() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate300() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_300, false) }
                .filter { it.not() }
                .flatMapCompletable { update300() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate293() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_293, false) }
                .filter { it.not() }
                .flatMapCompletable { update293() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate287() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_287, false) }
                .filter { it.not() }
                .flatMapCompletable { update287() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate279() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_279, false) }
                .filter { it.not() }
                .flatMapCompletable { update279() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun handleUpdate272() {
        compositeDisposable.add(
            Single.fromCallable { preferencesHelper.get(UPDATES_271, false) }
                .filter { it.not() }
                .flatMapCompletable { update272() }
                .doOnError { logger.log(it) }
                .subscribeOn(schedulerProvider.io())
                .subscribe({}, { it.printStackTrace() })
        )
    }

    private fun update323(): Completable {
        return Completable.fromAction { }
            .andThen(Completable.fromAction { platformDetailsDao.deleteByName(PlatformEnum.MINTOS.name) })
            .andThen(Completable.fromAction { platformDetailsDao.deleteByName(PlatformEnum.MINTOS.name) })
            .andThen(Completable.fromAction { platformDao.deleteByName(PlatformEnum.MINTOS.name) })
            .andThen(Completable.fromAction { platformStatementDao.deleteByName(PlatformEnum.CROWDESTOR.name) })
            .andThen(Completable.fromAction { platformDetailsDao.deleteByName(PlatformEnum.CROWDESTOR.name) })
            .andThen(Completable.fromAction { platformDao.deleteByName(PlatformEnum.CROWDESTOR.name) })
            .andThen(Completable.fromAction { preferencesHelper.put(UPDATES_323, true) })
    }

    private fun update320(): Completable = Completable.fromAction {
        platformDao.getPlatformByName(PlatformEnum.MINTOS.name)?.let {
            it.twofactor = false
            it.frequentUpdates = false
            platformDao.update(it)
        }
    }.andThen(
        Completable.fromAction {
            preferencesHelper.put(UPDATES_320, true)
        }
    )

    private fun update305(): Completable =
        platformDao.getPlatformsByTypeIn(
            listOf(
                PlatformEnum.BETS,
                PlatformEnum.CASH,
                PlatformEnum.SAVINGS
            )
        )
            .flattenAsObservable { it }
            .flatMapCompletable {
                platformStatementDao.deleteByNameAndType(it.name, StatementType.REFERRAL)
                    .andThen(
                        savingsStatementDao.deleteByNameAndType(
                            it.name,
                            StatementType.REFERRAL
                        )
                    )
                    .andThen(
                        gamblingStatementDao.deleteByNameAndType(
                            it.name,
                            StatementType.REFERRAL
                        )
                    )
                    .andThen(cashStatementDao.deleteByNameAndType(it.name, StatementType.REFERRAL))
                    .andThen(
                        Completable.fromAction {
                            preferencesHelper.put(
                                PlatformEnum.SAVINGS.prefLastFullStatementScan + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.SAVINGS.prefLastUpdateStatement + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.SAVINGS.prefLastUpdateDetails + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.BETS.prefLastFullStatementScan + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.BETS.prefLastUpdateStatement + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.BETS.prefLastUpdateDetails + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.CASH.prefLastFullStatementScan + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.CASH.prefLastUpdateStatement + it.name,
                                0L
                            )
                            preferencesHelper.put(
                                PlatformEnum.CASH.prefLastUpdateDetails + it.name,
                                0L
                            )
                            preferencesHelper.put(UPDATES_305, true)
                        }
                    )
            }

    private fun update300(): Completable =
        platformStatementDao.deleteByNameCompletable(PlatformEnum.ESTATEGURU.name)
            .andThen(
                Completable.fromAction {
                    preferencesHelper.put(PlatformEnum.ESTATEGURU.prefLastFullStatementScan, 0L)
                    preferencesHelper.put(PlatformEnum.ESTATEGURU.prefLastUpdateStatement, 0L)
                    preferencesHelper.put(UPDATES_300, true)
                }
            )

    private fun update293(): Completable = Completable.fromAction {
        platformDao.getPlatformByName(PlatformEnum.GRUPEER.name)?.let {
            it.twofactor = true
            platformDao.update(it)
        }
    }.andThen(
        Completable.fromAction {
            preferencesHelper.put(UPDATES_293, true)
        }
    )

    private fun update287(): Completable =
        platformStatementDao.deleteByNameCompletable(PlatformEnum.TWINO.name)
            .andThen(
                Completable.fromAction {
                    preferencesHelper.put(PlatformEnum.TWINO.prefLastFullStatementScan, 0L)
                    preferencesHelper.put(PlatformEnum.TWINO.prefLastUpdateStatement, 0L)
                    preferencesHelper.put(UPDATES_287, true)
                }
            )

    private fun update279(): Completable =
        platformStatementDao.deleteByNameCompletable(PlatformEnum.MINTOS.name)
            .andThen(
                Completable.fromAction {
                    preferencesHelper.put(PlatformEnum.MINTOS.prefLastFullStatementScan, 0L)
                    preferencesHelper.put(PlatformEnum.MINTOS.prefLastUpdateStatement, 0L)
                    preferencesHelper.put(UPDATES_279, true)
                }
            )

    private fun update272(): Completable = Completable.fromAction {
        preferencesHelper.put(PlatformEnum.ESTATEGURU.prefLastFullStatementScan, 0L)
        preferencesHelper.put(PlatformEnum.ESTATEGURU.prefLastUpdateStatement, 0L)
        preferencesHelper.put(UPDATES_271, true)
    }

    companion object {
        const val UPDATES_271 = "pt.tiagocarvalho.financetracker.updates.271"
        const val UPDATES_279 = "pt.tiagocarvalho.financetracker.updates.279"
        const val UPDATES_287 = "pt.tiagocarvalho.financetracker.updates.287"
        const val UPDATES_293 = "pt.tiagocarvalho.financetracker.updates.293"
        const val UPDATES_300 = "pt.tiagocarvalho.financetracker.updates.300"
        const val UPDATES_305 = "pt.tiagocarvalho.financetracker.updates.305"
        const val UPDATES_320 = "pt.tiagocarvalho.financetracker.updates.320"
        const val UPDATES_323 = "pt.tiagocarvalho.financetracker.updates.323"
    }
}
