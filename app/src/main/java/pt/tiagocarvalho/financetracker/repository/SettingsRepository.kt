package pt.tiagocarvalho.financetracker.repository

import android.content.Context
import android.content.pm.PackageManager
import android.net.TrafficStats
import io.reactivex.Single
import java.util.Locale
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.abs
import pt.tiagocarvalho.financetracker.utils.log.Logger

@Singleton
class SettingsRepository @Inject constructor(
    private val context: Context,
    private val logger: Logger
) {

    fun getNetworkUsage(packageName: String): Single<String> = getPackageUid(context, packageName)
        .flatMap { getNetworkUsageBytes(it) }
        .map { formatBytesToHuman(it) }
        .doOnError { logger.log(it) }
        .onErrorResumeNext { Single.just("Failed to collect info") }

    private fun formatBytesToHuman(bytes: Long): String {
        val s = if (bytes < 0) "-" else ""
        var b = if (bytes == Long.MIN_VALUE) Long.MAX_VALUE else abs(bytes)
        return when {
            b < 1000L -> "$bytes B"
            b in 1001..999949 -> String.format(Locale.ROOT, "%s%.1f kB", s, b / 1e3)
            1000.let { b /= it; b } < 999950L -> String.format(Locale.ROOT, "%s%.1f MB", s, b / 1e3)
            1000.let { b /= it; b } < 999950L -> String.format(Locale.ROOT, "%s%.1f GB", s, b / 1e3)
            1000.let { b /= it; b } < 999950L -> String.format(Locale.ROOT, "%s%.1f TB", s, b / 1e3)
            1000.let { b /= it; b } < 999950L -> String.format(Locale.ROOT, "%s%.1f PB", s, b / 1e3)
            else -> String.format(Locale.ROOT, "%s%.1f EB", s, b / 1e6)
        }
    }

    private fun getPackageUid(context: Context, packageName: String): Single<Int> = Single.create {
        val packageManager: PackageManager = context.packageManager
        var uid = -1
        try {
            val packageInfo =
                packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA)
            uid = packageInfo.applicationInfo.uid
        } catch (e: PackageManager.NameNotFoundException) {
            it.onError(e)
        }
        it.onSuccess(uid)
    }

    private fun getNetworkUsageBytes(uid: Int): Single<Long> = Single.create {
        val rxBytes = TrafficStats.getUidRxBytes(uid)
        it.onSuccess(rxBytes)
    }
}
