package pt.tiagocarvalho.financetracker.repository

import android.os.Build
import android.util.SparseArray
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.RoundingMode
import java.util.Calendar
import java.util.Date
import java.util.EnumMap
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.set
import kotlin.math.absoluteValue
import kotlin.math.pow
import org.decampo.xirr.Transaction
import org.decampo.xirr.Xirr
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.model.ForecastStatistics
import pt.tiagocarvalho.financetracker.model.GainsStatistics
import pt.tiagocarvalho.financetracker.model.GeneralStatistics
import pt.tiagocarvalho.financetracker.model.P2pStatistics
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.model.StatementType.DEPOSIT
import pt.tiagocarvalho.financetracker.model.StatementType.FEE
import pt.tiagocarvalho.financetracker.model.StatementType.INVESTMENT
import pt.tiagocarvalho.financetracker.model.StatementType.PRINCIPAL
import pt.tiagocarvalho.financetracker.model.StatementType.PRINCIPAL_FIX_NEGATIVE
import pt.tiagocarvalho.financetracker.model.StatementType.WITHDRAWAL
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_IDLE_MONEY_THRESHOLD
import pt.tiagocarvalho.financetracker.utils.Utils
import pt.tiagocarvalho.financetracker.utils.Utils.getTodayDate
import timber.log.Timber

@Singleton
class PlatformDetailsRepository @Inject constructor(
    private val platformDetailsDao: PlatformDetailsDao,
    private val platformStatementDao: PlatformStatementDao,
    private val preferencesHelper: PreferencesHelper
) {

    companion object {
        private const val MONTHS = 12
        private const val MONTHS_ONE = 13
        private const val YEARS = 40
        private const val YEARS_ONE = 41
        private const val FORECAST_SCALE = 3
        private const val COMPOUNT_SCALE = 10
    }

    fun loadGeneralStatistics(): Observable<GeneralStatistics> = Observable.create { emitter ->
        try {
            var deposits = ZERO
            var withdrawals = ZERO
            var fees = ZERO
            var interests = ZERO
            var referrals = ZERO
            var cashback = ZERO
            val balanceByType: MutableMap<String, BigDecimal> = HashMap()
            var count = 0
            platformDetailsDao.getAll().forEach {
                count++
                deposits = deposits.add(it.deposits)
                withdrawals = withdrawals.add(it.withdrawals)
                fees = fees.add(it.serviceFees)
                interests = interests.add(it.interest)
                referrals = referrals.add(it.campaignRewards)
                cashback = cashback.add(it.cashback)

                balanceByType[it.platformType.name] = (
                    balanceByType[it.platformType.name]
                        ?: ZERO
                    ).add(it.balance)
            }

            val generalStatistics = GeneralStatistics(
                balanceByType,
                deposits,
                withdrawals,
                fees,
                interests,
                referrals,
                cashback,
                count != 0
            )
            emitter.onNext(generalStatistics)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    fun loadReturnsStatistics(): Observable<GainsStatistics> = Observable.create { emitter ->
        try {
            val gainsByName: MutableMap<String, BigDecimal> = HashMap()
            val gainsByPlatformTypeType: MutableMap<PlatformTypeEnum, BigDecimal> =
                EnumMap(PlatformTypeEnum::class.java)
            val xirByName: MutableMap<String, Float> = HashMap()
            val roiByName: MutableMap<String, Float> = HashMap()
            val gainsByMonth: SparseArray<Float> = SparseArray(MONTHS)
            val gainsByYear: MutableMap<Int, Float> = HashMap()
            val currentMonthGains: MutableMap<String, BigDecimal> = HashMap()
            val currentYearGains: MutableMap<String, BigDecimal> = HashMap()
            var count = 0
            platformDetailsDao.getAll().forEach {
                count++

                gainsByName[it.name] = (
                    gainsByName[it.name]
                        ?: ZERO
                    ).add(it.profit)

                gainsByPlatformTypeType[it.platformType] =
                    (
                        gainsByPlatformTypeType[it.platformType]
                            ?: ZERO
                        ).add(it.profit)

                val statements = platformStatementDao.getAllByNameAndTypeInOrderByDateAsc(
                    it.name,
                    StatementType.getDepositsWithdrawals()
                ).toMutableList()

                statements.add(
                    PlatformStatement(
                        it.name, it.balance.toDouble(), getTodayDate(), WITHDRAWAL, ""
                    )
                )

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val xirr: Float = calculateXirr(statements)
                    if (xirr.isInfinite()) {
                        xirByName[it.name] = 0.0f
                    } else {
                        xirByName[it.name] = xirr
                    }
                }

                val roi = calculateRoi(statements)
                if (roi.isInfinite()) {
                    roiByName[it.name] = 0.0f
                } else {
                    roiByName[it.name] = roi
                }
            }

            val types = StatementType.getChangeValues()
            val cal = Calendar.getInstance()
            val date = Utils.getInitialYear()
            val currentMonth = getCurrentMonth()
            val currentYear = getCurrentYear()
            platformStatementDao.getByYear(date, types)
                .forEach {
                    cal.time = it.date
                    val month = cal.get(Calendar.MONTH)

                    if (it.type == FEE) {
                        gainsByMonth.put(
                            month,
                            gainsByMonth.get(month, 0.0f) - it.amount.toFloat()
                        )
                    } else {
                        gainsByMonth.put(
                            month,
                            gainsByMonth.get(month, 0.0f) + it.amount.toFloat()
                        )
                    }

                    if (month == currentMonth) {
                        if (it.type == FEE) {
                            currentMonthGains[it.name] = (
                                currentMonthGains[it.name]
                                    ?: ZERO
                                ).subtract(it.amount.toBigDecimal())
                        } else {
                            currentMonthGains[it.name] = (
                                currentMonthGains[it.name]
                                    ?: ZERO
                                ).add(it.amount.toBigDecimal())
                        }
                    }
                }

            platformStatementDao.getAllByTypes(types)
                .forEach {
                    cal.time = it.date
                    val year = cal.get(Calendar.YEAR)

                    if (it.type == FEE) {
                        gainsByYear[year] = (gainsByYear[year] ?: 0.0f) - it.amount.toFloat()
                    } else {
                        gainsByYear[year] = (gainsByYear[year] ?: 0.0f) + it.amount.toFloat()
                    }

                    if (year == currentYear) {
                        if (it.type == FEE) {
                            currentYearGains[it.name] = (
                                currentYearGains[it.name]
                                    ?: ZERO
                                ).subtract(it.amount.toBigDecimal())
                        } else {
                            currentYearGains[it.name] = (
                                currentYearGains[it.name]
                                    ?: ZERO
                                ).add(it.amount.toBigDecimal())
                        }
                    }
                }

            val generalStatistics = GainsStatistics(
                gainsByName,
                gainsByPlatformTypeType,
                xirByName,
                roiByName,
                gainsByMonth,
                gainsByYear.toSortedMap(),
                currentMonthGains,
                currentYearGains,
                count != 0
            )
            emitter.onNext(generalStatistics)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    fun loadP2pStatistics(): Observable<P2pStatistics> = Observable.create { emitter ->
        try {
            var count = 0
            var activeAccounts = 0
            var ongoingLoans = 0
            var totalStatementsCounter = 0
            val interestComparison: MutableMap<PlatformEnum, BigDecimal> =
                EnumMap(PlatformEnum::class.java)
            val byName: MutableMap<PlatformEnum, BigDecimal> = EnumMap(PlatformEnum::class.java)
            val statementsByName: MutableMap<PlatformEnum, Int> =
                EnumMap(PlatformEnum::class.java)
            val cumulativeInvestments: MutableMap<String, Double> =
                emptyMap<String, Double>().toMutableMap()
            val cumulativePrincipal: MutableMap<String, Double> =
                emptyMap<String, Double>().toMutableMap()

            var totalInvestments = 0.0
            var totalPrincipal = 0.0
            platformDetailsDao.getAllByPlatformTypeEnum(PlatformTypeEnum.P2P)
                .forEach { details ->
                    count++
                    val counter = platformStatementDao.getStatementsCount(details.name)
                    Timber.e("Fetched $counter statements for ${details.name}")

                    if (details.balance > ZERO) {
                        activeAccounts++
                    }

                    ongoingLoans += details.myInvestments ?: 0
                    totalStatementsCounter += counter

                    statementsByName[details.platform] = counter
                    byName[details.platform] = details.balance
                    interestComparison[details.platform] = details.netAnnualReturn ?: ZERO

                    val investmentTotal =
                        platformStatementDao.getAllByNameAndType(details.name, INVESTMENT)
                            .sumByDouble { it.amount.absoluteValue }

                    val principalPos =
                        platformStatementDao.getAllByNameAndType(details.name, PRINCIPAL)
                            .sumByDouble { it.amount.absoluteValue }
                    val principalNeg = platformStatementDao.getAllByNameAndType(
                        details.name,
                        PRINCIPAL_FIX_NEGATIVE
                    )
                        .sumByDouble { it.amount.absoluteValue }
                        .plus(-1.0)

                    val principalTotal = principalPos + principalNeg

                    cumulativeInvestments[details.name] = investmentTotal
                    cumulativePrincipal[details.name] = principalTotal

                    totalInvestments += investmentTotal
                    totalPrincipal += principalTotal
                }

            cumulativeInvestments["TOTAL"] = totalInvestments
            cumulativePrincipal["TOTAL"] = totalPrincipal
            val p2pStatistics = P2pStatistics(
                activeAccounts,
                ongoingLoans,
                totalStatementsCounter,
                interestComparison,
                byName,
                statementsByName,
                cumulativeInvestments,
                cumulativePrincipal,
                count != 0
            )
            emitter.onNext(p2pStatistics)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    fun loadForecastStatistics(): Observable<ForecastStatistics> = Observable.create { emitter ->
        try {
            var count = 0
            var yearly = ZERO
            var monthly = ZERO
            var daily = ZERO
            var secondly = ZERO

            val monthlyCompoundProjection: SparseArray<Float> = SparseArray(MONTHS_ONE)
            val yearlyCompoundProjection: SparseArray<Float> = SparseArray(YEARS_ONE)

            platformDetailsDao.getAll()
                .forEach {
                    count++

                    val total = it.balance
                    val netAnnualReturn = it.netAnnualReturn ?: ZERO
                    val netAnnualReturnDecimal =
                        netAnnualReturn.divide(
                            BigDecimal("100"),
                            FORECAST_SCALE,
                            RoundingMode.HALF_UP
                        )

                    val year = total.multiply(netAnnualReturnDecimal)
                    val month = year.divide(BigDecimal("12"), FORECAST_SCALE, RoundingMode.HALF_UP)
                    val day = year.divide(BigDecimal("365"), FORECAST_SCALE, RoundingMode.HALF_UP)
                    val seconds =
                        year.divide(BigDecimal("31536000"), FORECAST_SCALE, RoundingMode.HALF_UP)

                    yearly = yearly.add(year)
                    monthly = monthly.add(month)
                    daily = daily.add(day)
                    secondly = secondly.add(seconds)

                    val tempMonthly =
                        calculateMonthlyCompoundProjection(total, netAnnualReturnDecimal, MONTHS, 1)
                    for (i in 0 until tempMonthly.size()) {
                        val new = tempMonthly.valueAt(i)
                        val old = if (i < monthlyCompoundProjection.size()) {
                            monthlyCompoundProjection.valueAt(i) ?: 0.0f
                        } else {
                            0.0f
                        }
                        monthlyCompoundProjection.put(i, new + old)
                    }

                    val tempYearly =
                        calculateYearlyCompoundProjection(
                            total,
                            netAnnualReturnDecimal,
                            MONTHS,
                            YEARS
                        )
                    for (i in 0 until tempYearly.size()) {
                        val new = tempYearly.valueAt(i)
                        val old = if (i < yearlyCompoundProjection.size()) {
                            yearlyCompoundProjection.valueAt(i) ?: 0.0f
                        } else {
                            0.0f
                        }
                        yearlyCompoundProjection.put(i, new + old)
                    }
                }

            val projectionsStatistics = ForecastStatistics(
                yearly,
                monthly,
                daily,
                secondly,
                monthlyCompoundProjection,
                yearlyCompoundProjection,
                count != 0
            )
            emitter.onNext(projectionsStatistics)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    private fun calculateMonthlyCompoundProjection(
        total: BigDecimal,
        netAnnualReturn: BigDecimal,
        times: Int,
        time: Int
    ): SparseArray<Float> {
        val monthlyCompoundProjection: SparseArray<Float> = SparseArray(MONTHS_ONE)
        monthlyCompoundProjection.put(0, total.toFloat())

        var current = total
        for (i in 1..(times * time)) {
            if (netAnnualReturn.compareTo(ZERO) == 0) {
                monthlyCompoundProjection.put(i, total.toFloat())
                continue
            }
            val division =
                netAnnualReturn.divide(times.toBigDecimal(), COMPOUNT_SCALE, RoundingMode.HALF_UP)
            val increment = BigDecimal.ONE.add(division)
            val newValue = current.multiply(increment)
            monthlyCompoundProjection.put(i, newValue.toFloat())
            current = newValue
        }

        return monthlyCompoundProjection
    }

    private fun calculateYearlyCompoundProjection(
        total: BigDecimal,
        netAnnualReturn: BigDecimal,
        times: Int,
        time: Int
    ): SparseArray<Float> {
        val yearlyCompoundProjection: SparseArray<Float> = SparseArray(41)
        yearlyCompoundProjection.put(0, total.toFloat())

        var current = total
        for (i in 1..(times * time)) {
            if (i % 12 == 0 && netAnnualReturn.compareTo(ZERO) == 0) {
                yearlyCompoundProjection.put(i / 12, total.toFloat())
            }

            val division =
                netAnnualReturn.divide(times.toBigDecimal(), COMPOUNT_SCALE, RoundingMode.HALF_UP)
            val increment = BigDecimal.ONE.add(division)
            val newValue = current.multiply(increment)
            if (i % 12 == 0) {
                yearlyCompoundProjection.put(i / 12, newValue.toFloat())
            }
            current = newValue
        }

        return yearlyCompoundProjection
    }

    private fun calculateXirr(statements: List<PlatformStatement>): Float = try {
        Xirr(statements.map(this::convertToTransaction)).xirr().toFloat().times(100)
    } catch (e: Exception) {
        0.0f
    }

    private fun convertToTransaction(statement: PlatformStatement): Transaction = Transaction(
        if (statement.type == DEPOSIT) statement.amount.times(-1) else statement.amount.absoluteValue,
        statement.date
    )

    private fun calculateRoi(statements: MutableList<PlatformStatement>): Float {
        val startDate = statements.first().date
        val endDate = Date()
        val years = Utils.monthsBetween(startDate, endDate) / 12.0
        val deposits = statements.filter { it.type == DEPOSIT }.sumByDouble { it.amount }
        val withdrawals =
            statements.filter { it.type == WITHDRAWAL }.dropLast(1).sumByDouble { it.amount }
                .absoluteValue
        val expenses = deposits - withdrawals
        val endBalance = statements.last().amount

        if (deposits == 0.0 && withdrawals == 0.0) {
            return 0.0f
        }

        val roi = (endBalance - expenses) / expenses
        return (((1 + roi).pow(1 / years) - 1) * 100.00f).toFloat()
    }

    private fun getCurrentMonth() = Calendar.getInstance().get(Calendar.MONTH)

    private fun getCurrentYear() = Calendar.getInstance().get(Calendar.YEAR)

    fun deleteDetails(name: String): Completable = platformDetailsDao.deleteByName(name)

    fun getPlatformDetailsWithCashDrag(): Single<List<PlatformDetails>> {
        val threshold = preferencesHelper.get(PREF_IDLE_MONEY_THRESHOLD, 10).toBigDecimal()
        return platformDetailsDao.getAllAsSingle()
            .map { platforms ->
                platforms.filter {
                    (it.available ?: ZERO) > threshold
                }
            }
    }

    fun getPlatforms() = platformDetailsDao.getAllAsSingle()
}
