package pt.tiagocarvalho.financetracker.repository

import android.content.Context
import io.reactivex.Completable
import io.reactivex.Single
import java.math.BigDecimal
import javax.inject.Inject
import javax.inject.Singleton
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.billing.localdb.LocalBillingDb
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.CashAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.GamblingAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsAccountDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.CashAccount
import pt.tiagocarvalho.financetracker.data.local.storage.model.GamblingAccount
import pt.tiagocarvalho.financetracker.data.local.storage.model.Platform
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsAccount
import pt.tiagocarvalho.financetracker.model.Frequency
import pt.tiagocarvalho.financetracker.model.ManagementItem
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.utils.Constants.MAX_FREE_PLATFORMS

@Singleton
class PlatformRepository @Inject constructor(
    private val platformDao: PlatformDao,
    private val savingsAccountDao: SavingsAccountDao,
    private val gamblingAccountDao: GamblingAccountDao,
    private val cashAccountDao: CashAccountDao,
    private val preferencesHelper: PreferencesHelper,
    private val platformDetailsDao: PlatformDetailsDao,
    private val context: Context
) {

    fun getPlatformsNamesByTypeAsSingle(platformEnum: PlatformEnum) =
        platformDao.getPlatformsNamesByTypeAsSingle(platformEnum)

    fun deletePlatform(name: String): Completable = platformDao.deleteByName(name)

    fun getPlatformByName(name: String): Single<Platform> =
        platformDao.getPlatformByNameAsSingle(name)

    fun addOrUpdateP2p(
        platformEnum: PlatformEnum,
        email: String,
        password: String,
        edit: Boolean,
        frequentUpdates: Boolean,
        platformDetails: PlatformDetails
    ) {
        val platform = Platform(
            name = platformEnum.name,
            type = platformEnum,
            username = email,
            password = password,
            frequentUpdates = frequentUpdates,
            interest = null,
            twofactor = platformEnum.twofactor,
            interestsFrequency = null,
            recurringAmount = BigDecimal.ZERO,
            recurringFrequency = null
        )

        if (edit) {
            platformDao.update(platform)
        } else {
            platformDao.insert(platform)
        }

        platformDetailsDao.insert(platformDetails)
        updateLastDetailsTime(platformEnum)
    }

    fun addOrUpdateGambling(
        platformEnum: PlatformEnum,
        name: String,
        edit: Boolean
    ) {
        val platform = Platform(
            name = name,
            type = platformEnum,
            username = null,
            password = null,
            frequentUpdates = false,
            interest = null,
            twofactor = platformEnum.twofactor,
            interestsFrequency = null,
            recurringAmount = BigDecimal.ZERO,
            recurringFrequency = null
        )
        if (edit) {
            platformDao.update(platform)
        } else {
            platformDao.insert(platform)
        }

        val gamblingAccount = GamblingAccount(
            name = name
        )

        if (edit) {
            gamblingAccountDao.update(gamblingAccount)
        } else {
            gamblingAccountDao.insert(gamblingAccount)
        }
    }

    fun addOrUpdateCash(
        platformEnum: PlatformEnum,
        name: String,
        edit: Boolean
    ) {
        val platform = Platform(
            name = name,
            type = platformEnum,
            username = null,
            password = null,
            frequentUpdates = false,
            interest = null,
            twofactor = platformEnum.twofactor,
            interestsFrequency = null,
            recurringAmount = BigDecimal.ZERO,
            recurringFrequency = null
        )
        if (edit) {
            platformDao.update(platform)
        } else {
            platformDao.insert(platform)
        }

        val cashAccount = CashAccount(
            name = name
        )

        if (edit) {
            cashAccountDao.update(cashAccount)
        } else {
            cashAccountDao.insert(cashAccount)
        }
    }

    fun addOrUpdateSavings(
        platformEnum: PlatformEnum,
        name: String,
        interests: BigDecimal,
        interestFrequency: Frequency,
        recurringAmount: BigDecimal,
        recurringFrequency: Frequency,
        edit: Boolean
    ) {
        val platform = Platform(
            name = name,
            type = platformEnum,
            username = null,
            password = null,
            frequentUpdates = false,
            interest = interests,
            twofactor = platformEnum.twofactor,
            interestsFrequency = interestFrequency,
            recurringAmount = recurringAmount,
            recurringFrequency = recurringFrequency
        )
        if (edit) {
            platformDao.update(platform)
        } else {
            platformDao.insert(platform)
        }

        val savingsAccount = SavingsAccount(
            name = name,
            interest = interests,
            interestsFrequency = interestFrequency,
            recurringAmount = recurringAmount,
            recurringFrequency = recurringFrequency
        )

        if (edit) {
            savingsAccountDao.update(savingsAccount)
        } else {
            savingsAccountDao.insert(savingsAccount)
        }
    }

    fun getManagementPlatforms(): Single<List<ManagementItem>> = Single.fromCallable {
        val values = PlatformEnum.values().map { ManagementItem(it, false, 0) }
        platformDao.getPlatforms().forEach {
            updateValues(it.type, values)
        }
        values
    }

    private fun updateValues(platformEnum: PlatformEnum, list: List<ManagementItem>) {
        list.forEach {
            if (it.platform == platformEnum) {
                it.owned = true
                it.total = (it.total ?: 0) + 1
                return@forEach
            }
        }
    }

    fun updateLastDetailsTime(platform: PlatformEnum) {
        preferencesHelper.put(platform.prefLastUpdateDetails, System.currentTimeMillis())
    }

    fun canLogin(): Single<Boolean> = hasPremium()
        .flatMap { hasPremium ->
            platformDao.countAccounts()
                .map { hasPremium || it < MAX_FREE_PLATFORMS || BuildConfig.DEBUG }
        }

    private fun hasPremium(): Single<Boolean> = Single.create {
        val premium = LocalBillingDb.getInstance(context).entitlementsDao().getPremium()
        it.onSuccess(premium != null)
    }
}
