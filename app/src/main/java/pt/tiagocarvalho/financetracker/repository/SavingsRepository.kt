package pt.tiagocarvalho.financetracker.repository

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import java.util.Calendar
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.math.absoluteValue
import pt.tiagocarvalho.financetracker.data.local.prefs.PreferencesHelper
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformDetailsDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.PlatformStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.dao.SavingsStatementDao
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformStatement
import pt.tiagocarvalho.financetracker.data.local.storage.model.SavingsStatement
import pt.tiagocarvalho.financetracker.data.remote.SavingsService
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import pt.tiagocarvalho.financetracker.model.Resource
import pt.tiagocarvalho.financetracker.model.StatementType
import pt.tiagocarvalho.financetracker.repository.converter.SavingsConverter
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLogger
import pt.tiagocarvalho.financetracker.utils.log.AnalyticsLoggerImpl
import pt.tiagocarvalho.financetracker.utils.log.Logger

@Singleton
class SavingsRepository @Inject constructor(
    private val platformDetailsDao: PlatformDetailsDao,
    private val savingsStatementDao: SavingsStatementDao,
    private val platformStatementDao: PlatformStatementDao,
    private val savingsService: SavingsService,
    override val preferencesHelper: PreferencesHelper,
    override val logger: Logger,
    override val platformDao: PlatformDao,
    private val analyticsLogger: AnalyticsLogger
) : BaseRepository(preferencesHelper, logger, platformDao, PlatformEnum.SAVINGS) {

    fun loadDetails(force: Boolean, name: String): Single<Resource<PlatformDetails>> =
        if (shouldRefreshDetails(force, name)) {
            savingsService.getDetails(name)
                .doOnSuccess {
                    analyticsLogger.logDetailsRefresh(
                        AnalyticsLoggerImpl.REFRESH_REMOTE,
                        this.platform
                    )
                }
                .map { SavingsConverter.convert(it) }
                .flatMap {
                    platformDetailsDao.insertCompletable(it)
                        .doOnComplete { updateLastDetailsTime(name) }
                        .andThen(platformDetailsDao.getDetailsByNameAsSingle(name))
                        .map { t -> Resource.success(t) }
                }
                .onErrorResumeNext { t: Throwable ->
                    Completable.fromAction { logger.log(t) }
                        .andThen(platformDetailsDao.getDetailsByNameAsSingle(name))
                        .map { r ->
                            Resource.error(
                                t.message ?: "Something went wrong :(",
                                r
                            )
                        }
                }
        } else {
            Single.fromCallable {
                analyticsLogger.logDetailsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }.flatMap {
                platformDetailsDao.getDetailsByNameAsSingle(name)
                    .map { Resource.success(it) }
            }
        }

    fun loadStatements(force: Boolean, name: String): Single<List<PlatformStatement>> =
        if (shouldRefreshDailyStatements(force, name)) {
            savingsStatementDao.getAllByNameAsSingle(name)
                .doOnSuccess {
                    analyticsLogger.logStatementsRefresh(
                        AnalyticsLoggerImpl.REFRESH_REMOTE,
                        this.platform
                    )
                }
                .map {
                    it.map { savingsStatement ->
                        mapToPlatformStatement(
                            savingsStatement,
                            name
                        )
                    }
                }
                .flatMap {
                    platformStatementDao.deleteByNameCompletable(name)
                        .andThen(platformStatementDao.insertStatementsCompletable(it))
                        .doOnComplete { updateLastStatementsTime(name) }
                        .doOnComplete { updateLastFullStatementsScanTime(name) }
                        .andThen(platformStatementDao.getAllByNameOrderByDateDescAsSingle(name))
                }
                .onErrorResumeNext { t: Throwable ->
                    Completable.fromAction { logger.log(t) }
                        .andThen(platformStatementDao.getAllByNameOrderByDateDescAsSingle(name))
                }
        } else {
            Single.fromCallable {
                analyticsLogger.logStatementsRefresh(
                    AnalyticsLoggerImpl.REFRESH_LOCAL,
                    this.platform
                )
            }.flatMap { platformStatementDao.getAllByNameOrderByDateDescAsSingle(name) }
        }

    fun loadStatementsSync(name: String) = savingsStatementDao.getAllByNameOrderByDateDesc(name)

    override fun isRefreshDetailsNeeded(): Single<Boolean> =
        platformDao.getPlatformsByTypeAsSingle(platform)
            .map {
                it.firstOrNull { platform -> shouldRefreshDetails(false, platform.name) } != null
            }
            .onErrorResumeNext { Single.just(false) }

    override fun isRefreshStatementsNeeded(): Single<Boolean> =
        platformDao.getPlatformsByTypeAsSingle(platform)
            .map {
                it.firstOrNull { platform ->
                    shouldRefreshDailyStatements(
                        false,
                        platform.name
                    )
                } != null
            }
            .onErrorResumeNext { Single.just(false) }

    private fun mapToPlatformStatement(
        statement: SavingsStatement,
        name: String
    ): PlatformStatement = PlatformStatement(
        name,
        statement.amount,
        statement.date,
        statement.type,
        name + "_" + statement.id.toString()
    )

    fun editTransaction(
        amount: Double,
        mYear: Int,
        mMonth: Int,
        mDay: Int,
        statementType: StatementType,
        id: Long
    ): Observable<Boolean> = Observable.create { emitter ->
        try {
            val cal = Calendar.getInstance()
            cal.set(mYear, mMonth, mDay, 0, 0, 0)
            cal.set(Calendar.MILLISECOND, 0)

            val savingsStatement = savingsStatementDao.getById(id)

            savingsStatement.type = statementType
            savingsStatement.date = cal.time
            savingsStatement.amount = amount
            savingsStatementDao.update(savingsStatement)

            triggerFetch(savingsStatement.name)
            emitter.onNext(true)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    fun addTransaction(
        name: String,
        amount: Double,
        mYear: Int,
        mMonth: Int,
        mDay: Int,
        statementType: StatementType
    ): Observable<Boolean> = Observable.create { emitter ->
        try {
            val cal = Calendar.getInstance()
            cal.set(mYear, mMonth, mDay, 0, 0, 0)
            cal.set(Calendar.MILLISECOND, 0)

            savingsStatementDao.insert(
                SavingsStatement(
                    null,
                    name,
                    amount.absoluteValue,
                    cal.time,
                    statementType
                )
            )

            triggerFetch(name)
            emitter.onNext(true)
        } catch (e: Exception) {
            emitter.onError(e)
        }
    }

    fun removeStatement(id: String, name: String) {
        val localId = id.removePrefix(name + "_").toLong()
        platformStatementDao.deleteByLabel(id)
        savingsStatementDao.deleteById(localId)
        triggerFetch(name)
    }

    fun deleteAllByName(name: String): Completable = savingsStatementDao.deleteAllByName(name)
        .andThen(platformStatementDao.deleteByNameCompletable(name))

    fun insert(statements: List<SavingsStatement>) = savingsStatementDao.insert(statements)

    private fun triggerFetch(name: String) {
        preferencesHelper.put(platform.prefLastUpdateStatement + name, 0L)
        preferencesHelper.put(platform.prefLastUpdateDetails + name, 0L)
        preferencesHelper.put(platform.prefLastFullStatementScan + name, 0L)
    }

    fun triggerFetchCompletable(name: String): Completable = Completable.fromAction {
        preferencesHelper.put(platform.prefLastUpdateStatement + name, 0L)
        preferencesHelper.put(platform.prefLastUpdateDetails + name, 0L)
        preferencesHelper.put(platform.prefLastFullStatementScan + name, 0L)
    }

    fun loadStatement(id: String): Observable<SavingsStatement> {
        val localId = id.split("_")[1].toLong()
        return savingsStatementDao.getByIdAsObservable(localId)
    }
}
