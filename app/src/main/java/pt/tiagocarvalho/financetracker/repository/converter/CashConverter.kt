package pt.tiagocarvalho.financetracker.repository.converter

import pt.tiagocarvalho.financetracker.data.local.storage.model.PlatformDetails
import pt.tiagocarvalho.financetracker.model.CashInfo
import pt.tiagocarvalho.financetracker.model.PlatformEnum

object CashConverter {

    fun convert(cashAccount: CashInfo): PlatformDetails = PlatformDetails(
        name = cashAccount.name,
        platform = PlatformEnum.CASH,
        platformType = PlatformEnum.CASH.platformTypeEnum,
        balance = cashAccount.totalBalance,
        deposits = cashAccount.totalDeposits,
        withdrawals = cashAccount.totalWithdrawals,
        profit = cashAccount.profit,
        interest = cashAccount.interest,
        serviceFees = cashAccount.serviceFees,
        campaignRewards = cashAccount.bonus,
        changeValue = cashAccount.changeValue,
        changePercentage = cashAccount.changePercentage
    )
}
