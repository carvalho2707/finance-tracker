package pt.tiagocarvalho.financetracker

import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.hilt.work.HiltWorkerFactory
import androidx.multidex.MultiDexApplication
import androidx.preference.PreferenceManager
import androidx.work.Configuration
import androidx.work.Constraints
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.NetworkType
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.google.android.gms.ads.MobileAds
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.HiltAndroidApp
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import java.util.UUID
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import pt.tiagocarvalho.financetracker.repository.UpdatesRepository
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_DAY_NIGHT_OPTION
import pt.tiagocarvalho.financetracker.utils.Preferences.USER_ID
import pt.tiagocarvalho.financetracker.workers.BondoraWorker
import timber.log.Timber

@HiltAndroidApp
class FinanceTrackerApp : MultiDexApplication(), Configuration.Provider {

    @Inject
    lateinit var updatesRepository: UpdatesRepository

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    @Inject
    lateinit var workManager: WorkManager

    private lateinit var userId: String

    override fun onCreate() {
        super.onCreate()

        setupUserId()
        setupTimber()
        setupDefaultNightMode()
        setupWorkJobs()
        setupCrashlytics()
        setupRxJava()
        setupFirebaseAds()
        setupUpdates()
    }

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .setMinimumLoggingLevel(Log.DEBUG)
            .build()

    private fun setupUserId() {
        if (BuildConfig.DEBUG.not()) {
            val prefs = applicationContext.getSharedPreferences("data", 0)
            val id = prefs.getString(USER_ID, null)
            if (id == null) {
                userId = UUID.randomUUID().toString()
                prefs.edit().putString(USER_ID, userId).apply()
            } else {
                userId = id
            }
            val firebaseCrashlytics = FirebaseCrashlytics.getInstance()
            firebaseCrashlytics.setUserId(userId)
        }
    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun setupCrashlytics() {
        if (BuildConfig.DEBUG.not()) {
            val firebaseCrashlytics = FirebaseCrashlytics.getInstance()
            firebaseCrashlytics.checkForUnsentReports().addOnSuccessListener {
                if (it) {
                    firebaseCrashlytics.sendUnsentReports()
                }
            }
        }
    }

    private fun setupWorkJobs() {
        val workerConstraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()
        val notificationWorkRequest =
            PeriodicWorkRequestBuilder<BondoraWorker>(BondoraWorker.WORKER_PERIOD, TimeUnit.HOURS)
                .addTag(BondoraWorker.WORKER_TAG)
                .setConstraints(workerConstraints)
                .build()
        workManager.enqueueUniquePeriodicWork(
            BondoraWorker.UNIQUE_WORKER_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            notificationWorkRequest
        )
    }

    private fun setupDefaultNightMode() {
        val prefs = PreferenceManager.getDefaultSharedPreferences(this)
        val defaultOption = prefs.getString(PREF_DAY_NIGHT_OPTION, "-1") ?: "-1"
        AppCompatDelegate.setDefaultNightMode(defaultOption.toInt())
    }

    private fun setupRxJava() {
        RxJavaPlugins.setErrorHandler { throwable ->
            throwable.printStackTrace()
            if (throwable is UndeliverableException) {
                if (BuildConfig.DEBUG.not()) {
                    Timber.e(throwable, "UndeliverableException")
                    FirebaseCrashlytics.getInstance().recordException(throwable)
                }
            } else {
                FirebaseCrashlytics.getInstance().recordException(throwable)
                Timber.e(throwable, "UndeliverableException")
                throw RuntimeException(throwable)
            }
        }
    }

    private fun setupFirebaseAds() {
        if (BuildConfig.DEBUG) {
            MobileAds.initialize(this)
        } else {
            MobileAds.initialize(this)
        }
    }

    private fun setupUpdates() {
        updatesRepository.handleUpdates()
    }
}
