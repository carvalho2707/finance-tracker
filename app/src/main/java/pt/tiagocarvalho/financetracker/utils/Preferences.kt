package pt.tiagocarvalho.financetracker.utils

object Preferences {
    const val USER_ID = "pt.tiagocarvalho.financetracker.user.id"
    const val KEY_FILTER_BALANCE = "pt.tiagocarvalho.financetracker.filter.balance"
    const val KEY_FILTER_VARIATION = "pt.tiagocarvalho.financetracker.filter.variation"
    const val KEY_FILTER_POSITIVE_VARIATION =
        "pt.tiagocarvalho.financetracker.filter.positive_variation"
    const val KEY_FILTER_TYPE = "pt.tiagocarvalho.financetracker.filter.type_filter"
    const val KEY_SORT_ORDER = "pt.tiagocarvalho.financetracker.sort.order"

    const val PREF_DAY_NIGHT_OPTION = "pref_daynight_option"
    const val PREF_IDLE_MONEY_THRESHOLD = "pref_alerts_idle_money_threshold"
    const val PREF_DISPLAY_MODE = "pref_app_display_mode"

    const val PREF_RATER_SEEN_ONBOARDING = "pt.tiagocarvalho.financetracker.seen.onboarding"
    const val PREF_RATER_DONT_SHOW_AGAIN = "pt.tiagocarvalho.financetracker.rater.dontshowagain"
    const val PREF_RATER_LAUNCH_COUNT = "pt.tiagocarvalho.financetracker.rater.launch_count"
    const val PREF_RATER_DATE_FIRST_LAUNCH =
        "pt.tiagocarvalho.financetracker.rater.date_firstlaunch"
    const val LAST_SESSION_BALANCE_ACCOUNT = "pt.tiagocarvalho.financetracker.last.session.balance."

    const val NAME_ASC = 0
    const val NAME_DESC = 1
    const val BALANCE_ASC = 2
    const val BALANCE_DESC = 3
    const val VARIATION_PERCENTAGE_ASC = 4
    const val VARIATION_PERCENTAGE_DESC = 5
    const val VARIATION_VALUE_ASC = 6
    const val VARIATION_VALUE_DESC = 7

    const val BONDORA_TOKEN = "pt.tiagocarvalho.financetracker.bondora.token"
    const val RAIZE_DEVICE_ID = "pt.tiagocarvalho.financetracker.raize.device.id"

    fun getPreferences() = listOf(
        KEY_FILTER_BALANCE,
        KEY_FILTER_VARIATION,
        KEY_FILTER_POSITIVE_VARIATION,
        KEY_FILTER_TYPE,
        KEY_SORT_ORDER
    )
}
