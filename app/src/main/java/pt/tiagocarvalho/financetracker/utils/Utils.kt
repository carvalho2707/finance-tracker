package pt.tiagocarvalho.financetracker.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.Calendar
import java.util.Date

object Utils {

    fun isConnectingToInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val nw = connectivityManager.activeNetwork ?: return false
            val actNw = connectivityManager.getNetworkCapabilities(nw) ?: return false
            return when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            val nwInfo = connectivityManager.activeNetworkInfo ?: return false
            return nwInfo.isConnected
        }
    }

    fun getTodayDate(): Date {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal.time
    }

    fun getEndDate(): Date {
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_MONTH, 1)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal.time
    }

    fun getInitialYear(): Date {
        val cal = Calendar.getInstance()
        cal.set(Calendar.MONTH, 0)
        cal.set(Calendar.DAY_OF_YEAR, 0)
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal.time
    }

    fun monthsBetween(start: Date, end: Date): Int {
        var temp = end
        val cal = Calendar.getInstance()
        if (start.before(temp)) {
            cal.time = start
        } else {
            cal.time = temp
            temp = start
        }
        var c = 0
        while (cal.time.before(temp)) {
            cal.add(Calendar.MONTH, 1)
            c++
        }
        return c - 1
    }

    fun hideKeyboard(context: Context, view: View?) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        view?.let {
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    @ColorInt
    fun themeAttributeToColorInt(themeAttributeId: Int, context: Context): Int {
        val resolvedAttr = TypedValue()
        context.theme.resolveAttribute(themeAttributeId, resolvedAttr, true)
        val colorRes = resolvedAttr.run { if (resourceId != 0) resourceId else data }
        return ContextCompat.getColor(context, colorRes)
    }

    @ColorRes
    fun themeAttributeToColorRes(themeAttributeId: Int, context: Context): Int {
        val resolvedAttr = TypedValue()
        context.theme.resolveAttribute(themeAttributeId, resolvedAttr, true)
        return resolvedAttr.run { if (resourceId != 0) resourceId else data }
    }

    fun calculateChangePercentage(change: BigDecimal, balance: BigDecimal): BigDecimal {
        if (balance.compareTo(BigDecimal.ZERO) == 0) {
            return BigDecimal.ZERO
        }
        val startValue = balance.subtract(change)
        if (startValue.compareTo(BigDecimal.ZERO) == 0) {
            return change
        }
        return change.divide(startValue, 8, RoundingMode.HALF_UP).multiply(BigDecimal(100))
    }

    /**
     * Map a slideOffset (in the range `[-1, 1]`) to an alpha value based on the desired range.
     * For example, `slideOffsetToAlpha(0.5, 0.25, 1) = 0.33` because 0.5 is 1/3 of the way between
     * 0.25 and 1. The result value is additionally clamped to the range `[0, 1]`.
     */
    fun slideOffsetToAlpha(value: Float, rangeMin: Float, rangeMax: Float): Float =
        ((value - rangeMin) / (rangeMax - rangeMin)).coerceIn(0f, 1f)
}
