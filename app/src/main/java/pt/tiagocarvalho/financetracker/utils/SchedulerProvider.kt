package pt.tiagocarvalho.financetracker.utils

import io.reactivex.Scheduler

/**
 * Contract for a ReactiveX scheduler provider.
 */
interface SchedulerProvider {

    fun ui(): Scheduler

    fun io(): Scheduler
}
