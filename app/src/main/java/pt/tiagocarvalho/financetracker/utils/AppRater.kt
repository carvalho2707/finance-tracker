package pt.tiagocarvalho.financetracker.utils

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_RATER_DATE_FIRST_LAUNCH
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_RATER_DONT_SHOW_AGAIN
import pt.tiagocarvalho.financetracker.utils.Preferences.PREF_RATER_LAUNCH_COUNT

object AppRater {

    private const val APP_TITLE = "inWallet" // App Name
    private const val APP_PNAME = "pt.tiagocarvalho.financetracker" // Package Name
    private const val DAYS_UNTIL_PROMPT = 3 // Min number of days
    private const val LAUNCHES_UNTIL_PROMPT = 5 // Min number of launches

    fun appLaunched(mContext: Context) {
        val prefs = mContext.getSharedPreferences("data", 0)
        if (prefs.getBoolean(PREF_RATER_DONT_SHOW_AGAIN, false)) {
            return
        }

        val editor = prefs.edit()

        // Increment launch counter
        val launchCount = prefs.getLong(PREF_RATER_LAUNCH_COUNT, 0) + 1
        editor.putLong(PREF_RATER_LAUNCH_COUNT, launchCount)

        // Get date of first launch
        var dateFirstLaunch = prefs.getLong(PREF_RATER_DATE_FIRST_LAUNCH, 0)
        if (dateFirstLaunch == 0L) {
            dateFirstLaunch = System.currentTimeMillis()
            editor.putLong(PREF_RATER_DATE_FIRST_LAUNCH, dateFirstLaunch)
        }

        // Wait at least n days before opening
        if (launchCount >= LAUNCHES_UNTIL_PROMPT && System.currentTimeMillis() >= dateFirstLaunch + DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000) {
            showRateDialog(mContext, editor)
        }

        editor.apply()
    }

    private fun showRateDialog(mContext: Context, editor: SharedPreferences.Editor?) {
        val dialogBuilder = MaterialAlertDialogBuilder(mContext)
        dialogBuilder.setTitle("Rate $APP_TITLE")
        dialogBuilder.setMessage("If you enjoy using $APP_TITLE, please take a moment to rate it. Thanks for your support!")
        dialogBuilder.setPositiveButton("Rate $APP_TITLE") { dialog, _ ->
            mContext.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=$APP_PNAME")
                )
            )
            dialog.dismiss()
        }
        dialogBuilder.setNegativeButton("No, thanks") { dialog, _ ->
            if (editor != null) {
                editor.putBoolean(PREF_RATER_DONT_SHOW_AGAIN, true)
                editor.commit()
            }
            dialog.dismiss()
        }
        dialogBuilder.setNeutralButton("Remind me later") { dialog, _ ->
            if (editor != null) {
                editor.putLong(PREF_RATER_LAUNCH_COUNT, 0)
                editor.commit()
            }
            dialog.dismiss()
        }

        dialogBuilder.show()
    }
}
