package pt.tiagocarvalho.financetracker.utils.charts

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter
import pt.tiagocarvalho.financetracker.model.PlatformTypeEnum

class PlatformValueFormatter : ValueFormatter {

    private var isPlatformEnum: Boolean = false
    private lateinit var platformEnums: Array<String?>
    private lateinit var platformTypeEnums: Array<PlatformTypeEnum?>

    constructor(values: Array<String?>) {
        this.platformEnums = values
        isPlatformEnum = true
    }

    constructor(values: Array<PlatformTypeEnum?>) {
        this.platformTypeEnums = values
        isPlatformEnum = false
    }

    override fun getAxisLabel(value: Float, axis: AxisBase?): String = if (isPlatformEnum) {
        platformEnums[value.toInt() - 1] ?: "FAILED"
    } else {
        platformTypeEnums[value.toInt() - 1].toString()
    }
}
