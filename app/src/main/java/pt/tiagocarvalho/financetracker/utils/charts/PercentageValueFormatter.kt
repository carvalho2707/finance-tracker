package pt.tiagocarvalho.financetracker.utils.charts

import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

class PercentageValueFormatter : ValueFormatter() {

    private val numberFormat = NumberFormat.getNumberInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun getFormattedValue(value: Float): String {
        if (value == 0f) {
            return ""
        }
        return numberFormat.format(value) + " %"
    }
}
