package pt.tiagocarvalho.financetracker.utils

@Suppress("unused")
enum class MonthEnum(val value: Int, val displayName: String) {

    JANUARY(0, "January"),
    FEBRUARY(1, "February"),
    MARCH(2, "Mars"),
    APRIL(3, "April"),
    MAY(4, "May"),
    JUNE(5, "June"),
    JULY(6, "July"),
    AUGUST(7, "August"),
    SEPTEMBER(8, "September"),
    OCTOBER(9, "October"),
    NOVEMBER(10, "November"),
    DECEMBER(11, "December");

    companion object {

        fun valueOfByCalendarValue(calValue: Int): MonthEnum =
            values().firstOrNull { it.value == calValue }
                ?: throw IllegalArgumentException("Invalid value for MonthOfYear: $calValue")
    }
}
