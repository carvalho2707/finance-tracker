package pt.tiagocarvalho.financetracker.utils.log

import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.model.PlatformEnum

interface AnalyticsLogger {

    fun logLogin(type: PlatformEnum)
    fun logAccountRefresh(refreshType: String)
    fun logAccountRefresh(refreshType: String, type: PlatformEnum)
    fun logDetailsRefresh(refreshType: String, type: PlatformEnum)
    fun logStatementsRefresh(refreshType: String, type: PlatformEnum)
    fun logFullStatementsRefresh(refreshType: String, type: PlatformEnum)
    fun logTwitter()
    fun logSupportedCurrencies()
    fun logTotalBalance(balance: BigDecimal)
    fun logTotalAccounts(count: Int)

    // purchases
    fun logPurchaseOpened(screen: String)
    fun logPurchaseStarted(screen: String)
    fun logPurchaseCanceled(screen: String)
    fun logPurchaseError(screen: String)
    fun logPurchaseCompleted(id: String, screen: String)

    fun logAdPremiumNotNeeded()
    fun logAdSuccessToLoad()
    fun logAdFailedToLoad(code: String)

    // jobs
    fun logWorkerStatus(success: Boolean)
}
