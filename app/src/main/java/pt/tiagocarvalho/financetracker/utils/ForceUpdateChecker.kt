package pt.tiagocarvalho.financetracker.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import timber.log.Timber

class
ForceUpdateChecker private constructor(
    private val context: Context,
    private val onUpdateNeededListener: OnUpdateNeededListener?
) {

    private fun check() {
        if (!Utils.isConnectingToInternet(context)) {
            return
        }

        val remoteConfig = FirebaseRemoteConfig.getInstance()
        remoteConfig.setConfigSettingsAsync(
            FirebaseRemoteConfigSettings.Builder()
                .build()
        )
        remoteConfig.fetch().addOnCompleteListener {
            remoteConfig.activate().addOnCompleteListener {
                if (remoteConfig.getBoolean(KEY_UPDATE_REQUIRED)) {
                    val currentVersion = remoteConfig.getLong(KEY_CURRENT_VERSION)
                    val appVersion = getAppVersionCode(context)
                    val updateUrl = remoteConfig.getString(KEY_UPDATE_URL)

                    if (appVersion - currentVersion < 0 && onUpdateNeededListener != null) {
                        onUpdateNeededListener.onUpdateNeeded(updateUrl)
                    }
                }
            }
        }
    }

    private fun getAppVersionCode(context: Context): Long {
        var result: Long = 0

        try {
            result = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                context.packageManager
                    .getPackageInfo(context.packageName, 0)
                    .longVersionCode
            } else {
                context.packageManager
                    .getPackageInfo(context.packageName, 0)
                    .versionCode.toLong()
            }
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.e(e)
        }

        return result
    }

    interface OnUpdateNeededListener {
        fun onUpdateNeeded(updateUrl: String)
    }

    class Builder(private val context: Context) {
        private var onUpdateNeededListener: OnUpdateNeededListener? = null

        fun onUpdateNeeded(onUpdateNeededListener: OnUpdateNeededListener): Builder {
            this.onUpdateNeededListener = onUpdateNeededListener
            return this
        }

        fun build(): ForceUpdateChecker = ForceUpdateChecker(context, onUpdateNeededListener)

        fun check() {
            val forceUpdateChecker = build()
            forceUpdateChecker.check()
        }
    }

    companion object {

        const val KEY_UPDATE_REQUIRED = "force_update_required"
        const val KEY_CURRENT_VERSION = "force_update_current_version"
        const val KEY_UPDATE_URL = "force_update_store_url"

        fun with(context: Context): Builder = Builder(context)
    }
}
