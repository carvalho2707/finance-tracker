package pt.tiagocarvalho.financetracker.utils

import pt.tiagocarvalho.financetracker.ui.accounts.AccountItem
import pt.tiagocarvalho.financetracker.utils.Preferences.BALANCE_ASC
import pt.tiagocarvalho.financetracker.utils.Preferences.BALANCE_DESC
import pt.tiagocarvalho.financetracker.utils.Preferences.NAME_ASC
import pt.tiagocarvalho.financetracker.utils.Preferences.NAME_DESC
import pt.tiagocarvalho.financetracker.utils.Preferences.VARIATION_PERCENTAGE_ASC
import pt.tiagocarvalho.financetracker.utils.Preferences.VARIATION_PERCENTAGE_DESC
import pt.tiagocarvalho.financetracker.utils.Preferences.VARIATION_VALUE_ASC
import pt.tiagocarvalho.financetracker.utils.Preferences.VARIATION_VALUE_DESC

class AccountItemComparator(private val sortOrderId: Int) : Comparator<AccountItem> {

    override fun compare(o1: AccountItem, o2: AccountItem): Int = when (sortOrderId) {
        NAME_ASC -> {
            val result = o1.name.compareTo(o2.name)
            compareRemainingOrder(result, o1, o2)
        }
        NAME_DESC -> {
            val result = o2.name.compareTo(o1.name)
            compareRemainingOrder(result, o1, o2)
        }
        BALANCE_ASC -> {
            val result = o1.balance.compareTo(o2.balance)
            compareRemainingOrder(result, o1, o2)
        }
        BALANCE_DESC -> {
            val result = o2.balance.compareTo(o1.balance)
            compareRemainingOrder(result, o1, o2)
        }
        VARIATION_PERCENTAGE_ASC -> {
            val result = o1.changePercentage.compareTo(o2.changePercentage)
            compareRemainingOrder(result, o1, o2)
        }
        VARIATION_PERCENTAGE_DESC -> {
            val result = o2.changePercentage.compareTo(o1.changePercentage)
            compareRemainingOrder(result, o1, o2)
        }
        VARIATION_VALUE_ASC -> {
            val result = o1.changeValue.compareTo(o2.changeValue)
            compareRemainingOrder(result, o1, o2)
        }
        VARIATION_VALUE_DESC -> {
            val result = o2.changeValue.compareTo(o1.changeValue)
            compareRemainingOrder(result, o1, o2)
        }
        else -> {
            val result = o2.changeValue.compareTo(o1.changeValue)
            compareRemainingOrder(result, o1, o2)
        }
    }

    private fun compareRemainingOrder(
        compareResult: Int,
        o1: AccountItem,
        o2: AccountItem
    ): Int = if (compareResult == 0) {
        o2.balance.compareTo(o1.balance)
    } else {
        compareResult
    }
}
