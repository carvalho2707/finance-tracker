package pt.tiagocarvalho.financetracker.utils

import com.github.mikephil.charting.utils.ColorTemplate.rgb

object Constants {

    const val USER_AGENT_VALUE =
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36"

    const val DETAILS_REFRESH_PERIODICITY = 2L
    const val DETAILS_REFRESH_WORKER = 4L
    const val DETAILS_REFRESH_PERIODICITY_LONG = 12L

    const val STATEMENTS_REFRESH_PERIODICITY = 2L
    const val STATEMENTS_REFRESH_PERIODICITY_LONG = 12L

    const val FULL_STATEMENTS_REFRESH_PERIODICITY = 12L
    const val FULL_STATEMENTS_REFRESH_PERIODICITY_WORKER = 8L

    const val MAX_FREE_PLATFORMS = 5

    const val TWITTER_BASE_URL = "https://api.twitter.com"
    const val TWITTER_URL = "https://twitter.com/"

    const val TEST_AD_ID = "ca-app-pub-3940256099942544/6300978111"
    const val MAIN_AD_ID = "ca-app-pub-8033179797631683/9166495074"

    private const val COLOR_MATERIAL_GREEN_500 = "#4caf50"
    private const val COLOR_MATERIAL_RED_500 = "#f44336"
    private const val COLOR_MATERIAL_ORANGE_500 = "#ff9800"
    private const val COLOR_MATERIAL_BLUE_500 = "#2196f3"
    private const val COLOR_MATERIAL_DEEP_ORANGE_500 = "#ff5722"
    private const val COLOR_MATERIAL_INDIGO_500 = "#3f51b5"
    private const val COLOR_MATERIAL_PURPLE_500 = "#9c27b0"
    private const val COLOR_MATERIAL_LIGHT_BLUE_500 = "#03a9f4"
    private const val COLOR_MATERIAL_BLUE_GREY_500 = "#607d8b"
    private const val COLOR_MATERIAL_PINK_500 = "#e91e63"
    private const val COLOR_MATERIAL_LIME_500 = "#cddc39"
    private const val COLOR_MATERIAL_BROWN_500 = "#795548"
    private const val COLOR_MATERIAL_DEEP_PURPLE_500 = "#673ab7"
    private const val COLOR_MATERIAL_TEAL_500 = "#009688"
    private const val COLOR_MATERIAL_YELLOW_500 = "#ffeb3b"

    const val GLOBAL_DATE_PATTERN = "dd-MM-yyyy HH:mm:ss"
    const val STATEMENT_DATE_PATTERN = "dd MMM HH:mm"

    val MATERIAL_COLORS_500 = listOf(
        rgb(COLOR_MATERIAL_GREEN_500),
        rgb(COLOR_MATERIAL_ORANGE_500),
        rgb(COLOR_MATERIAL_BLUE_500),
        rgb(COLOR_MATERIAL_DEEP_ORANGE_500),
        rgb(COLOR_MATERIAL_INDIGO_500),
        rgb(COLOR_MATERIAL_PURPLE_500),
        rgb(COLOR_MATERIAL_LIGHT_BLUE_500),
        rgb(COLOR_MATERIAL_BLUE_GREY_500),
        rgb(COLOR_MATERIAL_PINK_500),
        rgb(COLOR_MATERIAL_LIME_500),
        rgb(COLOR_MATERIAL_BROWN_500),
        rgb(COLOR_MATERIAL_RED_500),
        rgb(COLOR_MATERIAL_DEEP_PURPLE_500),
        rgb(COLOR_MATERIAL_TEAL_500),
        rgb(COLOR_MATERIAL_YELLOW_500)
    )
}
