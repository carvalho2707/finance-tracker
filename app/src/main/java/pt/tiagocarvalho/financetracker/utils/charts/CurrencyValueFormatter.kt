package pt.tiagocarvalho.financetracker.utils.charts

import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

class CurrencyValueFormatter : ValueFormatter() {

    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun getFormattedValue(value: Float): String = numberFormat.format(value)
}
