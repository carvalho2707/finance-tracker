package pt.tiagocarvalho.financetracker.utils.log

import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails

interface Logger {

    fun log(throwable: Throwable)
    fun log(message: String)
    fun logMessages(message: List<String>)
    fun logDebug(mintosInfo: ThirdPartyDetails?)
    fun logPlatformRequest(name: String)
}
