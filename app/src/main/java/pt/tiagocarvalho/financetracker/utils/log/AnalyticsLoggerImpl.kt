package pt.tiagocarvalho.financetracker.utils.log

import android.content.Context
import android.os.Bundle
import com.google.firebase.analytics.FirebaseAnalytics
import java.math.BigDecimal
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.model.PlatformEnum
import timber.log.Timber

class AnalyticsLoggerImpl(
    private val context: Context
) : AnalyticsLogger {

    override fun logLogin(type: PlatformEnum) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_BRAND, type.name)
            }

            firebaseAnalytics.logEvent(LOGIN, bundle)
        }

        Timber.e("ID: $LOGIN -- ITEM_BRAND: ${type.name}")
    }

    override fun logAccountRefresh(refreshType: String) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, REFRESH_ACCOUNTS)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, refreshType)
            }

            firebaseAnalytics.logEvent(REFRESH, bundle)
        }

        Timber.e("ID: $REFRESH -- ITEM_CATEGORY: $REFRESH_ACCOUNTS -- ITEM_VARIANT: $refreshType")
    }

    override fun logAccountRefresh(refreshType: String, type: PlatformEnum) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, REFRESH_ACCOUNTS)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, refreshType)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, type.name)
            }

            firebaseAnalytics.logEvent(REFRESH, bundle)
        }

        Timber.e("ID: $REFRESH -- ITEM_CATEGORY: $REFRESH_ACCOUNTS -- ITEM_VARIANT: $refreshType -- ITEM_BRAND: ${type.name}")
    }

    override fun logDetailsRefresh(refreshType: String, type: PlatformEnum) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, REFRESH_DETAILS)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, refreshType)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, type.name)
            }

            firebaseAnalytics.logEvent(REFRESH, bundle)
        }

        Timber.e("ID: $REFRESH -- ITEM_CATEGORY: $REFRESH_DETAILS -- ITEM_VARIANT: $refreshType -- ITEM_BRAND: ${type.name}")
    }

    override fun logStatementsRefresh(refreshType: String, type: PlatformEnum) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, REFRESH_STATEMENTS)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, refreshType)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, type.name)
            }

            firebaseAnalytics.logEvent(REFRESH, bundle)
        }

        Timber.e("ID: $REFRESH -- ITEM_CATEGORY: $REFRESH_STATEMENTS -- ITEM_VARIANT: $refreshType -- ITEM_BRAND: ${type.name}")
    }

    override fun logFullStatementsRefresh(refreshType: String, type: PlatformEnum) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_CATEGORY, REFRESH_STATEMENTS_FULL)
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, refreshType)
                putString(FirebaseAnalytics.Param.ITEM_BRAND, type.name)
            }

            firebaseAnalytics.logEvent(REFRESH, bundle)
        }

        Timber.e("ID: $REFRESH -- ITEM_CATEGORY: $REFRESH_STATEMENTS_FULL -- ITEM_VARIANT: $refreshType -- ITEM_BRAND: ${type.name}")
    }

    override fun logTwitter() {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            firebaseAnalytics.logEvent(REFRESH, null)
        }

        Timber.e("ID: $TWITTER")
    }

    override fun logSupportedCurrencies() {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            firebaseAnalytics.logEvent(SUPPORTED_CURRENCIES, null)
        }

        Timber.e("ID: $SUPPORTED_CURRENCIES")
    }

    override fun logTotalBalance(balance: BigDecimal) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            firebaseAnalytics.setUserProperty(TOTAL_BALANCE, balance.toPlainString())
        }

        Timber.e("ID: $TOTAL_BALANCE")
    }

    override fun logTotalAccounts(count: Int) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            firebaseAnalytics.setUserProperty(TOTAL_ACCOUNTS, count.toString())
        }

        Timber.e("ID: $TOTAL_ACCOUNTS")
    }

    override fun logPurchaseOpened(screen: String) {

        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_NAME, screen)
            }
            firebaseAnalytics.logEvent(PURCHASE_OPENED, bundle)
        }

        Timber.e("ID: $PURCHASE_OPENED -- ITEM_NAME: $screen")
    }

    override fun logPurchaseStarted(screen: String) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_NAME, screen)
            }
            firebaseAnalytics.logEvent(PURCHASE_STARTED, bundle)
        }

        Timber.e("ID: $PURCHASE_STARTED -- ITEM_NAME: $screen")
    }

    override fun logPurchaseCompleted(id: String, screen: String) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_VARIANT, id)
                putString(FirebaseAnalytics.Param.ITEM_NAME, screen)
            }
            firebaseAnalytics.logEvent(PURCHASE_COMPLETED, bundle)
        }

        Timber.e("ID: $PURCHASE_COMPLETED -- ITEM_NAME: $screen -- ITEM_VARIANT: $id")
    }

    override fun logPurchaseError(screen: String) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_NAME, screen)
            }
            firebaseAnalytics.logEvent(PURCHASE_ERROR, bundle)
        }

        Timber.e("ID: $PURCHASE_ERROR -- ITEM_NAME: $screen")
    }

    override fun logPurchaseCanceled(screen: String) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)
            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_NAME, screen)
            }
            firebaseAnalytics.logEvent(PURCHASE_CANCELED, bundle)
        }

        Timber.e("ID: $PURCHASE_CANCELED -- ITEM_NAME: $screen")
    }

    override fun logAdPremiumNotNeeded() {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            firebaseAnalytics.logEvent(AD_NOT_NEEDED, null)
        }

        Timber.e("ID: $AD_NOT_NEEDED")
    }

    override fun logAdSuccessToLoad() {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            firebaseAnalytics.logEvent(AD_SUCCESS, null)
        }

        Timber.e("ID: $AD_SUCCESS")
    }

    override fun logAdFailedToLoad(code: String) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, code)
            }

            firebaseAnalytics.logEvent(AD_FAILED, bundle)
        }

        Timber.e("ID: $AD_FAILED -- ITEM_ID: $code")
    }

    override fun logWorkerStatus(success: Boolean) {
        if (BuildConfig.DEBUG.not()) {
            val firebaseAnalytics = FirebaseAnalytics.getInstance(context)

            val bundle = Bundle().apply {
                putString(FirebaseAnalytics.Param.ITEM_ID, success.toString())
            }

            firebaseAnalytics.logEvent(WORKER_STATUS, bundle)
        }

        Timber.e("ID: $WORKER_STATUS -- ITEM_ID: $success")
    }

    companion object {

        private const val REFRESH = "refresh"
        private const val LOGIN = "login"
        private const val PURCHASE_OPENED = "purchase_opened"
        private const val PURCHASE_STARTED = "purchase_started"
        private const val PURCHASE_CANCELED = "purchase_canceled"
        private const val PURCHASE_ERROR = "purchase_error"
        private const val PURCHASE_COMPLETED = "purchase_completed"
        private const val TWITTER = "twitter"
        private const val SUPPORTED_CURRENCIES = "currencies"
        private const val TOTAL_BALANCE = "total_balance"
        private const val TOTAL_ACCOUNTS = "total_accounts"
        private const val AD_FAILED = "ad_failed"
        private const val AD_SUCCESS = "ad_success"
        private const val AD_NOT_NEEDED = "ad_not_needed"
        private const val WORKER_STATUS = "worker_status"

        // ITEM_VARIANT
        const val REFRESH_LOCAL = "refresh_local"
        const val REFRESH_REMOTE = "refresh_remote"

        // ITEM_CATEGORY
        const val REFRESH_ACCOUNTS = "refresh_accounts"
        const val REFRESH_DETAILS = "refresh_details"
        const val REFRESH_STATEMENTS = "refresh_statements"
        const val REFRESH_STATEMENTS_FULL = "refresh_statements_full"
    }
}
