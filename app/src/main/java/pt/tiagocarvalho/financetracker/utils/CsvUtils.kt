package pt.tiagocarvalho.financetracker.utils

import android.content.Context
import android.net.Uri
import io.reactivex.Single
import java.io.BufferedReader
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStreamReader
import java.util.ArrayList
import java.util.Collections.emptyList

object CsvUtils {

    @Throws(IOException::class)
    fun readCsvFromUri(uri: Uri, context: Context): Single<List<String>> = Single.create {
        val result = ArrayList<String>()
        val inputStream = context.contentResolver.openInputStream(uri)
        if (inputStream == null) {
            it.onSuccess(emptyList())
            return@create
        }
        val reader = BufferedReader(InputStreamReader(inputStream))
        reader.forEachLine { line ->
            result.add(line)
        }
        inputStream.close()
        reader.close()
        it.onSuccess(result)
    }

    fun writeToCsv(uri: Uri, text: List<String>, context: Context) {
        val pfd = context.contentResolver.openFileDescriptor(uri, "w")
        val fileOutputStream = FileOutputStream(pfd?.fileDescriptor)
        val str = StringBuilder()
        for (line in text) {
            str.append(line).append("\n")
        }
        fileOutputStream.write(str.toString().toByteArray())
        // Let the document provider know you're done by closing the stream.
        fileOutputStream.close()
        pfd?.close()
    }
}
