package pt.tiagocarvalho.financetracker.utils.charts

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.ValueFormatter
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

class PointLabelCurrencyValueFormatter(private val count: Int) : ValueFormatter() {

    private val numberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    private val counter: Int = ((count - 2) / 4.0).toInt()

    init {
        numberFormat.minimumFractionDigits = 1
        numberFormat.maximumFractionDigits = 2
        numberFormat.currency = Currency.getInstance("EUR")
    }

    override fun getPointLabel(entry: Entry?): String {
        val index: Int = entry?.x?.toInt() ?: 0
        return if (index == 0 || index == count) {
            numberFormat.format(entry?.y ?: 0.0f)
        } else if (index % counter == 0) {
            numberFormat.format(entry?.y ?: 0.0f)
        } else {
            ""
        }
    }
}
