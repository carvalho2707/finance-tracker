package pt.tiagocarvalho.financetracker.utils

class PlatformRequestException(message: String) : Exception(message)
