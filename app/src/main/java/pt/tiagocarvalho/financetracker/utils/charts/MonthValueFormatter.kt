package pt.tiagocarvalho.financetracker.utils.charts

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter

class MonthValueFormatter : ValueFormatter() {

    override fun getAxisLabel(value: Float, axis: AxisBase?): String = when {
        value >= 0.0f && value < 1.0f -> "January"
        value >= 1.0f && value < 2.0f -> "February"
        value >= 2.0f && value < 3.0f -> "Mars"
        value >= 3.0f && value < 4.0f -> "April"
        value >= 4.0f && value < 5.0f -> "May"
        value >= 5.0f && value < 6.0f -> "June"
        value >= 6.0f && value < 7.0f -> "July"
        value >= 7.0f && value < 8.0f -> "August"
        value >= 8.0f && value < 9.0f -> "September"
        value >= 9.0f && value < 10.0f -> "October"
        value >= 10.0f && value < 11.0f -> "November"
        value >= 11.0f && value < 12.0f -> "December"
        else -> ""
    }
}
