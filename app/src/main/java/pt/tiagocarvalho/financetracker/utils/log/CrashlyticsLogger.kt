package pt.tiagocarvalho.financetracker.utils.log

import com.google.firebase.crashlytics.FirebaseCrashlytics
import pt.tiagocarvalho.financetracker.BuildConfig
import pt.tiagocarvalho.financetracker.utils.PlatformRequestException
import pt.tiagocarvalho.p2p.api.model.ThirdPartyDetails
import pt.tiagocarvalho.p2p.api.model.ThirdPartyException
import timber.log.Timber

class CrashlyticsLogger : Logger {

    override fun log(message: String) {
        val throwable = Throwable(message)
        Timber.e(message)
        if (BuildConfig.DEBUG.not()) {
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
    }

    override fun log(throwable: Throwable) {
        Timber.e(throwable)
        if (BuildConfig.DEBUG.not()) {
            if (throwable is ThirdPartyException) {
                throwable.content?.let {
                    FirebaseCrashlytics.getInstance().log(it)
                }
            }
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
    }

    override fun logMessages(message: List<String>) {
        val throwable = Throwable("Invalid Statements: $message")
        Timber.e(throwable)
        if (BuildConfig.DEBUG.not()) {
            message.forEach {
                FirebaseCrashlytics.getInstance().log(it)
            }
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
    }

    override fun logDebug(mintosInfo: ThirdPartyDetails?) {
        val throwable =
            Throwable("Custom log event: " + mintosInfo?.balance + ";" + mintosInfo?.aggregates)
        Timber.e(throwable)
        if (BuildConfig.DEBUG.not()) {
            mintosInfo?.balances?.let { FirebaseCrashlytics.getInstance().log(it) }
            mintosInfo?.aggregates?.let { FirebaseCrashlytics.getInstance().log(it) }
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
    }

    override fun logPlatformRequest(name: String) {
        val throwable =
            PlatformRequestException(name)
        Timber.e(throwable)
        if (BuildConfig.DEBUG.not()) {
            FirebaseCrashlytics.getInstance().recordException(throwable)
        }
    }
}
