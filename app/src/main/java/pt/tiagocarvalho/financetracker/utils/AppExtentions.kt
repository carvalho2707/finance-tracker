package pt.tiagocarvalho.financetracker.utils

import android.content.Context
import android.content.res.Configuration.UI_MODE_NIGHT_MASK
import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_UNDEFINED
import android.os.Handler
import android.os.Parcel
import android.view.View
import androidx.core.os.ParcelCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.databinding.Observable
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import java.io.IOException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

fun Date.toStringPattern(pattern: String = Constants.GLOBAL_DATE_PATTERN): String {
    val formatter = SimpleDateFormat(pattern, Locale.getDefault())
    return formatter.format(this)
}

fun Long.toStringPattern(pattern: String = Constants.GLOBAL_DATE_PATTERN): String {
    val formatter = SimpleDateFormat(pattern, Locale.getDefault())
    return formatter.format(Date(this))
}

fun String.toDate(): Date {
    val formatter = SimpleDateFormat(Constants.GLOBAL_DATE_PATTERN, Locale.getDefault())
    return formatter.parse(this)!!
}

fun Context.isDayMode(): Boolean {
    val prefs = PreferenceManager.getDefaultSharedPreferences(this)
    val defaultOption = prefs.getString(Preferences.PREF_DAY_NIGHT_OPTION, "-1") ?: "-1"

    return when (defaultOption.toInt()) {
        1 -> true
        2 -> false
        4 -> {
            val currentNightMode = resources.configuration.uiMode and UI_MODE_NIGHT_MASK
            currentNightMode == UI_MODE_NIGHT_NO || currentNightMode == UI_MODE_NIGHT_UNDEFINED
        }
        else -> true
    }
}

fun String.countSubstring(sub: String): Int = this.split(sub).size - 1

fun RecyclerView.smoothSnapToPosition(
    position: Int,
    snapMode: Int = LinearSmoothScroller.SNAP_TO_START
) {
    val smoothScroller = object : LinearSmoothScroller(this.context) {
        override fun getVerticalSnapPreference(): Int {
            return snapMode
        }

        override fun getHorizontalSnapPreference(): Int {
            return snapMode
        }
    }
    smoothScroller.targetPosition = position
    layoutManager?.startSmoothScroll(smoothScroller)
}

fun Throwable?.isNoNetworkException(): Boolean =
    this != null && (this is IOException || this.cause != null && this.cause is IOException || this is UnknownHostException)

fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
    addOnPropertyChangedCallback(
        object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) =
                callback.invoke(sender as T)
        })

fun View.doOnApplyWindowInsets(f: (View, WindowInsetsCompat, ViewPaddingState) -> Unit) {
    // Create a snapshot of the view's padding state
    val paddingState = createStateForView(this)
    ViewCompat.setOnApplyWindowInsetsListener(this) { v, insets ->
        f(v, insets, paddingState)
        insets
    }
    requestApplyInsetsWhenAttached()
}

/**
 * Call [View.requestApplyInsets] in a safe away. If we're attached it calls it straight-away.
 * If not it sets an [View.OnAttachStateChangeListener] and waits to be attached before calling
 * [View.requestApplyInsets].
 */
fun View.requestApplyInsetsWhenAttached() {
    if (isAttachedToWindow) {
        requestApplyInsets()
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                v.requestApplyInsets()
            }

            override fun onViewDetachedFromWindow(v: View) = Unit
        })
    }
}

private fun createStateForView(view: View) = ViewPaddingState(
    view.paddingLeft,
    view.paddingTop, view.paddingRight, view.paddingBottom, view.paddingStart, view.paddingEnd
)

data class ViewPaddingState(
    val left: Int,
    val top: Int,
    val right: Int,
    val bottom: Int,
    val start: Int,
    val end: Int
)

/** Write a boolean to a Parcel. */
fun Parcel.writeBooleanUsingCompat(value: Boolean) = ParcelCompat.writeBoolean(this, value)

/** Read a boolean from a Parcel. */
fun Parcel.readBooleanUsingCompat() = ParcelCompat.readBoolean(this)

/**
 * Linearly interpolate between two values.
 */
fun lerp(a: Float, b: Float, t: Float): Float = a + (b - a) * t

inline fun <T : ViewDataBinding> T.executeAfter(block: T.() -> Unit) {
    block()
    executePendingBindings()
}

fun NavController.navigateSafe(directions: NavDirections) {
    val action =
        currentDestination?.getAction(directions.actionId) ?: graph.getAction(directions.actionId)
    if (action != null && currentDestination?.id != action.destinationId) {
        navigate(directions)
    }
}

/**
 * An extension to `postponeEnterTransition` which will resume after a timeout.
 */
fun Fragment.postponeEnterTransition(timeout: Long) {
    postponeEnterTransition()
    Handler().postDelayed({ startPostponedEnterTransition() }, timeout)
}
