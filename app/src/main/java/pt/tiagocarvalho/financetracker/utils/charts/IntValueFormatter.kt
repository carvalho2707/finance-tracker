package pt.tiagocarvalho.financetracker.utils.charts

import com.github.mikephil.charting.formatter.ValueFormatter

class IntValueFormatter(
    private val hideZero: Boolean
) : ValueFormatter() {

    override fun getFormattedValue(
        value: Float
    ): String = if (hideZero) {
        if (value > 0.0) {
            value.toInt().toString()
        } else {
            ""
        }
    } else {
        value.toInt().toString()
    }
}
